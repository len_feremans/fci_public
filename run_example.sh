mvn clean compile assembly:single -DskipTests=True
JAR=./target/FCI-seq-1.1.0-jar-with-dependencies.jar
DATA=./data/trump_tweets_2017_text.csv
java -jar $JAR -s 5 -m 5 -c 0.1 -i $DATA -P -seq 0.3 -o ./temp/sequences.txt
java -jar $JAR -s 5 -m 5 -c 0.1 -i $DATA -P -seq 0.1 -epi 0.7 -rul 0.1
java -jar $JAR -s 5 -m 5 -c 0.1 -i $DATA -P -seq 0.1 -epi 0.1
java -jar $JAR -s 5 -m 5 -c 0.1 -i $DATA -P -seq 0.1 -split 10 -o ./temp/sequencesplit.txt
#Example sparse format:
java -jar $JAR -s 5 -m 5 -c 0.1 -i ./data/sparse_example.dat -l ./data/sparse_example.lab -S -seq 0.1 -o ./temp/sparse_example_output.txt
java -jar $JAR -s 5 -m 5 -c 0.1 -i ./data/sparse_example.dat -l ./data/sparse_example.lab -S -seq 0.1 -o sparse_example_output.txt