package be.uantwerpen.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class CountMap<K> {

	//Options: Use HASHMAP insteaf of TreeMap...
	private Map<K,Integer> map = new TreeMap<K,Integer>();
	
	public void add(K key){
		map.put(key, get(key) + 1);
	}
	
	public void remove(K key){
		map.remove(key);
	}
	
	public int get(K key){
		Integer i = map.get(key);
		return (i == null)?0:i;
	}
	
	public Map<K,Integer> getMap(){
		return map;
	}
	
	public Set<K> keySet(){
		return map.keySet();
	}
	
	public Integer computeMedian(){
		List<Integer> lst = new ArrayList<Integer>(map.values());
		Collections.sort(lst);
		return lst.get(lst.size()/2);
	}
	
	
	public List<K> getKeysSortedDescending(){
		List<Entry<K,Integer>> lst = new ArrayList<>(map.entrySet());
		List<K> keysSorted = lst.stream().
						sorted((entry1,entry2) -> entry2.getValue().compareTo(entry1.getValue())).
						map(entry -> entry.getKey()).collect(Collectors.toList());
		return keysSorted;
	}
}
