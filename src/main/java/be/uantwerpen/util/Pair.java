package be.uantwerpen.util;

/**
 * Utility class that holds pair of two objects
 * 
 * @author lfereman
 *
 * @param <F>
 * @param <S>
 */
@SuppressWarnings("rawtypes")
public class Pair<F, S>{
    private F first; 
    private S second;

    public Pair(F first, S second) {
        this.first = first;
        this.second = second;
    }
    
    public final void setFirst(F first) {
        this.first = first;
    }

    public final void setSecond(S second) {
        this.second = second;
    }

    public final F getFirst() {
        return first;
    }

    public final S getSecond() {
        return second;
    }
    
    @Override
    public final String toString()
    {
    	return String.format("%s:%s", first, second);
    }

	@Override
	public final int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((first == null) ? 0 : first.hashCode());
		result = prime * result + ((second == null) ? 0 : second.hashCode());
		return result;
	}

	@Override
	public final boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pair other = (Pair) obj;
		if (first == null) {
			if (other.first != null)
				return false;
		} else if (!first.equals(other.first))
			return false;
		if (second == null) {
			if (other.second != null)
				return false;
		} else if (!second.equals(other.second))
			return false;
		return true;
	}

	
    
    /*
    @SuppressWarnings("unchecked")
	@Override
    public int compareTo(Pair<F,S> otherPair)
    {
    	return first.compareTo(otherPair.first);
    }
	*/
}