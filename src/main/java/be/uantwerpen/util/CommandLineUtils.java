package be.uantwerpen.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.Arrays;

/**
 * Utilities code for running programs command line.
 * 
 * @author lfereman
 *
 */
public class CommandLineUtils {

	//see http://stackoverflow.com/questions/14165517/
	public static class StreamGobbler extends Thread {
		InputStream is;
		PrintStream os;
	
		public StreamGobbler(InputStream is, PrintStream os) {
			this.is = is;
			this.os = os;
		}
	
		@Override
		public void run() {
			try {
				InputStreamReader isr = new InputStreamReader(is);
				BufferedReader br = new BufferedReader(isr);
				String line = null;
				while ((line = br.readLine()) != null)
				{
					os.println(line);
					os.flush();
				}	
			}
			catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
	}

	public static int runCommand(String[] command) throws Exception{
		return CommandLineUtils.runCommand(command, System.out, System.err, null);
	}

	public static boolean TIMER_OUTPUT_ONLY_CONSOLE = false;

	/**
	 * Runs program command line.
	 * 
	 * e.g. runCommand({"ls", "-lah"}, System.out, System.err , '/temp') 
	 * 
	 * Command: specify program and all arguments as array
	 * PrintStream out and err: can be System.out or IOUtils.getPrintStream(logFile) for file
	 * Workdir: null or directory where command is executed
	 * 
	 */
	public static int runCommand(final String[] command, PrintStream out, PrintStream err, File workingDirectory) throws Exception{
		try{
			long timeStart = System.currentTimeMillis();
			String started = String.format(">Running %s\n", Utils.join(command, " "));
			System.out.format(started); 
			if(out != System.out)
				if(!TIMER_OUTPUT_ONLY_CONSOLE)
					out.format(started);
			//see http://stackoverflow.com/questions/14165517/
			ProcessBuilder pb = new ProcessBuilder().command(command);
			if(workingDirectory != null)
				pb.directory(workingDirectory);
			pb.redirectErrorStream(true);
			final Process p = pb.start();
			CommandLineUtils.StreamGobbler errorGobbler = new CommandLineUtils.StreamGobbler(p.getErrorStream(),  err);
			// any output?
			CommandLineUtils.StreamGobbler outputGobbler = new CommandLineUtils.StreamGobbler(p.getInputStream(),  out);
			// start gobblers
			outputGobbler.start();
			errorGobbler.start();
			// add shutdown hook -> If Java is killed, subprocessed should be killed
			// unsure if this works...
			// See also http://stackoverflow.com/questions/269494/how-can-i-cause-a-child-process-to-exit-when-the-parent-does
			Thread closeChildThread = new Thread() {
			    public void run() {
			    	try{
			    		p.exitValue(); //Check if process active, if not exception
			    	}
			    	catch(IllegalThreadStateException e)
			    	{
			    		//process is active -> Destoy
			    		System.out.format("Killing %s\n", command[0]);
			    		p.destroy();
			    	}
			    }
			};
			Runtime.getRuntime().addShutdownHook(closeChildThread); 
			p.waitFor();
			double elapsed = (System.currentTimeMillis() - timeStart)/1000.0;
			if(p.exitValue() != 0)
			{
				String msg = String.format("Error running %s\n", Arrays.toString(command));
				err.format( msg); 
				System.err.format( msg);
			}
			String ended = String.format("<Finished took %5.2f seconds\n", elapsed);
			System.out.format(ended); 
			if(!TIMER_OUTPUT_ONLY_CONSOLE)
				out.format(ended);
			err.flush();
			out.flush();
			return p.exitValue();
		}catch(Exception e)
		{
			System.err.format("Error running %s\n", Arrays.toString(command));
			e.printStackTrace();
			return -1;
		}
	}

	

}
