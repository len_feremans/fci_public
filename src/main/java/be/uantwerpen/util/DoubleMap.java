package be.uantwerpen.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Double indexed map, that is indexed on both key and value.
 * 
 * For example:
 * DoubleMap<String,Integer> map = new DoubleMap<>();
 * map.put("Len", 37);
 * map.put("Boris", 44);
 * Integer value = map.get("Len"); //fast, based on Index (HashMap) on key
 * String key = map.getByValue(37); //also fast, based on Index (HashMap) on value
 * 
 * 
 * @author lfereman
 *
 * @param <T1>
 * @param <T2>
 */
public class DoubleMap<T1,T2> {

	private Map<T1,T2> map = new HashMap<T1,T2>();
	private Map<T2,T1> mapReverse = new HashMap<T2,T1>();
	
	public void put(T1 key, T2 value){
		map.put(key, value);
		mapReverse.put(value, key);
	}
	
	public void remove(T1 key){
		T2 value = map.get(key);
		map.remove(key);
		mapReverse.remove(value);
	}
	
	public T2 get(T1 key){
		return map.get(key);
	}
	
	public Set<T1> keySet(){
		return map.keySet();
	}

	public T1 getByValue(T2 value){
		return mapReverse.get(value);
	}
	
	public Set<T2> valueSet(){
		return mapReverse.keySet();
	}

	
	public String toString(){
		return map.toString();
	}
}
