package be.uantwerpen.pattern_mining;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.ListMultimap;
import com.google.common.primitives.Ints;

import be.uantwerpen.pattern_mining.data.MainDFSDatastructure;

/**
 * Extension of ComputeMinimalWindows for mining dominant sequential patterns.
 * 
 * Computing the number of occurrences of sequential patterns, brings with it additional complexity. 
 * So far we were only interested in the size of the minimal window at each occurrence. 
 * However, at first glance, given a cohesive itemset X = {i1, . . . , in}, we must now count each occurrence 
 * of up to n! sequential permutations in the worst case.  In order to compute the number of occurrences correctly 
 * we must also deal with the fact that more than one  sequential pattern can occur within one minimal occurrence of the itemset. 
 * For example, given sequence aba we need to take into account that both sequential patterns ab and ba occur at time stamp 2 b 
 * since the occurrences of both are equally long as the minimal occurrence of itemset {a, b}. 
 * 
 * @author lfereman
 *
 */
public class ComputeMinimalWindowsSequence {

	public static class SequenceWindow{
		private int position; //position in sequence
		private int size;     //minimal window size 
		List<int[]> minimalWindowsOccurences = new ArrayList<>();  //multiple minimal windows with same length possible 
		List<ListMultimap<Integer,Integer>> minimalWindowsOccurencesItemPositions = new ArrayList<>();
		
		public SequenceWindow(int pos, int size, int[] occurence){
			this.position = pos;
			this.size = size;
			this.minimalWindowsOccurences.add(occurence);	
		}
		
		public String toString(){
			String minimalWindowsOccurencesStr = "";
			for(int[] window: minimalWindowsOccurences){
				minimalWindowsOccurencesStr = minimalWindowsOccurencesStr + ",[" + Ints.min(window) + "-" + Ints.max(window) + "]";
			}
			String positionsStr = "";
			for(ListMultimap<Integer,Integer> positions: minimalWindowsOccurencesItemPositions){
				positionsStr = positionsStr + ",[" + positions + "]";
			}
			return String.format("SequenceWindow[pos:%d,size:%d, windows:%s, positions:%s]", position , size, minimalWindowsOccurencesStr, positionsStr);
		}
	}
	
	private MainDFSDatastructure data;
	public ComputeMinimalWindowsSequence(MainDFSDatastructure data){
		this.data = data;
	}

	private List<SequenceWindow> swindows_final = new ArrayList<>(); 	
	public List<SequenceWindow> getMinimalSequenceWindowsLastRun(){
		return this.swindows_final;
	}

	public double sumMinimalWindowsForSequences(int[] x){
		return sumMinimalWindowsForSequences(x,Integer.MAX_VALUE);
	}

		
	/**
	 * For sequences it's important to keep all minimal windows + keep track of multiple positions, within one minimal window
	 * 
	 * Thus
	 * 	- we want to return multiple minimal windows, 
	 *      e.g. for sequence ...aba... [a,b] and [b,a] for the second b, 
	 *  - we want to return multiple positions within one minimal window,
	 *      e.g. for sequence ..abbd.. and pattern abd  or ..abcbd for pattern abcd, we want the positions of both b's 
	 * 
	 * This algorithm is an adaptation of ComputeMinimalWindows. The main difference is that we now return a list of minimal windows, 
	 * where for each occurrence t we maintain possibly  multiple instances of the minimal windows at each occurrence. 
	 * 
	 * The list of final windows is first initialised and returned together with the sum of minimal windows. 
	 * As a result, within the inner loop we now not only update the minimal width of active windows,  but maintain, where necessary, 
	 * multiple instances of windows with the same minimal length.
	 * If a new window is found with a lower minimal width, we reset the instances of active windows (line 12). If a new window is found with the 
	 * same minimal width, we add that window to the instances. For example, given sequence . . . bxaba . . ., for itemset {a, b},
	 * we will find one minimal window for the two occurrences of a, and the first occurrence of b, but we will find two minimal 
	 * windows for the second occurrence of b. Note that, compared to ComputeMinimalWindows,
	 * we omitted the condition of removing windows where window.width == |X| from the list of active windows, 
	 * because we now want to enumerate all instances of the minimal windows.
	 * 
	 * @param x
	 * @return
	 */
	public double sumMinimalWindowsForSequences(int[] x, double maxMinWindows)
	{
		if(x.length == 0){
			return 1;
		}
		if(x.length == 1){
			return data.getFrequency(x[0]); 
		}
		int position[][] = new int[x.length][]; //holds positions for each element, e.g. 'a': 1 3 10, 'b': 2, 4, 5
		int lastPosition[] = new int[x.length]; //holds lastPosition, e.g. 'a': 4, 'b':2 
		int startFrom[] = new int[x.length];	//holds next (not-visited) offset in positions e.g. 'a': 1, 'b': 1 (after lastPosition)
		List<SequenceWindow> windows_current = new ArrayList<>();
		this.swindows_final =  new ArrayList<>();
		int frequency = 0;
		for(int i=0; i<x.length; i++){
			int ch = x[i];
			int positions_array[] = data.getPositions(ch);
			frequency += positions_array.length;
			position[i] = positions_array;
			startFrom[i] = 0;
			lastPosition[i] = Integer.MIN_VALUE;
		}
		int prev_min = Integer.MIN_VALUE;
		double running_sum = 0.0;
		for(int idx=0; idx<frequency;idx++){
			//get current position
			int pos = Integer.MAX_VALUE;
			for(int i=0; i<x.length; i++){
				if(startFrom[i] != position[i].length){
					pos = Math.min(pos, position[i][startFrom[i]]);
				}
			}
			//update lastPosition
			for(int i=0; i<x.length; i++){
				if(startFrom[i] != position[i].length && position[i][startFrom[i]] == pos){
					lastPosition[i] = pos;
					startFrom[i] = startFrom[i]+1;
				}
			}
			//compute window_size
			int min_pos = Integer.MAX_VALUE;
			int max_pos = Integer.MIN_VALUE;
			for(int lastPos: lastPosition){
				min_pos = Math.min(lastPos, min_pos);
				max_pos = Math.max(lastPos, max_pos);
			}
			int w = Integer.MAX_VALUE;
			if(min_pos != Integer.MIN_VALUE){ //at beginning, lastPos might be Integer.MIN_VALUE
				w = max_pos - min_pos + 1;
			}
			//check condition:
			boolean condition = min_pos != Integer.MIN_VALUE && prev_min < min_pos; 
			if(condition){
				//update windows with minimal size
				List<SequenceWindow> newWindows = new ArrayList<SequenceWindow>(windows_current.size());
				for(SequenceWindow window: windows_current){
					int new_width = max_pos - Math.min( window.position, min_pos) + 1;
					if(window.size >  new_width){
						window.size = new_width;
						int[] currentOccurence = makeCopy(lastPosition);
						window.minimalWindowsOccurences.clear();
						window.minimalWindowsOccurences.add(currentOccurence);
					}
					else if(window.size == new_width){ //keep multiple windows
						int[] currentOccurence = makeCopy(lastPosition);
						window.minimalWindowsOccurences.add(currentOccurence);
					}
					//if final
					if( window.position < min_pos || window.size <= (max_pos -  window.position + 1)){ //not: window.size == x.length 
						running_sum +=window.size ;
						swindows_final.add(window);
					}
					else{//if not final
						newWindows.add(window);
					}
				}
				windows_current = newWindows;
			}
			//add window, and update previous
			windows_current.add(new SequenceWindow(pos, w, makeCopy(lastPosition)));
			prev_min = min_pos;
			//MAX_SUM:
			if(running_sum + (frequency - idx -1 + windows_current.size()) * x.length  > maxMinWindows){
				return Double.MAX_VALUE;
			}
		}
		//end
		swindows_final.addAll(windows_current);
		for(SequenceWindow window: windows_current){
			running_sum += window.size;
		}
		return running_sum;
	}
	
	static int[] makeCopy(int v[]){
		int[] cpy = new int[v.length];
		System.arraycopy(v, 0, cpy, 0, v.length);
		return cpy;
	}
	

}
