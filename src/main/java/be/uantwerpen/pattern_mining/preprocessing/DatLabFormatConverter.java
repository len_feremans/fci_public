package be.uantwerpen.pattern_mining.preprocessing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import be.uantwerpen.util.DoubleMap;
import be.uantwerpen.util.FileStream;

/**
 * FCI_Seq and closepi (@see be.uantwerpen.pattern_mining.experiments.WrapperClosedEpisodesMining)
 * required files to be in dat-lab format. The ".lab" is  a file with all item labels, seperate by a newline.
 * The ".dat" file is a space separated file, with all tokens in stream, index with each line in
 * 
 * For example: mylabels.lab:
 * this
 * are
 * the 
 * labels
 * 
 * For example: mysequence.dat: 
 * 0 1 2 3 4
 * 
 * @author lfereman
 *
 */
public class DatLabFormatConverter {

	/**
	 * e.g.moby_whole.dat, moby.lab -> moby.txt
	 */
	public static void convertDatLab2Normal(File dataSequenceFile, File dataDictionaryFile, File outputFile) throws IOException{
		//parse moby.lab
		FileStream fs = new FileStream(dataDictionaryFile, '\n');
		String token = fs.nextToken();
		int pos=0;
		HashMap<Integer,String> indexToToken = new HashMap<>();
		while(token != null){
			indexToToken.put(pos,token);
			token = fs.nextToken();
			pos++;
		}
		//parse moby_whole, and save
		fs = new FileStream(dataSequenceFile, ' ');
		token = fs.nextToken();
		pos=0;
		FileWriter writer = new FileWriter(outputFile);
		while(token != null){
			String tokenAsStr = indexToToken.get(Integer.valueOf(token));
			writer.write(tokenAsStr);
			writer.write(" ");
			token = fs.nextToken();
			if(pos % 10 ==0)
				writer.write("\n"); //newline every 10 tokens for readability
			pos++;
		}
		writer.close();
		System.out.println("Dict size:" + indexToToken.size() + " Sequence size:" + pos );
	}
	
	/**
	 * e.g. moby.txt -> moby_whole.dat, moby.lab
	 */
	public static void convertNormal2DatLab(File input, File outputDataSequenceFile, File outputDataDictionaryFile) throws IOException{
		DoubleMap<String,Integer> dict = new DoubleMap<String,Integer>(); //abstraction over TreeMap
		FileStream fs = new FileStream(input,' ','\n');
		String token = fs.nextToken();
		int index = 0;
		while(token !=null){
			if(token.isEmpty()){
				token = fs.nextToken();
				continue;
			}
			if(dict.get(token)==null){
				dict.put(token, index++);
			}
			token = fs.nextToken();
		}
		List<Integer> sequence = new ArrayList<Integer>();
		fs = new FileStream(input,' ','\n');
		token = fs.nextToken();
		while(token !=null){
			if(token.isEmpty()){
				token = fs.nextToken();
				continue;
			}
			int idx = dict.get(token);
			sequence.add(idx);
			token = fs.nextToken();
		}
		//save files
		//outputDataDictionaryFile, ends with '.lab'
		//format token1\ntoken2...
		BufferedWriter writer = new BufferedWriter(new FileWriter(outputDataDictionaryFile));
		for(int i=0; i<dict.keySet().size(); i++){
			writer.write(dict.getByValue(i));
			writer.write("\n");
		}
		writer.close();
		System.out.println("Saved " + outputDataDictionaryFile);
		writer = new BufferedWriter(new FileWriter(outputDataSequenceFile));
		for(Integer i: sequence){
			writer.write(String.valueOf(i));
			writer.write(" ");
		}
		writer.close();
		System.out.println("Saved " + outputDataSequenceFile);
	}
	
	//e.g. sequence of 1000.000 items 0 1 2 0 2 2 2 2 1 0 2 reduct to size 10
	public static void sampleSequenceFile(File inputDat, File outputDat, int maxTokens) throws IOException{
		BufferedReader reader = new BufferedReader(new FileReader(inputDat));
		String line = reader.readLine();
		reader.close();
		String[] sequence = line.split(" ");
		BufferedWriter writer = new BufferedWriter(new FileWriter(outputDat));
		for(int i=0; i<maxTokens && i<sequence.length; i++){
			writer.write(sequence[i]);
			writer.write(" ");
		}
		writer.close();
	}
	
	public static List<File> split(File inputDat, int numberOfSplits) throws IOException{
		BufferedReader reader = new BufferedReader(new FileReader(inputDat));
		String line = reader.readLine();
		reader.close();
		String[] sequence = line.split(" ");
		int partition_length = (sequence.length / numberOfSplits);
		List<File> files = new ArrayList<>();
		for(int i=0; i< numberOfSplits; i++) {
			int start = i * partition_length;
			File outputI = new File(inputDat.getParentFile(), inputDat.getName() + "_split_" + String.format("%03d",i+1));
			BufferedWriter writer = new BufferedWriter(new FileWriter(outputI));
			for(int j=start; j<=start+partition_length && j<sequence.length; j++){
				writer.write(sequence[j]);
				writer.write(" ");
			}
			writer.close();
			files.add(outputI);
		}
		return files;
	}
}
