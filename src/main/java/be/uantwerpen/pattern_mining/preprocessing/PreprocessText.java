package be.uantwerpen.pattern_mining.preprocessing;

import org.tartarus.snowball.SnowballStemmer;
import org.tartarus.snowball.ext.englishStemmer;

import be.uantwerpen.util.Utils;

/**
 * For stemming/stopwords removal
 * @author lfereman
 *
 */
public class PreprocessText {
	
	static weka.core.stopwords.Rainbow stopWordsFilter = new weka.core.stopwords.Rainbow();
	//static weka.core.stemmers.SnowballStemmer stemmer = new weka.core.stemmers.SnowballStemmer();
	
	static SnowballStemmer stemmer = new englishStemmer();
	
	//supporting english
	public static String convertLine(String line, boolean stem, boolean stopWords){
		//escape non-ascii
		line= Utils.escapeNonAscii(line);
		//line = Utils.escapeNonAscii(line);
		//common seperators in text: (. is also for numbers, but fuck it)
		line = line.replaceAll("[\\.,\\?!\\-\\—\"\"'';\\|]+", " ");
		//remove non-alphanumerical tokens (works completely?)
		line = line.replaceAll("[^\\w+|\\d+|\\d+\\.\\d+|\\s+]"," ");
		//to lower case!
		line = line.toLowerCase().trim();
		//remove numbers -> optional?
		line = line.replaceAll("\\d+|\\d+\\.\\d+"," ");
		//remove more-then-one-space
		line = line.replaceAll("\\s+", " ");
		//tokenize, see also nltk regex tokenizer package: http://www.nltk.org/_modules/nltk/tokenize/regexp.html
		String[] tokens = line.split("[^\\w+|\\d+|\\d\\.\\d+]");
		//remove stop words
		StringBuffer buff = new StringBuffer();
		for(String token: tokens){
			token = token.trim();
			//stopword?
			if(stopWords && stopWordsFilter.isStopword(token)) 
				continue;
			//stem
			if(stem) {
				stemmer.setCurrent(token);
			    stemmer.stem();
			    token = stemmer.getCurrent();
			    //token = stemmer.stem( token);
			}
			buff.append(token).append(" ");
		}
		return buff.toString();
	}
}
