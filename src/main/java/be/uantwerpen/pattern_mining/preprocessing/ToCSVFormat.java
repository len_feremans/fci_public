package be.uantwerpen.pattern_mining.preprocessing;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import be.uantwerpen.util.FileStream;

public class ToCSVFormat {

	public static void toCSV(File dataSequenceFile, File dataDictFile, File outputCSVFile) throws IOException {
		//parse moby.lab
		FileStream fs = new FileStream(dataDictFile, '\n');
		String token = fs.nextToken();
		int pos=0;
		HashMap<Integer,String> indexToToken = new HashMap<>();
		while(token != null){
			indexToToken.put(pos,token);
			token = fs.nextToken();
			pos++;
		}
		//parse moby_whole, and save
		fs = new FileStream(dataSequenceFile, ' ');
		token = fs.nextToken();
		pos=0;
		FileWriter writer = new FileWriter(outputCSVFile);
		while(token != null){
			String tokenAsStr = indexToToken.get(Integer.valueOf(token));
			writer.write(tokenAsStr);
			writer.write(";");
			writer.write("" + pos);
			writer.write("\n"); 
			token = fs.nextToken();
			pos++;
		}
		writer.close();
		System.out.println("Dict size:" + indexToToken.size() + " Sequence size:" + pos );
	}
}
