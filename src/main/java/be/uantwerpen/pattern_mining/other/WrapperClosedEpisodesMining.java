package be.uantwerpen.pattern_mining.other;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;

import be.uantwerpen.pattern_mining.preprocessing.DatLabFormatConverter;
import be.uantwerpen.pattern_mining.preprocessing.PreprocessText;
import be.uantwerpen.util.CommandLineUtils;
import be.uantwerpen.util.FileStream;
import be.uantwerpen.util.Timer;
import be.uantwerpen.util.Utils;

/**
 * Wrapper for adjusted version of closepi, which contains implementations of Winepi, Laxman and Marbles, by Len Feremans.
 * 
 * Since closepi mines episodes, here all non-parallel episodes are filtered from output, and 
 * closure is off, since this would prune parallel episodes in output
 * 
 * We slightly adjusted the underlying C++ implementation of the state-of-art methods, namely closepi, 
 * see /other_tools/closedepisodeminer_len. 
 * Since closepi mines general episodes, instead of only parallel episodes, or itemsets, and then generates rules 
 * with potentially both general episodes on the left- and right-hand side, this causes an order-of-magnitude more rules, 
 * and requires additional computing resources.  Therefore we made sure that parallel episodes, closed by a partial episode, 
 * are not removed, and instead removed all non-parallel episodes before mining association rules.
 * 
 * @author lfereman
 */
public class WrapperClosedEpisodesMining {

	public static String BINARY_LOCATION_CLOSEPI = "./other_tools/closedepisodeminer_len/closepi";

	public static boolean MIN_WINDOWS_ON = true;
	public static boolean MIN_WINDOWS_OFF = false;
	public static boolean WEIGHTED_WINDOWS_ON = true;
	public static boolean WEIGHTED_WINDOWS_OFF = false;
	public static boolean ITEMSETS_ON = true;
	public static boolean ITEMSETS_OFF = false;
	public static boolean SP_ON = true;
	public static boolean SP_OFF = false;
	public static boolean GENERAL_EPI_ON = true;
	public static boolean GENERAL_EPI_OFF = false;
	public static boolean SPARSE_ON = true;
	public static boolean SPARSE_OFF = false;
	
	//for running marbles
	public static void main(String[] args) throws Exception {
		File inputRaw = new File(args[0]);
		File inputProcessed= new File(inputRaw.getParentFile(), inputRaw.getName() + ".processed.txt");
		File inputProcessed_as_dat= new File(inputRaw.getParentFile(), inputRaw.getName() + ".processed.dat");
		File inputProcessed_as_lab= new File(inputRaw.getParentFile(), inputRaw.getName() + ".processed.lab");
		//pre-processs
		boolean stem = true ,stopwords = true;
		FileStream fs = new FileStream(inputRaw, '\n');
		String line = fs.nextToken();
		FileWriter writer = new FileWriter(inputProcessed);
		while(line != null){
			line = PreprocessText.convertLine(line, stem, stopwords);
			if(!line.isEmpty()){
				writer.write(line);
				writer.write("\n");
			}
			line = fs.nextToken();
		}
		writer.close();
		DatLabFormatConverter.convertNormal2DatLab(inputProcessed,inputProcessed_as_dat, inputProcessed_as_lab);
		//run marbles rules
		int minsupport = Integer.valueOf(args[1]);
		int winsize = Integer.valueOf(args[2]);
		double min_conf = Double.valueOf(args[3]);
		runRules(inputProcessed_as_dat, inputProcessed_as_lab, minsupport, winsize, false, true, min_conf);
	}
	
	public static File runRules(File sequence, File labels, double minSupport, int windowSize, 
			boolean useMinimalWindows, boolean useWeightedWindows, double min_conf) throws Exception{
		Timer timer = new Timer("closepi");
		File output = new File("./temp/closepi_output.txt");
		File outputRules = new File("./temp/closepi_output_rules.txt");
		if(! new File(BINARY_LOCATION_CLOSEPI).exists()) {
			throw new Exception("closepi not found: set BINARY_LOCATION_CLOSEPI in be.uantwerpen.pattern_mining.WrappperClosedEpisodes. Current:" + BINARY_LOCATION_CLOSEPI);
		}
		String[] command = new String[]{BINARY_LOCATION_CLOSEPI,
				"-i",sequence.getAbsolutePath(), "-l", labels.getAbsolutePath(),
				"-o",output.getAbsolutePath(),
				"-t",String.valueOf(minSupport),
				"-w",String.valueOf(windowSize),
				"-u", //UNIQUE labels
				//"-f", // output only f-closed items
				"-N", //don't close nodes
				"-E",  //don't close edges
				"-c",String.valueOf(min_conf),  //confidence threshold
				"-a",outputRules.getAbsolutePath()
		};
		if(useMinimalWindows){
			command = Utils.concatenate(command,new String[]{"-x"});
		}
		if(useWeightedWindows){
			command = Utils.concatenate(command,new String[]{"-y"});
		}
		int result = CommandLineUtils.runCommand(command);
		if(result!=0){
			throw new RuntimeException("Error running closepi: " + Utils.join(command,","));
		}
		timer.end();
		System.out.println("Saved " + outputRules);
		//post process
		List<Map<String,String>> patterns = parseClosepiOutputRules(outputRules);
		List<Map<String,String>> filteredPatterns = new ArrayList<>();
		for(Map<String,String> pattern: patterns){
			if(pattern.get("type").equals("parallel") && pattern.get("to_type").equals("parallel")){
				filteredPatterns.add(pattern);
			}
		}
		Collections.sort(filteredPatterns, sortOnConfidence);
		//save
		String modus = (!useMinimalWindows&&!useWeightedWindows)?"regular":(useMinimalWindows?"min_windows":"weighted");
		File output2 = new File(String.format("./temp/closepi_%s_minSup=%03.3f_window=%03d_conf=%.3f_modus=%s__rules.csv", 
				Utils.getFilenameNoExtension(sequence), minSupport, windowSize, min_conf, modus));
		FileWriter writer = new FileWriter(output2);
		for(Map<String,String> pattern: filteredPatterns){
			String label = makeRuleLabel(pattern, labels);
			if(Double.valueOf(pattern.get("m-confidence")) < min_conf) {
				continue;
			}
			writer.write(String.format("%s;%.3f;%d;%d\n", 
					label, 
					Double.valueOf(pattern.get("m-confidence")),
					(int)(double)Double.valueOf(pattern.get("m-support")),  
					pattern.get("labels").split(" ").length));
		}
		writer.close();
		System.out.println("Saved " + output2);
		return output2;
	}
	
	public static void postProcessClosepiRules(File labels, File outputRules, File newFilename, double min_conf) throws IOException {
		//post process
		List<Map<String,String>> patterns = parseClosepiOutputRules(outputRules);
		List<Map<String,String>> filteredPatterns = new ArrayList<>();
		for(Map<String,String> pattern: patterns){
			if(pattern.get("type").equals("parallel") && pattern.get("to_type").equals("parallel")){
				filteredPatterns.add(pattern);
			}
		}
		Collections.sort(filteredPatterns, sortOnConfidence);
		//save;
		FileWriter writer = new FileWriter(newFilename);
		for(Map<String,String> pattern: filteredPatterns){
			String label = makeRuleLabel(pattern, labels);
			if(Double.valueOf(pattern.get("m-confidence")) < min_conf) {
				continue;
			}
			writer.write(String.format("%s;%.3f;%d;%d\n", 
					label, 
					Double.valueOf(pattern.get("m-confidence")),
					(int)(double)Double.valueOf(pattern.get("m-support")),  
					pattern.get("labels").split(" ").length));
		}
		writer.close();
		System.out.println("Saved " + newFilename);
	}

	public static File runMethod(File sequence, File labels, double minSupport, int windowSize, 
			boolean useMinimalWindows, boolean useWeightedWindows, 
			boolean keepItemsets, boolean keepSequences, boolean keepEpisodes) throws Exception{
			return runMethod(sequence, labels, minSupport, windowSize, useMinimalWindows, 
					useWeightedWindows, keepItemsets, keepSequences, keepEpisodes, false);
	}
	
	public static File getOutputClosepiRawOutput(File sequence, double minSupport, int windowSize, 
			boolean useMinimalWindows, boolean useWeightedWindows, 
			boolean keepItemsets, boolean keepSequences, boolean keepEpisodes) {
		String modus = (!useMinimalWindows&&!useWeightedWindows)?"regular":(useMinimalWindows?"min_windows":"weighted");
		String ptype =  keepItemsets?"itemsets": keepSequences?"sequences": "episodes";
		File output = new File(String.format("./temp/closepi_raw_%s_minSup=%03.3f_window=%03d_modus=%s_type=%s.csv", 
											Utils.getFilenameNoExtension(sequence), minSupport, windowSize, modus , ptype));
		return output;
	}
	
	public static File runMethod(File sequence, File labels, double minSupport, int windowSize, 
			boolean useMinimalWindows, boolean useWeightedWindows, 
			boolean keepItemsets, boolean keepSequences, boolean keepEpisodes, boolean sparse) throws Exception{
		Timer timer = new Timer("closepi");
		String modus = (!useMinimalWindows&&!useWeightedWindows)?"regular":(useMinimalWindows?"min_windows":"weighted");
		File output = getOutputClosepiRawOutput(sequence, minSupport, windowSize, useMinimalWindows, useWeightedWindows, keepItemsets, keepSequences, keepEpisodes);
		String[] command = new String[]{BINARY_LOCATION_CLOSEPI,
				"-i",sequence.getAbsolutePath(), 
				"-o",output.getAbsolutePath(),
				"-t",String.valueOf(minSupport),
				"-w",String.valueOf(windowSize),
				"-u",
				"-N", //don't close nodes
				"-E"  //don't close edges
		};
		if(labels != null){
			command = Utils.concatenate(command,new String[]{"-l", labels.getAbsolutePath(),});
		}
		if(sparse){
			command = Utils.concatenate(command,new String[]{"-s"});
		}
		if(useMinimalWindows){
			command = Utils.concatenate(command,new String[]{"-x"});
		}
		if(useWeightedWindows){
			command = Utils.concatenate(command,new String[]{"-y"});
		}
		int result = CommandLineUtils.runCommand(command);
		if(result!=0){
			throw new RuntimeException("Error running closepi: " + Utils.join(command,","));
		}
		timer.end();
		List<Map<String,String>> patterns = postProcess(output, useMinimalWindows, useWeightedWindows, keepItemsets, keepSequences, keepEpisodes);
		//save
		File output2 = new File(String.format("./temp/closepi_%s_minSup=%03.3f_window=%03d_modus=%s_type=%s.csv", 
				Utils.getFilenameNoExtension(sequence), minSupport, windowSize, modus, keepItemsets?"itemsets": keepSequences?"sequences": "episodes"));
		System.out.println("started " + output2.getName());
		FileWriter writer = new FileWriter(output2);
		for(Map<String,String> pattern: patterns){
			String label = getLabel(pattern);
			writer.write(String.format("%s;%s;%.3f;%d\n", 
					label, 
					Integer.valueOf(pattern.get("m-support")),
					Double.valueOf(pattern.get("m-minwin")),  
					pattern.get("nodes").split(" ").length));
		}
		writer.close();
		System.out.println("Saved " + output2);
		return output2;
	}

	private static String getLabel(Map<String, String> pattern) {
		String label = "";
		if(pattern.get("type").equals("parallel")){
			label = pattern.get("nodes");
			if(pattern.get("labels") != null &&  !pattern.get("labels").trim().isEmpty()) {
				label = pattern.get("labels");
			}
		}
		else if(pattern.get("type").equals("serial")){
			label = serialToSequenceLabel(pattern);
		}
		else if(pattern.get("type").equals("general")){
			label = generalToLabel(pattern);		
		}
		return label;
	}

	/**
	 * Output like:
graph: 0
size: 1 0
nodes: 0
edges:
type: parallel
labels: call
m-support: 2748
m-minwin: 189.000000
m-fclosed: 0

graph: 1
...
	 * @param outputFile
	 * @throws IOException 
	 */
	public static List<Map<String,String>> postProcess(File outputFile, boolean useMinimalWindows, boolean useWeightedWindows, boolean keepItemsets, boolean keepSequences, boolean keepEpisodes) throws IOException{
		List<Map<String,String>> patterns = parseClosepiOutput(outputFile);
		List<Map<String,String>> filteredPatterns = new ArrayList<>();
		if(keepItemsets){
			for(Map<String,String> pattern: patterns){
				if(pattern.get("type").equals("parallel") && !pattern.get("size").startsWith("1")){
					filteredPatterns.add(pattern);
				}
			}
			System.out.format("Found %d parallel (size >= 2) episodes.\n", filteredPatterns.size());
		}
		if(keepSequences){
			for(Map<String,String> pattern: patterns){
				if(pattern.get("type").equals("serial") & !pattern.get("size").startsWith("1")){
					filteredPatterns.add(pattern);
				}
			}
			System.out.format("Found %d serial episodes.\n", filteredPatterns.size());
		}
		if(keepEpisodes){
			for(Map<String,String> pattern: patterns){
				if(pattern.get("type").equals("general") & !pattern.get("size").startsWith("1")){
					filteredPatterns.add(pattern);
				}
			}
			System.out.format("Found %d general episodes.\n", filteredPatterns.size());
		}
		if(useMinimalWindows || useWeightedWindows){
			System.out.println("Sorting on min windows");
			Collections.sort(filteredPatterns, sortOnMinwin);
		}
		else{
			System.out.println("Sorting on support");
			Collections.sort(filteredPatterns, sortOnSupport);
		}
		return filteredPatterns;
	}
	
	private static Comparator<Map<String,String>> sortOnSupport =  (Map<String,String> map1, Map<String,String> map2) -> {
		int sup1 = Integer.valueOf(map1.get("m-support"));
		int sup2 = Integer.valueOf(map2.get("m-support"));
		int len1 = map1.get("nodes").split(" ").length;
		int len2 = map2.get("nodes").split(" ").length;
		//sort on support, of length if same support
		if(sup1 != sup2){
			return sup2-sup1;
		}
		//else: sort on desc by pattern.length
		return len2-len1;
	};

	/**
	 * We then sorted the output on the respective quality measures --- the sliding window frequency for \textsc{Winepi}, 
	 * the non-overlapping minimal window frequency for \textsc{Laxman}, the weighted window frequency for 
	 * \textsc{Marbles\textsubscript{w}}. We used the sum of support of individual items making up an itemset as the second criteria for ranking for CI, 
	 * and then pattern size and alphabetic order of the pattern,  to break any ties in all four methods.
	 * 
	 */
	private static  Comparator<Map<String,String>> sortOnMinwin =  (Map<String,String> map1, Map<String,String> map2) -> {
		Double minWin1 = Double.valueOf(map1.get("m-minwin"));
		Double minWin2 = Double.valueOf(map2.get("m-minwin"));
		int len1 = map1.get("nodes").split(" ").length; //was labels: bugfix len
		int len2 = map2.get("nodes").split(" ").length;
		//first sort on sliding window frequency
		if(!Utils.equals(minWin1, minWin2)){
			return minWin2.compareTo(minWin1);
		}
		else if(len2!=len1){
			return len2-len1;
		}
		else {
			return getLabel(map1).compareTo(getLabel(map2));
		}
	};
	

	private static  Comparator<Map<String,String>> sortOnConfidence =  (Map<String,String> map1, Map<String,String> map2) -> {
		Double conf1 = Double.valueOf(map1.get("m-confidence"));
		Double conf2 = Double.valueOf(map2.get("m-confidence"));
		Double sup1 = Double.valueOf(map1.get("m-support"));
		Double sup2 = Double.valueOf(map2.get("m-support"));
		//sort on support, of length if same support
		if(!Utils.equals(conf1, conf2)){
			return conf2.compareTo(conf1);
		}
		//else: sort on desc by pattern.length
		return sup2.compareTo(sup1);
	};	

	public static List<Map<String,String>> parseClosepiOutput(File closepiOutput) throws IOException{
		FileStream fs = new FileStream(closepiOutput, '\n');
		String line = fs.nextToken();
		List<Map<String,String>> patterns = new ArrayList<>();
		while(line != null){
			if(line.startsWith("graph:")){
				Map<String,String> attributes = new LinkedHashMap<String,String>();
				while(true){
					String[] pair = line.split(":\\s+");
					if(pair.length == 2)
						attributes.put(pair[0], pair[1]);
					if(pair[0].equals("m-fclosed"))
						break;
					line = fs.nextToken();
				}
				patterns.add(attributes);
			}
			line = fs.nextToken();
		}
		System.out.println("Parsed " + closepiOutput.getName() + " #patterns in total " + patterns.size());
		return patterns;
	}
	
	public static void saveParsedClosepiOutput(List<Map<String,String>> patterns, File closepiOutputNew) throws IOException{
		BufferedWriter os = new BufferedWriter(new FileWriter(closepiOutputNew));
		for(Map<String,String> pattern: patterns) {
			for(Entry<String,String> entry: pattern.entrySet()) {
				os.write(entry.getKey());
				os.write(": ");
				os.write(entry.getValue());
				os.write("\n");
			}
			os.write("\n");
		}
		os.close();
	}
	
	/**
	 * e.g. parse something like
	 * 
	    from:
		size: 2 0
		nodes: 4 35
		edges:
		type: parallel
		to:
		size: 3 0
		nodes: 3 4 35
		edges:
		type: parallel
		labels: natur select state
		m-support: 24.000000
		m-confidence: 0.838710
		
	 * @param closepiOutput
	 * @return
	 * @throws IOException
	 */
	private static List<Map<String,String>> parseClosepiOutputRules(File closepiOutput) throws IOException{
		FileStream fs = new FileStream(closepiOutput, '\n');
		String line = fs.nextToken();
		List<Map<String,String>> patterns = new ArrayList<>();
		while(line != null){
			if(line.startsWith("from:")){
				Map<String,String> attributes = new HashMap<String,String>();
				while(true){
					String[] pair = line.split(":\\s+");
					if(pair.length == 2){
						//note: size/nodes/edges/type occur twice for rules
						if(attributes.get(pair[0]) == null){
							attributes.put(pair[0], pair[1]);
						}
						else{
							attributes.put("to_" + pair[0], pair[1]);
						}
						
					}
					if(pair[0].equals("m-confidence"))
						break;
					line = fs.nextToken();
				}
				patterns.add(attributes);
			}
			line = fs.nextToken();
		}
		System.out.println("Parsed " + closepiOutput.getName() + " #patterns in total " + patterns.size());
		return patterns;
	}


		
	/** 
	 * input:
	 * edges: 0 -> 2 2 -> 1
	type: serial
	labels: chapter long whale

		output: chapter,whale,long
	 */	
	private static String serialToSequenceLabel(Map<String,String> pattern){
		//parse edges
		String labelsPattern[] = null;
		if(pattern.get("labels") == null || pattern.get("labels").trim().isEmpty()) {
			labelsPattern = pattern.get("nodes").split(" ");
		}
		else {
			labelsPattern = pattern.get("labels").split(" ");
		}
		String edges_tokens[] = pattern.get("edges").split("\\s?(->)?");
		List<Integer> edgesTokens = new ArrayList<>();
		for(String token: edges_tokens){
			if(token.trim().length() > 0){
				edgesTokens.add(Integer.valueOf(token));
			}
		}
		//for each node, compute transitive closure... if closure == #number-of-nodes -> Take that 
		//e.g. case: 1->0, 2->1
		for(int i=0; i<labelsPattern.length; i++){
			//compute closure
			List<Integer> order = new ArrayList<>();
			order.add(i);
			while(true){
				boolean update = false;
				for(int j=0;j<edgesTokens.size(); j+=2){
					Integer firstNode = edgesTokens.get(j);
					Integer secondNode = edgesTokens.get(j+1);
					if(order.contains(firstNode) && !order.contains(secondNode)){
						order.add(secondNode);
						update = true;
					}
				}
				if(!update)
					break;
			}
			//if closure == #number of nodes -> Take
			if(order.size() == labelsPattern.length){
				StringBuffer buff = new StringBuffer();
				for(Integer element: order){
					buff.append(labelsPattern[element]);
					buff.append(",");
				}
				String s = buff.toString();
				s = s.substring(0, s.length()-1);
				return s;
			}
		}
		throw new RuntimeException("Should not get here: serialToSequenceLabel " + pattern);
	}

	/**
	 * Transform closepi output format, e.g.
	nodes: 0 7 508
	edges: 0 -> 1
	type: general
	labels: chapter long whale
	m-support: 40
	m-minwin: 6.000000
	m-fclosed: 0
	 * to [general->chapter, whale]
	 * OR
	graph: 59840
	size: 3 2
	nodes: 0 7 508
	edges: 0 -> 1 0 -> 2
	type: general
	labels: chapter long whale
	 * TO [chapter->long, chapter-whale]
	 **/
	private static String generalToLabel(Map<String,String> pattern){
		List<String> tokens = new ArrayList<>();
		//parse edges
		String labelsPattern[] = pattern.get("nodes").split(" ");
		if(pattern.get("labels") != null &&  !pattern.get("labels").trim().isEmpty()) {
			labelsPattern = pattern.get("labels").split(" ");
		}
		String edges_tokens[] = pattern.get("edges").split("\\s?(->)?");
		List<Integer> edgesTokens = new ArrayList<>();
		for(String token: edges_tokens){
			if(token.trim().length() > 0){
				edgesTokens.add(Integer.valueOf(token));
			}
		}
		Set<String> labelsVisited = new HashSet<String>();
		for(int j=0;j<edgesTokens.size(); j+=2){
			Integer firstNode = edgesTokens.get(j);
			Integer secondNode = edgesTokens.get(j+1);
			tokens.add (labelsPattern[firstNode] + "->" + labelsPattern[secondNode]);
			labelsVisited.add(labelsPattern[firstNode]);
			labelsVisited.add(labelsPattern[secondNode]);
		}
		Collections.sort(tokens);
		//for any other label, add as wel
		for(String label: labelsPattern){
			if(!labelsVisited.contains(label)){
				tokens.add(label);
			}
		}
		return "[" + Utils.join(tokens, ", ") + "]";
	}
	
	private static List<String> labelsList = new ArrayList<>();
	private static String makeRuleLabel(Map<String, String> pattern, File labels) throws IOException {
		/** closepi format Example:
		from:
		size: 2 0
		nodes: 4 35
		edges:
		type: parallel
		to:
		size: 3 0
		nodes: 3 4 35
		edges:
		type: parallel
		labels: natur select state
		m-support: 24.000000
		m-confidence: 0.838710
	
		 **/
		if(labelsList.size() == 0){
			FileStream fs = new FileStream(labels, '\n');
			labelsList = fs.readAll();
		}
		String[] labelsFrom = pattern.get("nodes").split(" ");
		String[] labelsTo = pattern.get("to_nodes").split(" ");
		StringBuffer buff = new StringBuffer();
		for(String label: labelsFrom){
			String actualLabel = labelsList.get(Integer.valueOf(label));
			buff.append(actualLabel);
			buff.append(",");
		}
		String prefix = buff.toString().substring(0,buff.length()-1);
		buff = new StringBuffer();
		for(String label: labelsTo){
			if(ArrayUtils.contains(labelsFrom,label)){
				continue;
			}
			String actualLabel = labelsList.get(Integer.valueOf(label));
			buff.append(actualLabel);
			buff.append(",");
		}
		String suffix = buff.toString().substring(0,buff.length()-1);
		return prefix + "->" + suffix;
	}
}
