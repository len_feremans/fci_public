package be.uantwerpen.pattern_mining.other;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import be.uantwerpen.util.CommandLineUtils;
import be.uantwerpen.util.Utils;
public class WrapperCompactMinimalWindows {

	public static String CLOSEPI_PATH = "./other_tools/closedepisodeminer/closepi";
	public static String COMPACT_MINIMAL_WINDOWS_PATH = "./other_tools/tatti_episodes_len/tatti_episodes";

	/**
	 * Run closepi, than compact minimal windows
	 * E.g.
	 * ./other_tools/closedepisodeminer/closepi -t 10 -w 40 -x -f -s -i ./data/zimmerman_default_data.csv -o ./temp/episodes2.txt
	 * ./other_tools/tatti_episodes_len/tatti_episodes 
	 * 		-t ../data/zimmerman_default_data.csv 
	 * 		-r ../data/zimmerman_default_data.csv 
	 *      -d ../temp/episodes2.txt 
	 *      -a 0.5 
	 * 		-o ../temp/episodes_ranked2.txt
	 * @param threshold
	 * @param window
	 * @param alpha
	 * @throws Exception 
	 */
	public static File runMethod(File inputSparseFile, int threshold, int window, double alpha) throws Exception {
		CommandLineUtils.TIMER_OUTPUT_ONLY_CONSOLE = true;
		File closepi = new File(CLOSEPI_PATH);
		if(!closepi.exists()) {
			System.err.println("closepi not found. Path:" + CLOSEPI_PATH);
			return null;
		}
		File mincomwin = new File(COMPACT_MINIMAL_WINDOWS_PATH);
		if(!mincomwin.exists()) {
			System.err.println("compact minimal windows not found:" + COMPACT_MINIMAL_WINDOWS_PATH);
			return null;
		}
		//run laxman
		File output = new File("./temp/episodes_closepi_" + inputSparseFile.getName());
		CommandLineUtils.runCommand(new String[] {closepi.getAbsolutePath(),
				"-t", Integer.toString(threshold), 
				"-w", Integer.toString(window), 
				"-x", "-f", "-s",  "-u", //-u : only UNIQUE LABELS... ok for know
				"-N", //don't close nodes
				"-E",  //don't close edges
				"-i", inputSparseFile.getAbsolutePath(), 
				"-o", output.getAbsolutePath()});
		Utils.printHead(output,10);
		//split file
		File train = new File(inputSparseFile.getAbsolutePath() + "_train.txt");
		File test = new File(inputSparseFile.getAbsolutePath() + "_test.txt");
		BufferedReader reader = new BufferedReader(new FileReader(inputSparseFile));
		List<String> lines = new ArrayList<>();
		String line = reader.readLine();
		while(line != null) {
			lines.add(line);
			line = reader.readLine();
		}
		reader.close();
		BufferedWriter writer = new BufferedWriter(new FileWriter(train));
		for(int i=0; i<lines.size()/2; i++){
			writer.write(lines.get(i));
			writer.write("\n");
		}
		writer.close();
		writer = new BufferedWriter(new FileWriter(test));
		for(int i=lines.size()/2; i<lines.size(); i++){
			writer.write(lines.get(i));
			writer.write("\n");
		}
		writer.close();
		//run CMW
		File output2 = new File("./temp/episodes_closepi_" + inputSparseFile.getName() + "_ranked.txt");
		CommandLineUtils.runCommand(new String[] {mincomwin.getAbsolutePath(),"-t", train.getAbsolutePath(),
				"-r", test.getAbsolutePath(), "-d", output.getAbsolutePath(), "-a", Double.toString(alpha), 
				"-o", output2.getAbsolutePath()});
		Utils.printHead(output2,10);
		File finalOutput = Utils.sortCSVFile(output2, comp, true);
		Utils.printHead(finalOutput,10);
		return finalOutput;
	}
	
	private static Comparator<String[]> comp = new Comparator<String[]>() {

		@Override
		public int compare(String[] v1, String[] v2) {
			Double x1,x2;
			if(v1[5].equals("nan")) {
				x1 = Double.NEGATIVE_INFINITY;
			}
			else {
				x1 = Double.valueOf(v1[5]);
			}
			if(v2[5].equals("nan")) {
				x2 = Double.NEGATIVE_INFINITY;
			}
			else {
				x2 = Double.valueOf(v2[5]);
			}
			return x2.compareTo(x1);
		}
	};

	

	public static File runCMW(File sequenceFile, File labelFile, File closepiFile, double alpha, 
			boolean onlyItemsets, boolean onlySequences, boolean onlyGeneral) {
		try {
			if(onlyItemsets) {
				List<Map<String,String>> patterns = WrapperClosedEpisodesMining.parseClosepiOutput(closepiFile);
				List<Map<String,String>> filteredPatterns = new ArrayList<>();
				for(Map<String,String> pattern: patterns){
					if(pattern.get("type").equals("parallel") && !pattern.get("size").startsWith("1")){
						filteredPatterns.add(pattern);
					}
				}
				System.out.format("Found %d parallel (size >= 2) episodes.\n", filteredPatterns.size());
				closepiFile = new File(closepiFile.getAbsolutePath() + "_itemsets.txt");
				WrapperClosedEpisodesMining.saveParsedClosepiOutput(filteredPatterns, closepiFile);
			}
			if(onlySequences) {
				List<Map<String,String>> patterns = WrapperClosedEpisodesMining.parseClosepiOutput(closepiFile);
				List<Map<String,String>> filteredPatterns = new ArrayList<>();
				for(Map<String,String> pattern: patterns){
					if(pattern.get("type").equals("serial") && !pattern.get("size").startsWith("1")){
						filteredPatterns.add(pattern);
					}
				}
				System.out.format("Found %d serial (size >= 2) episodes.\n", filteredPatterns.size());
				closepiFile = new File(closepiFile.getAbsolutePath() + "_sp.txt");
				WrapperClosedEpisodesMining.saveParsedClosepiOutput(filteredPatterns, closepiFile);
			}
			if(onlyGeneral) {
				List<Map<String,String>> patterns = WrapperClosedEpisodesMining.parseClosepiOutput(closepiFile);
				List<Map<String,String>> filteredPatterns = new ArrayList<>();
				for(Map<String,String> pattern: patterns){
					if(pattern.get("type").equals("general") && !pattern.get("size").startsWith("1")){
						filteredPatterns.add(pattern);
					}
				}
				System.out.format("Found %d serial (size >= 2) episodes.\n", filteredPatterns.size());
				closepiFile = new File(closepiFile.getAbsolutePath() + "_sp.txt");
				WrapperClosedEpisodesMining.saveParsedClosepiOutput(filteredPatterns, closepiFile);
			}
			CommandLineUtils.TIMER_OUTPUT_ONLY_CONSOLE = true;
			File mincomwin = new File(COMPACT_MINIMAL_WINDOWS_PATH);
			if(!mincomwin.exists()) {
				System.err.println("compact minimal windows not found:" + COMPACT_MINIMAL_WINDOWS_PATH);
				return null;
			}
			File output = new File(closepiFile.getAbsolutePath() + "_ranked.txt");
			//split file
			File train = new File(sequenceFile.getAbsolutePath() + "_train.txt");
			File test = new File(sequenceFile.getAbsolutePath() + "test.txt");
			BufferedReader reader = new BufferedReader(new FileReader(sequenceFile));
			String line = reader.readLine();
			reader.close();
			String[] sequence = line.split(" ");
			BufferedWriter writer = new BufferedWriter(new FileWriter(train));
			for(int i=0; i<sequence.length/2; i++){
				writer.write(sequence[i]);
				writer.write(" ");
			}
			writer.close();
			writer = new BufferedWriter(new FileWriter(test));
			for(int i=sequence.length/2; i<sequence.length; i++){
				writer.write(sequence[i]);
				writer.write(" ");
			}
			writer.close();
			//labels specified, so _readable.txt file saved sorted on zone_two
			CommandLineUtils.runCommand(new String[] {mincomwin.getAbsolutePath(),"-t", train.getAbsolutePath(),
					"-r", test.getAbsolutePath(), "-d", closepiFile.getAbsolutePath(), "-a", Double.toString(alpha), 
					"-o", output.getAbsolutePath(), "-l", labelFile.getAbsolutePath()});
			Utils.printHead(output,10);
			output = new File(output.getAbsolutePath() + "_readable.txt");
			System.out.println(output.getAbsolutePath());
			Utils.printHead(output,10);
			return output;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
