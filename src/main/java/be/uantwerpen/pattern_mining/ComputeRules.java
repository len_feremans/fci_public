package be.uantwerpen.pattern_mining;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;

import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;

import be.uantwerpen.pattern_mining.ComputeMinimalWindows.Window;
import be.uantwerpen.pattern_mining.data.MainDFSDatastructure;

/**
 * When discovering a cohesive itemset X, we need to compute the exact minimal window Wt(X) 
 * containing X for each time stamp t at which an item x \in X occurs. In fact, we compute the sum of all such windows 
 * for each x \in X in (@see be.uantwerpen.pattern_mining.ComputeMinimalWindows), before adding them up into the overall 
 * sum needed to compute C(X). 
 * 
 * With these sums still in memory, we can easily compute the confidence of all association rules of the 
 * form x ⇒ X \ {x}, with x \in X, that can be generated from itemset X. 
 * 
 * @author lfereman
 *
 */
public class ComputeRules {
	
	private MainDFSDatastructure data;
	private ComputeMinimalWindows computerMinimalWindows;
	
	public ComputeRules(MainDFSDatastructure data){
		this.data = data;
		this.computerMinimalWindows = new ComputeMinimalWindows(data);
	}
	
	public static class Rule{
		public String antecedent;
		public String consequent;
		public Set<Integer> antecedentItems;
		public Set<Integer> consequentItems;
		public double conf;
		
		public String toString(){
			return String.format("%s->%s: %.3f", antecedent, consequent, conf);
		}
	}

	public List<Rule> findRulesFromItemSet(int[] itemsetCodes, double min_conf){
		//compute confidence of rules {x} -> X \ {x}
		List<Rule> rules = new ArrayList<>();
		computerMinimalWindows.sumMinimalWindows(itemsetCodes);
		List<Window> windows = computerMinimalWindows.getMinimalWindowsLastRun();
		HashMap<Integer, Double> confidenceSingletons = new HashMap<>();
		for(int i=0; i< itemsetCodes.length; i++){
			int[] positions = data.getPositions(itemsetCodes[i]);
			double sizeOfWindowsForI = 0;
			for(Window window: windows){
				if(ArrayUtils.contains(positions, window.position)){//todo: can be more efficient
					sizeOfWindowsForI += window.size;
				}
			}
			sizeOfWindowsForI = sizeOfWindowsForI / positions.length;
			double confidence = itemsetCodes.length / sizeOfWindowsForI;
			confidenceSingletons.put(itemsetCodes[i], confidence);
		}
		//enumerate all subsets of X 
		HashSet<Integer> hashSet = new HashSet<>();
		for(int el: itemsetCodes){
			hashSet.add(el);
		}
		Set<Set<Integer>> powerSet = Sets.powerSet(hashSet); //from Google, pretty fast implementation
		//generate rules that are confident
		for(Set<Integer> subset: powerSet){
			if(subset.size() == 0 || subset.size() == hashSet.size())
				continue;
			double frequencySubset =0;
			double wxy = 0;
			for(int i: subset){
				wxy += data.getFrequency(i) / confidenceSingletons.get(i);
				frequencySubset += data.getFrequency(i);
			}
			double confidence = frequencySubset/ wxy;
			if(confidence > min_conf){ //small change: >=
				String antecedent = "";
				for(int item: subset){
					antecedent += data.getLabel(item) + ",";
				}
				antecedent = antecedent.substring(0, antecedent.length()-1);
				String consequent = "";
				SetView<Integer> complement = Sets.difference(hashSet, subset);
				for(int item: complement){
					consequent+= data.getLabel(item) + ",";
				}
				consequent = consequent.substring(0, consequent.length()-1);
				Rule r = new Rule();
				r.antecedent = antecedent;
				r.antecedentItems = subset;
				r.consequent = consequent;
				r.consequentItems = complement;
				r.conf = confidence;
				rules.add(r);
			}
		}
		return rules;
	}
	

}
