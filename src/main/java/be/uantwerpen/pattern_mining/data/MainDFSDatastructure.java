package be.uantwerpen.pattern_mining.data;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import be.uantwerpen.util.CountMap;
import be.uantwerpen.util.FileStream;
import be.uantwerpen.util.Pair;

/**
 * Main data structure. See also @see be.uantwerpen.pattern_mining.data.IndexStructure.
 * Option to load data in regular or sparse format
 * 
 * @author lfereman
 *
 */
public class MainDFSDatastructure {
	public static boolean DEBUG_LOAD_DATA = false;

	private IndexStructure inverseIndex; //Cache
	private int[] frequentItems; //contains frequency... internally using auto-increment ID's for each item
	private String[] frequentItemsLabels; //from item(17) to  "hello"
	private Map<String,Integer> tokenToItemIdx; //from "the" to item(0)
	private List<String> labels;
	
	/**
	 * @param data
	 * @throws IOException 
	 */
	public MainDFSDatastructure(File inputLabels, File inputSequence, int minSupport) throws IOException  {
		loadItemsAndLabels(inputLabels, inputSequence, minSupport);
		loadSequenceInInverseIndex(inputSequence);
		debugLoadData();
	}
	
	/**
	 * @param data
	 * @throws IOException 
	 */
	public MainDFSDatastructure(File inputSequenceSparse, int minSupport) throws IOException  {
		loadItemsAndLabelsSparse(inputSequenceSparse, minSupport);
		loadSequenceInInverseIndexSparse(inputSequenceSparse);
		debugLoadData();
	}
	
	public final int getNumberOfItems(){
		return this.frequentItems.length;
	}
	
	public final int getFrequency(int item){
		return this.frequentItems[item];
	}

	public final int getFrequency(int[] pattern){
		int freq = 0;
		for(int item: pattern){
			freq += this.frequentItems[item];
		}
		return freq;
	}

	public int[] getPositions(int item){
		return this.inverseIndex.get(item);
	}

	//gets label based on frequent-label index
	public final String getLabel(int item){
		return this.frequentItemsLabels[item];
	}
	
	//gets label based on index/line number in original labels file
	public final String getLabelOriginal(int item){
		return this.labels.get(item);
	}

	public final int getItemCode(String label){
		if(this.tokenToItemIdx.get(label) == null){
			throw new RuntimeException("Label not found " + label);
		}
		return this.tokenToItemIdx.get(label);
	}

	/**
	 * Input like:
	 * @param inputLabels:    label1\n
	 *                        label2\n
	 * @param inputSequence:   0 1 2 3 .. 4 3 2 ... where i is index in label file 
	 * @param minSupport
	 * @throws IOException
	 */
	private void loadItemsAndLabels(File inputLabels, File inputSequence, int minSupport) throws IOException{
		//Fase1: first 'stream' over file, to get frequencies 
		CountMap<Long> count_map = countItemsInSequence(inputSequence);
		Map<Long,Integer> count_map_pruned = new HashMap<Long,Integer>();
		for(Entry<Long,Integer> entry: count_map.getMap().entrySet()){
			if(entry.getValue() >= minSupport){
				count_map_pruned.put(entry.getKey(), entry.getValue());
			}
		}
		int originalSize = count_map.getMap().size();
		System.out.println("All items size:" + originalSize);
		System.out.println("Frequent items size:" + count_map_pruned.size());
		
		//Load labels
		this.labels = loadLabelList(inputLabels);
		//Fase2: Build basic datastructure: 
		// - Items represented by unique id
		// - sorted on frequency
		// - map to translate from token in raw data to item id, and vice-versa
		int frequentItemSize = count_map_pruned.entrySet().size();
		List<Pair<Long,Integer>> listOfFrequentTokens = new ArrayList<>(frequentItemSize);
		for(Entry<Long,Integer> entry: count_map_pruned.entrySet()){
			listOfFrequentTokens.add(new Pair<>(entry.getKey(), entry.getValue()));
		}
		Collections.sort(listOfFrequentTokens, (o1,o2) ->  o1.getSecond() - o2.getSecond()); //sort on frequency ASCENDING... IMPORTANT
		this.frequentItems = new int[frequentItemSize];
		this.frequentItemsLabels = new String[frequentItemSize];
		this.tokenToItemIdx = new HashMap<String,Integer>();
		for(int i=0; i< frequentItemSize; i++){
			this.frequentItems[i] = listOfFrequentTokens.get(i).getSecond();
			long id = listOfFrequentTokens.get(i).getFirst();
			this.frequentItemsLabels[i] = labels.get((int)id);
			this.tokenToItemIdx.put(frequentItemsLabels[i], i);
		}
	}
	
	/**
	 * Input like timepoint code1\n
	 *           timepoint2 code2\n...
	 * 
	 * @param inputSequenceSparse
	 * @param minSupport
	 * @throws IOException 
	 */
	private void loadItemsAndLabelsSparse(File inputSequenceSparse, int minSupport) throws IOException {
		//Stream over file and get frequencies
		FileStream fs = new FileStream(inputSequenceSparse, ' ', '\n');
		String time = fs.nextToken();
		String label = fs.nextToken();
		List<String> labelsArray = new ArrayList<String>();
		CountMap<Integer> labelCount = new CountMap<>();
		while(label != null){
			label = label.trim();
			int labelCode = Integer.valueOf(label);
			labelCount.add(labelCode);
			if(labelsArray.size() <= labelCode){
				int addAmount = labelCode + 10 - labelsArray.size();
				for(int i=0; i< addAmount; i++){
					labelsArray.add(null);
				}
			}
			labelsArray.set(labelCode, label);
			time = fs.nextToken();
			label = fs.nextToken();
		}
		this.labels = labelsArray;
		//prune
		Map<Integer,Integer> count_map_pruned = new HashMap<Integer,Integer>();
		for(Entry<Integer,Integer> entry: labelCount.getMap().entrySet()){
			if(entry.getValue() >= minSupport){
				count_map_pruned.put(entry.getKey(), entry.getValue());
			}
		}
		//
		int frequentItemSize = count_map_pruned.entrySet().size();
		List<Pair<Integer,Integer>> listOfFrequentTokens = new ArrayList<>(frequentItemSize);
		for(Entry<Integer,Integer> entry: count_map_pruned.entrySet()){
			listOfFrequentTokens.add(new Pair<>(entry.getKey(), entry.getValue()));
		}
		Collections.sort(listOfFrequentTokens, (o1,o2) ->  o1.getSecond() - o2.getSecond());
		this.frequentItems = new int[frequentItemSize];
		this.frequentItemsLabels = new String[frequentItemSize];
		this.tokenToItemIdx = new HashMap<String,Integer>();
		for(int i=0; i< frequentItemSize; i++){
			this.frequentItems[i] = listOfFrequentTokens.get(i).getSecond();
			long labelCode = listOfFrequentTokens.get(i).getFirst();
			this.frequentItemsLabels[i] = String.valueOf(labelCode);
			this.tokenToItemIdx.put(frequentItemsLabels[i], i);
		}
	}
	
	private void loadSequenceInInverseIndex(File inputSequence) throws IOException{
		//Fase4: Loop again over file, this time make inverse index...
		//note: Using custom datastructure here, to conserve memory, and have fast 'array' index
		//much faster
		this.inverseIndex = new IndexStructure(this.frequentItems);
		FileStream fs = new FileStream(inputSequence,' ','\n');
		int position = 0;
		String token = fs.nextToken();
		while(token != null){
			Integer tokenIdx = tokenToItemIdx.get(labels.get(Integer.valueOf(token)));
			if(tokenIdx != null) {
				this.inverseIndex.push(tokenIdx, position);
			}
			else{//pruned... do nothing	
			}
			token = fs.nextToken();
			position++;
		}
	}
	
	private void loadSequenceInInverseIndexSparse(File inputSequenceSparse) throws IOException {
		this.inverseIndex = new IndexStructure(this.frequentItems);
		FileStream fs = new FileStream(inputSequenceSparse,' ','\n');
		String time = fs.nextToken();
		long firstTime = Long.valueOf(time);
		String label = fs.nextToken();
		while(label != null){
			label = label.trim();
			Integer tokenIdx = tokenToItemIdx.get(label);
			int position = (int)(Long.valueOf(time) - firstTime);
			if(tokenIdx != null) {
				this.inverseIndex.push(tokenIdx, position);
			}
			else{//pruned... do nothing	
			}
			time = fs.nextToken();
			label = fs.nextToken();
		}
	}

	
	private CountMap<Long> countItemsInSequence(File inputSequence) throws IOException{
		CountMap<Long> count_map = new CountMap<Long>(); //abstraction over TreeMap
		FileStream fs = new FileStream(inputSequence,' ','\n');
		String token = fs.nextToken();
		int size = 0;
		while(token !=null){
			count_map.add(Long.parseLong(token));
			size++;
			token = fs.nextToken();
		}
		System.out.println("Sequence size:" + size);
		return count_map;
	}
	
	private List<String> loadLabelList(File inputLabels) throws IOException{
		FileStream fs2 = new FileStream(inputLabels,' ','\n');
		List<String> labels = new ArrayList<String>();
		String label = fs2.nextToken();
		while(label != null){
			labels.add(label);
			label = fs2.nextToken();
		}
		
		return labels;
	}
	
	//debug/details about data
	private void debugLoadData() {
		if(!DEBUG_LOAD_DATA){
			return;
		}
		Runtime runtime = Runtime.getRuntime();
		System.out.println("Used Memory index:" + (runtime.totalMemory() - runtime.freeMemory()) / (1024*1024) + " Mb");
		System.out.println("Top 10:");
		for(int i=this.frequentItems.length-1; i> this.frequentItems.length-11 && i>-1; i--)
		{
			System.out.println("   " + this.frequentItemsLabels[i] + ":" + this.frequentItems[i]);
		}
		if(this.frequentItems.length > 10)
		{
			System.out.println("...");
			for(int i=10; i>-1; i--){
				System.out.println("   " + this.frequentItemsLabels[i] + ":" + this.frequentItems[i]);
			}
		}
		//debug load data
		System.out.println("ALL");
		for(int i=this.frequentItems.length-1; i>-1; i--){
			if(i % 100 == 0){
				System.out.println("item " + i + ": " + getLabel(i) + " count:" + (this.frequentItems[i]) + "");
			}
		}
	}

}
