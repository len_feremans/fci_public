package be.uantwerpen.pattern_mining.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Table structure, not that key does not have to be unique, 
 * e.g. (a,1),(a,2) is allowed
 * 
 * Internally list of pairs.
 * 
 * Used by @see be.uantwerpen.pattern_mining.ComputeSequentialPatterns to compute
 * cartesian product.
 * 
 * @author lfereman
 *
 */
public class Table {

	public static class Row extends ArrayList<Object>{	
		public Row(Object... objects){
			for(Object obj: objects){
				add(obj);
			}
		}
	}
	
	private List<String> columnNames = new ArrayList<>();
	private List<Row> rows = new ArrayList<>();

	public Table(String... columnNames){
		this.columnNames = Arrays.asList(columnNames);
	}
	
	public Table(List<String> columnNames){
		this.columnNames = columnNames;
	}
	
	public Row getRow(int i){
		return rows.get(i);
	}
	
	public List<Row> getRows(){
		return this.rows;
	}
	
	public void addRow(Row row){
		this.rows.add(row);
	}
	
	public List<String> getColumnNames(){
		return columnNames;
	}
	
	public static Table cartesianProduct(Table table1, Table table2){
		List<String> names = new ArrayList<>();
		names.addAll(table1.columnNames);
		names.addAll(table2.columnNames);
		Table product = new Table(names);
		for(Row row: table1.rows){
			for(Row otherRow: table2.rows){
				Row newRow = new Row();
				newRow.addAll(row);
				newRow.addAll(otherRow);
				product.rows.add(newRow);
			}
		}
		return product;
	}

	public static Table cartesianProduct(List<Table> tables){
		Table product = tables.get(0);
		for(int i=1; i< tables.size(); i++){
			product = cartesianProduct(product, tables.get(i));
		}
		return product;
	}
	
	public Table cartesianProduct(Table... otherTables){
		return cartesianProduct(Arrays.asList(otherTables));
	}
}
