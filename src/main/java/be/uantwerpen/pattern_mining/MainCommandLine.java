package be.uantwerpen.pattern_mining;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.ArrayUtils;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;

import be.uantwerpen.pattern_mining.data.MainDFSDatastructure;
import be.uantwerpen.pattern_mining.preprocessing.DatLabFormatConverter;
import be.uantwerpen.pattern_mining.preprocessing.PreprocessText;
import be.uantwerpen.util.FileStream;
import be.uantwerpen.util.Pair;
import be.uantwerpen.util.Utils;


/**
 * Author: Len Feremans
 *
 * Main command line class for mining Efficiently Mining Cohesion-based Patterns and Rules in Event Sequences.
 * 
 * Patterns are found based using the following constraints:
 * - Minimal frequency (or support)
 * - Minimal cohesion
 * - Maximum size of pattern
 * 
 * Not mentioned in Journal article: split parameter: 
 * - In addition we added and implementation a splitting/partioning mode, that allows to split the input sequence into X segments,
 * and we run FCI_seq on each segments. Next joining of "local" patterns in each segment is then done, by micro-averaging cohesion/occurence ratio/confidence 
 * of top-k patterns that re-occur in several subsequences.
 * 
 * For Parameters: See showUsage()
 */
public class MainCommandLine {

	private static boolean VERBOSE = false;
	public static void main(String[] args) throws Exception {
		try {
			Map<String,Object> arguments = parseArguments(args);
			if(arguments == null) {
				showUsage();
				return;
			}
			System.setProperty("java.awt.headless", "true");  //added this, otherwise issue on server with X11 server
			//get args
			File inputSequence = (File)arguments.get("-i");
			Optional<File> inputLabels = Optional.ofNullable((File)arguments.get("-l")); //null if sparse or preprocess is true
			Integer minsup = (Integer)arguments.get("-s");
			Integer maxsize = (Integer)arguments.get("-m");
			Double mincoh = (Double)arguments.get("-c");
			Optional<File> output = Optional.ofNullable((File)arguments.get("-o"));
			Optional<Double> min_or = Optional.ofNullable((Double)arguments.get("-seq"));
			Optional<Double> min_por = Optional.ofNullable((Double)arguments.get("-epi"));
			Optional<Double> conf = Optional.ofNullable((Double)arguments.get("-rul"));
			Optional<Boolean> sparse_input =  Optional.ofNullable((Boolean)arguments.get("-S"));
			Optional<Boolean> preprocess_input =  Optional.ofNullable((Boolean)arguments.get("-P"));
			Optional<Integer> split_input =  Optional.ofNullable((Integer)arguments.get("-split"));
			Optional<Boolean> verbose =  Optional.ofNullable((Boolean)arguments.get("-v"));
			//
			if(verbose.isPresent()) {
				if(verbose.get()) {
					MainDFSAlgorithm.DEBUG_DFS = true;
					MainDFSDatastructure.DEBUG_LOAD_DATA = true;
					VERBOSE=  true;
				}
			}
			//special input options
			if(preprocess_input.isPresent()) {
				Pair<File,File> filePair = preProcess(inputSequence);
				inputSequence = filePair.getFirst();
				inputLabels = Optional.of(filePair.getSecond());
			}
			//special case: split
			if(split_input.isPresent()) {
				runSplitMode(inputSequence, inputLabels, minsup, maxsize, mincoh, output, min_or, min_por, conf,
						sparse_input, split_input);
				return;
			}
			//run
			File outputFile = runMainAlgo(inputSequence, inputLabels, minsup, maxsize, mincoh, output, min_or, min_por,
					conf, sparse_input);
			//sort
			File outputFileSorted = sortOutput(min_or, min_por, conf, outputFile);
			Utils.printHead(outputFileSorted,1000);
		}catch(Exception e) {
			System.err.println("An error occurred. Details: " + e.getMessage());
			showUsage();
			e.printStackTrace();
		}
	}

	//len: ugly code, todo, more OO or something? less input/output to file..
	private static void runSplitMode(File inputSequence, Optional<File> inputLabels, 
			Integer minsup, Integer maxsize, Double mincoh, Optional<File> output, 
			Optional<Double> min_or, Optional<Double> min_por, Optional<Double> conf, 
			Optional<Boolean> sparse_input, Optional<Integer> split_input) throws IOException {
		List<File> inputSequenceParts = DatLabFormatConverter.split(inputSequence, split_input.get());
		List<File> outputs = new ArrayList<File>();
		for(File inputSequencePart: inputSequenceParts) {
			//run
			File outputI = new File("./temp/" + inputSequencePart.getName() + "_splitted_output.csv");
			runMainAlgo(inputSequencePart, inputLabels, minsup, maxsize, mincoh, Optional.of(outputI), min_or, min_por,
					conf, sparse_input);
			if(VERBOSE)
				Utils.printHead(outputI,5);
			outputs.add(outputI);
		}
		//join
		//1. clear output file
		File outputFile = output.isPresent()?output.get(): new File("./temp/" + inputSequence.getName() + "_splitted_output.csv");
		BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));
		writer.write("");
		writer.close();
		//read all csvs
		List<String[]> data = new ArrayList<>();
		String[] header = null;
		for(File outputI: outputs) {
			List<String> lines  = Utils.readFileUntil(outputI, Integer.MAX_VALUE);
			if(lines.size() == 0)
				continue;
			header = lines.get(0).split("\\s*;\\s*");
			for(int i=1; i<lines.size(); i++) {
				data.add(lines.get(i).split("\\s*;\\s*"));
			}
		}
		//2. normalize, e.g. itemsets-> sort items, or sequences/epi/rules are ok?
		if(ArrayUtils.contains(header, "itemset")) {
			int idx = ArrayUtils.indexOf(header,"itemset");
			for(String[] row: data) {
				List<String> items = Arrays.asList(row[idx].split(" "));
				Collections.sort(items);
				row[idx] = Utils.join(items, " ");
			}
		}
		//3. group by pattern, e.g. same item/seq patterns/rule/episode, compute total support, average cohesion, average occurrence ratio, etc.
		ListMultimap<String, Integer> pattern2support = ArrayListMultimap.create();
		ListMultimap<String, Double> pattern2cohesion = ArrayListMultimap.create();
		ListMultimap<String, Double> pattern2OccurenceRatio = ArrayListMultimap.create();//either confidence or occurence ratio
		ListMultimap<String, Double> pattern2confidence = ArrayListMultimap.create();
		ListMultimap<String, Integer> pattern2supportAntecedent = ArrayListMultimap.create();
		String patternType = "itemset";
		if(ArrayUtils.contains(header, "rule")) {
			patternType = "rule";
		}
		else if(ArrayUtils.contains(header, "sequential pattern")) {
			patternType = "sequential pattern";
		}
		else if(ArrayUtils.contains(header, "episode")) {
			patternType = "episode";
		}
		else if(ArrayUtils.contains(header, "rule")) {
			patternType = "rule";
		}
		int idxPattern = ArrayUtils.indexOf(header,patternType);
		int idxSupport = ArrayUtils.indexOf(header,"support");
		int idxCohesion = ArrayUtils.indexOf(header,"cohesion");
		int idxOccurenceRatio = ArrayUtils.indexOf(header,"occurence ratio");
		int idxConfidence = ArrayUtils.indexOf(header,"confidence");
		int idxAntecedentSupport = ArrayUtils.indexOf(header,"support antecedent");
		for(String[] row: data) {
			String pattern = row[idxPattern];
			Integer support = Integer.valueOf(row[idxSupport]);
			pattern2support.put(pattern, support);
			Double cohesion = Double.valueOf(row[idxCohesion]);
			pattern2cohesion.put(pattern, cohesion);
			Double occurrenceRatio =  idxOccurenceRatio!=-1?Double.valueOf(row[idxOccurenceRatio]): 0.0;
			pattern2OccurenceRatio.put(pattern, occurrenceRatio);
			Double confidence =  idxConfidence!=-1?Double.valueOf(row[idxConfidence]): 0.0;
			pattern2confidence.put(pattern, confidence);
			Integer supporAntecedent = idxAntecedentSupport!=-1?Integer.valueOf(row[idxAntecedentSupport]): 0;
			pattern2supportAntecedent.put(pattern,supporAntecedent);
		}	
		List<List<String>> groupedOutput = new ArrayList<>();
		for(String patternUnique: pattern2support.keySet()) {
			int totalSupport = pattern2support.get(patternUnique).stream().mapToInt(x->x).sum();
			int supportAntecedent = pattern2supportAntecedent.get(patternUnique).stream().mapToInt(x->x).sum();
			//macro average:
			//@SuppressWarnings("unused")
			//double cohesionAvg = pattern2cohesion.get(patternUnique).stream().mapToDouble(x->x).average().getAsDouble();
			//@SuppressWarnings("unused")
			//double occurrenceRatioAvg = pattern2OccurenceRatio.get(patternUnique).stream().mapToDouble(x->x).average().getAsDouble();
			//@SuppressWarnings("unused")
			//double confAvg = pattern2confidence.get(patternUnique).stream().mapToDouble(x->x).average().getAsDouble();
			//micro average is better... especially for cohesion, e.g. in chapter 10: window is 10, but 1 occurrence, in chapter 1: window is 5: but 50 occurrences...
			double cohesionMicroAvg = 0.0;
			for(int i=0; i< pattern2support.get(patternUnique).size(); i++) {
				int support = pattern2support.get(patternUnique).get(i);
				double cohesion = pattern2cohesion.get(patternUnique).get(i);
				cohesionMicroAvg += cohesion * support;
			}
			cohesionMicroAvg = cohesionMicroAvg / totalSupport;
			double ocurrenceRatioMicroAvg = 0.0;
			for(int i=0; i< pattern2support.get(patternUnique).size(); i++) {
				int support = pattern2support.get(patternUnique).get(i);
				double occurenceRatio = pattern2OccurenceRatio.get(patternUnique).get(i);
				ocurrenceRatioMicroAvg += occurenceRatio * support;
			}
			ocurrenceRatioMicroAvg = ocurrenceRatioMicroAvg / totalSupport;
			double confMicroAvg = 0.0;
			for(int i=0; i< pattern2support.get(patternUnique).size(); i++) {
				int support = pattern2support.get(patternUnique).get(i);
				double confidence = pattern2confidence.get(patternUnique).get(i);
				confMicroAvg += confidence * support;
			}
			confMicroAvg = confMicroAvg / totalSupport;
			
			
			List<String> vals = new ArrayList<>();
			for(String headerField: header) {
				if(headerField.equals(patternType)) {
					vals.add(patternUnique);
				}
				else if(headerField.equals("support")) {
					vals.add(String.valueOf(totalSupport));
				}
				else if(headerField.equals("cohesion")) {
					vals.add(String.format("%.6f", cohesionMicroAvg)); //changed: from avg -> microAvg
				}
				else if(headerField.equals("occurence ratio")) {
					vals.add(String.format("%.6f", ocurrenceRatioMicroAvg));
				}
				else if(headerField.equals("confidence")) {
					vals.add(String.format("%.3f",confMicroAvg));
				}
				else if(headerField.equals("support antecedent")) {
					vals.add(String.valueOf(supportAntecedent));
				}
				//else: throw away.. e.g. in mixed mode, throw away stuff
			}
			groupedOutput.add(vals);
		}
		//save
		writer = new BufferedWriter(new FileWriter(outputFile));
		writer.write(Utils.join(header, ";"));
		writer.write("\n");
		for(List<String> row: groupedOutput) {
			writer.write(Utils.join(row, ";"));
			writer.write("\n");
		}
		writer.close();
		//sort
		File outputFileSorted = sortOutput(min_or, min_por, conf, outputFile);
		Utils.printHead(outputFileSorted,1000);
		return;
	}

	private static File runMainAlgo(File inputSequence, Optional<File> inputLabels, Integer minsup, Integer maxsize,
			Double mincoh, Optional<File> output, Optional<Double> min_or, Optional<Double> min_por,
			Optional<Double> conf, Optional<Boolean> sparse_input) throws IOException {
		MainDFSAlgorithm mainDFS = null;
		if(sparse_input.isPresent()) {
			mainDFS = new MainDFSAlgorithm(inputSequence, minsup);
		}
		else {
			mainDFS = new MainDFSAlgorithm(inputLabels.get(), inputSequence,minsup);
		}
		//run algorithm
		File outputFile = mainDFS.minePatterns(maxsize, mincoh, min_or.isPresent(), min_por.isPresent(), conf.isPresent(), 
				min_or.isPresent()?min_or.get():0.0, min_por.isPresent()? min_por.get():0.0, conf.isPresent()? conf.get(): 0.0);
		//rename output
		if(output.isPresent()) {
			if(!output.get().getParentFile().exists()) {
				output.get().getParentFile().mkdirs();
			}
			boolean ok = outputFile.renameTo(output.get());
			if(ok) {
				outputFile = output.get();
				System.out.println("Saved output to " + output.get().getAbsolutePath());
			}
		}
		return outputFile;
	}

	private static File sortOutput(Optional<Double> min_or, Optional<Double> min_por, Optional<Double> conf, File outputFile) throws IOException {
		File outputFileSorted = outputFile;
		if(!min_or.isPresent() && !min_por.isPresent() && !conf.isPresent()) {
			outputFileSorted = Utils.sortCSVFile(outputFile, SortItemsets, true);
		}
		else if(min_or.isPresent() &&  !min_por.isPresent() && !conf.isPresent()) {
			outputFileSorted = Utils.sortCSVFile(outputFile, SortSequentialPatterns, true);
		}
		else if(min_por.isPresent() && !min_or.isPresent() && !conf.isPresent()) {
			outputFileSorted = Utils.sortCSVFile(outputFile, SortEpisodePatterns, true);
		}
		else if(!min_por.isPresent() && !min_or.isPresent() && conf.isPresent()) {
			outputFileSorted = Utils.sortCSVFile(outputFile, SortRules, true);
		}
		else {
			outputFileSorted = Utils.sortCSVFile(outputFile, SortItemsets, true);
		}
		if(!outputFileSorted.equals(outputFile)) {
			System.out.println("Saved sorted output to " + outputFileSorted.getAbsolutePath());
		}
		return outputFileSorted;
	}

	/***
	 * Pre-processing raw text file stemming/stopword removal etc, and convertion to Sequence/Label format
	 * */
	private static Pair<File,File> preProcess(File inputRaw) throws IOException {
		System.out.println("Preprocessing");
		FileStream fs = new FileStream(inputRaw, '\n');
		String line = fs.nextToken();
		File inputProcessed = new File(inputRaw.getParentFile(), inputRaw.getName() + "_processed.txt");
		FileWriter writer = new FileWriter(inputProcessed);
		while(line != null){
			line = PreprocessText.convertLine(line, true, true);
			if(!line.isEmpty()){
				writer.write(line);
				writer.write("\n");
			}
			line = fs.nextToken();
		}
		writer.close();
		System.out.println("Saved " + inputProcessed.getName());
		File inputProcessed_as_dat= new File(inputRaw.getParentFile(), inputRaw.getName() + "_processed.dat");
		File inputProcessed_as_lab= new File(inputRaw.getParentFile(), inputRaw.getName() + "_processed.lab");
		DatLabFormatConverter.convertNormal2DatLab(inputProcessed, inputProcessed_as_dat, inputProcessed_as_lab);
		return new Pair<File,File>(inputProcessed_as_dat,inputProcessed_as_lab);

	}

	private static Map<String,Object> parseArguments(String[] args) {
		if(ArrayUtils.contains(args, "-h")) {
			return null;
		}
		Map<String,String> options = arrToMap(new String[][]{
			{"-h", "<none>"}, {"-v", "<none>"},
			{"-s", "<int>"},{"-m", "<int>"},{"-c","<float>"}, 
			{"-i", "<file>"}, {"-l", "<file>"}, {"-o", "<file>"},
			{"-seq", "<float>"}, {"-epi", "<float>"}, {"-rul", "<float>"},
			{"-S", "<none>"}, {"-P", "<none>"}, {"-split", "<int>"}});
		Map<String,Object> values = new HashMap<String,Object>(); 
		for(int i=0; i<args.length; i++) {
			try {
				String argument = args[i];
				if(options.containsKey(argument)) {
					String type = options.get(argument);
					Object value = null;
					if(type.equals("<int>")) {
						value =  Integer.valueOf(args[i+1]);
						i+=1;
					}
					else if(type.equals("<float>")) {
						value =  Double.valueOf(args[i+1]);
						i+=1;
					}
					else if(type.equals("<bool>")) {
						value = Boolean.valueOf(args[i+1]);
						i+=1;
					}
					else if(type.equals("<file>")) {
						value = new File(args[i+1]);
						if(!((File)value).canRead() && !argument.equals("-o")) {
							throw new FileNotFoundException("Can not open " + args[i+1]);
						}
						i+=1;
					}
					else if(type.equals("<none>")) {
						value = Boolean.TRUE;
					}
					values.put(argument, value);
				}
				else {
					throw new IllegalArgumentException("Unknown option " + argument);
				}
			}catch(Exception e) {
				System.err.println("Unable to parse argument " + args[i] + ". Error: " + e.getClass() + ":" + e.getMessage());
				return null;	
			}
		}
		return values;
	}

	private static Map<String,String> arrToMap(String[][] arr){
		Map<String,String> map = new HashMap<>();
		for(String[] pair: arr) {
			map.put(pair[0],pair[1]);
		}
		return map;
	}

	public static void showUsage() {
		String usage = "java -jar FCI-seq-1.0.0.jar -s <min sup> -m <max size> -c <min cohesion> -i <input file sequence> -l <label file> -o <pattern output file>\n" +
				"    [-SPI] [-seq <occurence ratio sequences>] [-epi <occurence ratio episodes>] [-rul confidence threshold]\n" + 
				"    -h    print this help\n" +
				"    -v    verbose mode\n" +
				" Cohesive itemset miner:\n" + 
				"    -s     * <min sup> minimimal absolute support for each item \n" +
				"    -m     * <max size> maximum length of pattern\n" + 
				"    -c     * <min cohesion> minimum threshold on cohesion in [0,1], e.g. if 0.5 then on average at most as many gaps as there are items in pattern\n" + 
				"    -i     * <input file> single line sequence file of format <item1> <item2> ...., where each <item> is an integer x represented by a label on line x\n" +
				"    -l     <label file> multi line sequence of format <label1>\\n<label2>\\n..., where each <label> on line x (zero-based) is a string representing item x\n" + 
				"    -o     <pattern output file> filename of pattern output\n" +
				" Dominant sequential pattern/episode miner:\n" + 
				"    -seq   <minimal occurrence ratio> for sequential pattern mining in [0,1]\n"+
				"    -epi   <minimal occurrence ratio> for episode mining in [0,1]\n" + 
				" Association rules miner:\n" +
				"    -rul   <confidence threshold> in [0,1]\n" + 
				" Input file options r:\n" +
				"    -S     signals that input file sequence is in sparse format, that is like <timepoint1> <label1>\\n<timepoint2> <label2>...where <timepoint> is a long\n" +
				"    -P     signals that input file is a raw text file, and that it first pre-processsed using stemming/stop-word removal, and then convert to sequence/label format\n" +
				"    -split  <number of splits> splits sequences in to x pieces, and runs FCI on each, then joins resulting patterns. Usefull for find local patterns in long sequences.\n";
		System.err.println(usage);
	}

	//The patterns are first sorted on cohesion, then on support, and then alphabetically, if other attributes are equal. 
	//input like:
	//itemsets; support; cohesion
	//form speci ;2838;0.027783;
	public static Comparator<String[]> SortItemsets =  (String[] r1, String[] r2) -> {
		Double coh1, coh2;
		Integer sup1, sup2;
		String s1,s2;
		coh1 = Double.valueOf(r1[2].replaceAll(",", "."));
		coh2 = Double.valueOf(r2[2].replaceAll(",", "."));
		sup1 = Integer.valueOf(r1[1]);
		sup2 = Integer.valueOf(r2[1]);
		s1 = r1[0];
		s2 = r2[0];
		if(Math.abs(coh1 - coh2) > 0.0001) {
			return -1 * coh1.compareTo(coh2);
		}
		else if(sup1 - sup2 != 0) {
			return -1 * sup1.compareTo(sup2);
		}
		else {
			return s1.compareTo(s2);
		}
	};

	//The patterns are first sorted on cohesion, then on occurrence ratio, then on support, and then alphabetically, if other attributes are equal. 
	//input like:
	//sequential pattern;support;cohesion;occurence ratio sp;support sp
	//or episode;support;cohesion;occurence ratio G;support G\n"
	//<del,fuego>;14;1.000;1.000;
	//<tierra,del>;14;1.000;1.000;
	//where support is col[1] , cohesion is col[2] and occurence_ratio is col[3]
	public static Comparator<String[]> SortSequentialPatterns =  (String[] r1, String[] r2) -> {
		double delta = 0.0000001;
		Double coh1, coh2, occr1, occr2;
		Integer sup1, sup2;
		String s1,s2;
		coh1 = Double.valueOf(r1[2].replaceAll(",", "."));
		coh2 = Double.valueOf(r2[2].replaceAll(",", "."));
		occr1 = Double.valueOf(r1[3].replaceAll(",", "."));
		occr2 = Double.valueOf(r2[3].replaceAll(",", "."));
		sup1 = Integer.valueOf(r1[1]);
		sup2 = Integer.valueOf(r2[1]);
		s1 = r1[0];
		s2 = r2[0];
		if(Math.abs(coh1 - coh2) > delta) {
			return -1 * coh1.compareTo(coh2);
		}
		else if(Math.abs(occr1 - occr2) > delta) {
			return -1 * occr1.compareTo(occr2);
		}
		else if(sup1 - sup2 != 0) {
			return -1 * sup1.compareTo(sup2);
		}
		else {
			return s1.compareTo(s2);
		}
	};

	//The rules are first sorted on confidence, then on support of the antecedent, and then alphabetically, if other attributes are equal. 
	//input like:
	//rule;support;cohesion;confidence;support antecedent\n
	public static Comparator<String[]> SortRules =  (String[] r1, String[] r2) -> {
		Double conf1,conf2;
		Integer supA1, supA2;
		String s1,s2;
		conf1 = Double.valueOf(r1[3].replaceAll(",", "."));
		conf2 = Double.valueOf(r2[3].replaceAll(",", "."));
		supA1 = Integer.valueOf(r1[4]);
		supA2 = Integer.valueOf(r2[4]);
		s1 = r1[0];
		s2 = r2[0];
		if(Math.abs(conf1 - conf2) > 0.0001) {
			return -1 * conf1.compareTo(conf2);
		}
		else if(supA1 - supA2 != 0) {
			return -1 * supA1.compareTo(supA2);
		}
		else {
			return s1.compareTo(s2);
		}
	};


	//(copy/pasted from SortSequentialPattern)
	//input like: 
	//episode;support;cohesion;occurence ratio G;support G\n"
	//[natur->select, speci];2514;0.018;0.734;
	//[form, natur->select];1859;0.018;0.768;
	public static Comparator<String[]> SortEpisodePatterns =  SortSequentialPatterns;
}
