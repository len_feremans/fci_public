package be.uantwerpen.pattern_mining;

import java.util.ArrayList;
import java.util.List;

import be.uantwerpen.pattern_mining.data.MainDFSDatastructure;
import be.uantwerpen.util.Utils;

/**
 * Compute minimal windows for an itemset X. 
 * Internally uses MainDFSDatastructure.IndexStructure which is a two-dimensional array int[][]
 * 
 * @author lfereman
 *
 */
public class ComputeMinimalWindows {

	public static class Window{
		public int position; //position in sequence
		public int size;     //minimal window size 
		//public int[] occurence; //added for sequential patterns, e.g. if pattern is int x[]={1,2,3} then occurence will holds positions of [pos-of-1,pos-of-2, pos-of-3] in window
		
		public Window(int pos, int size, int[] occurence){
			this.position = pos; this.size = size; 
			//this.occurence = occurence;
		}
		public Window(int pos, int size){
			this.position = pos; this.size = size;
		}
		public String toString(){
			return String.format("Window[pos:%d,size:%d]", position , size);
		}
		public boolean samePosAndSize(Object obj) {
			return (this.position == ((Window)obj).position) && (this.size == ((Window)obj).size);
		}
	}

	private MainDFSDatastructure data;
	
	public ComputeMinimalWindows(MainDFSDatastructure data){
		this.data = data;
	}

	private List<Window> windows_final = new ArrayList<Window>(); 	
	public List<Window> getMinimalWindowsLastRun(){
		return this.windows_final;
	}
	
	public double sumMinimalWindows(int[] x){
		return sumMinimalWindows(x, Double.MAX_VALUE);
	}

	/**
	 * Loop over stream, from left to right...
	 * 'gather' most left window (== minimal window)
	 *
	 * If maxMinWindows>running sum -> return  maxMinWindows
	 * 
	 * Note: Expects Integers to be frequent
	 * @param x
	 * @return
	 */
	public double sumMinimalWindows(int[] x, double maxMinWindows)
	{
		if(x.length == 0){
			return 1;
		}
		if(x.length == 1){
			return data.getFrequency(x[0]); 
		}
		windows_final.clear();
		int position[][] = new int[x.length][]; //holds positions for each element, e.g. 'a': 1 3 10, 'b': 2, 4, 5
		int lastPosition[] = new int[x.length]; //holds lastPosition, e.g. 'a': 4, 'b':2 
		int startFrom[] = new int[x.length];	//holds next (not-visited) offset in positions e.g. 'a': 1, 'b': 1 (after lastPosition)
		List<Window> windows_current = new ArrayList<Window>();
		int frequency = 0;
		for(int i=0; i<x.length; i++){
			int ch = x[i];
			int positions_array[] = data.getPositions(ch);
			frequency += positions_array.length;
			position[i] = positions_array;
			startFrom[i] = 0;
			lastPosition[i] = Integer.MIN_VALUE;
		}
		int prev_min = Integer.MIN_VALUE;
		double running_sum = 0.0;
		for(int idx=0; idx<frequency;idx++){
			//get current position
			//When a new item comes in (line 14), we update the working variables, and compute the first and last position of the current window (line 17). 
			int pos = Integer.MAX_VALUE;
			for(int i=0; i<x.length; i++){
				if(startFrom[i] != position[i].length){
					pos = Math.min(pos, position[i][startFrom[i]]);
				}
			}
			//update lastPosition
			for(int i=0; i<x.length; i++){
				if(startFrom[i] != position[i].length && position[i][startFrom[i]] == pos){
					lastPosition[i] = pos;
					startFrom[i] = startFrom[i]+1;
				}
			}
			//compute window_size
			int min_pos = Integer.MAX_VALUE;
			int max_pos = Integer.MIN_VALUE;
			for(int lastPos: lastPosition){
				min_pos = Math.min(lastPos, min_pos);
				max_pos = Math.max(lastPos, max_pos);
			}
			int w = Integer.MAX_VALUE;
			if(min_pos != Integer.MIN_VALUE){ //at beginning, lastPos might be Integer.MIN_VALUE
				w = max_pos - min_pos + 1;
			}
			//If the smallest time stamp of the current window has changed, we go through the list of active windows and check whether a new shortest length has been found. 
			//If so, we update it (line 21). 
			//We then remove all windows for which we are certain that they cannot be improved from the list of active windows (line 23), 
			//and update the overall sum (line 24). Finally, we add the new window for the current time stamp to the list of active windows (line 25).
			//check condition:
			boolean condition = min_pos != Integer.MIN_VALUE && prev_min < min_pos; 
			if(condition){
				//update windows with minimal size
				List<Window> newWindows = new ArrayList<Window>(windows_current.size());
				for(Window window: windows_current){
					int new_width = max_pos - Math.min( window.position, min_pos) + 1;
					if(window.size >  new_width){
						window.size = new_width;
					}
					if( window.position < min_pos || window.size == x.length || window.size <= (max_pos -  window.position + 1)){
						running_sum +=window.size ;
						windows_final.add(window);
					}
					else{
						newWindows.add(window);
					}
				}
				windows_current = newWindows;
			}
			//add window, and update previous
			windows_current.add(new Window(pos, w, Utils.makeCopy(lastPosition)));
			prev_min = min_pos;
			//MAX_SUM:
			if(running_sum + (frequency - idx -1 + windows_current.size()) * x.length  > maxMinWindows){
				return Double.MAX_VALUE;
			}
		}
		//end
		windows_final.addAll(windows_current);
		for(Window window: windows_current){
			running_sum += window.size;
		}
		return running_sum;
	}

	
}
