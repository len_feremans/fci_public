package be.uantwerpen.pattern_mining;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import be.uantwerpen.pattern_mining.ComputeMinimalWindows.Window;
import be.uantwerpen.pattern_mining.ComputeMinimalWindowsSequence.SequenceWindow;
import be.uantwerpen.pattern_mining.ComputeRules.Rule;
import be.uantwerpen.pattern_mining.data.MainDFSDatastructure;
import be.uantwerpen.util.Pair;
import be.uantwerpen.util.Timer;
import be.uantwerpen.util.Utils;

/**
 * Created: 6-Jan-2016
 * Last update: 28-Jun-2018
 * Author: Len Feremans
 *
 * 
 * Find patterns in sequential (discretized) data. Based on paper
 * "Efficiently Mining Cohesion-based Patterns and Rules in Event Sequences"
 * by Boris Cule, and Len Feremans
 * 
 * Patterns are found based using the following contraints:
 * - Minimal frequency (or support), e.g. pattern "abc" if a,b and c occur > min_freq
 * - Minimal cohesion, e.g. pattern "abc" if cohesion > min_cohesion,
 * 	 where cohesion is computed as the average width of minimal window
 *   in which a,b and c occur, proportional to alphabet size. 
 *   For example if "aebec" then minimal window is 5, and cohesion is 3/5
 * - Maximum size of pattern
 * 
 */
public class MainDFSAlgorithm {
	public static boolean DEBUG_DFS = true;
	public static boolean PRUNE_UPPER_BOUND = true;
	public static boolean COUNT_NO_CANDIDATES = false;

	//constructor args
	private File inputLabels;
	private File inputSequence;

	//parameters
	private int minSupport;
	private boolean mineDominantSequences = false; //true or false
	private boolean mineDominantEpisodes = false; //true or false
	private boolean mineRules = false; 
	private double min_or = 0.0; //minimal occurrence ratio of dominant sequential patterns
	private double min_por = 0.0; //minimal occurrence ratio of dominant partial order
	private double min_conf = 0.0;

	//output options
	private boolean filterTotalOrderFromPartialOrders = false; //filter out episodes, where a total order exists, e.g. same as sequence
	private String outputType = "mixed";
	//loaded data
	//public for unit-test:
	public MainDFSDatastructure data;

	//services
	ComputeMinimalWindows computerMinimalWindows;
	public ComputeMinimalWindowsSequence computerMinimalWindowsSequence;
	ComputeSequentialPatterns computeSequences;
	ComputeEpisodes computeEpisodes;
	ComputeRules computeRules; 

	//set by class
	private File output;

	public MainDFSAlgorithm(File inputLabels, File inputSequence, int minSupport) throws IOException{
		this.inputLabels = inputLabels;
		this.inputSequence = inputSequence;
		this.minSupport = minSupport;
		//load data
		this.data = new MainDFSDatastructure(inputLabels, inputSequence, minSupport);
		this.computerMinimalWindows = new ComputeMinimalWindows(data);
		this.computerMinimalWindowsSequence = new ComputeMinimalWindowsSequence(data);
		this.computeSequences = new ComputeSequentialPatterns(data);
		this.computeEpisodes = new ComputeEpisodes(data);
		this.computeRules = new ComputeRules(data);
	}

	public MainDFSAlgorithm(File inputSequenceSparse, int minSupport) throws IOException{
		this.inputLabels = new File("no-labels-sparse.txt");
		this.inputSequence = inputSequenceSparse;
		this.minSupport = minSupport;
		//load data
		this.data = new MainDFSDatastructure(inputSequenceSparse, minSupport);
		this.computerMinimalWindows = new ComputeMinimalWindows(data);
		this.computerMinimalWindowsSequence = new ComputeMinimalWindowsSequence(data);
		this.computeSequences = new ComputeSequentialPatterns(data);
		this.computeEpisodes = new ComputeEpisodes(data);
		this.computeRules = new ComputeRules(data);
	}

	public MainDFSDatastructure getData(){
		return this.data;
	}

	public void setFilterTotalOrders(boolean filterTotalOrderFromPartialOrders) {
		this.filterTotalOrderFromPartialOrders = filterTotalOrderFromPartialOrders;
	}

	public File minePatterns(int maxLength, double minCohesion) throws IOException {
		this.mineDominantSequences = this.mineDominantEpisodes = this.mineRules = false;
		this.outputType = "itemsets";
		output = new File(String.format("./temp/mine_patterns_%s_minsup=%03d_mincoh=%03.4f_maxlen=%03d_type=%s.csv", 
				Utils.getFilenameNoExtension(this.inputLabels), this.minSupport, minCohesion,  maxLength, this.outputType));
		if(!output.getParentFile().exists())
			output.getParentFile().mkdirs();
		return minePatternsDFS(maxLength, minCohesion, new int[0]);
	}

	public File minePatterns(int maxLength, double minCohesion,  
			boolean mineDominantSequences, boolean mineDominantEpisodes, boolean mineRules, 
			double min_or, double min_por, double min_conf) throws IOException{
		this.mineDominantSequences = mineDominantSequences;
		this.mineDominantEpisodes = mineDominantEpisodes;
		this.mineRules = mineRules;
		this.min_or = min_or;
		this.min_por = min_por;
		this.min_conf = min_conf;
		this.outputType = "mixed";
		if(!mineDominantSequences && !mineDominantEpisodes && !mineRules) {
			this.outputType ="itemsets";
		}
		else if(mineDominantSequences && !mineDominantEpisodes && !mineRules) {
			this.outputType ="sequences";
		}
		else if(!mineDominantSequences && mineDominantEpisodes && !mineRules) {
			this.outputType ="episodes";
		}
		else if(!mineDominantSequences && !mineDominantEpisodes && mineRules) {
			this.outputType ="rules";
		}
		output = new File(String.format("./temp/mine_patterns_%s_minsup=%03d_mincoh=%03.4f_maxlen=%03d_type=%s.csv", 
				Utils.getFilenameNoExtension(this.inputLabels), this.minSupport, minCohesion,  maxLength, outputType));
		if(!output.getParentFile().exists())
			output.getParentFile().mkdirs();
		return minePatternsDFS(maxLength, minCohesion, new int[0]);
	}

	public File minePatternsDFS(int maxLength, double minCohesion, int[] pattern) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(output));
		Timer timer = new Timer(String.format("minePatterns(minsup=%d,mincoh=%.4f,maxlen=%s)",
				this.minSupport, minCohesion, maxLength));
		
		no_candidates =0;
		//Do depth first search
		//Note: as y decreases every time DFS goes deeper, just keep depth. Y is then sub-array starting at depth
		ArrayDeque<Pair<int[],Integer>> stack = new ArrayDeque<Pair<int[], Integer>>(); 
		stack.add(new Pair<int[],Integer>(pattern, 0));
		long noPatterns = 0;
		long visits = 0;
		long startTime = System.currentTimeMillis();
		long lastPrint = startTime;
		while(!stack.isEmpty()){
			//progress reporting:
			visits++;
			if(visits % 100 == 0){
				long elapsed =  System.currentTimeMillis() - lastPrint;
				if(elapsed > 1000 * 10){ //every 10 seconds
					printDebugDFS(stack, noPatterns, visits, startTime,elapsed);
					lastPrint = System.currentTimeMillis();
					writer.flush();
				}
			}
			//1. pop element 
			Pair<int[],Integer> pair = stack.removeFirst();
			int[] x = pair.getFirst();
			int depth = pair.getSecond();

			//2. prune using Cmax
			double cohesionPruningScore = -1;
			if(PRUNE_UPPER_BOUND){
				cohesionPruningScore = maxCohesionForPruning(x,depth, maxLength, minCohesion); //PERFORMANCE OK?
				updateCandidateCount(x);
				if(cohesionPruningScore  < minCohesion){
					continue;
				}
			}
			//3. check stop
			if(depth == data.getNumberOfItems())
			{
				if(x.length > 1){
					if (cohesionPruningScore == -1){ //can be true with lazy computation
						cohesionPruningScore = maxCohesionForPruning(x,depth, maxLength, minCohesion); //same as cohesion(X)
						if(cohesionPruningScore < minCohesion){
							continue;
						}
					}
					noPatterns++;
					String str = reportPatterns(x, cohesionPruningScore);
					if(!str.trim().isEmpty()) { //skip output, if no rules/sequences that conform to parameters...
						writer.write(str);
					}
					continue;
				}
				else
					continue;
			}
			//4 divide-and-conquor step
			int a = depth; //might seem strange, but y[0] = 0, and y[1] =1...
			//4.1 add element (and visit children)
			if(x.length + 1 <= maxLength){
				//visit X U {a}, Y \ {a}
				int x_new[] = new int[x.length+1];
				System.arraycopy(x, 0, x_new, 0, x.length);
				x_new[x.length] = a;
				stack.addFirst(new Pair<int[],Integer>(x_new,depth+1));
			}
			//4.2 go further without element
			//visit X, Y \ {a}
			stack.addFirst(new Pair<int[],Integer>(x,depth+1)); //x is immutable, so no copy is necessary!
		}
		timer.end();
		writer.close();
		System.out.println("No visits: " + visits);
		System.out.println("No patterns: " + noPatterns);
		System.out.println("Saved " + this.output.getName());
		return this.output;
	}

	private String header = null;
	
	private String reportPatterns(int[] x, double cohesionPruningScore) {
		int supportX= data.getFrequency(x);
		String patternReabable = toReadablePattern(x);
		//sequences
		List<SequenceWindow> sequenceWindows = null;
		ArrayList<Entry<String, Integer>> sequences = null;
		ArrayList<Entry<String, Integer>> filteredSequences = new ArrayList<>();
		if(mineDominantEpisodes || mineDominantSequences){ //compute sequences for sequence and partial order patterns
			computerMinimalWindowsSequence.sumMinimalWindowsForSequences(x);
			//compute minimal windows (and possibly new instances for each window)
			sequenceWindows = computerMinimalWindowsSequence.getMinimalSequenceWindowsLastRun();
			//computes "pruned" position list of each window
			computeSequences.computeWindowOccurenceFilteredPositionsListsForSequences(x, sequenceWindows);
			//mine sequences
			sequences = computeSequences.findSequentialPatterns(x, sequenceWindows);
			for(Entry<String, Integer> e: sequences){
				int supp = e.getValue();
				double occurenceRatioSeq = supp / (double) supportX;
				if(occurenceRatioSeq > min_or) {
					filteredSequences.add(e);
				}
			}
		}
		//episode
		Pair<Set<String>,Integer> episode = null; 
		if(mineDominantEpisodes){
			//compute "normal" position list of each window
			computeEpisodes.computeWindowOccurenceNormalPositionsLists(x, sequenceWindows);
			//mine dominant episode
			episode = computeEpisodes.findDominantPartialOrder(x, min_por, sequences, sequenceWindows);
		}
		//rules
		List<Rule> rules = null;
		if(mineRules) {
			rules = computeRules.findRulesFromItemSet(x, min_conf);
		}
		//output
		return savePatternToOutput(patternReabable, supportX, cohesionPruningScore, filteredSequences, episode, rules);
	}


	/**
	 * Compute cohesion upper bound:
	 * 
	 * C(x,y) = lenXY * (freqX + freqY) / (minWindows + lenXY * freqY)
	 * where lenXY = min(max_size, length(x + y))
	 * 		 b = frequency(x)
	 * 		 freqY = N_max(x,y) which is N(y) if length(x + y) <= max_size 
	 * 							  or (assuming Y elements are sorted on decreasing frequency)
	 * 								 N(substring(y,0,max_size - |X|)
	 * 		 minWindows = sum_minimal_windows(X)
	 * @param x
	 * @param y
	 * @return
	 */
	public double maxCohesionForPruning(int[] x, int depth, int maxLength, double minCohesion) {
		int ylen = data.getNumberOfItems() - depth;
		double lenXY = Math.min(maxLength, x.length + ylen);
		double freqX = data.getFrequency(x);
		double freqY = 0.0;
		//note: could also be pre-computed...
		for(int i=depth; i-depth<maxLength - x.length && i <data.getNumberOfItems(); i++){
			int ch = depth; //same as y[i]
			freqY += data.getFrequency(ch);
		}
		//VALUATE SUM LAZY
		double maxMinWindows = lenXY *  (freqX + freqY) / minCohesion - lenXY * freqY;
		double minWindows = computerMinimalWindows.sumMinimalWindows(x, maxMinWindows); 
		if(minWindows == Double.MAX_VALUE)
			return Double.MIN_VALUE;
		//cohesion:
		double maxCohesion = lenXY * (freqX + freqY) / (minWindows + lenXY * freqY);
		return maxCohesion;
	}


	public double cohesion(int[] pattern){
		double freqX = data.getFrequency(pattern);
		double sumW = computerMinimalWindows.sumMinimalWindows(pattern);
		double cohesion = pattern.length * freqX /sumW;
		return cohesion;
	}

	public int frequency(int[] pattern){
		return data.getFrequency(pattern);
	}

	public List<Window> getWindowsLastRun(){
		return computerMinimalWindows.getMinimalWindowsLastRun();
	}

	public File getOutput(){
		return this.output;
	}

	private String toReadablePattern(int[] x) {
		StringBuffer patternReabable = new StringBuffer();
		for(int i=0; i<x.length; i++){
			patternReabable.append(data.getLabel(x[i])).append(" ");
		}
		return patternReabable.toString();
	}


	private String savePatternToOutput(String patternReabable, int supportX, double cohesion, 
				ArrayList<Entry<String, Integer>> filteredSequences,
				Pair<Set<String>, Integer> episode, 
				List<Rule> rules) {
		if(this.outputType.equals("itemsets")) { 
			String line = String.format("%s;%d;%.6f\n", patternReabable, supportX, cohesion);
			if(header == null) {
				header = "itemset;support;cohesion\n";
				return header + line;
			}
			else {
				return line;
			}
		}
		if(this.outputType.equals("sequences")) {
			String lines = "";
			for(Entry<String, Integer> e: filteredSequences){
				int supp = e.getValue();
				double occurenceRatioSeq = supp / (double) supportX;
				lines += String.format("<%s>;%d;%.6f;%.3f\n", e.getKey(), supportX, cohesion, occurenceRatioSeq);
			}
			if(header == null) {
				header = "sequential pattern;support;cohesion;occurence ratio\n";
				return header + lines;
			}
			else {
				return lines;
			}
		}
		if(this.outputType.equals("episodes")) {
			Set<String> edges = episode.getFirst();
			int episodeSupport = episode.getSecond();
			if(edges.size() == 0) {
				return "";
			}
			if(this.filterTotalOrderFromPartialOrders && ComputeEpisodes.checkIsTotalOrder(String.valueOf(edges))){
				return "";
			}
			String line = String.format("%s;%d;%.6f;%.3f\n", edges, supportX, cohesion, episodeSupport / (double) supportX);
			if(header == null) {
				header = "episode;support;cohesion;occurence ratio\n";
				return header + line;
			}
			else {
				return line;
			}
		}
		if(this.outputType.equals("rules")){
			String lines = "";
			for(Rule rule: rules){
				String line = String.format("%s->%s;%d;%6f;%.3f;%d\n", rule.antecedent, rule.consequent, supportX, 
						cohesion, rule.conf, 
						frequency(toPattern(rule.antecedent.replace(",", " "))));
				lines += line;
			}
			if(header == null) {
				header = "rule;support;cohesion;confidence;support antecedent\n";
				return header + lines;
			}
			else {
				return lines;
			}
		}

		//combined
		if(this.outputType.equals("mixed")) {
			String itemStr;
			List<String> sequenceStrngs = new ArrayList<String>();
			String episodeStr = null;
			List<String> ruleStrngs =  new ArrayList<String>();
			//items
			itemStr = String.format("%s;%d;%.6f;", patternReabable, supportX, cohesion);
			String newHeader = "itemset;support;cohesion;";
			//sequences
			if(mineDominantSequences) { 
				for(Entry<String, Integer> e: filteredSequences){
					double occurenceRatioSeq = e.getValue() / (double) supportX;
					String line = String.format("<%s>;%.3f;", e.getKey(), occurenceRatioSeq);
					sequenceStrngs.add(line);
				}
				newHeader += "sequential pattern;occurrence ratio;";
			}
			//epsisode
			if(mineDominantEpisodes && episode.getFirst().size() !=0) {
				episodeStr = String.format("%s;%.3f;", episode.getFirst(), episode.getSecond() / (double) supportX);
				newHeader += "episode;occurence ratio G;";
			}
			//rules
			if(mineRules) {
				for(Rule rule: rules){
					String line = String.format("%s->%s;%.3f;", rule.antecedent, rule.consequent, rule.conf);
					ruleStrngs.add(line);
				}
				newHeader +="rule;confidence";
			}
			//collect and handle possible 0-to-many for sequences and rules
			String output = "";
			if(header == null) {
				if(newHeader.endsWith(";")) {
					newHeader = newHeader.substring(0,newHeader.length()-1);
				}
				output += newHeader + "\n";
				header = newHeader;
			}
			int rows = Math.max(Math.max(1, sequenceStrngs.size()), ruleStrngs.size());
			for(int i=0;i<rows;i++) {
				String line = itemStr;
				if(i < sequenceStrngs.size()) {
					line += sequenceStrngs.get(i);
				}
				else {
					line += ";;";
				}
				line += episodeStr!=null && i==0?episodeStr:";;";
				if(i < ruleStrngs.size()) {
					line += ruleStrngs.get(i);
				}
				else {
					line += ";;";
				}
				if(line.endsWith(";")) {
					line = line.substring(0,line.length()-1);
				}
				output += line + "\n";
			}
			return output;
		}
		else {
			throw new IllegalStateException("Error: should not get here");
		}
	}
	
	//for testing:
	//expected string with whitespace, e.g. "a b c", this is transformed to internal (integer) representation
	@SuppressWarnings("unused")
	public int[] toPattern(String s){
		String[] tokens = s.split(" ");
		int[] pattern = new int[tokens.length];
		for(int i=0;i<tokens.length;i++){
			String token = tokens[i];
			Integer value = data.getItemCode(token);
			if(value == null){
				throw new RuntimeException(String.format("%s not found", token));
			}
			pattern[i] = value;
		}
		return pattern;
	}
	
	//for debugging:
	private void printDebugDFS(ArrayDeque<Pair<int[], Integer>> stack,
			long noPatterns, long visits, long startTime, long elapsed) {
		if(!DEBUG_DFS)
			return;
		Runtime rt = Runtime.getRuntime();
		System.out.println("-------------------------------");
		System.out.print("Memory:" + (rt.totalMemory() - rt.freeMemory()) / (1024*1024) + " Mb ");
		System.out.print(" Elapsed: " + Utils.milisToStringReadable(elapsed));
		System.out.print(" Running for: " +  Utils.milisToStringReadable(System.currentTimeMillis() - startTime));
		System.out.print(" #Visits: " + visits);
		System.out.print(" #Patterns: " +  noPatterns);
		System.out.print(" Size stack: " +  stack.size());
		int firstItem = stack.getFirst().getFirst()[0];
		System.out.print("\nTop stack: ");
		int i=0;
		for(Pair<int[],Integer> pair: stack){
			if(i++>2){
				break;
			}
			System.out.print(toReadablePattern(pair.getFirst()));
			System.out.print(", ");
		}
		System.out.println();
		int[] x = stack.peek().getFirst();
		if(x.length > 0){
			System.out.println("Current element: " + x[0] + " of " + data.getNumberOfItems() + " (value=" + data.getLabel(x[0]) + ")");
		}
	}
	
	private long no_candidates = 0;
	private HashSet<Integer> visited = new HashSet<>();
	public long getNumberOfCandidates() {
		return no_candidates;
	}
	public int getNumberOfItems() {
		return data.getNumberOfItems();
	}
	public void updateCandidateCount(int[] candidate) {
		if(!COUNT_NO_CANDIDATES)
			return;
		int hashKey = java.util.Arrays.hashCode(candidate);
		if(!visited.contains(hashKey))
			no_candidates++;
		
	}
}
