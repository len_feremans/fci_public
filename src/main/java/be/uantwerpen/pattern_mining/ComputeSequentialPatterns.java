package be.uantwerpen.pattern_mining;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.lang3.ArrayUtils;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.math.BigIntegerMath;
import com.google.common.primitives.Ints;

import be.uantwerpen.pattern_mining.ComputeMinimalWindowsSequence.SequenceWindow;
import be.uantwerpen.pattern_mining.data.MainDFSDatastructure;
import be.uantwerpen.pattern_mining.data.Table;
import be.uantwerpen.pattern_mining.data.Table.Row;
import be.uantwerpen.util.CountMap;
import be.uantwerpen.util.Pair;
import be.uantwerpen.util.Utils;

/**
 * Compute dominance sequential patterns returns the set of all sequential patterns that occur within the minimal windows of underlying itemset X. 
 * 
 * In the main algorithm (see @see be.uantwerpen.pattern_mining.MainDFSAlgorithm) we only report sequences that occur dominantly, 
 * that is, we filter out sequential patterns where the occurrence ratio is lower than min_or. Remark that we have to deal
 * with multiple permutations within the same window caused by duplicate items. For example, given itemset {a, b, c, d} 
 * and a minimal window abcbd, both sequential patterns abcd and acbd occur within the same minimal window since b occurs more
 * than once. In general, our algorithm should discover any permutation of |X| elements. A naive approach would enumerate all |X|! 
 * candidate sequential patterns, however, we only generates candidate sequential patterns that occur in at least one window, 
 * making the method feasible even if |X| is large.
 * 
 * @author lfereman
 *
 */
public class ComputeSequentialPatterns {
	
	public static boolean DEBUG=false;
	
	private MainDFSDatastructure data;
	
	public ComputeSequentialPatterns(MainDFSDatastructure data){
		this.data = data;
	}
	/**
	 * Find Sequential Patterns starts by defining an empty multiset (be.uantwerpen.util.CountMap) which 
	 * is returned and updated with the discovered sequences in each window. The idea here is that we count each permutation that occurs at each
	 * occurrence, and return this multiset, for example returning {abcd : 5, acbd : 4}. The outer loop loops over all occurrences 
	 * of the itemset. 
	 **/
	public ArrayList<Entry<String, Integer>>  findSequentialPatterns(int[] x, List<SequenceWindow> windows){
		//We first create an empty set to count any distinct sequential pattern found within the current occurrence. 
		// Note that adding each sequential pattern directly to the multiset would result in potentially doubly counting a sequential pattern occurrence. 
		CountMap<String> countSequences = new CountMap<>();
		HashSet<String> sequences = new HashSet<String>();
		//Next, in the inner loop we loop over possibly multiple minimal window instances at each occurrence. In the inner loop we generate sequential patterns occurring
		//within each minimal window instance.
		for(SequenceWindow window: windows){
			//for each minimal window
			//		for each permutation
			//			count sequences distinctly, e.g. if 'aba' -> count ab and ba once
			for(int windowInstanceIdx=0; windowInstanceIdx < window.minimalWindowsOccurences.size(); windowInstanceIdx++){
				int[] minimalWindow = window.minimalWindowsOccurences.get(windowInstanceIdx);
				if(windowInstanceIdx > window.minimalWindowsOccurencesItemPositions.size()) {
					throw new RuntimeException("occurrences empty in findSequentialPatterns? Forgot to call computeWindowOccurenceFilteredPositionsListsForSequences?");
				}
				ListMultimap<Integer, Integer> itemPositions = window.minimalWindowsOccurencesItemPositions.get(windowInstanceIdx);
				String sequenceStr = getSequence(x, minimalWindow, data); //TODO: This is also not needed
				sequences.add(sequenceStr);
				//check if some items occur more then one within minimal window, is so generate different subsequences
				//e.g.: assuming   abcbd 
				// 		      a:0
				//			  b:1,3
				//		      c:2,4
				//			  d:5
				//generate:
				//fixed positions: a:0, d:5
				//then variable positions:
				//take cartisian product positions, e.g. bXc, thus [b:1,c:2],[b:1,c:4],...
				Table product = cartesianProduct(x, itemPositions);
				if(product.getRows().size() > BigIntegerMath.factorial(x.length).intValue()){
					System.err.format("Warning: carthesian product > n! |X|=%d, |X|!=%d, |cartesian-prod|=%d", 
							x.length, 
							BigIntegerMath.factorial(x.length).intValue(), 
							product.getRows().size());
				}
				for(Row row: product.getRows()){
					//TODO: This fragment of code is rather low-level.
					int[] occurence = new int[x.length];
					for(int i=0; i<row.size(); i++){
						occurence[i] = (Integer)row.get(i);
					}
					int[] sortedPositions = Utils.makeCopy(occurence);
					Arrays.sort(sortedPositions);
					String sequence = "";
					for(int i=0; i<x.length; i++){
						int positionI = sortedPositions[i];
						int originalIndex = ArrayUtils.indexOf(occurence, positionI);
						int originalLabel = Integer.valueOf(product.getColumnNames().get(originalIndex));
						sequence += data.getLabel(originalLabel) +",";
					}
					sequence = sequence.substring(0, sequence.length()-1);
					sequences.add(sequence);
				}
			}
			for(String sequence: sequences){
				countSequences.add(sequence);
			}
			sequences.clear();
		}
		ArrayList<Entry<String, Integer>> entries = new ArrayList<>(countSequences.getMap().entrySet());
		Collections.sort(entries, 
				(Entry<String,Integer> e1, Entry<String,Integer> e2) 
				-> e2.getValue().compareTo(e1.getValue()));
		return entries;
	}

	private String getSequence(int[] patternX, int[] windowOccurence, MainDFSDatastructure data){
		int[] sortedPositions = Utils.makeCopy(windowOccurence);
		Arrays.sort(sortedPositions);
		String sequence = "";
		for(int i=0; i<patternX.length; i++){
			int positionI = sortedPositions[i];
			int elementIdx = ArrayUtils.indexOf(windowOccurence, positionI);
			sequence += data.getLabel(patternX[elementIdx]) +",";
		}
		sequence = sequence.substring(0, sequence.length()-1);
		return sequence;
	}
	
	private Table cartesianProduct(int[] x, ListMultimap<Integer,Integer> positions)
	{
		List<Table> tables = new ArrayList<Table>();
		for(int item: x){
			List<Integer> positionsItem = positions.get(item);
			Table table = new Table("" + item);
			for(int pos: positionsItem){
				Row row = new Row(pos);
				table.addRow(row);
			}
			tables.add(table);
		}
		Table tableProduct = Table.cartesianProduct(tables);
		return tableProduct;
	}

	/**
	 * We first fetch the positions for each item of X within the window from the input sequence and retrieve a list of <i,t> positions. 
	 * We filter  out positions that are not relevant for generating candidate sequential patterns: For the boundary items at the minimal and maximal positions, 
	 * we know (by definition of a minimal window) that no other position of those items will result in a smaller window, thus each sequential pattern must
	 * start with the item found at the left boundary and end with the item on the right boundary. Therefore we can safely ignore any other positions of the boundary. 
	 **/
	public void computeWindowOccurenceFilteredPositionsListsForSequences(int[] x, List<SequenceWindow> windows){
		//e.g. windows are [0, [0-1]], [1, [0-1]], [23, [23-30], [17-24]]
		//for each window, loop over each occurence, then get all positions of each item within range
		//TODO: Option to make this more efficient
		for(SequenceWindow window: windows){
			for(int[] occurence: window.minimalWindowsOccurences){
				int minpos =Ints.min(occurence);
				int maxpos =Ints.max(occurence);
				ListMultimap<Integer, Integer> positions = ArrayListMultimap.create();
				for(int itemArrayOffset=0; itemArrayOffset<x.length; itemArrayOffset++){
					int itemCode = x[itemArrayOffset];
					int positions_array[] = data.getPositions(itemCode); //get sparse position list
					for(int sparseArrayOffset=0; sparseArrayOffset < positions_array.length; sparseArrayOffset++){
						int positionItem = positions_array[sparseArrayOffset];
						//first item: add only one position
						if(positionItem == minpos){
							positions.put(itemCode, positionItem);
							break;
						}
						if(positionItem >= minpos && positionItem <= maxpos){
							positions.put(itemCode, positionItem);
						}
						//last item: add only one position
						if(positionItem == maxpos){
							positions.removeAll(itemCode);
							positions.put(itemCode, positionItem);
							break;
						}
					}
				}
				//remove items that are next to each other e.g. (a,0),(b,1),(b,10),(c,20) -> remove (b,10)
				List<Pair<Integer,Integer>> positionsAny = new ArrayList<>();
				for(Integer item: positions.keys()){
					List<Integer> positionsItem = positions.get(item);
					for(Integer pos: positionsItem){
						positionsAny.add(new Pair<>(item,pos));
					}
				}
				Collections.sort(positionsAny, (Pair<Integer,Integer> itempos1, Pair<Integer,Integer> itempos2) -> itempos1.getSecond().compareTo(itempos2.getSecond()));
				ListMultimap<Integer, Integer> positionsNew = ArrayListMultimap.create();
				for(int i=1; i<positionsAny.size()-1; i++){
					Integer currentItem = positionsAny.get(i).getFirst();
					if(currentItem != positionsAny.get(i+1).getFirst()){
						positionsNew.put(currentItem, positionsAny.get(i).getSecond());
					}
				}
				//
				window.minimalWindowsOccurencesItemPositions.add(positionsNew);
			}
		}
	}
}
