package be.uantwerpen.pattern_mining;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.primitives.Ints;

import be.uantwerpen.pattern_mining.ComputeMinimalWindowsSequence.SequenceWindow;
import be.uantwerpen.pattern_mining.data.MainDFSDatastructure;
import be.uantwerpen.util.Pair;

/**
 * Find domiminant partial orders. 
 * 
 * We propose to discover exactly one episode for each frequent cohesive itemset, namely, the most specific episode (i.e., as many edges as possible) 
 * that describes a sufficient number of occurrences of the itemset itself, given a user-defined minimal occurrence ratio threshold min por. 
 * 
 * For example:
 * if sp_1=abc, then we have edges a->b, b->c and a->c (transitive closure)
 * if sp_2=bac, then we have edges b->a, a->c en b->c
 * Intersection is then b->c en a->c
 *  
 * @author lfereman
 *
 */
public class ComputeEpisodes {

	private MainDFSDatastructure data;

	public ComputeEpisodes(MainDFSDatastructure data){
		this.data = data;
	}

	/**
	 * @param x itemset
	 * @param relativeSupport relative support of 0.9 means: 90% of occurence are taken into account (min_por in paper)
	 * @param sequentialPatterns computed @see be.uantwerpen.pattern_mining.ComputeSequences
	 * @param windows computing @see be.uantwerpen.pattern_mining.ComputeMinimalWindowsSequence
	 * @return
	 */
	public Pair<Set<String>, Integer> findDominantPartialOrder(int[] x, 
					double relativeSupport, 
					List<Entry<String, Integer>> sequentialPatterns, 
					List<SequenceWindow> windows){
		//init
		int min_sup_G = (int)Math.ceil(data.getFrequency(x) * relativeSupport);
		int support_G = 0;
		Set<String> partialOrdersTopK = null;
		for(Entry<String,Integer> sequenceWithSupport: sequentialPatterns){
			//add transitive closure partial orders
			Set<String> partialOrders = new TreeSet<String>();
			String[] sequentialPattern = sequenceWithSupport.getKey().split(",");
			for(int j=0; j<sequentialPattern.length; j++){
				for(int k=j+1; k<sequentialPattern.length; k++){ //for abc add a->b, and a->c
					partialOrders.add(sequentialPattern[j] + "->" + sequentialPattern[k]);
				}
			}
			if(partialOrdersTopK == null)
				partialOrdersTopK = partialOrders;
			else{
				partialOrdersTopK.retainAll(partialOrders);
			}
			if(partialOrdersTopK.size() == 0) {
				break;
			}
			//compute correct support, e.g. (abc) en (acb) might occur at each window... thus we get occurence_ratio of 200% 
			support_G = computeSupport(x, windows, partialOrdersTopK);
			if(support_G > min_sup_G){
				break;
			}
		}
		//Remove redundant partial orders
		partialOrdersTopK = postProcessRemoveRedundantPartialOrders(partialOrdersTopK);
		//Add items not in any order
		partialOrdersTopK = postProcessAddMissingItems(x, partialOrdersTopK);
		//return
		Pair<Set<String>, Integer> result = new Pair<>(partialOrdersTopK, support_G);
		return result;
	}

	private int computeSupport(int[] x, List<SequenceWindow> windows, Set<String> partialOrdersAll) {
		int actual_support = 0;
		for(SequenceWindow window: windows){
			boolean coversWindow = false;
			for(int i=0; i< window.minimalWindowsOccurences.size(); i++){
				if(i > window.minimalWindowsOccurencesItemPositions.size()) {
					throw new RuntimeException("positions empty in ComputeEpisodes.computeSupport? Forgot to call computeWindowOccurenceNormalPositionsLists?");
				}
				if(window.minimalWindowsOccurencesItemPositions.get(i).size() < x.length){
					throw new RuntimeException("positions wrong size in ComputeEpisodes.computeSupport? Forgot to call computeWindowOccurenceNormalPositionsLists?");
				}
				ListMultimap<Integer, Integer> positions = window.minimalWindowsOccurencesItemPositions.get(i);
				//instanceCoversEpisode
				boolean coversInstance = true;
				for(String partialOrder: partialOrdersAll){
					String[] po = partialOrder.split("->");
					int itemBefore = data.getItemCode(po[0]);
					int itemAfter = data.getItemCode(po[1]);
					List<Integer> posBefore= positions.get(itemBefore);
					List<Integer> posAfter= positions.get(itemAfter);
					boolean anyBefore = false;
					for(int posi: posBefore){
						for(int posj: posAfter){
							if(posi<posj){
								anyBefore = true;
								break;
							}
						}
					}
					if(!anyBefore){
						coversInstance = false;
						break;
					}
				}
				if(coversInstance)
				{
					coversWindow = true;
					break;
				}
			}
			//if covers for one instance: this is true and support+1
			if(coversWindow){
				actual_support++;
			}
		}
		return actual_support;
	}

	//e.g. if a->b, b->c and a->c, prune a->c
	public Set<String> postProcessRemoveRedundantPartialOrders(Set<String> partialOrders){
		Set<String> notRedundantRules = new HashSet<String>();
		for(String rule: partialOrders){
			String[] po = rule.split("->");
			String lhs_rule =  po[0];
			Set<String> closureBefore = closure(lhs_rule, partialOrders);
			Set<String> newPartialOrders = new HashSet<String>(partialOrders);
			newPartialOrders.remove(rule);
			Set<String> closureAfter = closure(lhs_rule, newPartialOrders);
			if(closureBefore.size() > closureAfter.size()){
				notRedundantRules.add(rule);
			}
		}
		return notRedundantRules;
	}

	private Set<String> closure(String lhs, Set<String> partialOrders){
		Set<String> closure = new HashSet<String>();
		closure.add(lhs);
		while(true){
			boolean changes = false;
			for(String rule: partialOrders){
				String[] po = rule.split("->");
				String lhs_rule =  po[0];
				String rhs_rule = po[1];
				if(!closure.contains(rhs_rule) && closure.contains(lhs_rule)){
					closure.add(rhs_rule);
					changes = true;
				}
			}
			if(!changes){
				break;
			}
		}
		return closure;
	}

	public void computeWindowOccurenceNormalPositionsLists(int[] x, List<SequenceWindow> windows){
		//e.g. windows are [0, [0-1]], [1, [0-1]], [23, [23-30], [17-24]]
		//for each window, loop over each occurence, then get all positions of each item within range
		for(SequenceWindow window: windows){
			window.minimalWindowsOccurencesItemPositions.clear();
			for(int[] occurence: window.minimalWindowsOccurences){
				int minpos =Ints.min(occurence);
				int maxpos =Ints.max(occurence);
				ListMultimap<Integer, Integer> positions = ArrayListMultimap.create();
				for(int itemArrayOffset=0; itemArrayOffset<x.length; itemArrayOffset++){
					int itemCode = x[itemArrayOffset];
					int positions_array[] = data.getPositions(itemCode); //get sparse position list
					for(int sparseArrayOffset=0; sparseArrayOffset < positions_array.length; sparseArrayOffset++){
						int positionItem = positions_array[sparseArrayOffset];
						if(positionItem >= minpos && positionItem <= maxpos){
							positions.put(itemCode, positionItem);
						}
						if(positionItem > maxpos) {
							break;
						}
					}
				}
				window.minimalWindowsOccurencesItemPositions.add(positions);
			}
		}
	}

	private Set<String> postProcessAddMissingItems(int[] x, Set<String> partialOrdersBasis) {
		if(partialOrdersBasis.size() == 0){
			return partialOrdersBasis;
		}
		Set<String> labelsNotInPartialOrder = new TreeSet<String>();
		for(int i=0; i<x.length; i++){
			String label = data.getLabel(x[i]);
			boolean found = false;
			for(String partialOrder: partialOrdersBasis){
				String[] po = partialOrder.split("->");
				if(po[0].equals(label) || po[1].equals(label)){
					found = true;
					break;
				}
			}
			if(!found){
				labelsNotInPartialOrder.add(label);
			}
		}
		labelsNotInPartialOrder.addAll(partialOrdersBasis);
		return labelsNotInPartialOrder;
	}

	/**
	 * e.g. 	if episode [a->b] or [a->b, b->c] then this is a total order
	 * 
	 * @param episode
	 * @return
	 */
	public static boolean checkIsTotalOrder(String episode){
		//parse edges
		String edges[] = episode.substring(1, episode.length()-1).split("\\s*,\\s*");
		//if itemsets -> return false
		for(String edge: edges){
			if(!edge.contains("->"))
				return false;
		}
		if(edges.length == 1){
			return true;
		}

		//else check closure:
		Set<String> nodesSet = new HashSet<String>();
		for(String edge: edges){
			String[] nodesEdge = edge.split("->");
			nodesSet.add(nodesEdge[0]);
			nodesSet.add(nodesEdge[1]);
		}
		//for each node, compute transitive closure... if closure == #number-of-nodes -> Take that 
		for(String node: nodesSet){
			//compute closure
			List<String> order = new ArrayList<>();
			order.add(node);
			while(true){
				boolean update = false;
				for(String edge: edges){
					String[] nodesEdge = edge.split("->");
					if(order.contains(nodesEdge[0]) && !order.contains(nodesEdge[1])){
						order.add(nodesEdge[1]);
						update = true;
					}
				}
				if(!update)
					break;
			}
			//if closure == #number of nodes -> total order
			if(order.size() == nodesSet.size()){
				return true;
			}
		}
		return false;
	}
}
