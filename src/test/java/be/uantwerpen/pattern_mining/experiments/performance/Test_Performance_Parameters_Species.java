package be.uantwerpen.pattern_mining.experiments.performance;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.Test;

import be.uantwerpen.pattern_mining.MainDFSAlgorithm;
import be.uantwerpen.util.Timer;
import be.uantwerpen.util.Utils;

/**
 * Test performance of FCI_seq on Species while varying one parameter (cohesion/maxsize/support),
 * and keeping others fixed. 
 * 
 * For plots see python code plot_performance_parameters.py.
 *  
 * @author lfereman
 *
 */
public class Test_Performance_Parameters_Species {
	
	File inputProcessed_as_dat= new File("./data/origin_of_species_processed.dat");
	File inputProcessed_as_lab= new File("./data/origin_of_species_processed.lab");
	
	String dataset = "species";
	File ofile1 = new File("./temp/performance/performance_coh_" + dataset + ".csv");
	File ofile2 = new File("./temp/performance/performance_maxsize_" + dataset + ".csv");
	File ofile3 = new File("./temp/performance/performance_minsup_" + dataset + ".csv");

	@Test
	public void run_FCI_Varying_Cohesion_Performance() throws IOException{
		int maxlen = 4; 
		int minsup = 5;
		double[] cohesions = new double[]{1/10.0,1/20.0,1/30.0,1/40.0,1/50.0,1/60.0,1/70.0,1/80.0,1/90.0,1/100.0};
		MainDFSAlgorithm service = new MainDFSAlgorithm(inputProcessed_as_lab, inputProcessed_as_dat, minsup);
		if(!ofile1.getParentFile().exists())
			ofile1.getParentFile().mkdirs();
		FileWriter writer = new FileWriter(ofile1);
		for(double minCohesion: cohesions){
			Timer timer = new Timer("cohesion speed: " + minCohesion);
			service.minePatterns(maxlen, minCohesion);
			long elapsed = timer.end();
			long patternsFound = Utils.countLines(service.getOutput());
			writer.write(String.format("%s;%d;%d;%.3f;%d;%d\n", dataset, minsup, maxlen, minCohesion,  elapsed, patternsFound));
			writer.flush();
		}
		writer.close();
		System.out.println("run_FCI_Varying_Cohesion_Performance:");
		Utils.printHead(ofile1);
	}
	
	@Test
	public void run_FCI_Varying_Maxsize_Performance() throws IOException{
		double mincoh = 0.02; 
		int minsup = 350; 
		int[] maxlens = new int[]{2,4,8,16,32,Integer.MAX_VALUE}; 
		MainDFSAlgorithm service = new MainDFSAlgorithm(inputProcessed_as_lab, inputProcessed_as_dat, minsup);
		if(!ofile2.getParentFile().exists())
			ofile2.getParentFile().mkdirs();
		FileWriter writer = new FileWriter(ofile2);
		for(int maxlen: maxlens){
			Timer timer = new Timer("maxlen speed: " + maxlen);
			service.minePatterns(maxlen, mincoh);
			long elapsed = timer.end();
			long patternsFound = Utils.countLines(service.getOutput());
			writer.write(String.format("%s;%d;%d;%.3f;%d;%d\n", dataset, minsup, maxlen, mincoh,  elapsed, patternsFound));
			writer.flush();
		}
		writer.close();
		System.out.println("run_FCI_Varying_Maxsize_Performance:");
		Utils.printHead(ofile2);
	}
	
	@Test
	public void run_FCI_Varying_Support_Performance() throws IOException{
		double mincoh = 0.02;
		int maxlen = 5; //was 4 
		int supports[] = new int[]{567,144,36,16,10,5,3,2}; 
		if(!ofile3.getParentFile().exists())
			ofile3.getParentFile().mkdirs();
		FileWriter writer = new FileWriter(ofile3);
		for(int minsup: supports){
			MainDFSAlgorithm service = new MainDFSAlgorithm(inputProcessed_as_lab, 
															inputProcessed_as_dat, minsup);
			Timer timer = new Timer("minsup speed: " + minsup);
			service.minePatterns(maxlen, mincoh);
			long elapsed = timer.end();
			long patternsFound = Utils.countLines(service.getOutput());
			writer.write(String.format("%s;%d;%d;%.3f;%d;%d\n", dataset, minsup, maxlen, mincoh,  elapsed, patternsFound));
			writer.flush();
		}
		writer.close();
		System.out.println("run_FCI_Varying_Support_Performance:");
		Utils.printHead(ofile3);
	}
}

