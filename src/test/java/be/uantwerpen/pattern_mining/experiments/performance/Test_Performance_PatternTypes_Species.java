package be.uantwerpen.pattern_mining.experiments.performance;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.Test;

import be.uantwerpen.pattern_mining.MainDFSAlgorithm;
import be.uantwerpen.util.Timer;
import be.uantwerpen.util.Utils;

/**
 * Test performance of FCI_seq on Species for different modes:
 * 1) only cohesive itemsets
 * 2) itemsets + dominant sequential patterns
 * 3) itemsets + dominant episode
 * 4) itemsets + rules
 * 
 * For plots see python code plot_performance_patterntypes.py.
 *  
 * @author lfereman
 *
 */
public class Test_Performance_PatternTypes_Species {
	
	File inputProcessed_as_dat= new File("./data/origin_of_species_processed.dat");
	File inputProcessed_as_lab= new File("./data/origin_of_species_processed.lab");
	
	String dataset = "species";
	File ofile5 = new File("./temp/performance/performance_patterns_" + dataset + ".csv");
	
	@Test
	public void run_FCI_Varying_Pattern_Types_Performance() throws IOException, InterruptedException{
		int maxlen = 5; 
		int minsup = 5; //was 4 in paper
		double min_coh = 0.015;
		if(!ofile5.getParentFile().exists())
			ofile5.getParentFile().mkdirs();
		FileWriter writer = new FileWriter(ofile5);
		//itemsets
		Timer timer = new Timer("classic");
		MainDFSAlgorithm service = new MainDFSAlgorithm(inputProcessed_as_lab, inputProcessed_as_dat, minsup);
		service.minePatterns(maxlen, min_coh);
		long elapsed = timer.end();
		long patternsFound = Utils.countLines(service.getOutput());
		writer.write(String.format("itemsets;%s;%d;%d;%.3f;%d;%d\n", dataset, minsup, maxlen, min_coh,  elapsed, patternsFound));
		writer.flush();
		//sequences
		System.gc();
		Thread.sleep(1000);
		timer = new Timer("sequences");
		service = new MainDFSAlgorithm(inputProcessed_as_lab, inputProcessed_as_dat, minsup);
		service.minePatterns(maxlen, min_coh, true, false, false, 0.5, 0.0, 0.0);
		elapsed = timer.end();
		patternsFound = Utils.countLines(service.getOutput());
		writer.write(String.format("sequences;%s;%d;%d;%.3f;%d;%d\n", dataset, minsup, maxlen, min_coh,  elapsed, patternsFound));
		writer.flush();
		//episodes
		System.gc();
		Thread.sleep(1000);
		timer = new Timer("episodes");
		service = new MainDFSAlgorithm(inputProcessed_as_lab, inputProcessed_as_dat, minsup);
		service.setFilterTotalOrders(true);
		service.minePatterns(maxlen, min_coh, false, true, false, 0.0, 0.5, 0.0);
		elapsed = timer.end();
		patternsFound = Utils.countLines(service.getOutput());
		writer.write(String.format("episodes;%s;%d;%d;%.3f;%d;%d\n", dataset, minsup, maxlen, min_coh,  elapsed, patternsFound));
		writer.flush();
		//rules
		System.gc();
		Thread.sleep(1000);
		timer = new Timer("rules");
		service = new MainDFSAlgorithm(inputProcessed_as_lab, inputProcessed_as_dat, minsup);
		service.minePatterns(maxlen, min_coh, false, false, true, 0.0, 0.0, 0.7);
		elapsed = timer.end();
		patternsFound = Utils.countLines(service.getOutput());
		writer.write(String.format("rules;%s;%d;%d;%.3f;%d;%d\n", dataset, minsup, maxlen, min_coh,  elapsed, patternsFound));
		writer.flush();
		//
		writer.close();
		System.out.println("run_FCI_Varying_Pattern_Types_Performance:");
		Utils.printHead(ofile5);
	}
}

