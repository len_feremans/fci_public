package be.uantwerpen.pattern_mining.experiments.performance;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.Test;

import be.uantwerpen.pattern_mining.MainDFSAlgorithm;
import be.uantwerpen.util.Utils;
import org.apache.commons.math3.util.CombinatoricsUtils;

public class Test_Performance_Pruning {
	
	File inputProcessed_as_dat= new File("./data/origin_of_species_processed.dat");
	File inputProcessed_as_lab= new File("./data/origin_of_species_processed.lab");
	
	String dataset = "species";
	File ofile1 = new File("./temp/performance/performance_pruning_" + dataset + ".csv");
	
	@Test
	public void run_FCI_Varying_Maxsize_Pruning_Species() throws IOException{
		double mincoh = 0.5; 
		int minsup = 5; 
		int[] maxlens = new int[]{2,8,16,24,32,40, 48}; 
		MainDFSAlgorithm.COUNT_NO_CANDIDATES = true;
		MainDFSAlgorithm service = new MainDFSAlgorithm(inputProcessed_as_lab, inputProcessed_as_dat, minsup);
		if(!ofile1.getParentFile().exists())
			ofile1.getParentFile().mkdirs();
		FileWriter writer = new FileWriter(ofile1);
		for(int maxlen: maxlens){
			service.minePatterns(maxlen, mincoh);
			long numberOfCandidates = service.getNumberOfCandidates();
			int numberOfItems = service.getNumberOfItems();
			System.out.println(numberOfItems);
			double numberOfCandidatesTheoretic = 0;
			for(int i=2; i<=maxlen; i++) {
				numberOfCandidatesTheoretic += CombinatoricsUtils.binomialCoefficientDouble(numberOfItems, Math.min(i,numberOfItems));
			}
			String s = String.format("%s;%d;%d;%.3f;%d;%.1f;%s;%s\n", dataset, minsup, maxlen, mincoh, numberOfCandidates, numberOfCandidatesTheoretic,
							prettyPrint(numberOfCandidates), prettyPrint(numberOfCandidatesTheoretic));
			System.out.print(s);
			writer.write(s);
			writer.flush();
		}
		writer.close();
		System.out.println("run_FCI_Varying_Maxsize_Performance:");
		Utils.printHead(ofile1);
	}
	
	//pretty print:
	private String prettyPrint(double x) {
		int pow10 = 0;
		while(x > 10) {
			x = x / 10;
			pow10 +=1;
		}
		return String.format("%.3f*10^%d", x, pow10);
	}
}
