#Code, originally created in jupyter to plot performance of pattern types

#jupyter code:
series = ['itemsets','+ sequential\n patterns','+ episodes','+ rules']

#itemsets species/trump
itemsets_runtimes = [440341/1000.0, 231736/1000.0,]

#trump
runtimes2 = [394449, 529512, 250306] 
runtimes2 = [r/1000.0 -itemsets_runtimes[1]  for r in runtimes2]

#itemsets;trump;4;5;0.020;231736;16393
#sequences;trump;4;5;0.020;394449;16393
#episodes;trump;4;5;0.020;529512;12009
#rules;trump;4;5;0.020;250306;30


#species
#runtimes = [1111164, 1104930, 827302] #1083065
runtimes = [570592, 616762, 486879] #1083065
runtimes = [r/1000.0  -itemsets_runtimes[0] for r in runtimes]

#itemsets;species;4;5;0.015;440341;1339
#sequences;species;4;5;0.015;570592;1339
#episodes;species;4;5;0.015;616762;892
#rules;species;4;5;0.015;486879;25

#save
import numpy as np
import matplotlib.pyplot as plt
n_groups = 2
fig, ax = plt.subplots()
index = np.arange(n_groups)
bar_width = 0.3
opacity = 0.8

gap_series = 3

rects1 = plt.bar([0, 0.5, 1, 3, 3.5, 4],
                 [itemsets_runtimes[0], itemsets_runtimes[0], itemsets_runtimes[0], itemsets_runtimes[1], itemsets_runtimes[1], itemsets_runtimes[1]],
                 bar_width,
                 alpha=1.0,
                 color = (173.0/255.0, 202.0/255.0, 211.0/255.0),
                 label=series[0])

for idx, serie in enumerate(series[1:]):
    rects1 = plt.bar(index * gap_series + idx/2, [runtimes[idx],runtimes2[idx]], 
                     bar_width,
                     alpha=opacity,
                     bottom=itemsets_runtimes,
                     label=serie)
plt.xlabel('Dataset')
plt.ylabel('Runtime (s)')
plt.title('Runtime per pattern type')
plt.xticks(index * gap_series + 1, ('Species','Trump'))
plt.legend(loc='lower center') 
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)

plt.tight_layout()
plt.show()
fig.savefig('./performance_patterns.eps' , dpi=100)    
    