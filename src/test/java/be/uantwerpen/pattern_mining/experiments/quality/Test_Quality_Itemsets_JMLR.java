package be.uantwerpen.pattern_mining.experiments.quality;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import be.uantwerpen.pattern_mining.MainCommandLine;
import be.uantwerpen.pattern_mining.MainDFSAlgorithm;
import be.uantwerpen.pattern_mining.other.WrapperCompactMinimalWindows;
import be.uantwerpen.pattern_mining.other.WrapperClosedEpisodesMining;
import be.uantwerpen.pattern_mining.preprocessing.DatLabFormatConverter;
import be.uantwerpen.pattern_mining.preprocessing.PreprocessText;
import be.uantwerpen.util.CommandLineUtils;
import be.uantwerpen.util.FileStream;
import be.uantwerpen.util.Utils;
import static be.uantwerpen.pattern_mining.other.WrapperClosedEpisodesMining.*;

/** 
 * Preproces and mine itemsets using CMW, to validate same results on JMLR as published by Tatti.
 * Not published.
 *
 **/
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Test_Quality_Itemsets_JMLR {

	File inputRaw = new File("./data/JMLR_skopus.txt");
	File inputProcessed = new File("./data/my_JMLR_processed.txt");
	
	//in lab/dat format
	File inputProcessed_as_dat= new File("./data/my_JMLR_single.txt.seq");
	File inputProcessed_as_lab= new File("./data/my_JMLR_single.txt.lab");

	@Test
	public void A_preProcess() throws Exception{
		boolean stem = true ,stopwords = true;
		FileStream fs = new FileStream(inputRaw, '\n');
		String line = fs.nextToken();
		FileWriter writer = new FileWriter(inputProcessed);
		while(line != null){
			line = PreprocessText.convertLine(line, stem, stopwords);
			if(!line.isEmpty()){
				writer.write(line);
				writer.write("\n");
			}
			line = fs.nextToken();
		}
		writer.close();
		//make lab files
		DatLabFormatConverter.convertNormal2DatLab(inputProcessed,inputProcessed_as_dat, inputProcessed_as_lab);
		DatLabFormatConverter.convertDatLab2Normal(inputProcessed_as_dat, inputProcessed_as_lab, new File("./temp/test_origin_convertion.txt"));
		//sanity check:
		Utils.printHead(inputRaw);
		Utils.printHead(inputProcessed);
		Utils.printHead(inputProcessed_as_dat);
		Utils.printHead(inputProcessed_as_lab);
		Utils.printHead(new File("./temp/test_origin_convertion.txt"));
	}
	
	@Test
	public void B_runFCI_Seq() throws IOException{
		double minCohesion = 0.03;
		int support = 7; 
		int maxlen = 5;
		MainDFSAlgorithm service = new MainDFSAlgorithm(inputProcessed_as_lab, inputProcessed_as_dat, support);
		File output = service.minePatterns(maxlen, minCohesion);
		File sorted = Utils.sortCSVFile(output, MainCommandLine.SortItemsets, true);
		Utils.printHead(sorted);
	}
	
	double minSupport = 15;
	int windowSize = 20;
	
	@Test
	public void C_runClosepi() throws Exception{
		File output = WrapperClosedEpisodesMining.runMethod(inputProcessed_as_dat,inputProcessed_as_lab, minSupport, windowSize, 
				MIN_WINDOWS_ON, WEIGHTED_WINDOWS_OFF, ITEMSETS_ON, SP_OFF, GENERAL_EPI_OFF);
		Utils.printHead(output);
	}
	
	/*
	@Test
	public void C2_runClosepi() throws Exception{
		File output = new File("./temp/episodes_closepi_jmlr.txt");
		String CLOSEPI_PATH = "./other_tools/closedepisodeminer/closepi";
		CommandLineUtils.runCommand(new String[] {CLOSEPI_PATH ,
				"-t", Double.toString(20), 
				"-w", Integer.toString(40), 
				"-x", 
				"-f", 
				"-i", inputProcessed_as_dat.getAbsolutePath(),
				"-o", output.getAbsolutePath()});
		}
	*/
	
	
	@Test
	public void D_runCMW() {
		//CMW
		File laxmanRaw = WrapperClosedEpisodesMining.getOutputClosepiRawOutput(inputProcessed_as_dat, minSupport, windowSize, 
				MIN_WINDOWS_ON, WEIGHTED_WINDOWS_OFF, ITEMSETS_ON, SP_OFF, GENERAL_EPI_OFF);
		//File laxmanRaw = new File("./temp/episodes_closepi_jmlr.txt");
		double alpha = 0.5;
		WrapperCompactMinimalWindows.runCMW(inputProcessed_as_dat, inputProcessed_as_lab, laxmanRaw, alpha, ITEMSETS_ON, SP_OFF, GENERAL_EPI_OFF);
	}
	
}
