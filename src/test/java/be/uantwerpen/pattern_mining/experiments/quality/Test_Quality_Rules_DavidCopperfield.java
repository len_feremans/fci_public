package be.uantwerpen.pattern_mining.experiments.quality;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import be.uantwerpen.pattern_mining.MainDFSAlgorithm;
import be.uantwerpen.pattern_mining.preprocessing.DatLabFormatConverter;
import be.uantwerpen.pattern_mining.preprocessing.PreprocessText;
import be.uantwerpen.util.FileStream;
import be.uantwerpen.util.Utils;

/** 
 * Preproces and mine rules using our FCI_seq on David Copperield, as letter stream, in three languages
 *
 **/
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Test_Quality_Rules_DavidCopperfield {

	File[] files = new File[]{
			new File("./data/dutch_david_copperfield.txt"),
			new File("./data/english_david_copperfield.txt"),
			new File("./data/french_david_copperfield.txt")};
	
	@Test
	public void A_preProcessDatasets() throws Exception{	
		int maxSizeSequence = 500000;
		for(File input: files){
			File processed = Utils.getFileWithDifferentExtension(input,"_processed.txt");
			preProcessNotStemmingOrStopwordsLettersSeperate(input,processed);
			File lab = Utils.getFileWithDifferentExtension(input,"_processed.lab");
			File dat = Utils.getFileWithDifferentExtension(input,"_processed.dat");
			DatLabFormatConverter.convertNormal2DatLab(processed,dat, lab);
			DatLabFormatConverter.sampleSequenceFile(dat, dat, maxSizeSequence);
			//convert back, for testing:
			File back_to_txt = new File("./temp/test_convertion.txt");
			DatLabFormatConverter.convertDatLab2Normal(dat, lab, back_to_txt);
			//sanity check:
			Utils.printHead(input);
			Utils.printHead(processed);
			Utils.printHead(dat);
			Utils.printHead(lab);
			Utils.printHead(back_to_txt);
		}
	}

	@Test
	public void B_runFCI_rules() throws IOException{ 
		double minCohesion = 0.00;
		int maxlen = 4;
		int minsup = 0;
		double min_conf = 0.1;
		for(File input: files){
			File lab = Utils.getFileWithDifferentExtension(input,"_processed.lab");
			File dat = Utils.getFileWithDifferentExtension(input,"_processed.dat");
			//MainDFSAlgorithm.PRUNE_UPPER_BOUND = false;
			MainDFSAlgorithm service = new MainDFSAlgorithm(lab, dat, minsup);
			File output  = service.minePatterns(maxlen, minCohesion, false, false, true, 0.0, 0.0, min_conf);
			File outputSorted = Utils.sortCSVFile(output, 1, true);
			Utils.printHead(outputSorted);
		}
	}
	
	private void preProcessNotStemmingOrStopwordsLettersSeperate(File input, File output) throws Exception{
		boolean stem = false ,stopwords = false;
		FileStream fs = new FileStream(input, '\n');
		String line = fs.nextToken();
		FileWriter writer = new FileWriter(output);
		while(line != null){
			line = PreprocessText.convertLine(line, stem, stopwords);
			if(!line.isEmpty()){
				String[] tokens = line.split("[^\\w+|\\d+|\\d\\.\\d+]");
				StringBuffer buff = new StringBuffer();
				for(String token: tokens){
					if(token.trim().isEmpty())
						continue;
					for(char ch: token.toCharArray()){
						buff.append(ch).append(" ");
					}
					buff.append(" _ ");
				}
				writer.write(buff.toString());
				writer.write("\n");
			}
			line = fs.nextToken();
		}
		writer.close();
	}

}
