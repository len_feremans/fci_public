package be.uantwerpen.pattern_mining.experiments.quality;

import java.io.File;
import java.io.IOException;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import be.uantwerpen.pattern_mining.MainCommandLine;
import be.uantwerpen.pattern_mining.MainDFSAlgorithm;
import be.uantwerpen.pattern_mining.other.WrapperClosedEpisodesMining;
import be.uantwerpen.util.Utils;

/**
 *  Mine rules using our FCI_seq, Winepi, Marbles and Marbles_w on Origin of Species
 * @author lfereman
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Test_Quality_Rules_Species {
	
	File inputProcessed_as_dat= new File("./data/origin_of_species_processed.dat");
	File inputProcessed_as_lab= new File("./data/origin_of_species_processed.lab");

	@Test
	public void A_run_FCI_rules() throws IOException{ 
		int maxlen= 5;
		int minsup = 5;
		double mincoh = 0.015; 
		double min_conf = 0.02;
		MainDFSAlgorithm service = new MainDFSAlgorithm(inputProcessed_as_lab,inputProcessed_as_dat, minsup);
		File output = service.minePatterns(maxlen, mincoh, false, false, true, 0.0, 0.0, min_conf);
		File outputSorted = Utils.sortCSVFile(output, MainCommandLine.SortRules, true); 
		Utils.printHead(outputSorted,100);
		//Took 9.5 min
	}
	
	@Test
	public void B_run_Closepi_rules() throws Exception{ 
		//WINEPI
		boolean weighted = false;
		boolean minWindow = false;
		int windowSize=  15;
		int minSupport = 40; //otherwise takes to long
		double min_conf = 0.7;
		File output = WrapperClosedEpisodesMining.runRules(inputProcessed_as_dat,inputProcessed_as_lab, minSupport, windowSize, minWindow, weighted, min_conf);
		Utils.printHead(output);
		// Took 2 hours + 450MB output... best run command-line
	
		//LAXMAN
		weighted = false;
		minWindow = true;
		minSupport = 10; //otherwise takes to long
		output = WrapperClosedEpisodesMining.runRules(inputProcessed_as_dat,inputProcessed_as_lab, minSupport, windowSize, minWindow, weighted, min_conf);
		Utils.printHead(output); 
		
		//MARBLES
		minWindow = false;
		weighted = true;
		minSupport = 1; 
		output = WrapperClosedEpisodesMining.runRules(inputProcessed_as_dat,inputProcessed_as_lab, minSupport, windowSize, minWindow, weighted, min_conf);
		Utils.printHead(output);	
	}
	
	@Test
	public void postProcessWinepiOutput() throws IOException {
		int windowSize=  15;
		double minSupport = 40; 
		double min_conf = 0.7;
		String modus="regular";
		File outputClosepi = new File("./temp/closepi_output_rules.txt");
		File outputReadable = new File(String.format("./temp/closepi_%s_minSup=%03.3f_window=%03d_conf=%.3f_modus=%s__rules.csv", 
				Utils.getFilenameNoExtension(inputProcessed_as_dat), minSupport, windowSize, min_conf, modus));
		WrapperClosedEpisodesMining.postProcessClosepiRules(inputProcessed_as_lab,outputClosepi,outputReadable,min_conf);
 	}
	
	@Test
	public void postProcessWinepiOutput2() throws IOException {
		int windowSize=  15;
		double minSupport = 10; 
		double min_conf = 0.7;
		String modus="min_windows";
		File outputClosepi = new File("./temp/closepi_output_rules.txt");
		File outputReadable = new File(String.format("./temp/closepi_%s_minSup=%03.3f_window=%03d_conf=%.3f_modus=%s__rules.csv", 
				Utils.getFilenameNoExtension(inputProcessed_as_dat), minSupport, windowSize, min_conf, modus));
		WrapperClosedEpisodesMining.postProcessClosepiRules(inputProcessed_as_lab,outputClosepi,outputReadable,min_conf);
 	}
	
	@Test
	public void postProcessWinepiOutput3() throws IOException {
		int windowSize=  15;
		double minSupport = 1; 
		double min_conf = 0.7;
		String modus="weighted";
		File outputClosepi = new File("./temp/closepi_output_rules.txt");
		File outputReadable = new File(String.format("./temp/closepi_%s_minSup=%03.3f_window=%03d_conf=%.3f_modus=%s__rules.csv", 
				Utils.getFilenameNoExtension(inputProcessed_as_dat), minSupport, windowSize, min_conf, modus));
		WrapperClosedEpisodesMining.postProcessClosepiRules(inputProcessed_as_lab,outputClosepi,outputReadable,min_conf);
 	}
	
	
	@Test
	public void C_reportOverlap() throws IOException {
		File[] files = new File[] {
				new File("temp",  "mine_patterns_origin_of_species_processed_minsup=005_mincoh=0.0150_maxlen=005_type=rules._sorted.csv"),
				new File("temp", "closepi_origin_of_species_processed_minSup=40.000_window=015_conf=0.700_modus=regular__rules.csv"),
				new File("temp", "closepi_origin_of_species_processed_minSup=10.000_window=015_conf=0.700_modus=min_windows__rules.csv"),
				new File("temp", "closepi_origin_of_species_processed_minSup=1.000_window=015_conf=0.700_modus=weighted__rules.csv")};
		ExperimentUtils.reportOverlapAndStuff(files,"origin_of_species_rules");
	}	
	
}