package be.uantwerpen.pattern_mining.experiments.quality;

import java.io.File;
import java.io.IOException;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import be.uantwerpen.pattern_mining.MainCommandLine;
import be.uantwerpen.pattern_mining.MainDFSAlgorithm;
import be.uantwerpen.pattern_mining.other.WrapperClosedEpisodesMining;
import be.uantwerpen.util.Utils;

/**
 *  Mine rules using our FCI_seq, Winepi, Marbles and Marbles_w on Trump tweets
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Test_Quality_Rules_Trump {
	
	File inputProcessed_as_dat= new File("./data/trump_tweets_processed.dat");
	File inputProcessed_as_lab= new File("./data/trump_tweets_processed.lab");
	
	@Test
	public void A_run_FCI_rules() throws IOException{ 
		int maxlen= 5;
		double mincoh = 0.02;
		int minsup = 5;
		double min_conf = 0.001;
		MainDFSAlgorithm service = new MainDFSAlgorithm(inputProcessed_as_lab,inputProcessed_as_dat, minsup);
		File output = service.minePatterns(maxlen, mincoh, false, false, true, 0.0, 0.0, min_conf);
		File outputSorted = Utils.sortCSVFile(output,  MainCommandLine.SortRules, true);
		Utils.printHead(outputSorted,100);
		//Took 5.3 min
	}
	
	@Test
	public void B_run_Closepi_rules() throws Exception{ 
		//WINEPI
		boolean weighted = false;
		boolean minWindow = false;
		int windowSize=  15;
		int minSupport = 40; //was 60  
		double min_conf = 0.7;
		File output = WrapperClosedEpisodesMining.runRules(inputProcessed_as_dat,inputProcessed_as_lab, minSupport, windowSize, minWindow, weighted, min_conf);
		Utils.printHead(output);
		//2min
	
		//LAXMAN
		weighted = false;
		minWindow = true;
		minSupport = 5; //was 10
		output = WrapperClosedEpisodesMining.runRules(inputProcessed_as_dat,inputProcessed_as_lab, minSupport, windowSize, minWindow, weighted, min_conf);
		Utils.printHead(output); 
		
		//MARBLES
		minWindow = false;
		weighted = true;
		minSupport = 1; 
		output = WrapperClosedEpisodesMining.runRules(inputProcessed_as_dat,inputProcessed_as_lab, minSupport, windowSize, minWindow, weighted, min_conf);
		Utils.printHead(output);
	}
	
	@Test
	public void C_reportOverlap() throws IOException {
		File[] files = new File[] {
				new File("temp",  "mine_patterns_trump_tweets_processed_minsup=005_mincoh=0.0200_maxlen=005_type=rules._sorted.csv"),
				new File("temp", "closepi_trump_tweets_processed_minSup=40.000_window=015_conf=0.700_modus=regular__rules.csv"),
				new File("temp", "closepi_trump_tweets_processed_minSup=5.000_window=015_conf=0.700_modus=min_windows__rules.csv"),
				new File("temp", "closepi_trump_tweets_processed_minSup=1.000_window=015_conf=0.700_modus=weighted__rules.csv")};
		ExperimentUtils.reportOverlapAndStuff(files,"trump_rules");
	}	
}