package be.uantwerpen.pattern_mining.experiments.quality;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import be.uantwerpen.pattern_mining.MainCommandLine;
import be.uantwerpen.pattern_mining.MainDFSAlgorithm;
import be.uantwerpen.pattern_mining.other.WrapperCompactMinimalWindows;
import be.uantwerpen.pattern_mining.other.WrapperClosedEpisodesMining;
import be.uantwerpen.pattern_mining.preprocessing.DatLabFormatConverter;
import be.uantwerpen.pattern_mining.preprocessing.PreprocessText;
import be.uantwerpen.util.FileStream;
import be.uantwerpen.util.Utils;
import static be.uantwerpen.pattern_mining.other.WrapperClosedEpisodesMining.*;

/** 
 * Preproces and mine itemsets using FCI_seq, Winepi, Laxman, Marbles, CMW on Origin of Species
 *
 **/
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Test_Quality_Itemsets_Species {

	//source: from gutenberg,  all chapters, but no legal header/footer stuff of gutenberg
	File inputRaw = new File("./data/pg2009.txt"); 
	
	//tokenized/stemmed/stopwords removed/lower case/no special chars:
	File inputProcessed = new File("./data/origin_of_species_processed.txt");

	//in lab/dat format
	File inputProcessed_as_dat= new File("./data/origin_of_species_processed.dat");
	File inputProcessed_as_lab= new File("./data/origin_of_species_processed.lab");

	@Test
	public void A_preProcess() throws Exception{
		boolean stem = true ,stopwords = true;
		FileStream fs = new FileStream(inputRaw, '\n');
		String line = fs.nextToken();
		FileWriter writer = new FileWriter(inputProcessed);
		while(line != null){
			line = PreprocessText.convertLine(line, stem, stopwords);
			if(!line.isEmpty()){
				writer.write(line);
				writer.write("\n");
			}
			line = fs.nextToken();
		}
		writer.close();
		//make lab files
		DatLabFormatConverter.convertNormal2DatLab(inputProcessed,inputProcessed_as_dat, inputProcessed_as_lab);
		DatLabFormatConverter.convertDatLab2Normal(inputProcessed_as_dat, inputProcessed_as_lab, new File("./temp/test_origin_convertion.txt"));
		//sanity check:
		Utils.printHead(inputRaw);
		Utils.printHead(inputProcessed);
		Utils.printHead(inputProcessed_as_dat);
		Utils.printHead(inputProcessed_as_lab);
		Utils.printHead(new File("./temp/test_origin_convertion.txt"));
	}
	
	@Test
	public void B_runFCI_Seq() throws IOException{
		double minCohesion = 0.015;
		int support = 5; 
		int maxlen = 6;
		MainDFSAlgorithm service = new MainDFSAlgorithm(inputProcessed_as_lab, inputProcessed_as_dat, support);
		File output = service.minePatterns(maxlen, minCohesion);
		File sorted = Utils.sortCSVFile(output, MainCommandLine.SortItemsets, true);
		Utils.printHead(sorted);
		sorted.renameTo(new File("./temp/species_fci_itemsets.csv"));
	}
	
	@Test
	public void C_runClosepi() throws Exception{
		double minSupport = 30;
		int windowSize = 15;
		boolean minWindow = false;
		boolean weighted = false;
		//Winepi
		File output = WrapperClosedEpisodesMining.runMethod(inputProcessed_as_dat,inputProcessed_as_lab, minSupport, windowSize, minWindow, weighted, true, false, false);
		Utils.printHead(output);
		output.renameTo(new File("./temp/species_winepi_itemsets.csv"));
		//Marbles
		minWindow = false;
		weighted = true;
		minSupport = 1;
		output = WrapperClosedEpisodesMining.runMethod(inputProcessed_as_dat,inputProcessed_as_lab, minSupport, windowSize, minWindow, weighted, true, false, false);
		Utils.printHead(output);
		output.renameTo(new File("./temp/species_marbles_itemsets.csv"));
		//LAXMAN
		weighted = false;
		minWindow = true;
		minSupport = 5;
		output = WrapperClosedEpisodesMining.runMethod(inputProcessed_as_dat,inputProcessed_as_lab, minSupport, windowSize, minWindow, weighted, true, false, false);
		Utils.printHead(output);
		output.renameTo(new File("./temp/species_laxman_itemsets.csv"));
	}
	
	@Test
	public void D_runCMW() {
		//CMW
		File laxman = WrapperClosedEpisodesMining.getOutputClosepiRawOutput(
				inputProcessed_as_dat, 
				//same as laxman
				5, 15, true, false, true, false, false);
		double alpha = 0.5;
		File output = WrapperCompactMinimalWindows.runCMW(inputProcessed_as_dat, inputProcessed_as_lab, laxman, alpha, ITEMSETS_ON,SP_OFF,GENERAL_EPI_OFF);
		output.renameTo(new File("./temp/species_cmw_itemsets.csv"));
	}

	
	@Test
	public void E_reportOverlap() throws IOException {
		File[] files = new File[] {
				new File("temp", "species_fci_itemsets.csv"),
				new File("temp", "species_winepi_itemsets.csv"),
				new File("temp", "species_laxman_itemsets.csv"),
				new File("temp", "species_marbles_itemsets.csv"),
				new File("temp", "species_cmw_itemsets.csv")
		};
		ExperimentUtils.reportOverlapAndStuff(files,"origin_of_species");
	}
	
}
