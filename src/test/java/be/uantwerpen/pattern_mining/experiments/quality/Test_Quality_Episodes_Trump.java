package be.uantwerpen.pattern_mining.experiments.quality;

import java.io.File;
import java.io.IOException;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import be.uantwerpen.pattern_mining.MainCommandLine;
import be.uantwerpen.pattern_mining.MainDFSAlgorithm;
import be.uantwerpen.pattern_mining.other.WrapperClosedEpisodesMining;
import be.uantwerpen.util.Utils;

/** 
 * Mine episodes using our FCI_seq, Winepi, Laxman and Marbles on Trump tweets
 * Note that comparing episodes is not useful after filtering all total orders.
 * 
 **/
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Test_Quality_Episodes_Trump {

	//in lab/dat format: see Test_Quality_Itemsets for preprocessing
	File inputProcessed_as_dat= new File("./data/trump_tweets_processed.dat");
	File inputProcessed_as_lab= new File("./data/trump_tweets_processed.lab");

	@Test
	public void A_runFCI_episodes() throws IOException{ 
		int maxlen= 5;
		double mincoh = 0.02;
		int minsup = 5;
		double min_occ_ratio_sequences = 0.1;
		double min_occ_ratio_episodes = 0.7;

		MainDFSAlgorithm service = new MainDFSAlgorithm(inputProcessed_as_lab,inputProcessed_as_dat, minsup);
		service.setFilterTotalOrders(true);
		File output = service.minePatterns(maxlen, mincoh, true, true, false, min_occ_ratio_sequences, min_occ_ratio_episodes, 0.0);
		File outputSorted = Utils.sortCSVFile(output, MainCommandLine.SortEpisodePatterns, true);
		Utils.printHead(outputSorted,100);
		//Took 7.1 min
	}

	@Test
	public void B_runClosepi_Episodes() throws Exception{
		//WINEPI
		int minSupport = 30;
		int windowSize = 15;
		boolean minWindow = false;
		boolean weighted = false;
		File output = WrapperClosedEpisodesMining.runMethod(inputProcessed_as_dat,inputProcessed_as_lab, minSupport, windowSize, minWindow, weighted, false,false,true);
		Utils.printHead(output);

		//LAXMAN
		weighted = false;
		minWindow = true;
		minSupport = 5;
		output = WrapperClosedEpisodesMining.runMethod(inputProcessed_as_dat,inputProcessed_as_lab, minSupport, windowSize, minWindow, weighted,false,false,true);
		Utils.printHead(output);

		//MARBLES
		minWindow = false;
		weighted = true;
		minSupport = 1;
		output = WrapperClosedEpisodesMining.runMethod(inputProcessed_as_dat,inputProcessed_as_lab, minSupport, windowSize, minWindow, weighted, false,false,true);
		Utils.printHead(output);

	}

}
