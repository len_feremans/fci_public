package be.uantwerpen.pattern_mining.experiments.quality;

import static be.uantwerpen.pattern_mining.other.WrapperClosedEpisodesMining.*;

import java.io.File;
import java.io.IOException;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import be.uantwerpen.pattern_mining.MainCommandLine;
import be.uantwerpen.pattern_mining.MainDFSAlgorithm;
import be.uantwerpen.pattern_mining.other.WrapperCompactMinimalWindows;
import be.uantwerpen.pattern_mining.other.WrapperClosedEpisodesMining;
import be.uantwerpen.util.Utils;

/** 
 * Mine episodes using our FCI_seq, Winepi, Laxman and Marbles on Origin of Species.
 * Note that comparing episodes is not useful after filtering all total orders.
 * 
 **/
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Test_Quality_Episodes_Species {

	//in lab/dat format: see Test_Quality_Itemsets for preprocessing
	File inputProcessed_as_dat= new File("./data/origin_of_species_processed.dat");
	File inputProcessed_as_lab= new File("./data/origin_of_species_processed.lab");

	@Test
	public void A_runFCI_episodes() throws IOException{ 
		int maxlen= 5;
		int minsup = 5;
		double mincoh = 0.015; 
		double min_occ_ratio_por = 0.7;
		MainDFSAlgorithm service = new MainDFSAlgorithm(inputProcessed_as_lab,inputProcessed_as_dat, minsup);
		service.setFilterTotalOrders(true);
		File output = service.minePatterns(maxlen, mincoh, false, true, false, 0.0, min_occ_ratio_por, 0.0);
		File outputSorted = Utils.sortCSVFile(output, MainCommandLine.SortEpisodePatterns, true);
		Utils.printHead(outputSorted,100);
		//Took 10.7 min
	}
	
	@Test
	public void B_runClosepi_Episodes() throws Exception{
		//WINEPI
		int minSupport = 30;
		int windowSize = 15;
		boolean minWindow = false;
		boolean weighted = false;
		File output = WrapperClosedEpisodesMining.runMethod(inputProcessed_as_dat,inputProcessed_as_lab, minSupport, windowSize, minWindow, weighted, false,false,true);
		Utils.printHead(output);
		
		//LAXMAN
		weighted = false;
		minWindow = true;
		minSupport = 5;
		output = WrapperClosedEpisodesMining.runMethod(inputProcessed_as_dat,inputProcessed_as_lab, minSupport, windowSize, minWindow, weighted,false,false,true);
		Utils.printHead(output);
		
		//MARBLES
		minWindow = false;
		weighted = true;
		minSupport = 1;
		output = WrapperClosedEpisodesMining.runMethod(inputProcessed_as_dat,inputProcessed_as_lab, minSupport, windowSize, minWindow, weighted, false,false,true);
		Utils.printHead(output);
	}
	
	@Test
	public void C_runCMW() {
		//CMW
		File laxman = WrapperClosedEpisodesMining.getOutputClosepiRawOutput(
				inputProcessed_as_dat, 
				//same as laxman
				5, 15, true, false, false, false, true);
		double alpha = 0.5;
		File output = WrapperCompactMinimalWindows.runCMW(inputProcessed_as_dat, inputProcessed_as_lab, laxman, alpha, ITEMSETS_OFF, SP_OFF, GENERAL_EPI_ON);
		output.renameTo(new File("./temp/species_cmw_episodes.csv"));
	}

}
