package be.uantwerpen.pattern_mining.experiments.quality;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Test;

import be.uantwerpen.util.Pair;
import be.uantwerpen.util.Utils;

/**
 * Utility code for comparing the overlap in patterns (rules/episodes/sequences/itemsets), and generating latex tables
 * 
 * @author lfereman
 *
 */
public class ExperimentUtils {


	//files order is: FCI, Winepi, Laxman, Marbles
	public static void reportOverlapAndStuff(File[] files, String output) throws IOException {
		//parse files
		List<List<String>> patterns = new ArrayList<>();
		for(File file: files) {
			List<String> patternsCurrent = new ArrayList<>();
			List<String> lines = Utils.readFileUntil(file, Integer.MAX_VALUE);
			if(file.getName().toLowerCase().contains("fci")) { //skip header
				lines = lines.subList(1, lines.size());
			}
			else if(file.getName().toLowerCase().contains("cmw")) {
				lines = lines.subList(1, lines.size());
			}
			for(String line: lines) {
				int index = 0;
				if(file.getName().toLowerCase().contains("cmw"))
					index = 1;
				//e.g. line="new fake;2058;157.000;2"
				String first_token = line.split(";")[index];
				if(first_token.startsWith("<") && first_token.endsWith(">")) {
					line = line.replaceAll("<", "");
					line = line.replaceAll(">", "");
				}
				String[] tokens = line.split(";")[index].split(" ");
				String pattern = Arrays.asList(tokens).stream().sorted().collect(Collectors.joining(","));
				if(line.contains("->")){
					//for closepi/FCI: rules, for CMW, these are orders
					if(file.getName().toLowerCase().contains("cmw")) {
						pattern = serialToSequenceLabel(line.split(";")[index+1]);
					}
					else {
						tokens = line.split(";")[index].split("->");
						String[] tokensLHS = tokens[0].split(",");
						String[] tokensRHS = tokens[1].split(",");
						pattern = Arrays.asList(tokensLHS).stream().sorted().collect(Collectors.joining(","))
								+ "->" + Arrays.asList(tokensRHS).stream().sorted().collect(Collectors.joining(","));
					}
				}
				patternsCurrent.add(pattern);
			}
			patterns.add(patternsCurrent);
		}
		for(int k: new int[] {100,500,1000}) {
			List<String> fciTopK = patterns.get(0).subList(0, Math.min(k, patterns.get(0).size()));
			if(k < patterns.get(0).size()) {
				System.err.println("Warning: only " + patterns.get(0).size() + " patterns");
			}
			for(int j=1; j< patterns.size(); j++) {
				Set<String> other = new HashSet<String>(patterns.get(j).subList(0, Math.min(patterns.get(j).size(),k)));
				int overlap = 0;
				for(String patternTopK: fciTopK) {
					if(other.contains(patternTopK)) {
						overlap++;
					}
				}
				System.out.format("Overlap at %d: %d\n", k, overlap);
			}
		}
		//save combined top-1000 results
		Set<String> visited = new HashSet<String>();
		BufferedWriter writer = new BufferedWriter(new FileWriter("./temp/" + output + "_combined.csv"));
		writer.write("pattern;rank FCI;rank_winepi_rank;rank_laxman;rank_marbles;rank_cmw;len\n"); 
		for(int i=0; i<1000; i++) {
			String patternFCI = safeGet(patterns.get(0),i);
			String patternWin = safeGet(patterns.get(1),i);
			String patternLaxman = safeGet(patterns.get(2),i);
			String patternMarbles =  safeGet(patterns.get(3),i);
			String patternCMW =  safeGet(patterns.get(4),i);
			for(String p: new String[] {patternFCI, patternWin, patternLaxman, patternMarbles, patternCMW}) {
				if(p == null || visited.contains(p)) {
					continue;
				}
				visited.add(p);
				writer.write(String.format("%s;%d;%d;%d;%d;%d\n", 
						p, 
						patterns.get(0).indexOf(p),
						patterns.get(1).indexOf(p),
						patterns.get(2).indexOf(p),
						patterns.get(3).indexOf(p),
						patterns.get(4).indexOf(p),
						p.split(",").length
						));
			}
		}
		writer.close();
		System.out.println("Saved " +  output + "_combined.csv" );
		//show latex table
		int topk = 25;
		int foundk=1000;
		System.out.println("\\textsc{FCI} & \\textsc{Winepi} & \\textsc{Laxman} & \\textsc{Marbles\\textsubscript{w}} & \\textsc{CMW}\n");
		for(int i=0;i<topk;i++) {
			//e.g. &puerto, rico & \textbf{goofi, elizabeth, warren} & \textbf{luther, strang} & https, co\\
			String patternFCI =  safeGet(patterns.get(0),i);
			boolean foundFCI = found(patterns.get(1), patternFCI, foundk)
					       ||  found(patterns.get(2), patternFCI, foundk)
					       ||  found(patterns.get(3), patternFCI, foundk)
					       ||  found(patterns.get(4), patternFCI, foundk);
			String patternWin =  safeGet(patterns.get(1),i);
			boolean foundWin= found(patterns.get(0), patternWin, foundk);
			String patternLaxman =  safeGet(patterns.get(2),i);
			boolean foundLaxman= found(patterns.get(0), patternLaxman, foundk);
			String patternMarbles =  safeGet(patterns.get(3),i);
			boolean foundMarbles = found(patterns.get(0), patternMarbles, foundk);
			String patternCMW =  safeGet(patterns.get(4),i);
			boolean foundCMW = found(patterns.get(0), patternCMW, foundk);
			String row = renderBold(patternFCI, foundFCI) + " & " + 
						 renderBold(patternWin, foundWin) + " & " + 
						 renderBold(patternLaxman, foundLaxman) + " & " + 
						 renderBold(patternMarbles, foundMarbles) + " & " +
						 renderBold(patternCMW, foundCMW) + "\\\\";
			System.out.println(row);
		} 
	}
	
	private static boolean found(List<String> patterns, String pattern, int foundK) {
		return patterns.indexOf(pattern) >= 0 && patterns.indexOf(pattern) < foundK;
	}
	
	
	private static String safeGet(List<String> lst, int k) {
		if(k < lst.size()) {
			return lst.get(k);
		}
		else {
			return null;
		}
	}

	/**
	 * Transform something like:
	 * hit -> time market -> hit stock -> market time -> high
	 * 
	 * To:
	 * stock,market, hit, time, high
	 * 
	 * NOte: copy pasted code from WrapperClosedEpisodesMining.serialToSequenceLabel
	 * @param str
	 * @return
	 */
	@Test
	public void serialToSequenLabelTest() {
		System.out.println(serialToSequenceLabel("hit -> time market -> hit stock -> market time -> high"));
	}
	public static String serialToSequenceLabel(String orders){
		List<String> tokens = Arrays.asList(orders.split(" "));
		List<Pair<String,String>> orderPairs = new ArrayList<>();
		for(int i=0; i<tokens.size(); i+=3) {
			String first = tokens.get(i);
			String arrow = tokens.get(i+1);
			String second = tokens.get(i+2);
			if(!arrow.equals("->"))
				throw new RuntimeException("expecting => in " + orders);
			orderPairs.add(new Pair<>(first,second));
		}
		List<String> result = new ArrayList<>();
		//first: add all items, e.g.
		//hit,time,market,stock,high
		for(Pair<String,String> order: orderPairs) {
			if(!(result.contains(order.getFirst()))) {
				result.add(order.getFirst());
			}
			if(!(result.contains(order.getSecond()))) {
				result.add(order.getSecond());
			}
		}
		//then: fix orders (kinda like bubble sort)
		///hit,time,market,stock,high
		//hit -> time market -> hit stock -> market time -> high
		//0) hit,time,market,stock,high 
		//1) time,market,hit,stock,high	(market->hit)
		//2) market,hit,time,stock,high (stock -> market)
		//3) hit,time,stock,market,high (market->hit)
		//4) time, stock, market, hit, high ...
		while(true) {
			boolean allOrdersOk = true;
			Collections.shuffle(orderPairs);
			for(Pair<String,String> order: orderPairs) {
				int idx1 = result.indexOf(order.getFirst());
				int idx2 = result.indexOf(order.getSecond());
				if(order.getFirst().equals(order.getSecond())) //true for repeating elements
					continue;
				if(idx1 < idx2) {}
				else {
					allOrdersOk = false;
					result.add(idx1+1, order.getSecond());
					result.remove(idx2);
					break; //break for
				}
			}
			if(allOrdersOk)
				break;
		}
		return Utils.join(result, ",");
	}
	
	private static String renderBold(String pattern, boolean found) {
		if(pattern == null) {
			return "";
		}
		if(!found) {
			return "\\textbf{" + pattern.replace(",", ", ").trim() + "}";
		}
		else {
			return pattern.replace(",", ", ").trim();
		}
	
	}
}
