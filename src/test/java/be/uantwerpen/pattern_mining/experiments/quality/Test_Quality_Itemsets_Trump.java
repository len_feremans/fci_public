package be.uantwerpen.pattern_mining.experiments.quality;

import static be.uantwerpen.pattern_mining.other.WrapperClosedEpisodesMining.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import be.uantwerpen.pattern_mining.MainCommandLine;
import be.uantwerpen.pattern_mining.MainDFSAlgorithm;
import be.uantwerpen.pattern_mining.other.WrapperCompactMinimalWindows;
import be.uantwerpen.pattern_mining.other.WrapperClosedEpisodesMining;
import be.uantwerpen.pattern_mining.preprocessing.DatLabFormatConverter;
import be.uantwerpen.pattern_mining.preprocessing.PreprocessText;
import be.uantwerpen.util.FileStream;
import be.uantwerpen.util.Utils;

/** 
 * Preproces and mine itemsets using our FCI_seq, Winepi, Laxman, Marbles, CMW on Trump
 *
 **/
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Test_Quality_Itemsets_Trump {
	
	File inputRaw = new File("./data/trump_tweets_2017_text.csv");
	
	//tokenized/stemmed/stopwords removed/lower case/no special chars:
	File inputProcessed = new File("./data/trump_tweets_2017_processed.txt");

	//in lab/dat format
	File inputProcessed_as_dat= new File("./data/trump_tweets_processed.dat");
	File inputProcessed_as_lab= new File("./data/trump_tweets_processed.lab");

	@Test
	public void A_preProcessTrumpTweets() throws Exception{
		boolean stem = true ,stopwords = true;
		FileStream fs = new FileStream(inputRaw, '\n');
		String line = fs.nextToken();
		FileWriter writer = new FileWriter(inputProcessed);
		while(line != null){
			//extra: remove URL
			line = preprocessTweet(line);
			line = PreprocessText.convertLine(line, stem, stopwords);
			if(!line.isEmpty()){
				writer.write(line);
				writer.write("\n");
			}
			line = fs.nextToken();
		}
		writer.close();
		DatLabFormatConverter.convertNormal2DatLab(inputProcessed,inputProcessed_as_dat, inputProcessed_as_lab);
		DatLabFormatConverter.convertDatLab2Normal(inputProcessed_as_dat, inputProcessed_as_lab, new File("./temp/test_trump_tweets_labdat_convertion.txt"));
		//sanity check:
		Utils.printHead(inputProcessed);
		Utils.printHead(inputProcessed_as_dat);
		Utils.printHead(inputProcessed_as_lab);
		Utils.printHead(new File("./temp/test_trump_tweets_labdat_convertion.txt"));
	}

	@Test
	public void B_runFCI_Seq_itemsets() throws IOException{ 
		double mincoh = 0.02;
		int maxlen = 6;
		int minsupp = 5;
		MainDFSAlgorithm service = new MainDFSAlgorithm(inputProcessed_as_lab,inputProcessed_as_dat, minsupp);
		File output = service.minePatterns(maxlen, mincoh);
		File itemsets_csv = Utils.sortCSVFile(output, MainCommandLine.SortItemsets, true);
		Utils.printHead(itemsets_csv);
		itemsets_csv.renameTo(new File("./temp/trump_fci_itemsets.csv"));
		//Took 3.8 min
	}
	
	@Test
	public void C_runClosepi() throws Exception{
		//WINEPI
		int minSupport = 30;
		int windowSize = 15;
		boolean minWindow = false;
		boolean weighted = false;
		File output = WrapperClosedEpisodesMining.runMethod(inputProcessed_as_dat,inputProcessed_as_lab, minSupport, windowSize, minWindow, weighted,true,false,false);
		Utils.printHead(output);
		output.renameTo(new File("./temp/trump_winepi_itemsets.csv"));
		
		//MARBLES
		weighted = true;
		minSupport = 1;
		output = WrapperClosedEpisodesMining.runMethod(inputProcessed_as_dat,inputProcessed_as_lab, minSupport, windowSize, minWindow, weighted,true,false,false);
		Utils.printHead(output);
		output.renameTo(new File("./temp/trump_marbles_itemsets.csv"));
		
		//LAXMAN
		weighted = false;
		minWindow = true;
		minSupport = 5;
		output = WrapperClosedEpisodesMining.runMethod(inputProcessed_as_dat,inputProcessed_as_lab, minSupport, windowSize, minWindow, weighted,true,false,false);
		Utils.printHead(output);
		output.renameTo(new File("./temp/trump_laxman_itemsets.csv"));
	}
	
	@Test
	public void D_runCMW() {
		//CMW
		File laxman = WrapperClosedEpisodesMining.getOutputClosepiRawOutput(inputProcessed_as_dat, 
				//same as laxman
				5, 15, true, false, true, false, false);
		double alpha = 0.5;
		File output = WrapperCompactMinimalWindows.runCMW(inputProcessed_as_dat, inputProcessed_as_lab, laxman, alpha, ITEMSETS_ON, SP_OFF, GENERAL_EPI_OFF);
		output.renameTo(new File("./temp/trump_cmw_itemsets.csv"));
	}

	@Test
	public void E_reportOverlap() throws IOException {
		File[] files = new File[] {
				new File("temp", "trump_fci_itemsets.csv"),
				new File("temp", "trump_winepi_itemsets.csv"),
				new File("temp", "trump_laxman_itemsets.csv"),
				new File("temp", "trump_marbles_itemsets.csv"),
				new File("temp", "trump_cmw_itemsets.csv")
		};
		ExperimentUtils.reportOverlapAndStuff(files,"trump_tweets");
	}
	

	@Test
	public void testpreprocessTweet() {
		String str = "https://t.co/tbl94sCM01";
		String str2 = "@RepMarkMeadows @Jim_Jordan and @Raul_Labrador?#RepealANDReplace #Obamacare HILLARY";
		String str3= "\"On International Women's Day join me in honoring the critical role of women here in America &amp; around the world.\"";
		for(String s: new String[] {str, str2, str3}) {
			s = preprocessTweet(s);
			System.out.println(s);
		}
	}

	private String preprocessTweet(String s) {
		s = s.replaceAll("http(s)?\\:[A-Za-z\\\\.0-9\\/]*", "");
		s = s.replaceAll("\\&[a-z]+\\;", " ");
		s = s.replaceAll("\\s+", " ");
		s = s.replaceAll("\\@|\\#|\\_|\\?", " ");
		return s;
	}
	
}
