package be.uantwerpen.pattern_mining.experiments.quality;

import java.io.File;
import java.io.IOException;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import be.uantwerpen.pattern_mining.MainCommandLine;
import be.uantwerpen.pattern_mining.MainDFSAlgorithm;
import be.uantwerpen.pattern_mining.other.WrapperCompactMinimalWindows;
import be.uantwerpen.pattern_mining.other.WrapperClosedEpisodesMining;
import be.uantwerpen.util.Utils;
import static be.uantwerpen.pattern_mining.other.WrapperClosedEpisodesMining.*;

/** 
 * Mine sequential patterns using our FCI_seq, Winepi, Laxman and Marbles on Origin of Species
 *
 **/
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Test_Quality_Sequential_Patterns_Species {

	//in lab/dat format
	File inputProcessed_as_dat= new File("./data/origin_of_species_processed.dat");
	File inputProcessed_as_lab= new File("./data/origin_of_species_processed.lab");

	@Test
	public void A_runFCI_sequences() throws IOException{ 
		int maxlen = 6;
		int minsup = 5; 
		double mincoh = 0.015; 
		double min_occ_ratio = 0.7;
		MainDFSAlgorithm service = new MainDFSAlgorithm(inputProcessed_as_lab,inputProcessed_as_dat, minsup);
		File output = service.minePatterns(maxlen, mincoh, true, false, false, min_occ_ratio, 0.0, 0.0);
		File outputSorted = Utils.sortCSVFile(output, MainCommandLine.SortSequentialPatterns, true);
		Utils.printHead(outputSorted,100);
		//Finished minePatterns(minsup=.... Took 1.6 min
		//No visits: 9876311
		//No patterns: 54	
		outputSorted.renameTo(new File("./temp/species_fci_sp.csv"));
	}

	@Test
	public void B_runClosepi() throws Exception{
		double minSupport = 30;
		int windowSize = 15;
		boolean minWindow = false;
		boolean weighted = false;
		File output = WrapperClosedEpisodesMining.runMethod(inputProcessed_as_dat,inputProcessed_as_lab, minSupport, windowSize, minWindow, weighted, false, true, false);
		Utils.printHead(output);
		output.renameTo(new File("./temp/species_winepi_sp.csv"));
		//Marbles
		minWindow = false;
		weighted = true;
		minSupport = 1;
		output = WrapperClosedEpisodesMining.runMethod(inputProcessed_as_dat,inputProcessed_as_lab, minSupport, windowSize, minWindow, weighted,  false, true, false);
		Utils.printHead(output);
		output.renameTo(new File("./temp/species_marbles_sp.csv"));
		//LAXMAN
		weighted = false;
		minWindow = true;
		minSupport = 5;
		output = WrapperClosedEpisodesMining.runMethod(inputProcessed_as_dat,inputProcessed_as_lab, minSupport, windowSize, minWindow, weighted,  false, true, false);
		Utils.printHead(output);
		output.renameTo(new File("./temp/species_laxman_sp.csv"));
		
	}
	
	@Test
	public void C_runCMW() {
		//CMW
		File laxman = WrapperClosedEpisodesMining.getOutputClosepiRawOutput(
				inputProcessed_as_dat, 
				//same as laxman
				5, 15, true, false, false, true, false);
		double alpha = 0.5;
		File output = WrapperCompactMinimalWindows.runCMW(inputProcessed_as_dat, inputProcessed_as_lab, laxman, alpha,  ITEMSETS_OFF, SP_ON, GENERAL_EPI_OFF);
		output.renameTo(new File("./temp/species_cmw_sp.csv"));
	}


	@Test
	public void D_reportOverlap() throws IOException {
		File[] files = new File[] {
				new File("temp", "species_fci_sp.csv"),
				new File("temp", "species_winepi_sp.csv"),
				new File("temp", "species_laxman_sp.csv"),
				new File("temp", "species_marbles_sp.csv"),
				new File("temp", "species_cmw_sp.csv")};
		ExperimentUtils.reportOverlapAndStuff(files,"origin_of_species_sequences");
	}			
}