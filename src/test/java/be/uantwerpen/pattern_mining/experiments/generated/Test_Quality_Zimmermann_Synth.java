package be.uantwerpen.pattern_mining.experiments.generated;

import static be.uantwerpen.pattern_mining.other.WrapperClosedEpisodesMining.GENERAL_EPI_OFF;
import static be.uantwerpen.pattern_mining.other.WrapperClosedEpisodesMining.ITEMSETS_ON;
import static be.uantwerpen.pattern_mining.other.WrapperClosedEpisodesMining.MIN_WINDOWS_OFF;
import static be.uantwerpen.pattern_mining.other.WrapperClosedEpisodesMining.MIN_WINDOWS_ON;
import static be.uantwerpen.pattern_mining.other.WrapperClosedEpisodesMining.SPARSE_ON;
import static be.uantwerpen.pattern_mining.other.WrapperClosedEpisodesMining.SP_ON;
import static be.uantwerpen.pattern_mining.other.WrapperClosedEpisodesMining.WEIGHTED_WINDOWS_OFF;
import static be.uantwerpen.pattern_mining.other.WrapperClosedEpisodesMining.WEIGHTED_WINDOWS_ON;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import be.uantwerpen.pattern_mining.MainCommandLine;
import be.uantwerpen.pattern_mining.MainDFSAlgorithm;
import be.uantwerpen.pattern_mining.other.WrapperCompactMinimalWindows;
import be.uantwerpen.pattern_mining.other.WrapperClosedEpisodesMining;
import be.uantwerpen.util.CommandLineUtils;
import be.uantwerpen.util.Pair;
import be.uantwerpen.util.Utils;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Test_Quality_Zimmermann_Synth {

	public static int RUN=5;
	
	int minsupFCI = 0;
	int maxlengthFCI = 5;
	double alphaCMW = 0.5;
	
	int window = 40; //was 40
	int thresholdCMW = 10; //was 10
	double thresholdLaxman = 10; //was 10
	double thresholdWinepi = 80;  //was 80
	double thresholdMarbles = 1; //was 1
	

	int window2 = 50; //was 40
	int thresholdCMW2 = 5; //was 10
	double thresholdLaxman2 = 5; //was 10
	double thresholdWinepi2 = 80;  //was 80
	double thresholdMarbles2 = 1; //was 1
	
	//for generator
	int total_events = 5000;
	
	/**
	 * ===============================================================================================
	 * Validation
	 * ===============================================================================================
	 */
	@Test
	public void mineOnZimmermanSynth_Validation() throws Exception {
		maxlengthFCI = 5;
		int alphabet = 20;
		double noiseOmission = 0.0;
		int maxTimeDelay = 20;
		double noise = 0.95;
		int numberOfPatterns = 1;
		Map<String,Double> rankResults = new TreeMap<>();
		String basename = "./data/zimmermann/validation/" +  
		String.format("/fold=%d_generated_p=%.3f_o=%.3f_N=4_n=%d_M=%d_g=%d_h=%s_i=%s_s=%s_PU=%s",
					RUN, noise, noiseOmission, numberOfPatterns, alphabet, maxTimeDelay, 
					false, false, false, true);
		generateDatasetAndStoreRankDifferentMethods(basename, noise, alphabet, 
					noiseOmission, maxTimeDelay, numberOfPatterns,
					rankResults);
		System.out.println(rankResults);
	}
	
	/**
	 * ===============================================================================================
	 * Benchmark experiment Zimmermann
	 * ===============================================================================================
	 */
	@Test
	public void mineOnZimmermanSynth_Exp1A_Noise() throws Exception {
		int alphabet = 20;
		double noiseOmission = 0.0;
		int maxTimeDelay = 20;
		Double noises[] = new Double[] {0.05, 0.15, 0.25, 0.35, 0.45, 0.55, 0.65, 0.75, 0.85, 0.95}; 
		int numberOfPatterns = 1;
		List<Map<String,Double>> results = new ArrayList<>();
		for(double noise: noises) {
			Map<String,Double> rankResults = new TreeMap<>();
			String basename = "./data/zimmermann/exp1A-noise/" +  String.format("/fold=%d_generated_p=%.3f_o=%.3f_N=4_n=%d_M=%d_g=%d_h=%s_i=%s_s=%s_PU=%s",
					RUN, noise, noiseOmission, numberOfPatterns, alphabet, maxTimeDelay, false, false, false, true);
			generateDatasetAndStoreRankDifferentMethods(basename, noise, alphabet, noiseOmission, maxTimeDelay, numberOfPatterns,
					rankResults);
			results.add(rankResults);
		}
		saveResults("./data/zimmermann/exp1A-noise/", noises, results);
	}
	
	@Test
	public void mineOnZimmermanSynth_Exp1B_Noise_Poisson() throws Exception {
		int alphabet = 20;
		double noiseOmission = 0.0;
		int maxTimeDelay = 20;
		Double noises[] = new Double[] {0.05, 0.15, 0.25, 0.35, 0.45, 0.55, 0.65, 0.75, 0.85, 0.95}; 
		int numberOfPatterns = 1;
		boolean timeDelayOnEpisodes = false;
		boolean embeddedEpisodesInterleave = false;
		boolean embeddedEpisodesCanShare = false;
		boolean uniformDistribution = false; //WAS TRUE
		//this.thresholdCMW *=2;
		//	this.thresholdLaxman *=2;
		//this.thresholdMarbles *=2;
		//this.thresholdWinepi *=2;
		List<Map<String,Double>> results = new ArrayList<>();
		for(double noise: noises) {
			Map<String,Double> rankResults = new TreeMap<>();
			String basename = "./data/zimmermann/exp1B-noise-poisson/" +  String.format("/fold=%d_generated_p=%.3f_o=%.3f_N=4_n=%d_M=%d_g=%d_h=%s_i=%s_s=%s_PU=%s",
					RUN, noise, noiseOmission, numberOfPatterns, alphabet, maxTimeDelay, timeDelayOnEpisodes, embeddedEpisodesInterleave, embeddedEpisodesCanShare, uniformDistribution);
			generateDatasetAndStoreRankDifferentMethods(basename, noise, alphabet, noiseOmission, maxTimeDelay, numberOfPatterns,
					timeDelayOnEpisodes, embeddedEpisodesInterleave, embeddedEpisodesCanShare, uniformDistribution,
					rankResults);
			results.add(rankResults);
		}
		//this.thresholdCMW /=2;
		//this.thresholdLaxman /=2;
		//this.thresholdMarbles /=2;
		//this.thresholdWinepi /=2;
		saveResults("./data/zimmermann/exp1B-noise-poisson/", noises, results);
	}
	
	@Test
	public void mineOnZimmermanSynth_Exp2A_alphabet() throws Exception {
		double noiseOmission = 0.0;
		double noise = 0.5;
		int maxTimeDelay = 20;
		Integer alphabet_sizes[] = new Integer[] {4,8,12,16,20,24,28,32,36,40};
		int numberOfPatterns = 1;
		List<Map<String,Double>> results = new ArrayList<>();
		for(int alphabet: alphabet_sizes) {
			Map<String,Double> rankResults = new TreeMap<>();
			String basename = "./data/zimmermann/exp2A-alphabet/" +  String.format("/fold=%d_generated_p=%.3f_o=%.3f_N=4_n=%d_M=%d_g=%d_h=%s_i=%s_s=%s_PU=%s",
					RUN, noise, noiseOmission, numberOfPatterns, alphabet, maxTimeDelay, false, false, false, true);
			generateDatasetAndStoreRankDifferentMethods(basename,noise, alphabet, noiseOmission, maxTimeDelay, numberOfPatterns, 
					rankResults);
			results.add(rankResults);
		}
		saveResults("./data/zimmermann/exp2A-alphabet/", alphabet_sizes, results);
	}
	
	@Test
	public void mineOnZimmermanSynth_Exp2B_alphabet_poisson() throws Exception {
		double noiseOmission = 0.0;
		double noise = 0.5;
		int maxTimeDelay = 20;
		Integer alphabet_sizes[] = new Integer[] {4,8,12,16,20,24,28,32,36,40}; 
		int numberOfPatterns = 1;
		boolean timeDelayOnEpisodes = false;
		boolean embeddedEpisodesInterleave = false;
		boolean embeddedEpisodesCanShare = false;
		boolean uniformDistribution = false;
		List<Map<String,Double>> results = new ArrayList<>();
		for(int alphabet: alphabet_sizes) {
			Map<String,Double> rankResults = new TreeMap<>();
			String basename = "./data/zimmermann/exp2B-alphabet-poisson/" +  String.format("/fold=%d_generated_p=%.3f_o=%.3f_N=4_n=%d_M=%d_g=%d_h=%s_i=%s_s=%s_PU=%s",
					RUN, noise, noiseOmission, numberOfPatterns, alphabet, maxTimeDelay, timeDelayOnEpisodes, embeddedEpisodesInterleave, embeddedEpisodesCanShare, uniformDistribution);
			generateDatasetAndStoreRankDifferentMethods(basename,noise, alphabet, noiseOmission, maxTimeDelay, numberOfPatterns, 
					timeDelayOnEpisodes,embeddedEpisodesInterleave, embeddedEpisodesCanShare, uniformDistribution,
					rankResults);
			results.add(rankResults);
		}
		saveResults("./data/zimmermann/exp2B-alphabet-poisson/", alphabet_sizes, results);
	}
	
	@Test
	public void mineOnZimmermanSynth_Exp3A_omission() throws Exception {
		double noise = 0.5;
		int alphabet = 20;
		int maxTimeDelay = 20;
		Double noise_omissions[] = new Double[] {0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9};
		int numberOfPatterns = 1;
		List<Map<String,Double>> results = new ArrayList<>();
		for(double noiseOmission: noise_omissions) {
			Map<String,Double> rankResults = new TreeMap<>();
			String basename = "./data/zimmermann/exp3A-omission/" +  String.format("/fold=%d_generated_p=%.3f_o=%.3f_N=4_n=%d_M=%d_g=%d_h=%s_i=%s_s=%s_PU=%s",
					RUN, noise, noiseOmission, numberOfPatterns, alphabet, maxTimeDelay, false, false, false, true);
			generateDatasetAndStoreRankDifferentMethods(basename,noise, alphabet, noiseOmission, maxTimeDelay, numberOfPatterns, 
					rankResults);
			results.add(rankResults);
		}
		saveResults("./data/zimmermann/exp3A-omission/", noise_omissions, results);
	}
	
	@Test
	public void mineOnZimmermanSynth_Exp3B_omission_poisson() throws Exception {
		double noise = 0.5;
		int alphabet = 20;
		int maxTimeDelay = 20;
		Double noise_omissions[] = new Double[] {0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9};
		int numberOfPatterns = 1;
		boolean timeDelayOnEpisodes = false;
		boolean embeddedEpisodesInterleave = false;
		boolean embeddedEpisodesCanShare = false;
		boolean uniformDistribution = false; //OK?
		List<Map<String,Double>> results = new ArrayList<>();
		for(double noiseOmission: noise_omissions) {
			Map<String,Double> rankResults = new TreeMap<>();
			String basename = "./data/zimmermann/exp3B-omission-poisson/" +  String.format("/fold=%d_generated_p=%.3f_o=%.3f_N=4_n=%d_M=%d_g=%d_h=%s_i=%s_s=%s_PU=%s",
					RUN, noise, noiseOmission, numberOfPatterns, alphabet, maxTimeDelay, timeDelayOnEpisodes, embeddedEpisodesInterleave, embeddedEpisodesCanShare, uniformDistribution);
			generateDatasetAndStoreRankDifferentMethods(basename,noise, alphabet, noiseOmission, maxTimeDelay, numberOfPatterns, rankResults);
			results.add(rankResults);
		}
		saveResults("./data/zimmermann/exp3B-omission-poisson/", noise_omissions, results);
	}
	
	@Test
	public void mineOnZimmermanSynth_Exp4A_max_delay() throws Exception {
		double noise = 0.5;
		int alphabet = 20;
		double noiseOmission = 0.0;
		Integer[] maxTimeDelays = new Integer[]{20,30,40,50,60,70,80,90,100};
		this.window2 = this.window2*2;
		int numberOfPatterns = 1;
		List<Map<String,Double>> results = new ArrayList<>();
		for(int maxTimeDelay: maxTimeDelays) {
			Map<String,Double> rankResults = new TreeMap<>();
			String basename = "./data/zimmermann/exp4A-max_delay/" +  String.format("/fold=%d_generated_p=%.3f_o=%.3f_N=4_n=%d_M=%d_g=%d_h=%s_i=%s_s=%s_PU=%s",
					RUN, noise, noiseOmission, numberOfPatterns, alphabet, maxTimeDelay, false, false, false, true);
			generateDatasetAndStoreRankDifferentMethods(basename,noise, alphabet, noiseOmission, maxTimeDelay, numberOfPatterns,
					rankResults);
			results.add(rankResults);
		}
		this.window2 = this.window2/2;
		saveResults("./data/zimmermann/exp4A-max_delay/", maxTimeDelays, results);
	}
	
	@Test
	public void mineOnZimmermanSynth_Exp4B_max_delay_poisson() throws Exception {
		double noise = 0.5;
		int alphabet = 20;
		double noiseOmission = 0.0;
		boolean timeDelayOnEpisodes = false;
		boolean embeddedEpisodesInterleave = false;
		boolean embeddedEpisodesCanShare = false;
		boolean uniformDistribution = false; //TODO: check
		Integer[] maxTimeDelays = new Integer[]{20,30,40,50,60,70,80,90,100};
		int numberOfPatterns = 1;
		List<Map<String,Double>> results = new ArrayList<>();
		this.window2 = this.window2*2;
		for(int maxTimeDelay: maxTimeDelays) {
			Map<String,Double> rankResults = new TreeMap<>();
			String basename = "./data/zimmermann/exp4B-max_delay-poisson/" +  String.format("/fold=%d_generated_p=%.3f_o=%.3f_N=4_n=%d_M=%d_g=%d_h=%s_i=%s_s=%s_PU=%s",
					RUN, noise, noiseOmission, numberOfPatterns, alphabet, maxTimeDelay, timeDelayOnEpisodes, embeddedEpisodesInterleave, embeddedEpisodesCanShare, uniformDistribution);
			generateDatasetAndStoreRankDifferentMethods(basename,noise, alphabet, noiseOmission, maxTimeDelay, numberOfPatterns,
					timeDelayOnEpisodes, embeddedEpisodesInterleave, embeddedEpisodesCanShare, uniformDistribution,
					rankResults);
			results.add(rankResults);
		}
		this.window2 = this.window2/2;
		saveResults("./data/zimmermann/exp4B-max_delay-poisson/", maxTimeDelays, results);
	}
	
	@Test
	public void mineOnZimmermanSynth_Exp4C_max_delay_embedding_var() throws Exception {
		double noise = 0.5;
		int alphabet = 20;
		boolean timeDelayOnEpisodes = true; //was false
		boolean embeddedEpisodesInterleave = false;
		boolean embeddedEpisodesCanShare = false;
		boolean uniformDistribution = true;
		double noiseOmission = 0.0;
		Integer[] maxTimeDelays = new Integer[]{20,30,40,50,60,70,80,90,100};
		int numberOfPatterns = 1;
		this.window2 = this.window2*2;
		//this.thresholdCMW*=2; //ECHT NODIG
		//this.thresholdLaxman*=2;
		//this.thresholdMarbles*=2;
		//this.thresholdWinepi*=2;
		List<Map<String,Double>> results = new ArrayList<>();
		for(int maxTimeDelay: maxTimeDelays) {
			Map<String,Double> rankResults = new TreeMap<>();
			String basename = "./data/zimmermann/exp4C-max_delay-wembedding/" +  String.format("/fold=%d_generated_p=%.3f_o=%.3f_N=4_n=%d_M=%d_g=%d_h=%s_i=%s_s=%s_PU=%s",
					RUN, noise, noiseOmission, numberOfPatterns, alphabet, maxTimeDelay, timeDelayOnEpisodes, embeddedEpisodesInterleave, embeddedEpisodesCanShare, uniformDistribution);
			generateDatasetAndStoreRankDifferentMethods(basename, noise, alphabet, noiseOmission, maxTimeDelay, numberOfPatterns, 
					timeDelayOnEpisodes, embeddedEpisodesInterleave, embeddedEpisodesCanShare, uniformDistribution,
					rankResults);
			results.add(rankResults);
		}
		//this.thresholdCMW/=2;
		//this.thresholdLaxman/=2;
		//this.thresholdMarbles/=2;
		//this.thresholdWinepi/=4;
		this.window2 = this.window2/2;
		saveResults("./data/zimmermann/exp4C-max_delay-wembedding/", maxTimeDelays, results);
	}
	
	/*
	@Test
	public void mineOnZimmermanSynth_Patterns() throws Exception {
		mineOnZimmermanSynth_Exp5A_patterns();
		mineOnZimmermanSynth_Exp5B_patterns_poisson();
		mineOnZimmermanSynth_Exp5C_patterns_interleave();
		mineOnZimmermanSynth_Exp5D_patterns_share();
	}
	*/
	
	@Test
	public void mineOnZimmermanSynth_Exp5A_patterns() throws Exception {
		double noise = 0.5;
		int alphabet = 20;
		boolean timeDelayOnEpisodes = false;
		boolean embeddedEpisodesInterleave = false;
		boolean embeddedEpisodesCanShare = false;
		double noiseOmission = 0.0;
		boolean uniformDistribution = true;
		int maxTimeDelay = 20;
		Integer[] numberOfPatternsSet = new Integer[] {1,2,3,4,5};
		List<Map<String,Double>> results = new ArrayList<>();
		for(int numberOfPatterns: numberOfPatternsSet) {
			Map<String,Double> rankResults = new TreeMap<>();
			String basename = "./data/zimmermann/exp5A-patterns/" +  String.format("/fold=%d_generated_p=%.3f_o=%.3f_N=4_n=%d_M=%d_g=%d_h=%s_i=%s_s=%s_PU=%s",
					RUN, noise, noiseOmission, numberOfPatterns, alphabet, maxTimeDelay, timeDelayOnEpisodes, embeddedEpisodesInterleave, embeddedEpisodesCanShare, uniformDistribution);
			generateDatasetAndStoreRankDifferentMethods(basename, noise, alphabet, noiseOmission, maxTimeDelay, numberOfPatterns,
					timeDelayOnEpisodes, embeddedEpisodesInterleave, embeddedEpisodesCanShare, uniformDistribution,
					rankResults);
			results.add(rankResults);
		}
		saveResults("./data/zimmermann/exp5A-patterns/", numberOfPatternsSet, results);
	}
	

	@Test
	public void mineOnZimmermanSynth_Exp5B_patterns_poisson() throws Exception {
		double noise = 0.5;
		int alphabet = 20;
		boolean timeDelayOnEpisodes = false;
		boolean embeddedEpisodesInterleave = false;
		boolean embeddedEpisodesCanShare = false;
		double noiseOmission = 0.0;
		boolean uniformDistribution = false; //TODO: check
		int maxTimeDelay = 20;
		Integer[] numberOfPatternsSet = new Integer[] {1,2,3,4,5};
		List<Map<String,Double>> results = new ArrayList<>();
		for(int numberOfPatterns: numberOfPatternsSet) {
			Map<String,Double> rankResults = new TreeMap<>();
			String basename = "./data/zimmermann/exp5B-patterns-poisson/" +  String.format("/fold=%d_generated_p=%.3f_o=%.3f_N=4_n=%d_M=%d_g=%d_h=%s_i=%s_s=%s_PU=%s",
					RUN,noise, noiseOmission, numberOfPatterns, alphabet, maxTimeDelay, timeDelayOnEpisodes, embeddedEpisodesInterleave, embeddedEpisodesCanShare, uniformDistribution);
			generateDatasetAndStoreRankDifferentMethods(basename, noise, alphabet, noiseOmission, maxTimeDelay, numberOfPatterns,
					timeDelayOnEpisodes, embeddedEpisodesInterleave, embeddedEpisodesCanShare, uniformDistribution,
					rankResults);
			results.add(rankResults);
		}
		saveResults("./data/zimmermann/exp5B-patterns-poisson/", numberOfPatternsSet, results);
	}
	
	
	@Test
	public void mineOnZimmermanSynth_Exp5C_patterns_interleave() throws Exception {
		double noise = 0.5;
		int alphabet = 20;
		boolean timeDelayOnEpisodes = false;
		boolean embeddedEpisodesInterleave = true;
		boolean embeddedEpisodesCanShare = false;
		boolean uniformDistribution = true;
		double noiseOmission = 0.0;
		int maxTimeDelay = 20;
		Integer[] numberOfPatternsSet = new Integer[] {1,2,3,4,5};
		List<Map<String,Double>> results = new ArrayList<>();
		for(int numberOfPatterns: numberOfPatternsSet) {
			Map<String,Double> rankResults = new TreeMap<>();
					String basename = "./data/zimmermann/exp5C-patterns-interleave/" +  
					String.format("/fold=%d_generated_p=%.3f_o=%.3f_N=4_n=%d_M=%d_g=%d_h=%s_i=%s_s=%s_PU=%s",
					RUN, noise, noiseOmission, numberOfPatterns, alphabet, maxTimeDelay, timeDelayOnEpisodes, 
					embeddedEpisodesInterleave, embeddedEpisodesCanShare, uniformDistribution);
			generateDatasetAndStoreRankDifferentMethods(basename, noise, alphabet, noiseOmission, maxTimeDelay, numberOfPatterns,
					timeDelayOnEpisodes, embeddedEpisodesInterleave, embeddedEpisodesCanShare, uniformDistribution,
					rankResults);
			results.add(rankResults);
		}
		saveResults( "./data/zimmermann/exp5C-patterns-interleave/", numberOfPatternsSet, results);
	}
	
	@Test
	public void mineOnZimmermanSynth_Exp5D_patterns_share() throws Exception {
		double noise = 0.5;
		int alphabet = 20;
		boolean timeDelayOnEpisodes = false;
		boolean embeddedEpisodesInterleave = false;
		boolean embeddedEpisodesCanShare = true;
		boolean uniformDistribution = true;
		double noiseOmission = 0.0;
		int maxTimeDelay = 20;
		Integer[] numberOfPatternsSet = new Integer[] {1,2,3,4,5};
		List<Map<String,Double>> results = new ArrayList<>();
		for(int numberOfPatterns: numberOfPatternsSet) {
			Map<String,Double> rankResults = new TreeMap<>();
			String basename = "./data/zimmermann/exp5D-patterns-share/" +  String.format("/fold=%d_generated_p=%.3f_o=%.3f_N=4_n=%d_M=%d_g=%d_h=%s_i=%s_s=%s_PU=%s",
					RUN, noise, noiseOmission, numberOfPatterns, alphabet, maxTimeDelay, timeDelayOnEpisodes, embeddedEpisodesInterleave, embeddedEpisodesCanShare, uniformDistribution);
			generateDatasetAndStoreRankDifferentMethods(basename, noise, alphabet, noiseOmission, maxTimeDelay, numberOfPatterns,
					timeDelayOnEpisodes, embeddedEpisodesInterleave, embeddedEpisodesCanShare, uniformDistribution,
					rankResults);
			results.add(rankResults);
		}
		saveResults("./data/zimmermann/exp5D-patterns-share/", numberOfPatternsSet, results);
	}
	

	/**
	 * ===============================================================================================
	 * Generation
	 * ===============================================================================================
	 */
	public void generateDatasetAndStoreRankDifferentMethods(String basename,
			double noise, int alphabet, double noiseOmission, int maxTimeDelay, int numberOfPatterns, 
			Map<String,Double> rankResults) throws Exception {
		generateDatasetAndStoreRankDifferentMethods(basename, 
				noise, alphabet, noiseOmission, maxTimeDelay,  numberOfPatterns, 
				false, false, false, 
				true, rankResults);
	}
	
	public void generateDatasetAndStoreRankDifferentMethods(String basename,
			double noise, int alphabet, double noiseOmission, int maxTimeDelay, int numberOfPatterns, 
			boolean timeDelayOnEpisodes, boolean embeddedEpisodesInterleave, boolean embeddedEpisodesCanShare, 
			boolean uniformDistribution, Map<String,Double> rankResults) throws Exception {
		//generate data using zimmermann
		File output = new File(basename + ".csv");
		if(output.exists()) {
			output.delete();
		}
		else {
			output.getParentFile().mkdirs();
		}
		File output2 = new File(basename + "_data.csv");
		PrintStream writeToFile = new PrintStream(new FileOutputStream(output, true)); 
		CommandLineUtils.TIMER_OUTPUT_ONLY_CONSOLE = true;
		String command[] = new String[] {"java", "-jar", 
				"other_tools/zimmermann_sequence_generator/sequence_generator.jar", "zimmermann_sequence_generator",
				"-N" ,"4" , //generate serial (!) episode with 4 elements
				"-M",  String.valueOf(alphabet), //total of 20 different events
				"-p" , String.valueOf(noise), //chance of noise event is 0.2
				"-o",  String.valueOf(noiseOmission),  //Omit generated event with probability float (default 0)
				"-g",  String.valueOf(maxTimeDelay) ,//max time delay from, sample between [1,int] (default 20)
				"-n" , String.valueOf(numberOfPatterns) , //generate 1 episode
				"-T",  String.valueOf(total_events), //total events generated
				"-d", "u" , //uniform distribution noise delays
				"-D" ,"u" , //uniform distribution episode delays
				"-P", uniformDistribution?"u":"n"}; //uniform distribution noise events
		if(timeDelayOnEpisodes) {
			//Enforce maximum time delay between successive events of embedded episode (specified by -g or m*G)
			command = Utils.concatenate(command, new String[] {"-h"});
		}
		if(embeddedEpisodesInterleave) {
			command = Utils.concatenate(command, new String[] {"-i"});
		}
		if(embeddedEpisodesCanShare) { //can share event types
			command = Utils.concatenate(command, new String[] {"-s"});
		}
		CommandLineUtils.runCommand(command,writeToFile, System.err, new File("."));
		//parse output generator - data
		Utils.printHead(output,15);
		List<String> lines = Utils.readFileUntil(output, Integer.MAX_VALUE);
		int dataIndex = 0;
		for(String line: lines) {
			if(line.startsWith("#")) dataIndex++;
			else break;
		}
		List<String> linesData = lines.subList(dataIndex, lines.size()); //remove header
		Utils.saveFile(linesData, output2);
		Utils.printHead(output2,10);
		//parse output generator - get embedded patterns
		List<String> embeddedPatterns = new ArrayList<>();
		for(int i=0; i<dataIndex; i++) {
			if(lines.get(i).startsWith("# Embedded episode")) {
				String pattern = Utils.substringAfter(lines.get(i),":"); //parse # Embedded episode 0: 4 <8> 14 <19> 17 <9> 5
				pattern = pattern.replaceAll("<[0-9]+>\\s?", "").trim();
				System.out.println("Pattern:" + pattern);
				embeddedPatterns.add(pattern);
			}
		}
		runMiners(rankResults, basename, output2, embeddedPatterns);
	}

	/**
	 * ===============================================================================================
	 * Mining
	 * ===============================================================================================
	 */
	private void runMiners(Map<String, Double> rankResults, String basename, File inputSequence, List<String> embeddedPatterns) throws IOException, Exception {
		//run FCI
		System.out.println("============== FCI ==============");
		MainDFSAlgorithm algo = new MainDFSAlgorithm(inputSequence, minsupFCI);
		double minCohFoundPattern = 99999;
		for(String pattern: embeddedPatterns) {
			double cohesionPattern = algo.cohesion(algo.toPattern(pattern));
			System.out.println("cohesion pattern " + pattern + ": " + cohesionPattern);
			minCohFoundPattern = Math.min(minCohFoundPattern, cohesionPattern);
		}
		double mincoh = minCohFoundPattern - 0.0001;
		//double mincoh = 0.02;
		File patterns = algo.minePatterns(maxlengthFCI, mincoh, false, false, false, 0.05, 0.0, 0.0);
		File patternsSorted = Utils.sortCSVFile(patterns, MainCommandLine.SortItemsets, true);
		File fciPatternsFile = new File(basename + "_patterns_fci.csv");
		patternsSorted.renameTo(fciPatternsFile);
		Utils.printHead(fciPatternsFile,30);
		Pair<Double,Double> rankFCI = averageRankParallelEpisodeFCI(fciPatternsFile, embeddedPatterns);
		rankResults.put("FCI_seq", rankFCI.getSecond());

		//Run winepi
		System.out.println("============== WINEPI ==============");
		File patternsSortedWinepi = WrapperClosedEpisodesMining.runMethod(inputSequence, null, thresholdWinepi, window,
				MIN_WINDOWS_OFF, WEIGHTED_WINDOWS_OFF, ITEMSETS_ON, SP_ON, GENERAL_EPI_OFF, SPARSE_ON);
		File winepiPatternsFile = new File(basename + "_patterns_winepi.csv");
		patternsSortedWinepi.renameTo(winepiPatternsFile);
		Utils.printHead(winepiPatternsFile,30);
		Pair<Double,Double> rankWinepi = averageRankParallelEpisodeClosepi(winepiPatternsFile, embeddedPatterns);
		rankResults.put("Winepi", rankWinepi.getSecond());
		if(rankWinepi.getFirst() ==-1 ) {
			patternsSortedWinepi = WrapperClosedEpisodesMining.runMethod(inputSequence, null, thresholdWinepi2, window2,
					MIN_WINDOWS_OFF, WEIGHTED_WINDOWS_OFF, ITEMSETS_ON, SP_ON, GENERAL_EPI_OFF, SPARSE_ON);
			patternsSortedWinepi.renameTo(winepiPatternsFile);
			Utils.printHead(winepiPatternsFile,30);
			rankWinepi = averageRankParallelEpisodeClosepi(winepiPatternsFile, embeddedPatterns);
			rankResults.put("Winepi", rankWinepi.getSecond());
		}

		//Run laxman: only itemsets
		System.out.println("============== LAXMAN ==============");
		File patternsSortedLaxman = WrapperClosedEpisodesMining.runMethod(inputSequence, null, thresholdLaxman, window,
				MIN_WINDOWS_ON, WEIGHTED_WINDOWS_OFF, ITEMSETS_ON, SP_ON, GENERAL_EPI_OFF, SPARSE_ON);
		File laxmanPatternsFile = new File(basename + "_patterns_laxman.csv");
		patternsSortedLaxman.renameTo(laxmanPatternsFile);
		Utils.printHead(laxmanPatternsFile,30);
		Pair<Double,Double> rankLaxman = averageRankParallelEpisodeClosepi(laxmanPatternsFile, embeddedPatterns);
		rankResults.put("Laxman", rankLaxman.getSecond());
		if(rankLaxman.getFirst() ==-1 ) {
			patternsSortedLaxman = WrapperClosedEpisodesMining.runMethod(inputSequence, null, thresholdLaxman2, window2,
					MIN_WINDOWS_ON, WEIGHTED_WINDOWS_OFF, ITEMSETS_ON, SP_ON, GENERAL_EPI_OFF, SPARSE_ON);
			patternsSortedLaxman.renameTo(laxmanPatternsFile);
			Utils.printHead(laxmanPatternsFile,30);
			rankLaxman = averageRankParallelEpisodeClosepi(laxmanPatternsFile, embeddedPatterns);
			rankResults.put("Laxman", rankLaxman.getSecond());
		}
		
		//Run weighted
		System.out.println("============== WEIGHTED ==============");
		File patternsSortedWeighted = WrapperClosedEpisodesMining.runMethod(inputSequence, null, thresholdMarbles, window, 
				MIN_WINDOWS_OFF, WEIGHTED_WINDOWS_ON, ITEMSETS_ON, SP_ON, GENERAL_EPI_OFF, SPARSE_ON);
		File weightedPatternsFile = new File(basename + "_patterns_weighted.csv");
		patternsSortedWeighted.renameTo(weightedPatternsFile);
		Utils.printHead(weightedPatternsFile,30);
		Pair<Double,Double> rankWeighted = averageRankParallelEpisodeClosepi(weightedPatternsFile, embeddedPatterns);
		rankResults.put("Marbles", rankWeighted.getSecond());
		if(rankWeighted.getFirst() ==-1 ) {
			patternsSortedWeighted = WrapperClosedEpisodesMining.runMethod(inputSequence, null, thresholdMarbles2, window2, 
					MIN_WINDOWS_OFF, WEIGHTED_WINDOWS_ON, ITEMSETS_ON, SP_ON, GENERAL_EPI_OFF, SPARSE_ON);
			patternsSortedWeighted.renameTo(weightedPatternsFile);
			Utils.printHead(weightedPatternsFile,30);
			rankWeighted = averageRankParallelEpisodeClosepi(weightedPatternsFile, embeddedPatterns);
			rankResults.put("Marbles", rankWeighted.getSecond());
		}
		
		//Run tatti compact minimal episodes
		System.out.println("============== CMW ==============");
		File patternsSortedTatti = WrapperCompactMinimalWindows.runMethod(inputSequence, thresholdCMW, window, alphaCMW);
		File cmwPatternsFile = new File(basename + "_patterns_cmw.csv");
		patternsSortedTatti.renameTo(cmwPatternsFile);
		Utils.printHead(cmwPatternsFile,30);
		Pair<Double,Double> rankCMW = averageRankParallelEpisodeCMW(cmwPatternsFile, embeddedPatterns);
		rankResults.put("CMW", rankCMW.getSecond());
		if(rankCMW.getFirst() == -1) {
			patternsSortedTatti = WrapperCompactMinimalWindows.runMethod(inputSequence, thresholdCMW2, window2, alphaCMW);
			patternsSortedTatti.renameTo(cmwPatternsFile);
			Utils.printHead(cmwPatternsFile,30);
			rankCMW = averageRankParallelEpisodeCMW(cmwPatternsFile, embeddedPatterns);
			rankResults.put("CMW", rankCMW.getSecond());
		}
	}

	/**
	 * ===============================================================================================
	 * Evaluation
	 * ===============================================================================================
	 */
	private <T> void saveResults(String path, T[] xAxis, List<Map<String, Double>> results) {
		System.out.println("====== RESULTS =======");
		String[] methods = new String[] {"FCI_seq", "Winepi", "Laxman", "Marbles", "CMW"};
		List<String> output = new ArrayList<>();
		String line = "x=[";
		for(T v: xAxis) {
			line+= String.valueOf(v);
			line+= ",";
		}
		line+= "]";
		output.add(line);
		line = "";
		for(String method: methods) {
			line=method;
			line+= "=[";
			for(Map<String,Double> result: results) {
				line+= String.format("%.1f", result.get(method));
				line+= ",";
			}
			line+= "]";
			output.add(line);
		}
		System.out.println(Utils.join(output, "\n"));
		try {
			Utils.saveFile(output, new File(path, "results_" + RUN + ".csv"));
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	private static Pair<Double,Double> averageRankParallelEpisodeClosepi(File episodes, List<String> patterns) throws IOException {
		double averageRank = 0.0;
		for(String pattern: patterns) {
			int rank = rankParallelEpisodeClosepi(episodes,pattern);
			if(rank == -1) {
				averageRank = -1;
				break;
			}
			else
				averageRank += rank / (double)patterns.size();
		}
		double averageRankCapped = 0.0;
		for(String pattern: patterns) {
			int rank = rankParallelEpisodeClosepi(episodes,pattern);
			averageRankCapped += rankTransform(rank) / (double)patterns.size();
		}
		return new Pair<Double,Double>(averageRank, averageRankCapped) ;
	}
	
	private static int rankTransform(int rank) {
		if(rank == -1) {
			return 1000;
		}
		else{
			return Math.min(rank,1000);
		}
	}
	//copy pasted
	private static Pair<Double,Double> averageRankParallelEpisodeCMW(File episodes, List<String> patterns) throws IOException {
		double averageRank = 0.0;
		for(String pattern: patterns) {
			int rank = rankParallelEpisodeCMW(episodes,pattern);
			if(rank == -1) {
				averageRank = -1;
				break;
			}
			else
				averageRank += rank / (double)patterns.size();
		}
		double averageRankCapped = 0.0;
		for(String pattern: patterns) {
			int rank = rankParallelEpisodeCMW(episodes,pattern);
			averageRankCapped += rankTransform(rank)  / (double)patterns.size();
		}
		return new Pair<Double,Double>(averageRank, averageRankCapped) ;
	}
	
	private static Pair<Double,Double> averageRankParallelEpisodeFCI(File episodes, List<String> patterns) throws IOException {
		double averageRank = 0.0;
		for(String pattern: patterns) {
			int rank = rankParrallelEpisodeFCI(episodes,pattern);
			if(rank == -1) {
				averageRank = -1;
				break;
			}
			else
				averageRank += rank  / (double)patterns.size();
		}
		double averageRankCapped = 0.0;
		for(String pattern: patterns) {
			int rank = rankParrallelEpisodeFCI(episodes,pattern);
			averageRankCapped += rankTransform(rank)  / (double)patterns.size();
		}
		return new Pair<Double,Double>(averageRank, averageRankCapped) ;
	}
	
	/*
	private static double averageRankSerialEpisodeClosepi(File episodes, List<String> patterns) throws IOException {
		double averageRank = 0.0;
		for(String pattern: patterns) {
			int rank = rankSerialEpisodeClosepi(episodes,pattern);
			averageRank += rank;
		}
		return averageRank / patterns.size();
	}
	
	private static double averageRankSerialEpisodeCMW(File episodes, List<String> patterns) throws IOException {
		double averageRank = 0.0;
		for(String pattern: patterns) {
			int rank = rankSerialEpisodeCMW(episodes,pattern);
			averageRank += rank;
		}
		return averageRank / patterns.size();
	}
	
	private static double averageRankSerialEpisodeFCI(File episodes, List<String> patterns) throws IOException {
		double averageRank = 0.0;
		for(String pattern: patterns) {
			int rank = rankSerialEpisodeFCI(episodes,pattern);
			averageRank += rank;
		}
		return averageRank / patterns.size();
	}
	
	*/
	
	private static int rankParallelEpisodeClosepi(File episodes, String pattern) throws IOException {
		String[] patternArgumentTokens = pattern.split(" ");
		int rank = 0;
		List<String> lines = Utils.readFileUntil(episodes, Integer.MAX_VALUE); //read Top-1000 patterns
		for(int i=0; i<lines.size();i++) {
			String line = lines.get(i);
			String[] tokens = line.split(";");
			if(tokens[0].contains(",")) //skip sequences
				continue;
			String[] items = tokens[0].split(" \\s*");
			if(matchItemset(patternArgumentTokens, items)) {
				System.out.println("rank:" + rank);
				return rank;
			}
			rank++;
		}
		System.out.println("rank: -1 (returning 1000)");
		return -1;
	}
	
	/*
	private static int rankSerialEpisodeClosepi(File episodes, String pattern) throws IOException {
		String[] patternArgumentTokens = pattern.split(" ");
		int rank = 0;
		List<String> lines = Utils.readFileUntil(episodes, Integer.MAX_VALUE); //read Top-1000 patterns
		for(int i=0; i<lines.size();i++) {
			String line = lines.get(i);
			String[] tokens = line.split(";");
			if(!tokens[0].contains(",")) //skip non-sequences
				continue;
			String[] items = tokens[0].split(",\\s*");
			if(matchSequence(patternArgumentTokens, items)) {
				System.out.println("rank:" + rank);
				return rank;
			}
			rank++;
		}
		System.out.println("rank: -1 (returning 1000)");
		return  -1;
	}
	*/
	//e.g.
	//episode_id;graph_nodes;graph_edges;graph_type;zone;zone_two;p_one;p_two;sample;occurrence;mean;std
	//18499;4 9 ;;parallel;9.968244;9.968244;1.000000;0.000000;0.057995;659;0.096904;0.276005
	private static int rankParallelEpisodeCMW(File episodes, String pattern) throws IOException {
		String[] patternArgumentTokens = pattern.split(" ");
		int rank = 0;
		List<String> lines = Utils.readFileUntil(episodes, Integer.MAX_VALUE); //read Top-1000 patterns
		for(int i=1; i<lines.size();i++) {
			String line = lines.get(i);
			String[] tokens = line.split(";");
			if(!tokens[3].contains("parallel")) //skip non-itemsets
				continue;
			String[] items = tokens[1].split(" \\s*");
			if(matchItemset(patternArgumentTokens, items)) {
				System.out.println("rank:" + rank);
				return rank;
			}
			rank++;
		}
		System.out.println("rank: -1 (returning 1000)");
		return -1;
	}
	
	/*
	private static int rankSerialEpisodeCMW(File episodes, String pattern) throws IOException {
		String[] patternArgumentTokens = pattern.split(" ");
		int rank = 0;
		List<String> lines = Utils.readFileUntil(episodes, Integer.MAX_VALUE); //read Top-1000 patterns
		for(int i=1; i<lines.size();i++) {
			String line = lines.get(i);
			String[] tokens = line.split(";");
			if(!tokens[3].contains("serial")) //skip non-serial episodes
				continue;
			String[] items = ExperimentUtils.serialToSequenceLabel(tokens[2]).split(",\\s*");
			if(matchSequence(patternArgumentTokens, items)) {
				System.out.println("rank:" + rank);
				return rank;
			}
			rank++;
		}
		System.out.println("rank: -1 (returning 1000)");
		return -1;
	}
	
	private static int rankSerialEpisodeFCI(File episodes, String pattern) throws IOException {
		String[] patternArgumentTokens = pattern.split(" ");
		int rank = 0;
		List<String> lines = Utils.readFileUntil(episodes, Integer.MAX_VALUE); //read TOP-1000 patterns
		for(int i=1; i<lines.size();i++) {
			String line = lines.get(i);
			String[] tokens = line.split(";");
			String[] items = tokens[0].replace("<","").replace(">","").split(",");
			if(matchSequence(patternArgumentTokens, items)) {
				System.out.println("rank:" + rank);
				return rank;
			}
			rank++;
		}
		System.out.println("rank: -1 (returning 1000)");
		return -1;
	}

	private static boolean matchSequence(String[] tokens1, String[] tokens2) {
		if(tokens1.length != tokens2.length)
			return false;
		for(int i=0; i<tokens1.length; i++) {
			if(!tokens1[i].equals(tokens2[i])) {
				return false;
			}
		}
		return true;	
	}
	*/
	
	private static int rankParrallelEpisodeFCI(File episodes, String pattern) throws IOException {
		String[] patternArgumentTokens = pattern.split(" ");
		int rank = 0;
		List<String> lines = Utils.readFileUntil(episodes, Integer.MAX_VALUE); //read TOP-1000 patterns
		String[] lastItems = new String[0];
		for(int i=1; i<lines.size();i++) {
			String line = lines.get(i);
			String[] tokens = line.split(";");
			String[] items = tokens[0].replace("<","").replace(">","").split(" "); //"," for sequences
			if(matchItemset(patternArgumentTokens, items)) {
				System.out.println("rank:" + rank);
				return rank;
			}
			if(!matchItemset(items,lastItems)) {
				rank++;
			}
			lastItems = items;	
		}
		System.out.println("rank: -1 (returning 1000)");
		return -1;
	}
	
	
	private static boolean matchItemset(String[] tokens1, String[] tokens2) {
		for(String s: tokens1) {
			if(!ArrayUtils.contains(tokens2, s)) {
				return false;
			}
		}
		for(String s: tokens2) {
			if(!ArrayUtils.contains(tokens1, s)) {
				return false;
			}
		}
		return true;
	}
	
}
		

