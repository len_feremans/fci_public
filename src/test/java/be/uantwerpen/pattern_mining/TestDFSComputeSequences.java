package be.uantwerpen.pattern_mining;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.Test;

import be.uantwerpen.pattern_mining.preprocessing.DatLabFormatConverter;
import be.uantwerpen.util.Timer;
import be.uantwerpen.util.Utils;

public class TestDFSComputeSequences {

	@Test
	public void testDFSSequentialPatternsTestCase() throws IOException{
		System.out.println("======= TEST DFS COMPUTATION 1 =======");
		Timer.VERBOSE = false;
		String data = "abceeeabc";
		File[] files =  TestUtils.makeTestFilesForLetterSequence(data);
		System.out.println("data:" + data);
		MainDFSAlgorithm service = new MainDFSAlgorithm(files[0],files[1], 1);
		MainDFSAlgorithm.DEBUG_DFS = true;
		service.minePatterns(1000,0.001, true, false, false, 0.8, 0.0, 0.0);
		File sorted = Utils.sortCSVFile(service.getOutput(),1, true); //on support
		Utils.printHead(sorted);
	}

	@Test
	public void testDFSSequentialPatternsFragmentMoby() throws IOException{
		System.out.println("======= TEST DFS COMPUTATION 2: Moby fragment =======");
		makeTestFile();
		MainDFSAlgorithm service = new MainDFSAlgorithm(new File("moby-frag.lab"),new File("moby-frag.dat"), 3);
		double d = service.cohesion(service.toPattern("moby dick"));
		System.out.println("cohesion moby dick:" + d);
		double d2 = service.cohesion(service.toPattern("white whale"));
		System.out.println("cohesion white whale:" + d2);
		double d3 = service.cohesion(service.toPattern("captain ahab"));
		System.out.println("cohesion captain ahab:" + d3);
		File output = service.minePatterns(5, 0.1, true, false, false, 0.7, 0.0, 0.0);
		File outputSorted = Utils.sortCSVFile(output, 2, true); //sort on cohesion
		Utils.printHead(outputSorted,100);
	}
	
	private void makeTestFile() throws IOException {
		String data = "All ye mast-headers have before now heard me give orders about a white whale. Look ye! d'ye see this Spanish ounce of gold?\"—holding up a broad bright coin to the sun—\"it is a sixteen dollar piece, men. D'ye see it? Mr. Starbuck, hand me yon top-maul.\" While the mate was getting the hammer, Ahab, without speaking, was slowly rubbing the gold piece against the skirts of his jacket, as if to heighten its lustre, and without using any words was meanwhile lowly humming to himself, producing a sound so strangely muffled and inarticulate that it seemed the mechanical humming of the wheels of his vitality in him. Receiving the top-maul from Starbuck, he advanced towards the main-mast with the hammer uplifted in one hand, exhibiting the gold with the other, and with a high raised voice exclaiming: \"Whosoever of ye raises me a white-headed whale with a wrinkled brow and a crooked jaw; whosoever of ye raises me that white-headed whale, with three holes punctured in his starboard fluke—look ye, whosoever of ye raises me that same white whale, he shall have this gold ounce, my boys!\" \"Huzza! huzza!\" cried the seamen, as with swinging tarpaulins they hailed the act of nailing the gold to the mast. \"It's a white whale, I say,\" resumed Ahab, as he threw down the topmaul: \"a white whale. Skin your eyes for him, men; look sharp for white water; if ye see but a bubble, sing out.\" All this while Tashtego, Daggoo, and Queequeg had looked on with even more intense interest and surprise than the rest, and at the mention of the wrinkled brow and crooked jaw they had started as if each was separately touched by some specific recollection. \"Captain Ahab,\" said Tashtego, \"that white whale must be the same that some call Moby Dick.\" \"Moby \"Dick\"Dick?\" shouted Ahab. \"Do ye know the white whale then, Tash?\" \"Does he fan-tail a little curious, sir, before he goes down?\" said the Gay-Header deliberately. \"And has he a curious spout, too,\" said Daggoo, \"very bushy, even for a parmacetty, and mighty quick, Captain Ahab?\" \"And he have one, two, three—oh! good many iron in him hide, too, Captain,\" cried Queequeg disjointedly, \"all twiske-tee be-twisk, like him—him—\" faltering hard for a word, and screwing his hand round and round as though uncorking a bottle—\"like him—him—\" \"Corkscrew!\" cried Ahab, \"aye, Queequeg, the harpoons lie all twisted and wrenched in him; aye, Daggoo, his spout is a big one, like a whole shock of wheat, and white as a pile of our Nantucket wool after the great annual sheep-shearing; aye, Tashtego, and he fan-tails like a split jib in a squall. Death and devils! men, it is Moby Dick ye have seen—Moby Dick—Moby Dick!";
		data = data.replaceAll("-", " ");
		data = data.replaceAll("—", " ");
		data = data.replaceAll("\"", " ");
		data = data.replaceAll("\\.", " ");
		data = data.replaceAll("\\?", " ");
		data = data.replaceAll("\\!", " ");
		data = data.replaceAll("\\,", " ");
		data = data.replaceAll("\\;", " ");
		data = data.toLowerCase();
		FileWriter writer = new FileWriter(new File("moby-frag.txt"));
		writer.write(data);
		writer.close();
		DatLabFormatConverter.convertNormal2DatLab(new File("moby-frag.txt"),new File("moby-frag.dat"), new File("moby-frag.lab"));
	}

}
