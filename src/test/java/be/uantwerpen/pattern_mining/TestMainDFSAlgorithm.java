package be.uantwerpen.pattern_mining;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.junit.Test;

import be.uantwerpen.pattern_mining.ComputeMinimalWindows.Window;
import be.uantwerpen.pattern_mining.data.MainDFSDatastructure;
import be.uantwerpen.util.Timer;
import be.uantwerpen.util.Utils;

/**
 * Testing computation in MainDFSAlgorithm
 * 
 * @author lfereman
 *
 */
public class TestMainDFSAlgorithm {

	@Test
	public void testLoadData() throws IOException{
		System.out.println("======= TEST WINDOW COMPUTATION =======");
		String data = "abceeeabc";
		File[] files = TestUtils.makeTestFilesForLetterSequence(data);
		String pattern = "a b";
		System.out.println("data:" + data + " pattern:" + pattern);
		MainDFSDatastructure.DEBUG_LOAD_DATA = true;
		MainDFSAlgorithm service = new MainDFSAlgorithm(files[0], files[1], 1);
		MainDFSDatastructure.DEBUG_LOAD_DATA = false;
	}
	
	@Test
	public void testLoadDataSparse() throws IOException{
		System.out.println("======= TEST WINDOW COMPUTATION =======");
		String data = "abceeeabc";
		//sparse input:
		FileWriter writer = new FileWriter("./temp/sparse_test_file.txt");
		for(int i=0; i<data.length(); i++){
			writer.write(String.format("%d %d\n", i, (int)(data.charAt(i) - 'a')));
		}
		writer.close();
		String pattern = "0 1";
		System.out.println("data:" + data + " pattern:" + pattern);
		MainDFSAlgorithm service = new MainDFSAlgorithm(new File("./temp/sparse_test_file.txt"), 1);
		double w= service.computerMinimalWindows.sumMinimalWindows(service.toPattern(pattern));
		System.out.println(w);
		List<Window> windows = service.computerMinimalWindows.getMinimalWindowsLastRun();
		for(Window window: windows){
			System.out.println(window);
		}
		assertEquals(w,8.0,0.001);
		assertTrue(windows.get(0).samePosAndSize(new Window(0,2)));
		assertTrue(windows.get(1).samePosAndSize(new Window(1,2)));
		assertTrue(windows.get(2).samePosAndSize(new Window(6,2)));
		assertTrue(windows.get(3).samePosAndSize(new Window(7,2)));
	}
	
	@Test
	public void testCohesion1() throws IOException{
		System.out.println("======= TEST COHESION COMPUTATION =======");
		String data = "abceeeabc";
		File[] files =  TestUtils.makeTestFilesForLetterSequence(data);
		System.out.println("data:" + data);
		MainDFSAlgorithm service = new MainDFSAlgorithm(files[0],files[1], 1);
		for(String pattern: new String[]{"a","a b","a b c","e c"}){
			int[] p = service.toPattern(pattern);
			double cohesion = service.cohesion(p);
			System.out.format("pattern %s, cohesion: %f\n", pattern, cohesion);
			assertEquals(pattern.contains("a")?1.0:0.666, cohesion, 0.01);
		}
	}
	
	@Test
	public void testMineParallel() throws IOException{
		System.out.println("======= TEST DFS COMPUTATION =======");
		Timer.VERBOSE = false;
		String data = "abceeeabc";
		File[] files =  TestUtils.makeTestFilesForLetterSequence(data);
		System.out.println("data:" + data);
		MainDFSAlgorithm service = new MainDFSAlgorithm(files[0],files[1], 1);
		MainDFSAlgorithm.DEBUG_DFS = true;
		service.minePatterns(1000,0.001);
		Utils.printHead(service.getOutput());
	}

	@Test
	public void testMineParallel2() throws IOException{
		System.out.println("======= TEST DFS COMPUTATION =======");
		Timer.VERBOSE = false;
		String data = "acefbaacdeafdg";
		File[] files =  TestUtils.makeTestFilesForLetterSequence(data);
		System.out.println("data:" + data);
		MainDFSAlgorithm service = new MainDFSAlgorithm(files[0],files[1], 1);
		MainDFSAlgorithm.DEBUG_DFS = false;
		service.minePatterns(1000,0.001);
		Utils.printHead(service.getOutput());
	}
	
}
