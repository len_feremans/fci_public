package be.uantwerpen.pattern_mining;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.junit.Test;

import be.uantwerpen.pattern_mining.ComputeMinimalWindowsSequence.SequenceWindow;

public class TestComputeSequences {
	
	@Test
	public void testSequentialPatterns1() throws IOException{
		String data = "abceeeabc";
		String pattern = "e c";
		testSequentialPattern(data,pattern);
	}
	
	@Test
	public void testSequentialPatterns2() throws IOException{
		String data = "abcbd";
		String pattern = "a b c d";
		testSequentialPattern(data,pattern);
	}


	@Test
	public void testSequentialPatterns3() throws IOException{
		String data = "xxxxxabcbcdxxxx";
		String pattern = "a b c d";
		testSequentialPattern(data, pattern);
	}
	
	public void testSequentialPattern(String data, String pattern) throws IOException{
		File[] files =  TestUtils.makeTestFilesForLetterSequence(data);
		MainDFSAlgorithm service = new MainDFSAlgorithm(files[0], files[1], 1);
		ComputeSequentialPatterns computeSequences = new ComputeSequentialPatterns(service.data);
		System.out.println("======= TEST SEQUENTIAL PATTERN 1 =======");
		System.out.println("data:" + data + " pattern:" + pattern);
		double w= service.computerMinimalWindowsSequence.sumMinimalWindowsForSequences(service.toPattern(pattern));
		System.out.println(w);
		List<SequenceWindow> windows = service.computerMinimalWindowsSequence.getMinimalSequenceWindowsLastRun();
		for(SequenceWindow window: windows){
			System.out.println(window);
		}
		int[] x = service.toPattern(pattern);
		service.computeSequences.computeWindowOccurenceFilteredPositionsListsForSequences(x, windows);
		ArrayList<Entry<String,Integer>> sequences = computeSequences.findSequentialPatterns(x, windows);
		for(Entry<String, Integer> e: sequences){
			int supp = e.getValue();
			System.out.format("%s supp=%d\n", e.getKey(), supp);
		}
	}
	
}