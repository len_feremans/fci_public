package be.uantwerpen.pattern_mining;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import be.uantwerpen.util.Timer;
import be.uantwerpen.util.Utils;

public class TestDFSComputeEpisodes {

	@Test
	public void testDFSEpisodes() throws IOException{
		System.out.println("======= TEST DFS COMPUTATION 1=======");
		Timer.VERBOSE = false;
		String data = "abc...acb";
		File[] files =  TestUtils.makeTestFilesForLetterSequence(data);
		System.out.println("data:" + data);
		MainDFSAlgorithm service = new MainDFSAlgorithm(files[0],files[1], 1);
		MainDFSAlgorithm.DEBUG_DFS = true;
		service.setFilterTotalOrders(true);
		service.minePatterns(1000,0.00,true,true,false,0.5,0.5,0.0);
		File outputSorted = Utils.sortCSVFile(service.getOutput(), 2, true);
		Utils.printHead(outputSorted);
	}

	@Test
	public void testDFSEpisodesFragmentMoby() throws IOException{
		System.out.println("======= TEST DFS COMPUTATION 2: Moby Fragment=======");
		MainDFSAlgorithm service = new MainDFSAlgorithm(new File("moby-frag.lab"),new File("moby-frag.dat"), 3);
		service.setFilterTotalOrders(true);
		File output = service.minePatterns(5, 0.1, true, true, false, 0.6, 0.6, 0.0);
		File outputSorted = Utils.sortCSVFile(output, 2, true);
		Utils.printHead(outputSorted);
		//filter on occurence_ratio sequences
		//moby dick ;11;0.956522;po;[moby->dick];11;1.000;;
		Utils.printHead(outputSorted,250);	
	}


}
