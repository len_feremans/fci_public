package be.uantwerpen.pattern_mining;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.junit.Test;

import com.google.common.primitives.Ints;

import be.uantwerpen.pattern_mining.ComputeMinimalWindows.Window;
import be.uantwerpen.pattern_mining.ComputeMinimalWindowsSequence.SequenceWindow;
import be.uantwerpen.pattern_mining.ComputeRules.Rule;
import be.uantwerpen.pattern_mining.data.MainDFSDatastructure;
import be.uantwerpen.util.FileStream;
import be.uantwerpen.util.Pair;

public class TestSinglePattern {
	
	//Sequences
	@Test
	public void testOnTrumpCase1() throws IOException {
		File inputProcessed_as_dat= new File("./data/trump_tweets_processed.dat");
		File inputProcessed_as_lab= new File("./data/trump_tweets_processed.lab");
		String pattern = "make america great";
		detailedAnalysisSinglePattern(inputProcessed_as_dat, inputProcessed_as_lab, pattern);
	}
	@Test
	public void testOnTrumpCase2() throws IOException {
		File inputProcessed_as_dat= new File("./data/trump_tweets_processed.dat");
		File inputProcessed_as_lab= new File("./data/trump_tweets_processed.lab");
		String pattern = "migrat chain";
		detailedAnalysisSinglePattern(inputProcessed_as_dat, inputProcessed_as_lab, pattern);
	}
	
	
	//Episodes
	@Test
	public void testOnOriginOfSpeciesEpisodeCase1() throws IOException {
		File inputProcessed_as_dat= new File("./data/origin_of_species_processed.dat");
		File inputProcessed_as_lab= new File("./data/origin_of_species_processed.lab");
		String pattern = "eject foster brother";
		detailedAnalysisSinglePattern(inputProcessed_as_dat, inputProcessed_as_lab, pattern);
	}
	
	@Test
	public void testOnOriginOfSpeciesEpisodeCase2() throws IOException {
		File inputProcessed_as_dat= new File("./data/origin_of_species_processed.dat");
		File inputProcessed_as_lab= new File("./data/origin_of_species_processed.lab");
		String pattern = "hexagon prism sphere";
		detailedAnalysisSinglePattern(inputProcessed_as_dat, inputProcessed_as_lab, pattern);
	}
	
	@Test
	public void testOnOriginOfSpeciesEpisodeCase3() throws IOException {
		File inputProcessed_as_dat= new File("./data/origin_of_species_processed.dat");
		File inputProcessed_as_lab= new File("./data/origin_of_species_processed.lab");
		String pattern = "leptali ithomia mimick";
		detailedAnalysisSinglePattern(inputProcessed_as_dat, inputProcessed_as_lab, pattern);
	}
	
	@Test
	public void testOnOriginOfSpeciesEpisodeCase4() throws IOException {
		File inputProcessed_as_dat= new File("./data/origin_of_species_processed.dat");
		File inputProcessed_as_lab= new File("./data/origin_of_species_processed.lab");
		String pattern = "tierra fuego";
		detailedAnalysisSinglePattern(inputProcessed_as_dat, inputProcessed_as_lab, pattern);
	}
	
	
	//Rules
	@Test
	public void testOnOriginOfSpeciesRuleCase1() throws IOException {
		File inputProcessed_as_dat= new File("./data/origin_of_species_processed.dat");
		File inputProcessed_as_lab= new File("./data/origin_of_species_processed.lab");
		String pattern = "tierra fuego del";
		detailedAnalysisSinglePattern(inputProcessed_as_dat, inputProcessed_as_lab, pattern);
	}
	
	@Test
	public void testOnOriginOfSpeciesRuleCase2() throws IOException {
		File inputProcessed_as_dat= new File("./data/origin_of_species_processed.dat");
		File inputProcessed_as_lab= new File("./data/origin_of_species_processed.lab");
		String pattern = "divis kingdom anim";
		detailedAnalysisSinglePattern(inputProcessed_as_dat, inputProcessed_as_lab, pattern);
	}
	
	@Test
	public void testOnOriginOfSpeciesRuleCase3() throws IOException {
		File inputProcessed_as_dat= new File("./data/origin_of_species_processed.dat");
		File inputProcessed_as_lab= new File("./data/origin_of_species_processed.lab");
		String pattern = "divis kingdom";
		detailedAnalysisSinglePattern(inputProcessed_as_dat, inputProcessed_as_lab, pattern);
	}
	
	/*
	@Test
	public void testDavidCopperFieldRuleCase1() throws IOException {
		File inputProcessed_as_dat= new File("./data/dutch_david_copperfield._processed.dat");
		File inputProcessed_as_lab= new File("./data/dutch_david_copperfield._processed.lab");
		String pattern = "x n e _";
		detailedAnalysisSinglePattern(inputProcessed_as_dat, inputProcessed_as_lab, pattern);
	}
	
	@Test
	public void testDavidCopperFieldRuleCase2() throws IOException {
		File inputProcessed_as_dat= new File("./data/dutch_david_copperfield._processed.dat");
		File inputProcessed_as_lab= new File("./data/dutch_david_copperfield._processed.lab");
		String pattern = "x e _";
		detailedAnalysisSinglePattern(inputProcessed_as_dat, inputProcessed_as_lab, pattern);
		//SLOW
	}
	
	@Test
	public void testDavidCopperFieldRuleCase3() throws IOException {
		File inputProcessed_as_dat= new File("./data/dutch_david_copperfield._processed.dat");
		File inputProcessed_as_lab= new File("./data/dutch_david_copperfield._processed.lab");
		String pattern = "q u";
		detailedAnalysisSinglePattern(inputProcessed_as_dat, inputProcessed_as_lab, pattern);
		//SLOW
	}
	*/
	
	private void detailedAnalysisSinglePattern(File inputProcessed_as_dat, File inputProcessed_as_lab, String pattern) throws IOException {
		MainDFSAlgorithm service = new MainDFSAlgorithm(inputProcessed_as_lab, inputProcessed_as_dat, 5);
		MainDFSDatastructure.DEBUG_LOAD_DATA = false;
		//debug window sequences:
		//  1) load sequence as not-indexed single sequence
		FileStream fs = new FileStream(inputProcessed_as_dat,' ','\n');
		String token = fs.nextToken();
		List<String> sequenceNormal = new ArrayList<String>();
		while(token != null){
			String label = service.data.getLabelOriginal(Integer.valueOf(token));
			sequenceNormal.add(label);
			token = fs.nextToken();
		}

		//basics:
		System.out.println("Pattern: " + pattern);
		int[] x = service.toPattern(pattern);
		System.out.println("coh:" + service.cohesion(x));
		System.out.println("sup:" + service.frequency(x));
		for(int i=0; i<x.length; i++) {
			System.out.println("sup " + pattern.split(" ")[i] + " (code=" + x[i] + "): " + service.frequency(new int[] {x[i]}));
		}
		//windows standard:
		service.computerMinimalWindows.sumMinimalWindows(x);
		List<Window> wins = service.computerMinimalWindows.getMinimalWindowsLastRun();
		System.out.println("windows sizes:");
		int[] counter = {0,0,0,0,0}; //cohesion of 1.0, 0.5, 0.1, or 0.01, or 0.001
		for(Window win: wins) {
			if(win.size == x.length) {
				counter[0]+=1;
			}
			else if(win.size <=  x.length * 2) {
				counter[1]+=1;
			}
			else if(win.size <=  x.length * 10) {
				counter[2]+=1;
			}
			else if(win.size <=  x.length * 100) {
				counter[3]+=1;
			}
			else {
				counter[4]+=1;
			}
		}
		System.out.println("windows sizes / item:");
		for(String s: pattern.split(" ")) {
			int[] counter2 = {0,0,0,0,0}; //cohesion of 1.0, 0.5, 0.1, or 0.01, or 0.001
			for(Window win: wins) {
				if(!sequenceNormal.get(win.position).equals(s)) {
					continue;
				}
				if(win.size == x.length) {
					counter2[0]+=1;
				}
				else if(win.size <=  x.length * 2) {
					counter2[1]+=1;
				}
				else if(win.size <=  x.length * 10) {
					counter2[2]+=1;
				}
				else if(win.size <=  x.length * 100) {
					counter2[3]+=1;
				}
				else {
					counter2[4]+=1;
				}
			}
			System.out.println("cohesion (x->X)" + s + "of 1.0, 0.5, 0.1, or 0.01, or 0.001" + Arrays.toString(counter2));
		}
		//windows sequences:
		System.out.println("sumMinimalWindowsForSequences");
		service.computerMinimalWindowsSequence.sumMinimalWindowsForSequences(x);
		List<SequenceWindow> swindows = service.computerMinimalWindowsSequence.getMinimalSequenceWindowsLastRun();
		//System.out.println("swindows:" + swindows);
		//  2) print projection of windows in sequence
		System.out.println("projection of windows in sequence:");
		int n =0;
		for(SequenceWindow win: swindows) {
			int k=0;
			for(int[] instance: win.minimalWindowsOccurences) {
				int start = Ints.min(instance);
				int end = Ints.max(instance);
				//System.out.println("  window " + n + " instance " + k + " [" + start + "-" + end + "]: " + sequenceNormal.subList(start, end+1));
				k++;
			}
			
			n++;
		}
		//sequential patterns:
		System.out.println("computeWindowOccurenceFilteredPositionsListsForSequences");
		service.computeSequences.computeWindowOccurenceFilteredPositionsListsForSequences(x, swindows);
		//System.out.println("windows with filtered pos lists:" + swindows);
		System.out.println("findSequentialPatterns");
		ArrayList<Entry<String, Integer>> sequentialPatterns = service.computeSequences.findSequentialPatterns(x, swindows);
		//System.out.println("sequential patterns: " + sequentialPatterns);
		System.out.println("computeWindowOccurenceNormalPositionsLists");
		service.computeEpisodes.computeWindowOccurenceNormalPositionsLists(x, swindows);
		//episodes
		//System.out.println("windows with normal pos lists:" + swindows);
		System.out.println("findDominantPartialOrder");
		Pair<Set<String>, Integer> data = service.computeEpisodes.findDominantPartialOrder(x, 0.7, sequentialPatterns, swindows);
		System.out.println("dominant epi:" + data.getFirst());
		System.out.println("support epi:" + data.getSecond());
		//rules
		System.out.println("Rules");
		System.out.println("findRulesFromItemSet");
		List<Rule> rules = service.computeRules.findRulesFromItemSet(x, 0.00001);
		for(Rule r: rules) {
			System.out.println(r.antecedent + "->" + r.consequent + " conf: " + r.conf);
		}
	}
}
