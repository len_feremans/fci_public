package be.uantwerpen.pattern_mining;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.junit.Test;

import be.uantwerpen.pattern_mining.ComputeMinimalWindowsSequence.SequenceWindow;
import be.uantwerpen.util.Pair;

public class TestComputeEpisodes {

	@Test 
	public void testCheckIsTotalOrder(){
		System.out.println(ComputeEpisodes.checkIsTotalOrder("[cruet->ham, vinegar->cruet]"));
	}

	@Test
	public void testEpisodes1() throws IOException{
		String data = "abc....abc....bac....bac....bac....cba";
		String pattern = "a b c";
		testEpisodes(data, pattern, 0.7);
	}
	
	@Test
	public void testEpisodes2() throws IOException{
		String data = "abcbd......acbcbd....";
		String pattern = "a b c d";
		testEpisodes(data, pattern, 0.7);
	}

	@Test
	public void testEpisodes3() throws IOException{
		String data = "abc......abc......acb";
		String pattern = "a b c";
		testEpisodes(data,pattern,0.5);
	}
	
	@Test
	public void testEpisodes4() throws IOException{
		String data = "abc.....cab";
		String pattern = "a b c";
		testEpisodes(data,pattern,0.5);
	}
	
	@Test
	public void testEpisodes5B() throws IOException{
		String data = "abc.....cba";
		String pattern = "a b c";
		testEpisodes(data,pattern,0.9);
	}
	
	@Test
	public void testEpisodes5() throws IOException{
		String data = "abcab.....abcab...abcab....";
		String pattern = "a b c";
		testEpisodes(data,pattern,0.7);
	}
	
	public void testEpisodes(String data, String pattern, double min_occ_ratio) throws IOException{
		File[] files =  TestUtils.makeTestFilesForLetterSequence(data);
		MainDFSAlgorithm service = new MainDFSAlgorithm(files[0], files[1], 1);
		ComputeEpisodes computeEpisodes = new ComputeEpisodes(service.data);
		ComputeSequentialPatterns computeSequence = new ComputeSequentialPatterns(service.data);
		
		System.out.println("======= TEST EPISODE PATTERN COMPUTATION  =======");
		System.out.println("data:" + data + " pattern:" + pattern);
		int[] x = service.toPattern(pattern);
		double w= service.computerMinimalWindowsSequence.sumMinimalWindowsForSequences(x);
		System.out.println("sum of windows:" + w);
		System.out.println("windows:");
		List<SequenceWindow> windows = service.computerMinimalWindowsSequence.getMinimalSequenceWindowsLastRun();
		for(SequenceWindow window: windows){
			System.out.println(window);
		}	
		service.computeSequences.computeWindowOccurenceFilteredPositionsListsForSequences(x, windows);
		List<Entry<String,Integer>> sequences = computeSequence.findSequentialPatterns(x, windows);
		System.out.println("sequences:");
		for(Entry<String,Integer> sequence: sequences){
			System.out.format("%s: %d\n",sequence.getKey(), sequence.getValue());
		}
		computeEpisodes.computeWindowOccurenceNormalPositionsLists(x, windows);
		Pair<Set<String>,Integer> partialOrders90 = computeEpisodes.findDominantPartialOrder(x, min_occ_ratio, sequences, windows);
		System.out.format("dominant episode (min_occ_ratio=%.3f): %s supp=%d", min_occ_ratio, partialOrders90.getFirst(), partialOrders90.getSecond());
	}
	

}
