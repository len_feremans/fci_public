package be.uantwerpen.pattern_mining;

import java.io.File;

import org.junit.Test;

import be.uantwerpen.util.Utils;

public class TestDFSComputeRules {
	
	File lab = new File("moby-frag.lab");
	File dat = new File("moby-frag.dat");
	int min_sup = 3;
	double min_conf = 0.3;
	
	@Test
	public void mineRulesOnMobyFragment() throws Exception{
		MainDFSAlgorithm service = new MainDFSAlgorithm(lab, dat, min_sup);
		File output = service.minePatterns(5, 0.1, false, false, true, 0.0, 0.0, min_conf);
		File outputSorted = Utils.sortCSVFile(output, 1, true);
		Utils.printHead(outputSorted,100);
	}
}
