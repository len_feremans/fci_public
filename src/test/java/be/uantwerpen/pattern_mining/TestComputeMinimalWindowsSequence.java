package be.uantwerpen.pattern_mining;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.Test;

import be.uantwerpen.pattern_mining.ComputeMinimalWindowsSequence.SequenceWindow;

public class TestComputeMinimalWindowsSequence {
	@Test
	public void testSequenceWindowWidth1() throws IOException{
		System.out.println("======= TEST WINDOW COMPUTATION =======");
		String data = "abaxxxxabaxxxxxaxbxa";
		File[] files = TestUtils.makeTestFilesForLetterSequence(data);
		String pattern = "a b";
		System.out.println("data:" + data + " pattern:" + pattern);
		MainDFSAlgorithm service = new MainDFSAlgorithm(files[0], files[1], 1);
		double w= service.computerMinimalWindowsSequence.sumMinimalWindowsForSequences(service.toPattern(pattern));
		System.out.println(w);
		List<SequenceWindow> windows = service.computerMinimalWindowsSequence.getMinimalSequenceWindowsLastRun();
		for(SequenceWindow window: windows){
			System.out.println(window);
		}
		//assertEquals(w,8.0,0.001);
	}
	 
	@Test
	public void testSequenceWindowWidth2() throws IOException{
		System.out.println("======= TEST WINDOW COMPUTATION =======");
		String data = "abbbbd";
		File[] files =  TestUtils.makeTestFilesForLetterSequence(data);
		String pattern = "a b d";
		System.out.println("data:" + data + " pattern:" + pattern);
		MainDFSAlgorithm service = new MainDFSAlgorithm(files[0],files[1], 0);
		double w= service.computerMinimalWindowsSequence.sumMinimalWindowsForSequences(service.toPattern(pattern));
		System.out.println(w);
		List<SequenceWindow> windows = service.computerMinimalWindowsSequence.getMinimalSequenceWindowsLastRun();
		for(SequenceWindow window: windows){
			System.out.println(window);
		}
		//assertEquals(w,61,0.001);
	}
	
	@Test
	public void testtSequenceWindowWidth3() throws IOException{
		System.out.println("======= TEST WINDOW COMPUTATION =======");
		String data = "abecbed";
		File[] files =  TestUtils.makeTestFilesForLetterSequence(data);
		String pattern = "a b c d e";
		System.out.println("data:" + data + " pattern:" + pattern);
		MainDFSAlgorithm service = new MainDFSAlgorithm(files[0], files[1], 1);
		double w= service.computerMinimalWindowsSequence.sumMinimalWindowsForSequences(service.toPattern(pattern));
		System.out.println(w);
		List<SequenceWindow> windows = service.computerMinimalWindowsSequence.getMinimalSequenceWindowsLastRun();
		for(SequenceWindow window: windows){
			System.out.println(window);
		}
		//assertEquals(w,15.0,0.001);
	}
}
