package be.uantwerpen.pattern_mining;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TestUtils {
	
	public static File[] makeTestFilesForLetterSequence(String sequence) throws IOException{
		char[] chars = sequence.toCharArray();
		List<Character> unique = new ArrayList<Character>();
		for(char ch: chars){
			boolean found = unique.contains(new Character(ch));
			if(!found){
				unique.add(ch);
			}
		}
		List<Integer> sequenceLst = new ArrayList<Integer>();
		for(char ch: chars){
			sequenceLst.add(unique.indexOf(new Character(ch)));
		}
		//save:
		String fname = sequence;
		if(sequence.length() > 10){
			fname = sequence.substring(0, 10);
		}
		FileWriter fw = new FileWriter("./temp/" + fname  + ".lab");
		for(Character ch: unique){
			fw.write(ch);
			fw.write("\n");
		}
		fw.close();
		fw = new FileWriter("./temp/" + fname  + ".dat");
		for(Integer i: sequenceLst){
			fw.write("" + i);
			fw.write(" ");
		}
		fw.close();
		return new File[]{new File("./temp/" + fname  + ".lab"), new File("./temp/" + fname + ".dat")};
	}
}
