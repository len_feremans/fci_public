package be.uantwerpen.pattern_mining;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.Test;

import be.uantwerpen.pattern_mining.ComputeMinimalWindows.Window;

public class TestComputeMinimalWindows {
	
	@Test
	public void testWindowWidth1() throws IOException{
		System.out.println("======= TEST WINDOW COMPUTATION =======");
		String data = "abceeeabc";
		File[] files = TestUtils.makeTestFilesForLetterSequence(data);
		String pattern = "a b";
		System.out.println("data:" + data + " pattern:" + pattern);
		MainDFSAlgorithm service = new MainDFSAlgorithm(files[0], files[1], 1);
		double w= service.computerMinimalWindows.sumMinimalWindows(service.toPattern(pattern));
		System.out.println(w);
		List<Window> windows = service.computerMinimalWindows.getMinimalWindowsLastRun();
		for(Window window: windows){
			System.out.println(window);
		}
		assertEquals(w,8.0,0.001);
		assertTrue(windows.get(0).samePosAndSize(new Window(0,2)));
		assertTrue(windows.get(1).samePosAndSize(new Window(1,2)));
		assertTrue(windows.get(2).samePosAndSize(new Window(6,2)));
		assertTrue(windows.get(3).samePosAndSize(new Window(7,2)));
	}
	
	
	@Test
	public void testWindowWidth2() throws IOException{
		System.out.println("======= TEST WINDOW COMPUTATION =======");
		String data = "cghedabacadejckdlcmd";
		File[] files =  TestUtils.makeTestFilesForLetterSequence(data);
		String pattern = "a c d";
		System.out.println("data:" + data + " pattern:" + pattern);
		MainDFSAlgorithm service = new MainDFSAlgorithm(files[0],files[1], 2);
		double w= service.computerMinimalWindows.sumMinimalWindows(service.toPattern(pattern));
		System.out.println(w);
		List<Window> windows = service.computerMinimalWindows.getMinimalWindowsLastRun();
		for(Window window: windows){
			System.out.println(window);
		}
		assertEquals(w,61,0.001);
		assertTrue(windows.get(0).samePosAndSize(new Window(0,6)));
		assertTrue(windows.get(6).samePosAndSize(new Window(10,3)));
		assertTrue(windows.get(10).samePosAndSize(new Window(19,11)));
	}
	
	@Test
	public void testWindowWidth3() throws IOException{
		System.out.println("======= TEST WINDOW COMPUTATION =======");
		String data = "abceeeabc";
		File[] files =  TestUtils.makeTestFilesForLetterSequence(data);
		String pattern = "e c";
		System.out.println("data:" + data + " pattern:" + pattern);
		MainDFSAlgorithm service = new MainDFSAlgorithm(files[0], files[1], 1);
		double w= service.computerMinimalWindows.sumMinimalWindows(service.toPattern(pattern));
		System.out.println(w);
		List<Window> windows = service.computerMinimalWindows.getMinimalWindowsLastRun();
		for(Window window: windows){
			System.out.println(window);
		}
		assertEquals(w,15.0,0.001);
	}

	
}
