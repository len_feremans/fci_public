#ifndef DEFINES_H
#define DEFINES_H

#include <stdint.h>
#include <vector>
#include <list>
#include <set>


typedef std::vector<uint32_t> uintvector;
typedef std::list<uint32_t> uintlist;
typedef std::set<uint32_t> uintset;

typedef double prob_t;
typedef std::vector<prob_t> probvector;



#endif
