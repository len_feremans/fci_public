#include <algorithm>
#include "sequence.h"

#include <limits>
#include <algorithm>
#include <map>


sequence::sequence() 
{
	TAILQ_INIT(&m_sequence);
}

bool
event_comp(event *a, event *b)
{
	return a->symbol < b->symbol;
}

void
sequence::read_full(FILE *f)
{
	uint32_t cnt = 0;
	int32_t a;
	int32_t m = 0;
	while (fscanf(f, "%d", &a) == 1) {
		if (a >= 0) cnt++;
		if (a > m) m = a;
	}

	m_data.resize(cnt);
	m_events.resize(m + 1);

	for (uint32_t i = 0; i < m_events.size(); i++)
		TAILQ_INIT(&m_events[i]);

	rewind(f);

	uint32_t i = 0;
	int32_t sid = 1;
	while (fscanf(f, "%d", &a) == 1) {
		if (a < 0) {
			sid++;
			continue;
		}
		event *e = &m_data[i];
		e->symbol = a;
		e->index = i;
		e->sid = sid;
		TAILQ_INSERT_TAIL(&m_sequence, e, entries);
		TAILQ_INSERT_TAIL(&(m_events[a]), e, similar);
		i++;
	}
}


void
sequence::read_sparse(FILE *f)
{
	uint32_t cnt = 0;
	uint32_t a;
	int32_t ind;
	uint32_t m = 0;
	while (fscanf(f, "%d%u", &ind, &a) == 2) {
		cnt++;
		if (a > m) m = a;
	}

	m_data.resize(cnt);
	m_events.resize(m + 1);

	for (uint32_t i = 0; i < m_events.size(); i++)
		TAILQ_INIT(&m_events[i]);

	rewind(f);

	uint32_t i = 0;
	while (fscanf(f, "%d%u", &ind, &a) == 2) {
		if (i > 0 && ind <= m_data[i - 1].index)  {
			fprintf(stderr, "Input file doesn't have monotonic indices: %u (line %u) vs. %u (line %u)\n", ind, i, m_data[i - 1].index, i - 1);
			exit(1);
		}
			
		event *e = &m_data[i];
		e->symbol = a;
		e->index = ind;
		e->sid = 1;
		TAILQ_INSERT_TAIL(&m_sequence, e, entries);
		TAILQ_INSERT_TAIL(&(m_events[a]), e, similar);
		i++;
	}
}

probvector
sequence::probs() const
{
	uintvector cnt(m_events.size());

	event *e;
	TAILQ_FOREACH(e, &m_sequence, entries)
		cnt[e->symbol]++;

	probvector p(m_events.size());
	double l = length();

	for(uint32_t i = 0; i < cnt.size(); i++)
		p[i] = cnt[i]/l;

	return p;
}



sequence::iterator::iterator(const std::vector<const event *> & evs) :
	m_eits(evs.size(), 0), m_size(0)
{
	for (uint32_t i = 0; i < evs.size(); i++) {
		if (evs[i] != 0) {
			m_eits[m_size++] = event_iterator(evs[i]);
		}
	}
	

	std::make_heap(m_eits.begin(), m_eits.begin() + m_size);
}

sequence::iterator &
sequence::iterator::operator ++()
{
	std::pop_heap(m_eits.begin(), m_eits.begin() + m_size);
	
	++m_eits[m_size - 1];

	if (!m_eits.back().eos())
		std::push_heap(m_eits.begin(), m_eits.begin() + m_size);
	else
		m_size--;

	return *this;
}

sequence::iterator
sequence::project(const uintvector & s) const
{
	std::vector<const event *> evs(s.size(), 0);

	for (uint32_t i = 0; i < s.size(); i++)
		if (s[i] < m_events.size())
			evs[i] = TAILQ_FIRST(&m_events[s[i]]);

	return  iterator(evs);
}
