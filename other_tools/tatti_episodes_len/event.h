/*	$Id$	*/
#ifndef EVENT_H
#define EVENT_H

#include <vector>
#include <assert.h>
#include <stdint.h>
#include <limits>
#include "queue.h"

typedef int32_t index_t;
const index_t index_minimal = std::numeric_limits<index_t>::min();


struct event;
TAILQ_HEAD(eventlist, event);

struct event {
	uint32_t symbol;
	uint32_t sid;
	index_t index;

	bool operator < (const event & b) const {return index < b.index;}
	bool operator > (const event & b) const {return index > b.index;}

	event * next() {return TAILQ_NEXT(this, entries);}

	TAILQ_ENTRY(event) entries;
	TAILQ_ENTRY(event) similar;
};



#endif
