Compact Minimal Windows code was provided by Tatti.
Original paper: Tatti, Nikolaj. "Discovering episodes with compact minimal windows." Data mining and knowledge discovery 28.4 (2014): 1046-1077.


Readme by Len Feremans
Created on 11/10/2018
Last updated on 16/10/2018

Dependencies:
- Boost (1.67 works)
- Sqlite
E.g. on mac:
 brew install boost
 brew install sqlite3

Changes Len Feremans:
2018-10 Removing dependency on episodes stored in database, and using closepi output as file
		Removing dependency on saving ranking in database, and saving output to file
		Human-readable output using labels file and sorting on ranking score
Run:
- Usage: tatti_episodes -t <train sequence> -r <test sequence> -d <episode file> 
						-a <alpha parameter> -o <output ranked episodes file> [-l <label file>]
				 		-s    sequences are in sparse format
				 		-h    print this help
- Sparse format like 'timestamp item-id'
- If label file is provided, will output a second human readable file with filename of output 
  and suffix "readable_txt" will be saved with episodes sorted on zone_two, which is abs(zone_one)

Run example on JMLR Abstract:
- run ./run_example.sh

For running closepi:
Usage: 
closepi -t <threshold> -w <window> -i <input file> -o <output file> [-xysfhNE] [-l label file] [-c confidence threshold] [-a rule output]
  -h    print this help
Episode miner:
  -t    mining threshold
  -w    size of the window
  -i    input file
  -o    output file
  -l    label file
  -u    generate episodes only with unique labels
  -x    use minimal windows instead of fixed windows
  -y    use weighted minimal windows instead of fixed windows
  -s    input file is in sparse format
  -f    output only f-closed items
  -N    don't apply node closure
  -E    don't apply edge closure
Rule miner:
  -c    threshold for confide
  
  Default we use: -m (minimal windows) and -f output only f-closed items