#ifndef MACHINE_H
#define MACHINE_H

#include "defines.h"
#include <map>
#include <set>


template <class NK, class NV, class EV>
class machine {
	public:

		machine() : m_edgecnt(0) {}

		typedef NK nodekey;

		class node;
		class edge;

		typedef std::multimap<uint32_t, edge *> edgemap;

	
		struct node {
			node() : def(this), outdeg(0) {}
			bool source() const {return children.size() == 0;}

			NK key;
			NV value;	
			edgemap children;
			node *def;
			uint32_t outdeg, outcnt;
		};

		struct edge {
			EV value;
			node *child;
		};

		typedef std::vector<node *> nodevector;
		typedef std::list<node *> nodelist;

		node*
		add(const NK & k, NV & v)
		{
			node *n = new node;
			n->key = k;
			n->value = v;
			m_nodes[k] = n;
			return n;
		}

		node*
		add(const NK & k)
		{
			node *n = new node;
			n->key = k;
			m_nodes[k] = n;
			return n;
		}

		node*
		find(const NK & k)
		{
			typename nodemap::iterator it = m_nodes.find(k);
			if (it == m_nodes.end())
				return 0;
			return it->second;
		}

		void
		bind(node *f, node *t, uint32_t k, const EV & v)
		{
			edge *e = new edge;
			m_edgecnt++;
			e->child = f;
			e->value = v;
			f->outdeg++;
			t->children.insert(std::make_pair(k, e));
		}

		void
		bind(node *f, node *t, uint32_t k)
		{
			edge *e = new edge;
			m_edgecnt++;
			e->child = f;
			f->outdeg++;
			t->children.insert(std::make_pair(k, e));
		}


		nodelist
		sources()
		{
			nodelist src;
			for (typename nodemap::iterator it = m_nodes.begin(); it != m_nodes.end(); ++it) {
				if (it->second->source())
					src.push_back(it->second);
			}
			return src;
		}

		nodevector
		order()
		{
			nodelist sinks;

			// Init
			for (typename nodemap::iterator it = m_nodes.begin(); it != m_nodes.end(); ++it) {
				it->second->outcnt = 0;
				if (it->second->outdeg == 0)
					sinks.push_back(it->second);
			}

			nodevector n(m_nodes.size());
			uint32_t cnt = n.size();

			// Order
			while (!sinks.empty()) {
				node *s = sinks.front();
				sinks.pop_front();
				n[--cnt] = s;
				for (typename edgemap::iterator eit = s->children.begin(); eit != s->children.end(); ++eit) {
					node *m = eit->second->child;
					m->outcnt++;
					if (m->outdeg == m->outcnt) sinks.push_back(m);
				}
			}


			return n;
		}

		void print() 
		{
			nodevector nodes = order();

			for (uint32_t i = 0; i < nodes.size(); i++) {
				node *n = nodes[i];
				printf("%p (%p %p): ", n, n->key.first, n->key.second);
				for (typename edgemap::iterator eit = n->children.begin(); eit != n->children.end(); ++eit) {
					node *m = eit->second->child;
					printf("(%d: %p) ", eit->first, m);
				}
				printf("\n");
			}

		}


		void clear()
		{
			for (typename nodemap::iterator it = m_nodes.begin(); it != m_nodes.end(); ++it) {
				for (typename edgemap::iterator eit = it->second->children.begin(); eit != it->second->children.end(); ++eit)
					delete eit->second;	
				delete it->second;
			}
			m_nodes.clear();
		}

		~machine() {clear();}

		typedef std::map<NK, node *> nodemap;
		
		nodemap & nodes() {return m_nodes;}

		uint32_t nodecnt() const {return m_nodes.size();}
		uint32_t edgecnt() const {return m_edgecnt;}

	protected:

		nodemap m_nodes;
		uint32_t m_edgecnt;
};


template<class SM, class M>
typename SM::node *
simplify(SM & sm, const typename SM::nodekey & keys)
{
	// Check whether there is a source node.
	for (typename SM::nodekey::iterator it = keys.begin(); it != keys.end(); ++it) {
		if ((*it)->children.size() == 0) {
			typename SM::nodekey k;
			k.insert(*it);
			typename SM::node *n = sm.find(k);
			if (n == 0) n = sm.add(k);
			return n;
		}
	}

	typename SM::node *sn = sm.find(keys);
	if (sn) return sn;

	sn = sm.add(keys);

	// Build join set of all edge labels
	uintset edgelabels;
	for (typename SM::nodekey::iterator it = keys.begin(); it != keys.end(); ++it) {
		for (typename M::edgemap::iterator eit = (*it)->children.begin(); eit != (*it)->children.end(); ++eit) {
			edgelabels.insert(eit->first);
		}
	}

	for (uintset::iterator lit = edgelabels.begin(); lit != edgelabels.end(); ++lit) {
		// Build new keyset
		typename SM::nodekey k;
		for (typename SM::nodekey::iterator it = keys.begin(); it != keys.end(); ++it) {
			typename M::edgemap::iterator eit = (*it)->children.find(*lit);
			if (eit == (*it)->children.end())
				k.insert(*it);
			else
				k.insert(eit->second->child);
		}
		typename SM::node *n = simplify<SM, M>(sm, k);
		sm.bind(n, sn, *lit);
	}

	return sn;
}

template<class SM, class M>
typename SM::node *
simplify(SM & sm, typename M::node *n)
{
	typename SM::nodekey keys;

	keys.insert(n);
	return simplify<SM, M>(sm, keys);
}


template <class JM, class M1, class M2, typename F>
typename JM::node *
join(JM & jm, typename M1::node *n1, typename M2::node *n2, F & f)
{
	typename JM::nodekey jk = std::make_pair(n1, n2);

	typename JM::node *jn = jm.find(jk);
	if (jn) return jn;

	jn = jm.add(jk);

	if (n1->def == 0 || n2->def == 0) {
		jn->def = 0;
		return jn;
	}

	jn->def = f(jn, n1->def, n2->def, (uint32_t)-1);
	if (jn->def == 0)
		jn->def = join<JM, M1, M2, F>(jm, n1->def, n2->def, f);


	typename M1::edgemap::iterator it1 = n1->children.begin();
	typename M2::edgemap::iterator it2 = n2->children.begin();


	while (it1 != n1->children.end() && it2 != n2->children.end()) {
		typename M1::node *m1 = n1->def;
		typename M2::node *m2 = n2->def;
		uint32_t l = std::min(it1->first, it2->first);
		if (it1->first == l) {
			m1 = it1->second->child;
			++it1;
		}
		if (it2->first == l) {
			m2 = it2->second->child;
			++it2;
		}

		typename JM::node *n = f(jn, m1, m2, l);
		if (n == 0)
			n = join<JM, M1, M2, F>(jm, m1, m2, f);
		jm.bind(n, jn, l);
	}

	while (it1 != n1->children.end()) {
		typename JM::node *n = f(jn, it1->second->child, n2->def, it1->first);
		if (n == 0)
			n = join<JM, M1, M2, F>(jm, it1->second->child, n2->def, f);
		jm.bind(n, jn, it1->first);
		++it1;
	}
	while (it2 != n2->children.end()) {
		typename JM::node *n = f(jn, n1->def, it2->second->child, it2->first);
		if (n == 0)
			n = join<JM, M1, M2, F>(jm, n1->def, it2->second->child, f);
		jm.bind(n, jn, it2->first);
		++it2;
	}

	return jn;
}


#endif
