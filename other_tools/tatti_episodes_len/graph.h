#ifndef GRAPH_H
#define GRAPH_H

#include <stdint.h>
#include <list>
#include <vector>
#include <string>
#include <assert.h>

#include "defines.h"

class graph {
	public:
		struct node;
		struct edge;

		typedef uintlist edgelist;
		typedef std::list<node *> nodelist;

		struct node {
			uint32_t label;
			edgelist in;
			uint32_t outdeg;
		};

		struct edge {
			uint32_t from, to;
		};

		struct sinks {
			uintvector degree;
			nodelist nodes;
		};

		void init(sinks & src);
		sinks mark(const sinks & src, const nodelist::const_iterator & nit);

		void reserve(uint32_t nodecnt, uint32_t edgecnt) {m_nodes.reserve(nodecnt); m_edges.reserve(edgecnt);}
		graph() {}
		
		void addnode(uint32_t l) {node n; n.label = l; n.outdeg = 0;  m_nodes.push_back(n);}
		void bind(uint32_t f, uint32_t t); 

		uint32_t nodecnt() const {return m_nodes.size();}
		uint32_t edgecnt() const {return m_edges.size();}

		//new len:
		std::string to_string() const;

	protected:
		graph(const graph &) {} // Just to be safe

		typedef std::vector<node> nodevector;
		typedef std::vector<edge> edgevector;

		nodevector m_nodes;
		edgevector m_edges;
};

#endif
