#ifndef EPISODE_H
#define EPISODE_H

#include "graph.h"
#include "machine.h"
#include "sequence.h"
#include <map>
#include <set>

#include <stdio.h>
#include <sqlite3.h>

struct machstats {

	struct ms {
		ms() : ncnt(0), ecnt(0) {}
		uint32_t ncnt, ecnt;
	} cover, sm, minm, com;

	const machstats &
	operator += (const machstats & b)
	{
		cover.ncnt += b.cover.ncnt;
		sm.ncnt += b.sm.ncnt;
		minm.ncnt += b.minm.ncnt;
		com.ncnt += b.com.ncnt;

		cover.ecnt += b.cover.ecnt;
		sm.ecnt += b.sm.ecnt;
		minm.ecnt += b.minm.ecnt;
		com.ecnt += b.com.ecnt;

		return *this;
	}
};


class episode {
	public:
		//static episode * read(sqlite3 *db, uint32_t id, uint32_t nodecnt, uint32_t edgecnt);

		//new len:
		static episode * read(FILE *f, uint32_t id);

		void build(const probvector & p, prob_t alpha);

		void minepi(const sequence & s);
		//void rank(sqlite3 *db, index_t slen);

		//new len:
		void rank(FILE *output, index_t slen);

		void clear() {m_machine.clear(); m_simple.clear(); m_occur.clear(); m_cooccur.clear();}

		const machstats & stats() const {return m_stats;}

		const graph & dag() const {return m_graph;}

	protected:
		graph m_graph;

		struct empty {};
		typedef machine<uint32_t, const event*, empty> basicmachine;
		typedef machine<std::set<basicmachine::node *>, empty, empty> simplemachine;

		struct transition {
		};

		struct costate {
			costate() : c(0), r(1) {m[0] = m[1] = m[2] = cm[0] = cm[1] = 0;}
			
			prob_t c; // a_0 constant
			prob_t r; // Probability of repeating the prefious state.
			prob_t m[3]; // Moments
			prob_t cm[2]; // Cross-moment for Y_i alpha ^ Y_i
		};
		
		struct state {
			state() : c(0), r(1), conode(0) {m[0] = m[1] = m[2] = cm[0] = cm[1] = 0;}
			
			prob_t c; // a_0 constant
			prob_t r; // Probability of repeating the prefious state.
			prob_t m[3]; // Moments
			prob_t cm[2]; // Cross-moment for Y_i alpha ^ Y_i

			prob_t saved[3]; // Saved moments used as seeds for comachine

			// Forward declaration of inner classes doesn't work -> need to go around for comachine::node
			typedef machine<std::pair<simplemachine::node *, simplemachine::node *>, state, transition>::node nodetype;
			machine<std::pair<nodetype *, nodetype *>, costate, transition>::node *conode;
		};
		


		typedef machine<std::pair<simplemachine::node *, simplemachine::node *>, state, transition> minmachine;
		typedef machine<std::pair<minmachine::node *, minmachine::node *>, costate, transition> comachine;

		struct occurtest {
			occurtest(const probvector & pr, minmachine::node *d) : p(pr), drain(d) {}

			const probvector & p;
			minmachine::node *drain;

			minmachine::node * operator () (minmachine::node *n, simplemachine::node *n1, simplemachine::node *n2, uint32_t l);

		};

		struct cooccurtest {
			cooccurtest(const probvector & pr, minmachine::node *d, comachine::node *cd) : p(pr), drain(d), codrain(cd) {}

			const probvector & p;
			minmachine::node *drain;
			comachine::node *codrain;

			comachine::node * operator () (comachine::node *n, minmachine::node *n1, minmachine::node *n2, uint32_t l);
		};

		typedef std::map<graph::nodelist, basicmachine::node *> idmap;

		basicmachine::node * buildbasic(const graph::sinks & src, idmap & nodeids);
		
		basicmachine m_machine;
		simplemachine m_simple;
		minmachine m_occur;
		comachine m_cooccur;

		basicmachine::node *m_source;

		prob_t m_cov[3];
		prob_t m_p, m_q;
		prob_t m_mean, m_var;

		prob_t m_sample;
		uint32_t m_count;

		prob_t m_alpha;

		uint32_t m_id;

		machstats m_stats;
};

#endif
