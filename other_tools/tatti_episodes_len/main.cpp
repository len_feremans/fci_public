#include <stdio.h>
#include <getopt.h>

#include "episode.h"
#include "sequence.h"

#include <sqlite3.h>
#include <math.h>
#include "len_util.h"
#include <algorithm>

probvector
probs(const char *name, bool sparse)
{
	FILE *sf = fopen(name, "r");

	if (sf == NULL) {
		fprintf(stderr, "missing train sequence\n");
		exit(1);
	}

	sequence s;
	s.read(sf, sparse);
	fclose(sf);
	return s.probs();	
}

int
main(int argc, char **argv)
{
	static struct option longopts[] = {
		{"train",           required_argument,  NULL, 't'},
		{"test",            required_argument,  NULL, 'r'},
		{"data",            required_argument,  NULL, 'd'},
		{"alpha",           required_argument,  NULL, 'a'},
        {"output",          required_argument,  NULL, 'o'},
        {"label",           optional_argument,  NULL, 'l'},
        {"output2",         optional_argument,  NULL, 'h'},
		{"sparse",          no_argument,        NULL, 's'},
		{"help",            no_argument,        NULL, 'h'},
		{ NULL,             0,                  NULL,  0 }
	};

	const char *dbname = NULL;
	const char *testname = NULL;
	const char *trainname = NULL;
    const char *outputname = NULL;
    const char *labelname = NULL;
	bool sparse = false;
	float alpha = 0.5;


	int ch;
	while ((ch = getopt_long(argc, argv, "t:r:d:o:l:sha:", longopts, NULL)) != -1) {
		switch (ch) {
			case 'h':
				printf("Usage: %s -t <train sequence> -r <test sequence> -d <episode file> -o <output-ranked-episodes file> [-l <label file>] [-sh]\n", argv[0]);
				printf("  -s    sequences are in sparse format\n");
				printf("  -h    print this help\n");
				return 0;
				break;
			case 'd':
				dbname = optarg;
				break;
			case 't':
				trainname = optarg;
				break;
			case 'r':
				testname = optarg;
				break;
			case 'a':
				alpha = atof(optarg);
				break;
			case 's':
				sparse = true;
				break;
			case 'o':
                outputname = optarg;
                break;
            case 'l':
                labelname = optarg;
                break;
		}
	}


	typedef std::list<episode *> episodelist;

	probvector p = probs(trainname, sparse);

	FILE *sf = fopen(testname, "r");
	sequence test;
	test.read(sf, sparse);
	fclose(sf);

	//Len
	//sqlite3 *db;
	//sqlite3_open(dbname, &db);
	//sqlite3_stmt *stmt;
	//sqlite3_prepare_v2(db, "select EPISODEID, NODECNT, EDGECNT from EPISODES", -1, &stmt, 0);


	episodelist epilist;

	printf("Reading episodes\n");

	//Len
	/*
	while (sqlite3_step(stmt) == SQLITE_ROW) {
		episode *e = episode::read(db, sqlite3_column_int(stmt, 0), sqlite3_column_int(stmt, 1), sqlite3_column_int(stmt, 2));
		epilist.push_back(e);
	}
	sqlite3_finalize(stmt);
	*/

	//new code Len:
	FILE * inputD = fopen(dbname, "r");
	if (inputD == NULL) {
		fprintf(stderr, "missing episode list\n");
		exit(1);
	}
    uint32_t id = 0;
	while (episode *ep = episode::read(inputD, id++)){
		epilist.push_back(ep);
	}
	printf("Processing episodes %d\n", epilist.size());

	machstats total;

	uint32_t cnt = 0;
	for (episodelist::iterator it = epilist.begin(); it != epilist.end(); ++it) {
	    if(cnt++ % 1000 == 0){ //len: added
		    printf("%d\r", cnt);
		    fflush(stdout);
        }
		episode *e = *it;
		e->build(p, alpha);
		total += e->stats();
		e->minepi(test);
		e->clear();
	}

	cnt = 0;
	for (episodelist::iterator it = epilist.begin(); it != epilist.end(); ++it) {
		episode *e = *it;
		if (e->dag().nodecnt() > 1) cnt++;
	}


	printf("Machine Stats: Episode Cover Simple Min Co-occur\n");
	printf("%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", epilist.size(), cnt, total.cover.ncnt, total.cover.ecnt, total.sm.ncnt,
	        total.sm.ecnt, total.minm.ncnt, total.minm.ecnt, total.com.ncnt, total.com.ecnt);

	printf("Saving episodes\n");

    FILE *outputFile = fopen(outputname, "w");
	//sqlite3_exec(db, "begin transaction; drop table if exists MINWINRANK; create table MINWINRANK (EPISODEID integer primary key, ZONE real, ZTWO real, PONE real, PTWO real, SAMPLE real, OCCUR integer, MEAN real, STD real);", NULL, NULL, NULL);
    fprintf(outputFile, "episode_id;graph_nodes;graph_edges;graph_type;zone;zone_two;p_one;p_two;sample;occurrence;mean;std\n");
	for (episodelist::iterator it = epilist.begin(); it != epilist.end(); ++it) {
		episode *e = *it;
		//e->rank(db, test.length());
        e->rank(outputFile, test.length());
	}
	fclose(outputFile);
	printf("Saved %s\n", outputname);

	for (episodelist::iterator it = epilist.begin(); it != epilist.end(); ++it)
		delete *it;

	//sqlite3_exec(db, "commit;", NULL, NULL, NULL);
	//sqlite3_close(db);

	//Len new: translate pattern to labels
	if(labelname != NULL){
        printf("Loading labels %s\n", labelname);
        std::vector<std::vector<std::string>> dataLabels = readCSVFile(std::string(labelname), ';'); //actually only single token per line, but ok
        std::vector<std::vector<std::string>> dataPatterns = readCSVFile(std::string(outputname),';');
        //e.g. example input:
        //episode_id;graph_nodes;graph_edges;graph_type;zone;zone_two;p_one;p_two;sample;occurrence;mean;std
        //0;0 ;;parallel;0.000000;-0.000000;1.000000;1.000000;0.500000;40;0.500000;0.000000
        //1;0 5 ;;parallel;-1.479907;-1.479907;0.069449;0.000000;0.069717;77;0.055901;2.532648
        //2;0 5 ;0 -> 5 ;serial;-2.403476;-2.403476;0.008120;0.000000;0.087633;39;0.055901;3.581706
        for(uint i=1; i<dataPatterns.size(); i++){
            std::vector<std::string>& row = dataPatterns[i];
            //graph_nodes column:
            //translates ints with labels, where int <i> is token on line <i> in labels file
            std::vector<std::string> graph_nodes = split(row[1],' ');
            std::vector<std::string> graph_nodes_with_labels;
            for(const std::string& graph_node: graph_nodes){
                std::string graph_node_t = graph_node;
                trim(graph_node_t);
                int label_id = stoi(graph_node_t);
                graph_nodes_with_labels.emplace_back(dataLabels.at(label_id).at(0));
            }
            row[1] = join(graph_nodes_with_labels,' ');
            //graph_edges_column:
            //translate ints with labels
            std::vector<std::string> graph_edges = split(row[2],' '); //either number of '->'
            std::vector<std::string> graph_edges_with_labels;
            for(const std::string& graph_edge: graph_edges){
                std::string graph_edge_t = graph_edge;
                trim(graph_edge_t);
                if(graph_edge_t != "->"){
                    int label_id = stoi(graph_edge_t);
                    graph_edges_with_labels.emplace_back(dataLabels.at(label_id).at(0));
                }
                else{
                    graph_edges_with_labels.emplace_back(graph_edge_t);
                }
            }
            row[2] = join(graph_edges_with_labels,' ');
        }
        std::string outputname2 = std::string(outputname) + "_readable.txt";
        //sort on zone
        //see https://stackoverflow.com/questions/16894700/c-custom-compare-function-for-stdsort
        std::sort(dataPatterns.begin()+1, dataPatterns.end(), [](const auto& row1, const auto& row2){
            double zone1 = stod(row1[5]);
            double zone2 = stod(row2[5]);
            return zone2 < zone1; //sort on zone_two (fabs(zone_one)) descending
        });
        writeCSVFile(outputname2, dataPatterns, ';');
        std::cout << "Saved human readable output " << outputname2 << std::endl;
	}
    return 0;
}