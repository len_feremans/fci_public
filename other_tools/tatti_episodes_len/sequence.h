/*	$Id$	*/
#ifndef SEQUENCE_H
#define SEQUENCE_H

#include <stdio.h>
#include <stdint.h>
#include <algorithm>
#include "defines.h"
#include "event.h"

#include <vector>



class sequence {
	public:
		sequence();
		void read(FILE *f, bool sparse = false) {if (sparse) read_sparse(f); else read_full(f);}

		probvector probs() const;

		index_t length() const {return TAILQ_LAST(&m_sequence, eventlist)->index - TAILQ_FIRST(&m_sequence)->index + 1;}

		class event_iterator {
			public:
				event_iterator(const event *ev) : m_ev(ev) {}

				event_iterator & operator ++() {m_ev = TAILQ_NEXT(m_ev, similar); return *this;}

				bool eos() const {return m_ev == NULL;}

				bool operator < (const event_iterator & it) const {return *m_ev  > **it;}

				const event * operator * () const {return m_ev;}

			protected:
				const event *m_ev;
		};

		class iterator {
			public:
				iterator(const std::vector<const event *> & evs);
				sequence::iterator & operator ++(); //LEN, as sequence::iterator::

				const event * operator * () const {return *m_eits.front();}

				bool eos() const {return m_size == 0;}

			protected:
				std::vector<event_iterator> m_eits;
				uint32_t m_size;
		};

		iterator project(const uintvector & s) const;



	protected:
		void read_full(FILE *f);
		void read_sparse(FILE *f);

		std::vector<eventlist> m_events;
		std::vector<event> m_data;

		eventlist m_sequence;
};


#endif
