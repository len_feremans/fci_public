#include "episode.h"
#include "graph.h"
#include <string.h>
#include <sqlite3.h>
#include <math.h>

#include <boost/math/distributions/normal.hpp>


episode::minmachine::node *
episode::occurtest::operator () (minmachine::node *n, simplemachine::node *n1, simplemachine::node *n2, uint32_t l)
{
	if (l < p.size())
		n->value.r -= p[l];

	if (n1 == n2) return drain;

	return 0;
}

episode::comachine::node *
episode::cooccurtest::operator () (comachine::node *n, minmachine::node *n1, minmachine::node *n2, uint32_t l)
{
	if (l < p.size())
		n->value.r -= p[l];

	if (n1 == drain || n2 == drain)
		return codrain;

	return 0;
}


template <class M>
void
computemoments(typename M::nodevector & nodes, const probvector & p, prob_t alpha)
{
	for (uint32_t i = 0; i < nodes.size(); i++) {
		typename M::node *n = nodes[i];
		//printf("n: %p\n", n);

		prob_t r = n->def == n ? n->value.r : 0;

		n->value.cm[0] = n->value.cm[1] = n->value.m[0] = n->value.m[1] = n->value.m[2] = 0;


		for (typename M::edgemap::iterator it = n->children.begin(); it != n->children.end(); ++it) {
			typename M::node *m = it->second->child;
			//printf(" %p %f\n", m, p[it->first]);
			for (uint32_t k = 0; k < 3; k++)
				n->value.m[k] += p[it->first] * (m->value.m[k] + m->value.c);
			for (uint32_t k = 0; k < 2; k++)
				n->value.cm[k] += p[it->first] * m->value.cm[k];
		}

		double a = 1;
		for (uint32_t k = 0; k < 3; k++) {
			n->value.m[k] += r * n->value.c;
			n->value.m[k] /= a - r;
			a /= alpha;
		}

		a = 1;
		for (uint32_t k = 0; k < 2; k++) {
			n->value.cm[k] += n->value.m[k] * a;
			n->value.cm[k] /= a - r;
			a /= alpha;
		}


		//printf("%f %f %p\n", n->value.c, n->value.r, n->def);
		//printf("%f %.10f %.10f\n", n->value.m[0], n->value.m[1], n->value.m[2]);
	}
}


#if 0
template <class M>
void
computemoments(typename M::nodevector & nodes, const probvector & p)
{
	for (uint32_t i = 0; i < nodes.size(); i++) {
		typename M::node *n = nodes[i];
		//printf("%p\n", n);

		prob_t r = n->def == n ? n->value.r : 0;

		n->value.m[0] = n->value.m[1] = n->value.m[2] = 0;

		for (typename M::edgemap::iterator it = n->children.begin(); it != n->children.end(); ++it) {
			typename M::node *m = it->second->child;
			//printf(" %p %f\n", m, p[it->first]);
			n->value.m[0] += p[it->first] * (m->value.m[0] + m->value.c);
			n->value.m[1] += p[it->first] * m->value.m[1];
			n->value.m[2] += p[it->first] * m->value.m[2];
		}

		n->value.m[0] += r * n->value.c;
		n->value.m[0] /= 1 - r;

		n->value.m[1] += n->value.m[0];
		n->value.m[1] /= 1 - r;

		n->value.m[2] += 2*n->value.m[1] - n->value.m[0];
		n->value.m[2] /= 1 - r;

		//printf("%f %f %p\n", n->value.c, n->value.r, n->def);
		//printf("%f %f %f\n", n->value.m[0], n->value.m[1], n->value.m[2]);
	}
}
#endif


void
episode::build(const probvector & probs, prob_t alpha)
{
	m_alpha = alpha;

	graph::sinks src;
	m_graph.init(src);
	idmap nodeids;
	basicmachine::node *n = buildbasic(src, nodeids);

	m_stats.cover.ncnt = m_machine.nodecnt();
	m_stats.cover.ecnt = m_machine.edgecnt();

	// Construct simple machine
	simplemachine::node *sn = simplify<simplemachine, basicmachine>(m_simple, n);

	m_stats.sm.ncnt = m_simple.nodecnt();
	m_stats.sm.ecnt = m_simple.edgecnt();

	simplemachine::nodekey k;
	k.insert(m_source);
	m_simple.find(k)->def = 0;


	// Construct occur machine
	minmachine::node *mn = m_occur.add(std::make_pair(sn, sn));
	minmachine::node *drain = m_occur.add(std::make_pair((simplemachine::node *)0, (simplemachine::node *)0));

	mn->def = drain;
	drain->def = 0;

	occurtest ot(probs, drain);

	for (simplemachine::edgemap::iterator it = sn->children.begin(); it != sn->children.end(); ++it) {
		minmachine::node *n = join<minmachine, simplemachine, simplemachine>(m_occur, it->second->child, sn, ot);
		m_occur.bind(n, mn, it->first);
	}

	minmachine::nodevector occurnodes = m_occur.order();

	m_stats.minm.ncnt = m_occur.nodecnt() - 1; // don't count the drain node
	m_stats.minm.ecnt = m_occur.edgecnt() - drain->outdeg;

	// Construct co-occur machine
	comachine::node *codrain = m_cooccur.add(std::make_pair(drain, drain));
	codrain->def = 0;

	cooccurtest cot(probs, drain, codrain);
	for (uint32_t i = 0; i < occurnodes.size(); i++) {
		minmachine::node *n = occurnodes[i];
		if (n != mn &&  !n->source()) {
			n->value.conode = join<comachine, minmachine, minmachine>(m_cooccur, n, mn, cot);
		}
	}

	m_stats.com.ncnt = m_cooccur.nodecnt() - 1; // don't count the drain node
	m_stats.com.ecnt = m_cooccur.edgecnt() - codrain->outdeg;


	// Initialize the sources.
	minmachine::nodelist occursrc = m_occur.sources();
	for (minmachine::nodelist::iterator it = occursrc.begin(); it != occursrc.end(); ++it) {
		minmachine::node *n = *it;
		if (n == drain)
			continue;
		n->value.c = 1;
	}

	// Compute the moments
	computemoments<minmachine>(occurnodes, probs, m_alpha);

	//alpha = m_alpha = exp(log(alpha) * mn->value.m[0] /  mn->value.cm[0]);
	//computemoments<minmachine>(occurnodes, probs, m_alpha);


	// Save the moments
	for (uint32_t i = 0; i < occurnodes.size(); i++) {
		minmachine::node *n = occurnodes[i];
		n->value.saved[0] = n->value.m[0];
		n->value.saved[1] = n->value.m[1];
		n->value.saved[2] = n->value.m[2];
	}

	// Compute the mean and the covariance

	prob_t p = mn->value.saved[0];
	prob_t q = mn->value.saved[1];
	prob_t v = mn->value.saved[2];
	prob_t cp = mn->value.cm[0];
	prob_t cq = mn->value.cm[1];


	prob_t cem00 = (cp - p)*p;
	prob_t cem01 = (cp - p)*q;
	prob_t cem10 = (cq - q)*p;
	prob_t cem11 = (cq - q)*q;

	// Initialize the co-machine sources.
	comachine::nodelist cooccursrc = m_cooccur.sources();
	for (comachine::nodelist::iterator it = cooccursrc.begin(); it != cooccursrc.end(); ++it) {
		comachine::node *n = *it;
		if (n == codrain) continue;
		assert(n->key.first->source());
		n->value.c = n->key.second->value.saved[0];
	}

	// Compute the co-moments
	comachine::nodevector cooccurnodes = m_cooccur.order();
	computemoments<comachine>(cooccurnodes, probs, m_alpha);

	for (uint32_t i = 0; i < occurnodes.size(); i++) {
		minmachine::node *n = occurnodes[i];
		n->value.c = n->value.conode ? n->value.conode->value.m[0] : 0;
	}
	computemoments<minmachine>(occurnodes, probs, alpha);

	prob_t com000 = mn->value.m[0];
	prob_t com001 = mn->value.m[1];

	for (uint32_t i = 0; i < occurnodes.size(); i++) {
		minmachine::node *n = occurnodes[i];
		n->value.c = n->value.conode ? n->value.conode->value.m[1] : 0;
	}
	computemoments<minmachine>(occurnodes, probs, alpha);

	prob_t com010 = mn->value.m[0];
	prob_t com011 = mn->value.m[1];

	for (uint32_t i = 0; i < occurnodes.size(); i++) {
		minmachine::node *n = occurnodes[i];
		n->value.c = n->value.conode ? n->value.conode->value.m[2] : 0;
	}
	computemoments<minmachine>(occurnodes, probs, alpha);

	prob_t com020 = mn->value.m[0];

	// (Re)-Initialize the co-machine sources.
	for (comachine::nodelist::iterator it = cooccursrc.begin(); it != cooccursrc.end(); ++it) {
		comachine::node *n = *it;
		if (n == codrain) continue;
		n->value.c = n->key.second->value.saved[1];
	}

	// Compute the co-moments
	computemoments<comachine>(cooccurnodes, probs, alpha);

	for (uint32_t i = 0; i < occurnodes.size(); i++) {
		minmachine::node *n = occurnodes[i];
		n->value.c = n->value.conode ? n->value.conode->value.m[0] : 0;
	}
	computemoments<minmachine>(occurnodes, probs, alpha);

	prob_t com100 = mn->value.m[0];
	prob_t com101 = mn->value.m[1];

	for (uint32_t i = 0; i < occurnodes.size(); i++) {
		minmachine::node *n = occurnodes[i];
		n->value.c = n->value.conode ? n->value.conode->value.m[1] : 0;
	}
	computemoments<minmachine>(occurnodes, probs, alpha);

	prob_t com110 = mn->value.m[0];

	for (uint32_t i = 0; i < occurnodes.size(); i++) {
		minmachine::node *n = occurnodes[i];
		n->value.c = n->value.conode ? n->value.conode->value.m[2] : 0;
	}
	computemoments<minmachine>(occurnodes, probs, alpha);

	prob_t com121 = mn->value.m[1];



	//m_cov[0] = v - q*q + 2*(com011 + com110 + com101 + com020 - cem11);
	//m_cov[2] = p - p*p + 2*(com000 - cem00);
	//m_cov[1] = q - q*p + (com100 + com010 + com010 + com001 - cem01 - cem10);

	m_cov[0] = v - q*q  + 2*(com121 - cem11);
	m_cov[2] = p - p*p + 2*(com000 - cem00);
	m_cov[1] = q - q*p + (com110 + com011 - cem01 - cem10);

	//m_cov[0] = v - q*q;
	//m_cov[2] = p - p*p;
	//m_cov[1] = q - q*p;

	m_mean = q / p;
	m_var = (m_cov[0] - 2*m_mean*m_cov[1] + m_mean*m_mean*m_cov[2]) / (p*p);

	//m_mean = log(q / p);
	//m_var = m_cov[0] / (q*q)  - 2*m_cov[1] / (p*q) + m_cov[2] / (p*p);

	//printf("%.13f %f %f % f%d %d\n", q, p, cp / p, alpha, m_graph.nodecnt(), m_graph.edgecnt());


	if (m_graph.nodecnt() > 1) {
        //assert(m_var >= 0);
        if(m_var < 0){
            printf("Error assertion m_var >=0 failed: %.9f %s\n", m_var, m_graph.to_string().c_str());
        }
	}

	//m_mean = q;
	//m_var = m_cov[0];

	m_p = p;
	m_q = q;


	//m_occur.print();
	//printf("\n");
	//m_cooccur.print();

	//prob_t d1 = 2*(com011 + com110 + com101 + com020 - cem11);
	//prob_t d2 = 2*(com000 - cem00);
	//prob_t d3 = (com100 + com010 + com010 + com001 - cem01 - cem10);
	//prob_t d =  m_mean*m_mean*2*(com011 + com110 + com101 + com020 - cem11) + 2*(com000 - cem00) -2*m_mean*(com100 + com010 + com010 + com001 - cem01 - cem10);


	//printf("%d %d %f %f %f %f %f", m_occur.nodes().size(), m_cooccur.nodes().size(), p, q, v, m_mean, sqrt(m_var));
	//printf(": %f %f %f %f %f %f", com121, cp, cq, m_var, m_cov[0]/ (p*p), m_cov[2]);
	//printf(" (%f %f %f)", d1, d2, d3);
	//printf("\n");

}



episode::basicmachine::node *
episode::buildbasic(const graph::sinks & src, idmap & nodeids)
{
	//printf("g: %d %d\n", src.degree.size(), src.nodes.size());

	idmap::iterator it = nodeids.find(src.nodes);
	if (it != nodeids.end())
		return it->second;
	
	basicmachine::node *n = m_machine.add(nodeids.size());
	nodeids[src.nodes] = n;

	if (src.nodes.size() == 0)
		m_source = n;

	for (graph::nodelist::const_iterator it = src.nodes.begin(); it != src.nodes.end(); ++it) {
		graph::sinks s = m_graph.mark(src, it);
		basicmachine::node *m = buildbasic(s, nodeids);
		m_machine.bind(m, n, (*it)->label);
	}

	return n;
}

//LEN: commented code
/*
episode *
episode::read(sqlite3 *db, uint32_t id, uint32_t nodecnt, uint32_t edgecnt)
{
	episode *e = new episode;
	e->m_id = id;
	e->m_graph.reserve(nodecnt, edgecnt);

	sqlite3_stmt *stmt;

	sqlite3_prepare_v2(db, "select LABEL from NODES where EPISODEID = ? order by NODEID", -1, &stmt, NULL);
	sqlite3_bind_int(stmt, 1, id);

	//printf("%d %d\n", nodecnt, edgecnt);

	for (uint32_t i = 0; i < nodecnt; i++) {
		int ret = sqlite3_step(stmt);
		assert(ret == SQLITE_ROW);
		e->m_graph.addnode(sqlite3_column_int(stmt, 0));
		//printf("l %d\n", sqlite3_column_int(stmt, 0));
	}

	sqlite3_finalize(stmt);

	sqlite3_prepare_v2(db, "select SOURCE, TARGET from EDGES where EPISODEID = ?", -1, &stmt, NULL);
	sqlite3_bind_int(stmt, 1, id);

	for (uint32_t i = 0; i < edgecnt; i++) {
		int ret = sqlite3_step(stmt);
		assert(ret == SQLITE_ROW);
		e->m_graph.bind(sqlite3_column_int(stmt, 0), sqlite3_column_int(stmt, 1));
	}

	sqlite3_finalize(stmt);

	return e;
}
 */

//LEN: Uncommented + slightly adjusted
//adjustedments: saving id
episode *
episode::read(FILE *f, uint32_t id)
{
	char buffer[1024];
	episode *e = 0;

	do {
		if (fgets(buffer, 1024, f) == NULL)
			return e;
	} while(strncmp("graph:", buffer, 6) != 0); 

	e = new episode;
	e->m_id = id;

	do  {
		if (strncmp("size:", buffer, 5) == 0) {
			uint32_t nodecnt, edgecnt;
			sscanf(buffer + 5, "%d%d", &nodecnt, &edgecnt);
			e->m_graph.reserve(nodecnt, edgecnt);
		}
		else if (strncmp("nodes:", buffer, 6) == 0) {
			for (char *t = strtok(buffer + 6, " \n"); t != NULL; t = strtok(NULL, " \n"))
				e->m_graph.addnode(atoi(t));
		}
		else if (strncmp("edges:", buffer, 6) == 0) {
			for (char *t = strtok(buffer + 6, " \n"); t != NULL; t = strtok(NULL, " \n")) {
				uint32_t from = atoi(t);
				strtok(NULL, " ");
				t = strtok(NULL, " ");
				uint32_t to = atoi(t);

				e->m_graph.bind(from, to);
			}
		}

		if (fgets(buffer, 1024, f) == NULL)
			return e;
	} while(strcmp("\n", buffer) != 0); 


	return e;
}


void
episode::minepi(const sequence & s)
{
	typedef std::list<std::pair<basicmachine::node *, basicmachine::node *> > edgelist;
	typedef std::map<uint32_t, edgelist> edgemap;

	edgemap edges;

	uint64_t winsumtotal = 0;
	uint32_t wintotal = 0;

	basicmachine::nodevector nodes = m_machine.order();

	basicmachine::node *sink = nodes.back();

	event sentinel;
	sentinel.index = index_minimal;
	sentinel.sid = 0;

	for (uint32_t i = 0; i < nodes.size(); i++) {
		basicmachine::node *n = nodes[i];
		n->value = &sentinel;
		for (basicmachine::edgemap::iterator it = n->children.begin(); it != n->children.end(); ++it) {
			edges[it->first].push_front(std::make_pair(it->second->child, n));
		}
	}

	uintvector sym(edges.size());

	uint32_t c = 0;
	for (edgemap::iterator it = edges.begin(); it != edges.end(); ++it)
		sym[c++] = it->first;

	m_sample = 0;

	for( sequence::iterator sit = s.project(sym); !sit.eos(); ++sit) {
		const event *ev = *sit;
		m_source->value = ev;
		edgelist & e = edges[ev->symbol];

		//printf("%d %d | ", ev->index , ev->symbol);

		const event *old = sink->value;
		for (edgelist::iterator it = e.begin(); it != e.end(); ++it) {
			if (it->first->value->index > it->second->value->index) {
				it->second->value = it->first->value;
			}
		}
		if (sink->value->index > old->index && sink->value->sid == ev->sid) {
			wintotal++;
			//printf("%d %d | ", ev->index , sink->value);
			winsumtotal += 1 + ev->index - sink->value->index;
			m_sample += pow(m_alpha, 1 + ev->index - sink->value->index);
		}
	}

	m_count = wintotal;
	if (wintotal > 0)
		m_sample /= wintotal;
		//m_sample = prob_t(winsumtotal) / prob_t(wintotal);
	//printf("sup: %f %d\n", m_sample, wintotal);
}

//Len: commented code
/*
void
episode::rank(sqlite3 *db, index_t len)
{
	//double pdiff = sqrt(len) * (double(m_count) / len - m_p);

	//double mean = sqrt(len) * m_q + pdiff * m_cov[1]/m_cov[2];
	//double var = m_cov[0] - m_cov[1]*m_cov[1]/m_cov[2];

	double mean = m_mean;
	double var = m_var;


	//printf("%f, %f, %f, %f, %f, %d\n", mean, var, m_sample / sqrt(len), m_q, m_p, m_count);

	double l = -sqrt(len) * (m_sample - mean) / sqrt(var);
	sqlite3_stmt *stmt;

		
	boost::math::normal stdnorm;

	double p1 = 1;
	double p2 = 1;
	
	if (var > 0) {
		p1 = boost::math::cdf(stdnorm, l);
		p2 = 2*boost::math::cdf(stdnorm, -sqrt(len)*fabs(l)); // Check this multiplication by 2
	}
	//printf("%f\n", p1);

	if (m_graph.nodecnt() == 1) {
		p1 = p2 = 1;
		l = 0;
	}


	sqlite3_prepare_v2(db, "insert into MINWINRANK (EPISODEID, ZONE, ZTWO, PONE, PTWO, SAMPLE, OCCUR, MEAN, STD) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", -1, &stmt, NULL);
	sqlite3_bind_int(stmt, 1, m_id);
	sqlite3_bind_double(stmt, 2, l );
	sqlite3_bind_double(stmt, 3, -fabs(l));
	sqlite3_bind_double(stmt, 4, p1);
	sqlite3_bind_double(stmt, 5, p2);
	sqlite3_bind_double(stmt, 6, m_sample);
	sqlite3_bind_int(stmt, 7, m_count);
	sqlite3_bind_double(stmt, 8, mean);
	sqlite3_bind_double(stmt, 9, sqrt(var));

	sqlite3_step(stmt);

	sqlite3_finalize(stmt);
}
*/

//LEN: New method, save output to file
void
episode::rank(FILE *output, index_t len)
{
    double mean = m_mean;
    double var = m_var;

    double l = -sqrt(len) * (m_sample - mean) / sqrt(var);

    boost::math::normal stdnorm;

    double p1 = 1;
    double p2 = 1;

    if (var > 0) {
        p1 = boost::math::cdf(stdnorm, l);
        p2 = 2*boost::math::cdf(stdnorm, -sqrt(len)*fabs(l)); // Check this multiplication by 2
    }
    //printf("%f\n", p1);

    if (m_graph.nodecnt() == 1) {
        p1 = p2 = 1;
        l = 0;
    }

	/*
	sqlite3_stmt *stmt;
    sqlite3_prepare_v2(db, "insert into MINWINRANK (EPISODEID, ZONE, ZTWO, PONE, PTWO, SAMPLE, OCCUR, MEAN, STD) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", -1, &stmt, NULL);
    sqlite3_bind_int(stmt, 1, m_id);
    sqlite3_bind_double(stmt, 2, l );
    sqlite3_bind_double(stmt, 3, -fabs(l));
    sqlite3_bind_double(stmt, 4, p1);
    sqlite3_bind_double(stmt, 5, p2);
    sqlite3_bind_double(stmt, 6, m_sample);
    sqlite3_bind_int(stmt, 7, m_count);
    sqlite3_bind_double(stmt, 8, mean);
    sqlite3_bind_double(stmt, 9, sqrt(var));
    sqlite3_step(stmt);
    sqlite3_finalize(stmt);
    */
	//fprintf(output, "graph_nodes;episode_id;graph_edges;graph_type;zone;zone_two;p_one;p_two;sample;occurrence;mean;std");
	std::string graph2str = m_graph.to_string();
	//bug fix Len 19-11-2019: was -fabs(l), now fabs(l)
	fprintf(output, "%d;%s;%f;%f;%f;%f;%f;%d;%f;%f\n", m_id, graph2str.c_str() ,l,fabs(l),p1,p2,m_sample,m_count,mean,sqrt(var));

}
