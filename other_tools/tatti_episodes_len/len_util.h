//
// Created by Len on 16/10/2018.
// Some utility code for post-processing compact minimal epsidodes, to inline labels
//

#ifndef TATTI_EPISODES_LEN_LEN_UTIL_H
#define TATTI_EPISODES_LEN_LEN_UTIL_H

#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>

//Len: new
//see https://stackoverflow.com/questions/236129/how-do-i-iterate-over-the-words-of-a-string?rq=1
template<typename Out>
void split(const std::string &s, char delim, Out result) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        *(result++) = item;
    }
}

std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, std::back_inserter(elems));
    return elems;
}

std::vector<std::vector<std::string>> readCSVFile(const std::string& filename, char separator){
    std::ifstream ifs(filename);
    std::string str;
    std::vector<std::vector<std::string>> data;
    while(std::getline(ifs,str)){
        std::vector<std::string> tokens = split(str, separator);
        data.emplace_back(tokens);
    }
    ifs.close();
    return data;
}

std::string join(const std::vector<std::string> &s, char delim) {
    std::ostringstream out;
    for(const auto& token: s){
        out << token << delim;
    }
    return out.str();
}

void writeCSVFile(const std::string& filename, const std::vector<std::vector<std::string>>& data, char separator){
    std::ofstream ofs(filename);
    for(uint i=0; i<data.size(); i++){
        ofs << join(data[i],separator) << std::endl;
    }
    ofs.close();
    return;
}

//see https://stackoverflow.com/questions/216823/whats-the-best-way-to-trim-stdstring
// trim from start (in place)
static inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) { return !std::isspace(ch); }));
}

// trim from end (in place)
static inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) { return !std::isspace(ch); }).base(), s.end());
}

// trim from both ends (in place)
static inline void trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}


#endif //TATTI_EPISODES_LEN_LEN_UTIL_H
