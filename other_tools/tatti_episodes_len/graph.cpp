#include "graph.h"
#include <string>
#include <sstream>
void
graph::bind(uint32_t f, uint32_t t)
{
	assert(f < m_nodes.size());
	assert(t < m_nodes.size());
	edge e = {f, t};
	m_nodes[t].in.push_back(m_edges.size());
	m_edges.push_back(e);
	m_nodes[f].outdeg++;
}

void
graph::init(sinks & src)
{
	src.degree.resize(m_nodes.size());
	src.nodes.clear();


	for (uint32_t i = 0; i < m_nodes.size(); i++) {
		src.degree[i] = m_nodes[i].outdeg;
		if (src.degree[i] == 0)
			src.nodes.push_back(&m_nodes[i]);
	}	
}

graph::sinks
graph::mark(const sinks & src, const nodelist::const_iterator & nit)
{
	sinks s;
	s.degree = src.degree;

	s.nodes.insert(s.nodes.end(), src.nodes.begin(), nit);
	nodelist::const_iterator it = nit;
	++it;
	s.nodes.insert(s.nodes.end(), it, src.nodes.end());


	node *n = *nit;

	for (edgelist::iterator eit = n->in.begin(); eit != n->in.end(); ++eit) {
		uint32_t f= m_edges[*eit].from;
		s.degree[f]--;
		if (s.degree[f] == 0)
			s.nodes.push_back(&m_nodes[f]);
	}

	return s;
}

//Len: added (started from copy/paste of closepi
//returns nodes; node0 -> node1 ...; type
std::string  graph::to_string() const
{
	std::stringstream ss;
	for (uint32_t i = 0; i < nodecnt(); i++)
		ss << m_nodes[i].label << " "; //get_label does not exisst

	ss << ";";
	for (uint32_t i = 0; i < edgecnt(); i++) { //changed loop
		const edge e = m_edges[i];
		ss << m_nodes[e.from].label << " -> " << m_nodes[e.to].label << " ";
	}
	ss << ";";
	if (edgecnt() == 0) //was last_essential() == NULL
		ss << "parallel";
	else if (edgecnt() == nodecnt() * (nodecnt() - 1) / 2)
		ss << "serial";
	else
	    ss << "general";
	return ss.str();
}

