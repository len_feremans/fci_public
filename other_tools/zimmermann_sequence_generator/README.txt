The program call takes the form java zimmermann_sequence_generator [-N int | -M int | -p float | -o float | -g int | -n int | -T int | -d {n,u} | -D {n,u} | -G double | -m int | -P {p,u} | -E | -i | -h | -r | -s | -?]

A short description of the options follows:

-N int  Generate source episode with int elements (default 4)
-M int  Sample events from [1,int] (default 20)
-p float        Generate noise event with probability float (default 0.2)
-o float        Omit generated event with probability float (default 0)
-g int  Sample time delay from [1,int] (default 20)
-n int  Generate int source episodes (default 1)
-T int  Generate int events in total (default 5000)
-d {n,u}        Distribution of noise delay values: mixture of normal distributions or uniform (default u)
-D {n,u}        Distribution of episode delay values: mixture of normal distributions or uniform (default u)
-G int  Base mean value for the normal distributions for episode/noise delays (default 300)
-m int  Number of normal distributions mixed, with means 1/2/.../m * base mean
-P {p,u}        Distribution of noise events: Poisson or uniform (default u)
-W      Source episodes' probability is not uniform
-S      source episodes are embedded successively not concurrently
-i      Allow interleaving of several embeddings of the same source episode
-h      Enforce maximum time delay between successive events of embedded episode (specified by -g or m*G)
-r      Enforce repetition of event types in source episodes
-s      Enforce sharing of event types among source episodes (only effective for n > 1)
-?      Help (prints this list and exits program)

The output is written to standard out.

As can be seen from the default values above, calling 

java zimmermann_sequence_generator > exmpl.dat

is equivalent to calling

java zimmermann_sequence_generator -N 4 -M 20 -p 0.2 -o 0 -g 20 -n 1 -T 5000 -d u -D u -P u > exmpl.dat

and produces the example file included in the .zip.

-W, -S, -h, -i, -r, -s are triggers that change standard generator behavior - in which source episodes are given the same probability of occurrence, are embedded concurrently, can have delays between event occurrences that are larger than g, and are not allowed to interleave, repeat event types within an episode or share event types across episodes - to the behavior connected to the trigger. 

-i, and -r can be used with -n 1 since interleaving of episode embeddings and sharing of event types within an episode can be meaningful.
-s is meaningless, is meaningless for single embedded episodes since no inter-episode sharing is possible.

In the current implementation, all embedded episodes have the same size, so increasing N will lead to all of them becoming longer. In the standard setting (without -r and -s), this also means that M needs to be larger than or equal to N*n, otherwise the generator will become stuck in an endless loop.

Finally, if episode or noise delays should be generated from a normal instead of a uniform distribution (trigger -D n and/or -d n), the base mean value of the distribution has to be specified, using -G as well as the number of distributions to be mixed (using -m). In the case of -G 300 and -m 1, for instance, this would lead to all time delays being sampled from a single normal distribution with mean 300. exmpl2.dat shows data with such characteristics, generated with -n 2 and -r.

All data files begin with a number of comment lines that list the characteristics with which they were generated, as well as the source episodes with their probabilities.