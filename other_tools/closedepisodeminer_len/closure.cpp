#include "episode.h"
#include "graphset.h"


void
closure::mark(episode *ep)
{
	indexvector m;
	graph *cl = ep->closure();
	m.reserve(cl->nodecnt());
	episode_marker em(this, ep);

	traverse_subset<episodeset, episode_marker>(&m_episodes.root(), cl->label_begin(), cl->label_end(), cl, em, m);
}

bool
closure::mark(episode *ep, episodeset::graph_tree *et)
{
	episode *c = et->value();
	if (c == ep) return false;

	if (c->condition() < ep->condition())
		return false;
	if (c->condition() == ep->condition()) 
		c->close(ep);

	return true;
}
