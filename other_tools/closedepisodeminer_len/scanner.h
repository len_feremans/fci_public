#ifndef SCANNER_H
#define SCANNER_H

#include "defines.h"
#include "graph.h"
#include "event.h"


class scanner {
	public:
		struct state {
			event *ev;
			index_t border;
		};

		scanner(uint32_t nodecnt, uint32_t entrycnt) : m_nodemap(nodecnt), m_states(entrycnt), m_entries(entrycnt)  {TAILQ_INIT(&m_update);}

		void init(uint32_t eind, const graph::node *n, event *ev);
		void set(uint32_t i, event *ev) {m_states[i].ev = ev;}

		void mark(const graph::node *n, const event *ev);
		void mark(uint32_t i, index_t b, bool proper = true);

		event * update(event *last);


		typedef std::vector<state> statevector;

		const statevector & states() const {return m_states;}
		void set(const statevector & state) {m_states = state;}

	protected:
		struct entry {
			uint32_t index;
			const graph::node *n;
			TAILQ_ENTRY(entry) update;
			bool marked;
			bool proper;
		};

		typedef std::vector<entry> entryvector;

		TAILQ_HEAD(entrylist, entry);

		uintvector m_nodemap;

		statevector m_states;
		entryvector m_entries;
		entrylist m_update;
};


#endif
