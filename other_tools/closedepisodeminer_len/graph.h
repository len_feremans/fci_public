/*	$id$	*/
#ifndef GRAPH_H
#define GRAPH_H

#include <vector>
#include <stdio.h>
#include <stdint.h>
#include "queue.h"
#include "event.h"
#include "defines.h"


struct symbol_t {
	uint32_t label;
	uint32_t occur;

	bool operator == (const symbol_t & a) const {return label == a.label && occur  == a.occur;}
	bool operator != (const symbol_t & a) const {return label != a.label || occur  != a.occur;}
	bool operator < (const symbol_t & a) const {return label < a.label || (label == a.label && occur < a.occur);}
};

typedef std::vector<symbol_t> symbolvector;
void normalize(symbolvector & sv);


class label_iterator {
	public:
		label_iterator(const symbolvector & symbols, int32_t ind) : m_symbols(symbols), m_index(ind) {}

		bool operator == (const label_iterator & it) const {return m_index == it.m_index;}
		bool operator != (const label_iterator & it) const {return !(*this == it);}

		label_iterator & operator ++ () {m_index++; return *this;}

		const uint32_t & operator * () const {return m_symbols[m_index].label;}

		uint32_t index() const {return m_index;}

		bool solitary() const {return test() && m_symbols[m_index].occur == 0;}

		bool test() const {return m_index == m_symbols.size() - 1 || m_symbols.at(m_index + 1).label != m_symbols.at(m_index).label;}
	protected:
		const symbolvector & m_symbols;
		uint32_t m_index;
};

typedef uintvector indexvector;

struct sigul {
	uint32_t from, to;

	sigul map(const indexvector & m) const {sigul s = {m[from], m[to]}; return s;}

	bool operator == (const sigul & a) const {return from == a.from && to == a.to;}
	bool operator != (const sigul & a) const {return from != a.from || to != a.to;}
	bool operator < (const sigul & a) const {return from < a.from || (from == a.from && to < a.to);}
	bool operator <= (const sigul & a) const {return from < a.from || (from == a.from && to <= a.to);}
};



class graph {
	public:
	

		struct edge {
			edge() {in.target = out.target = 0; isess = false;}
			
			operator sigul() const {sigul s = {in.target, out.target}; return s;}

			bool isess;
			struct {
				uint32_t target;
				TAILQ_ENTRY(edge) entries;
				TAILQ_ENTRY(edge) essential;
			} in, out;
		};

		TAILQ_HEAD(edgelist, edge);

		struct node {
			symbol_t symbol;
			uint32_t index;

			struct {
				edgelist edges;
				edgelist essentials;
				uint32_t degree;
			} in, out;

			TAILQ_ENTRY(node) sources;
		};

		TAILQ_HEAD(nodelist, node);


		class edge_iterator {
			public:
				edge_iterator(graph *g, int32_t ind, edge *e) : m_g(g), m_index(ind), m_edge(e), m_mask(g->nodecnt()), m_map(NULL) {}
				edge_iterator(graph *g) : m_g(g), m_index(-1), m_edge(NULL), m_mask(g->nodecnt()), m_map(NULL) {++(*this);}
				edge_iterator(graph *g, uint32_t m) : m_g(g), m_index(-1), m_edge(NULL), m_mask(m), m_map(NULL) {++(*this);}
				edge_iterator(graph *g, const uintvector * map) : m_g(g), m_index(-1), m_edge(NULL), m_mask(g->nodecnt()), m_map(map) {++(*this);}

				bool operator == (const edge_iterator & it) const {return m_g == it.m_g && m_index == it.m_index && m_edge == it.m_edge;}
				bool operator != (const edge_iterator & it) const {return !(*this == it);}

				const sigul & operator * () const {return m_s;}
				const sigul * operator -> () const {return &m_s;}

				edge_iterator &
				operator ++ ()
				{
					do {
						move();
					} while (m_edge && int32_t(m_edge->out.target) == m_mask);

					if (m_index < int32_t(m_g->nodecnt()))
						form_sigul();
					return *this;
				}

				bool test() const {return m_edge->isess && m_g->get_label(m_index) != m_g->get_label(m_edge->out.target);}

				edge *value() {return m_edge;}

			protected:

				void
				move()
				{
					if (m_edge != NULL)
						m_edge = TAILQ_NEXT(m_edge, out.entries);

					while (m_edge == NULL) {
						m_index++;
						if (m_index == int32_t(m_g->nodecnt()))
							return;
						if (m_index == m_mask)
							continue;
						m_edge = TAILQ_FIRST(&m_g->m_nodes[m_index].out.edges);
					}
				}

				void form_sigul()
				{
					m_s.from = m_index;
					m_s.to = m_edge->out.target;

					if (int32_t(m_s.from) > m_mask)
						m_s.from--;
					if (int32_t(m_s.to) > m_mask)
						m_s.to--;
					
					if (m_map) {
						m_s.from = (*m_map)[m_s.from];
						m_s.to = (*m_map)[m_s.to];
					}
				}

				graph *m_g;
				int32_t m_index;
				edge *m_edge;
				sigul m_s;
				int32_t m_mask;
				const uintvector *m_map;
		};

		class label_iterator {
			public:
				label_iterator(const graph *g, int32_t ind) : m_graph(g), m_index(ind) {}

				bool operator == (const label_iterator & it) const {return m_index == it.m_index;}
				bool operator != (const label_iterator & it) const {return !(*this == it);}

				label_iterator & operator ++ () {m_index++; return *this;}

				const uint32_t & operator * () const {return m_graph->get_symbol(m_index).label;}

				uint32_t index() const {return m_index;}

			protected:
				const graph *m_graph;
				uint32_t m_index;
		};


		class symbol_iterator {
			public:
				symbol_iterator(const graph *g, int32_t ind) : m_graph(g), m_index(ind) {}

				bool operator == (const symbol_iterator & it) const {return m_index == it.m_index;}
				bool operator != (const symbol_iterator & it) const {return !(*this == it);}

				symbol_iterator & operator ++ () {m_index++; return *this;}

				const symbol_t & operator * () const {return m_graph->get_symbol(m_index);}

				uint32_t index() const {return m_index;}

			protected:
				const graph *m_graph;
				uint32_t m_index;
		};


		void check() const;



		void print(FILE *f) const;
		void print_sql(FILE *f, uint32_t id) const;

		const nodelist * sources() const {return &m_sources;}
		uint32_t nodecnt() const {return m_nodes.size();}
		uint32_t edgecnt() const {return m_edgecnt;}
		uint32_t sourcecnt() const {return m_sourcecnt;}

		void set_symbol(uint32_t ind, const symbol_t & s) {m_nodes[ind].symbol = s;}
		const symbol_t & get_symbol(uint32_t ind) const {return m_nodes[ind].symbol;}
		const uint32_t get_label(uint32_t ind) const {return m_nodes[ind].symbol.label;}
		graph::node & get_node(uint32_t ind) {return m_nodes[ind];}
		const graph::node & get_node(uint32_t ind) const {return m_nodes[ind];}
		const edge & get_edge(uint32_t ind) const {return m_edges[ind];}


		void sieve(const uintvector & order);
		void build_essentials();

		symbol_iterator symbol_begin() {return symbol_iterator(this, 0);}
		symbol_iterator symbol_end() {return symbol_iterator(this, nodecnt());}

		label_iterator label_begin() {return label_iterator(this, 0);}
		label_iterator label_end() {return label_iterator(this, nodecnt());}

		edge_iterator edge_begin() {return edge_iterator(this);}
		edge_iterator edge_begin(uint32_t  m) {return edge_iterator(this, m);}
		edge_iterator edge_begin(const uintvector *m) {return edge_iterator(this, m);}
		const edge_iterator & edge_end() {return m_edgeend;}
		const edge * last_essential() const;

		bool potential(graph *cl, const indexvector & m, uint32_t from, uint32_t to) const;
		bool potential(uint32_t from, uint32_t to) const;
		bool adjacent(uint32_t from, uint32_t to) const;
		bool isloner(uint32_t node) const;
		bool label_exists(uint32_t label) const;

		static graph * create_full_graph(const graph *g);
		static graph * create_empty_graph(const symbolvector & sym);
		static graph * create_copy_graph(const graph *g);
		static graph * create_expanded_graph(const graph *g, uint32_t extraedges);
		static graph * create_expanded_graph(const graph *g, uint32_t from, uint32_t to);
		static graph * create_expanded_graph(const graph *g, const symbolvector & s, const indexvector & m);
		static graph * create_potential_graph(const graph *g, const graph *cl, uint32_t from, uint32_t to);


		bool subset(const graph *g, const indexvector & m) const;
		bool subset(const graph *g) const;
		bool subset(const graph *g, indexvector & m, uint32_t a, uint32_t b) const;

		void bind(uint32_t from, uint32_t to) {bind_edge(from, to, m_edgecnt++);}

	protected:
		graph(uint32_t nodecnt, uint32_t edgecnt);
		graph(const graph & g, uint32_t extraedges = 0);

		void build_essential();

		void bind_edge(uint32_t from, uint32_t to, uint32_t edge);

		void bind_closure(uint32_t from, uint32_t to, const graph *ref, bool addself = false);

		nodelist m_sources;

		std::vector<node> m_nodes;
		std::vector<edge> m_edges;

		edge_iterator m_edgeend;

		uint32_t m_edgecnt;
		uint32_t m_sourcecnt;
	
};

#endif
