#include "sequence.h"
#include "episode.h"
#include <getopt.h>
#include <stdlib.h>

#include <string>
#include <vector>

#include "params.h"
#include "indexset.h"

void
outofmemory()
{
	fprintf(stderr, "Oh noes! out of memory\n");
	exit(1);
}

labelvector
readnames(FILE *names)
{
	labelvector data;
	char buf[1024];

	while (fgets(buf, 1024, names) != NULL) {
		std::string s(buf);

		size_t first = s.find_first_not_of(" \n\t\r");
		size_t last = s.find_last_not_of(" \n\t\r");
		if (first == std::string::npos)
			data.push_back("");	
		else
			data.push_back(s.substr(first,last - first + 1));
	}

	return data;
}

int
main(int argc, char ** argv)
{
	static struct option longopts[] = {
		{"thresh",          required_argument,  NULL, 't'},
		{"win",             required_argument,  NULL, 'w'},
		{"maxn",            required_argument,  NULL, 'm'},
		{"out",             required_argument,  NULL, 'o'},
		{"in",              required_argument,  NULL, 'i'},
		{"labels",          required_argument,  NULL, 'l'},
		{"ar-thresh",       required_argument,  NULL, 'c'},
		{"ar-out",          required_argument,  NULL, 'a'},
		{"sparse",          no_argument,        NULL, 's'},
		{"fcl",             no_argument,        NULL, 'f'},
		{"sql",             no_argument,        NULL, 'q'},
		{"minwin",          no_argument,        NULL, 'x'},
		{"wminwin",         no_argument,        NULL, 'y'},
		{"unique",          no_argument,        NULL, 'u'},
		{"no-node-closure", no_argument,        NULL, 'N'},
		{"no-edge-closure", no_argument,        NULL, 'E'},
		{"help",            no_argument,        NULL, 'h'},
		{ NULL,             0,                  NULL,  0 }
	};

	char *inname = NULL;
	char *outname = NULL;
	char *labname = NULL;
	char *arname = NULL;
	labelvector names;
	params par;

	std::set_new_handler(outofmemory);


	indexset::set<symbol_t, uint32_t> foo;

	int ch;
	while ((ch = getopt_long(argc, argv, "t:w:o:i:hsfl:m:xyuqNEa:c:", longopts, NULL)) != -1) {
		switch (ch) {
			case 'h':
				printf("Usage: %s -t <threshold> -w <window> -i <input file> -o <output file> [-xysfhNE] [-l label file] [-c confidence threshold] [-a rule output]\n", argv[0]);
				printf("  -h    print this help\n");
				printf("Episode miner:\n");
				printf("  -t    mining threshold\n");
				printf("  -w    size of the window\n");
				printf("  -i    input file\n");
				printf("  -o    output file\n");
				printf("  -l    label file\n");
				printf("  -u    generate episodes only with unique labels\n");
				printf("  -x    use minimal windows instead of fixed windows\n");
				printf("  -y    use weighted minimal windows instead of fixed windows\n");
				printf("  -s    input file is in sparse format\n");
				printf("  -f    output only f-closed items\n");
				printf("  -N    don't apply node closure\n");
				printf("  -E    don't apply edge closure\n");
				printf("Rule miner:\n");
				printf("  -c    threshold for confidence\n");
				printf("  -a    output file\n");

				return 0;
				break;
			case 'm':
				par.maxnodes = atoi(optarg);
				break;
			case 't':
				par.thresh = atof(optarg);
				break;
			case 'w':
				par.win = atoi(optarg);
				break;
			case 'i':
				inname = optarg;
				break;
			case 'l':
				labname = optarg;
				break;
			case 'o':
				outname = optarg;
				break;
			case 'f':
				par.fcl = true;
				break;
			case 'q':
				par.sql = true;
				break;
			case 'u':
				par.unique = true;
				break;
			case 'N':
				par.nclosure = false;
				break;
			case 'E':
				par.eclosure = false;
				break;
			case 'x':
				par.c = threshold_minwin;
				break;
			case 'y':
				par.c = threshold_weighted;
				break;
			case 's':
				par.sparse = true;
				break;
			case 'c':
				par.ar_thresh = atof(optarg);
				par.ar = true;
				break;
			case 'a':
				arname = optarg;
				break;
		}
	}


	if (inname == NULL) {
		fprintf(stderr, "Missing input file\n");
		return 1;
	}
	
	if (outname == NULL) {
		fprintf(stderr, "Missing output file\n");
		return 1;
	}

	if (arname == NULL && par.ar) {
		fprintf(stderr, "Missing rule output file\n");
		return 1;
	}
	
	


	if (labname != NULL) {
		FILE *l = fopen(labname, "r");
		names = readnames(l);
		fclose(l);
	}


	sequence s(par);

	printf("Reading and building sequence\n");
	FILE *f = fopen(inname, "r");
	s.read(f);
	fclose(f);


	episode_collection ec(&s, par);
	printf("Mining\n");
	ec.compute_base();

	printf("Mining done\n");

	printf("Marking\n");
	ec.mark();
	printf("Marking done\n");

	FILE *out = fopen(outname, "w");
	if (par.sql)
		ec.print_sql(out, names);
	else
		ec.print(out, names);
	fclose(out);

	printf("Printing done\n");

	if (par.ar) {
		assoc & r = ec.ruleminer();
		printf("Association rule miner:\n");
		printf("\t%d generators\n", r.cnt());
		printf("Mining\n");
		r.mine();
		printf("Mining done\n");
		printf("\t%d rules\n", r.rules().cnt());
		printf("\t%d frequent rules\n", r.rules().frequent());
		printf("Purging\n");
		uint32_t c = r.rules().purge();
		printf("Purging done\n");
		printf("\t%d closed rules\n", c);

		printf("Printing\n");
		out = fopen(arname, "w");
		r.rules().print(out, names);
		fclose(out);
		printf("Printing done\n");
	}



	return 0;
}
