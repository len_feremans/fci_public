/*	$Id$	*/
#ifndef EPISODE_H
#define EPISODE_H

#include <stdint.h>
#include <stdio.h>
#include <vector>
#include <list>
#include <map>
#include <string>
#include "queue.h"
#include "params.h"


#include "sequence.h"
#include "graph.h"
#include "seq_tree.h"

class episode_collection;
class base;

typedef std::vector<std::string> labelvector;

class episode
{
	public:	
		episode(graph *g, graph *cl, const indexvector & m, uint32_t freq, double minwincnt, double cond, uint32_t id, bool red = false) :
			m_graph(g), m_closure(cl), m_nodemap(m),
			m_id(id),
			m_freq(freq), m_minwincnt(minwincnt), m_condition(cond),
			m_fclosed(0), 
			m_redundant(red) {}
		~episode() {delete m_graph; delete m_closure; }

		void print(FILE *f, const labelvector & labels);
		void print_sql(FILE *f);

		uint32_t frequency() const {return m_freq;}
		double minwincnt() const {return m_minwincnt;}
		double condition() const {return m_condition;} // Either frequency of minwincnt.

		graph *closure() {return m_closure;}
		graph *generator() {return m_graph;}
		indexvector  & nodemap() {return m_nodemap;}

		void close(episode *ep) {m_fclosed = ep->m_id;}

		bool fclosed() const {return m_fclosed == 0;}

		bool redundant() const {return m_redundant;}

	protected:	
		typedef std::list<episode *> childlist;

		friend class episode_collection;
		friend class base;

		graph *m_graph;
		graph *m_closure;
		indexvector m_nodemap;

		uint32_t m_id;
		uint32_t m_freq;
		double m_minwincnt;
		double m_condition;
		uint32_t m_fclosed;

		bool m_redundant;
};




class base {
	public:
		base(const symbolvector & symbols) : m_symbols(symbols), m_flat(0), m_cands(symbols.size()*(symbols.size() - 1) + 1) {}
		~base() {seq_tree::clean(&m_free);}

		bool test(graph *g);

	protected:
		typedef seq_tree::tree<sigul, episode *> episode_tree;
		typedef std::list<episode_tree *> episodelist;


		episode_tree *add(episode *ep);

		friend class episode_collection;

		episode_tree m_free;

		symbolvector m_symbols;
		episode *m_flat;

		std::vector<episodelist> m_cands;
}; 

#include "closure.h"
#include "assoc.h"


class episode_collection {
	public:
		episode_collection(sequence *s, const params & par) : m_ruleminer(s, par), m_data(s), m_wl(s->size()), m_params(par), m_episodecnt(0) {}
		~episode_collection() {seq_tree::clean(&m_bases);}

		void print(FILE *f, const labelvector & labels);
		void print_sql(FILE *f, const labelvector & labels);
		void compute_base();

		void mark() {m_closed.mark();}

		assoc & ruleminer() {return m_ruleminer;}

	protected:
		typedef seq_tree::tree<uint32_t, base *> base_tree;
		typedef std::list<base_tree *> baselist;
		typedef std::map<uint32_t, baselist *> basemap;
		typedef std::list<base::episode_tree *> episodelist;

		bool test(graph *g);


		base::episode_tree * add_episode(base *b, episode *ep);
		base_tree *add_episode(const symbolvector & symbol, episode *ep);

		bool test_episode(base *b, graph *g);
		void tryout_episode(base *b, graph *g, uint32_t from, uint32_t to);
		void expand_episode(base *b, base::episode_tree *tr);
		void init_episode(base *b);
		void compute_episode(base *b);

		void future_candidates(base *b, episode *tr);

		void future_nodes(episode *ep);
		void future_node(episode *ep, uint32_t node, uint32_t index);
		void future_doublenode(episode *ep, uint32_t node, uint32_t index);

		void future_edges(base *b, episode *ep);
		void future_edges(base *b, episode *ep, uint32_t from, uint32_t to);

		base_tree * add_base(const symbolvector & symbols);
		bool test_base(const symbolvector & syms);
		void tryout_base(const symbolvector & syms);
		void expand_base(base_tree *b);
		void init_base();


		base_tree m_bases;
		basemap m_cands;

		closure m_closed;
		assoc m_ruleminer;

		sequence * m_data;
		windowlist m_wl;

		const params & m_params;
		uint32_t m_episodecnt;
};

#endif
