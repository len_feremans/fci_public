#include "episode.h"

void
assoc::add(episode *ep)
{
	entry *e = 0;
	entryindex::node *n = m_index.safeadd(ep->closure(), e);
	e = n->value;

	if (e == NULL) {
		e = new entry;
		e->g = ep->closure();
		n->value = e;
		e->visited = 0;
		m_entries.push_back(e);
	}

	e->episodes.push_back(ep);
	m_episodecnt++;

}


double
assoc::condition(entry *n, entry *m, windowlist & wl)
{
	if (m_params.c == threshold_support) {
		episode *e1 = n->episodes.front();
		episode *e2 = m->episodes.front();

		return double(e2->condition()) / e1->condition();
	}
	else {
		windowlist twl(m_data->size());
		m_data->build(m->g, twl);
		return wl.mconfidence(twl);
	}
}

void
assoc::mine()
{
	windowlist wl(m_data->size());

	int i=0;//len: added
	int l=m_entries.size();//len: added
    int maxlength=5;

    //len added
    int countParalell = 0;
    for (entrylist::iterator it = m_entries.begin(); it != m_entries.end(); ++it) {
        entry *from = *it;
        if(from->g->last_essential() != 0 || from->g->nodecnt() > maxlength){
            continue;
        }
        else{
            countParalell++;
        }
    }
    printf("paralell and |V|<5 only: %d/%d\n", countParalell, l);

	for (entrylist::iterator it = m_entries.begin(); it != m_entries.end(); ++it) {
		//len: added status reporting
		if(i++ % 1000 == 0){
			printf("%d/%d\n", i++, l);
		}
		entry *from = *it;
		//len added
		if(from->g->last_essential() != 0 || from->g->nodecnt() > maxlength){
			continue;
		}
		if (m_params.c != threshold_support)
			m_data->build(from->g, wl);

		for (entryindex::qiterator qit = m_index.query(from->g); !qit.end(); ++qit) {
			entry *to = *qit;
			if (to->visited == from) continue;
			to->visited = from;
			test(from, to, wl);
		}
	}
}

void
assoc::test(entry *from, entry *to, windowlist & wl)
{
	double cond = condition(from, to, wl);
	for (episodelist::iterator it = from->episodes.begin(); it != from->episodes.end(); ++it) {
		m_rules.add(*it, to->episodes.front(), cond);
	}
}

void
rule_collection::add(episode *from, episode *to, double cond)
{

	graph *fg = from->generator();
	graph *tg = to->closure();

	if (fg->nodecnt() == tg->nodecnt() && fg->edgecnt() == tg->edgecnt()) // from and to are have equal graphs -> ignore
		return;

	rule *r = new rule;
	r->from = from; r->to = to;
	r->condition = cond;
	r->closed = cond >= m_params.ar_thresh;
	if (r->closed)
		m_rulecnt++;

	tail t;
	t.key = r->from->generator();

	ruleindex::node *n = m_index.safeadd(t.key, t);
	n->value.rules.safeadd(r->to->closure(), r);
	m_rules.push_back(r);
}

void
rule_collection::bind(rule *a, rule *b)
{
	if (a == b) return;

	for (rulelist::iterator it = a->super.begin(), itnext; it != a->super.end(); it = itnext) {
		itnext = it; ++itnext;
		if (**it <= *b) return;
		if (*b <= **it)
			a->super.erase(it);
	}
	a->super.push_back(b);
}


uint32_t 
rule_collection::purge()
{
	for (ruleindex::iterator fhead = m_index.nodes(); !fhead.end(); ++fhead) {
		tailindex & htails = fhead->rules;
		for (ruleindex::qiterator thead = m_index.query(fhead->key); !thead.end(); ++thead) {
			tailindex & ttails = thead->rules;
			for (tailindex::iterator ttail = ttails.nodes(); !ttail.end(); ++ttail)
				for (tailindex::qiterator ftail = htails.query((*ttail)->to->closure()); !ftail.end(); ++ftail)
					bind(*ttail, *ftail);
		}
	}

	labelvector l;
	for (rulelist::iterator it = m_rules.begin(); it != m_rules.end(); ++it) {
		rule *a = *it;
		for (rulelist::iterator s = a->super.begin(); s != a->super.end(); ++s) {
			rule *b = *s;
			if (a->condition == b->condition) {
				if (a->closed) m_rulecnt--;
				a->closed = false;
				break;
			}	
		}
	}

	return m_rulecnt;
}

void
rule_collection::print(FILE *f, const labelvector & labels)
{
	for (rulelist::iterator it = m_rules.begin(); it != m_rules.end(); ++it)
		//Len change:
		//if((*it)->closed)
        print(*it, f, labels);
}

void
rule_collection::print(rule *r, FILE *f, const labelvector & labels)
{
	fprintf(f, "from:\n");
	r->from->generator()->print(f);
	fprintf(f, "to:\n");
	r->to->closure()->print(f);
	fprintf(f, "labels:");
	for (uint32_t i = 0; i < r->to->closure()->nodecnt(); i++) {
		uint32_t s = r->to->closure()->get_label(i);
		if (s < labels.size())
			fprintf(f, " %s", labels[s].c_str());
		}
	fprintf(f, "\n");

	fprintf(f, "m-support: %f\n", r->to->condition());
	fprintf(f, "m-confidence: %f\n", r->condition);

	fprintf(f, "\n");
}

