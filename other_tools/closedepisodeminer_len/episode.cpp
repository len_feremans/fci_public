#include "episode.h"

base::episode_tree *
base::add(episode *ep)
{
	episode_tree *et = seq_tree::add(&m_free, ep->m_graph->edge_begin(), ep->m_graph->edge_end(), ep);

	m_cands[ep->m_graph->edgecnt()].push_back(et);
	return et;
}

bool
base::test(graph *g)
{
	episode_tree *c = seq_tree::find(&m_free, g->edge_begin(), g->edge_end());
	return c && c->value();
}


void
episode::print(FILE *f, const labelvector & labels)
{
	fprintf(f, "graph: %d\n", m_id);

	m_closure->print(f);

	fprintf(f, "labels:");
	for (uint32_t i = 0; i < m_closure->nodecnt(); i++) {
		uint32_t s = m_closure->get_label(i);
		if (s < labels.size())
			fprintf(f, " %s", labels[s].c_str());
	}
	fprintf(f, "\n");

	fprintf(f, "m-support: %d\n", m_freq);
	fprintf(f, "m-minwin: %f\n", m_minwincnt);
	fprintf(f, "m-fclosed: %d\n\n", m_fclosed);
}

void
episode::print_sql(FILE *f)
{
	m_closure->print_sql(f, m_id);
	fprintf(f, "INSERT INTO SUPPORT (EPISODEID, FIXEDSUPPORT, MINSUPPORT, CLOSED, ICLOSED) VALUES (%d, %d, %f, %d, 0);\n", m_id, m_freq, m_minwincnt, m_fclosed);
}



base::episode_tree *
episode_collection::add_episode(base *b, episode *ep)
{
	return b->add(ep);
}


bool
episode_collection::test(graph *g)
{
	base_tree *bt = seq_tree::find(&m_bases, g->label_begin(), g->label_end());

	if (bt == 0 || bt->value() == 0)  return false;
	return bt->value()->test(g);
}



episode_collection::base_tree *
episode_collection::add_episode(const symbolvector & symbol, episode *ep)
{
	base_tree *bt = seq_tree::find(&m_bases, label_iterator(symbol, 0), label_iterator(symbol, symbol.size()));
	
	if (bt == 0 || bt->value() == 0)
		bt = add_base(symbol);

	if (ep->m_graph->last_essential() == 0) { // Test whether the episode is parallel
		assert(bt->value()->m_flat == 0);
		bt->value()->m_flat = ep;
	}

	add_episode(bt->value(), ep);

	return bt;
}



bool
episode_collection::test_episode(base *b, graph *g)
{
	episodelist subs;

	if (!seq_tree::test(&b->m_free, g->edge_begin(), g->edge_end(), subs)) {
		return false;
	}
	
	episodelist::iterator sit = subs.begin();
	for (graph::edge_iterator it = g->edge_begin(); it != g->edge_end(); ++it) {
		if (it.test()) {
			const sigul & s = *it;
			graph * c = (*sit)->value()->m_closure;
			indexvector & m = (*sit)->value()->m_nodemap;
			if (c->adjacent(m[s.from], m[s.to]))
				return false;
			if (c->adjacent(m[s.to], m[s.from]))
				return false;
			++sit;
		}
	}

	for (uint32_t i = 0; i < g->nodecnt(); i++) {
		if (!g->isloner(i))
			continue;

		const graph::node & n = g->get_node(i);

		uint32_t j = i;
		while (j < g->nodecnt() && n.symbol.label == g->get_label(j))
			j++;
		
		base_tree *sub = seq_tree::find(&m_bases, label_iterator(b->m_symbols, 0), label_iterator(b->m_symbols, j - 1));
		if (sub == 0) return false;

		sub = seq_tree::find(sub, label_iterator(b->m_symbols, j), label_iterator(b->m_symbols, g->nodecnt()));
		if (sub == 0 || sub->value() == 0) return false;

		base::episode_tree *r = seq_tree::find(&sub->value()->m_free, g->edge_begin(i), g->edge_end());

		if (r == 0 || !r->value()) 
			return false;	
		if (n.in.degree + n.out.degree == 0 && r->value()->m_closure->label_exists(n.symbol.label))
			return false;

	}

	return true;
}

void
episode_collection::tryout_episode(base *b, graph *g, uint32_t from, uint32_t to)
{

	graph *test = graph::create_expanded_graph(g, from, to); 


	// Test that the from -> to is the last essential edge.
	const graph::edge * e= &test->get_edge(test->edgecnt() - 1);
	const graph::edge * l= test->last_essential();

	if (e != l) {
		delete test;
		return;
	}


	if (test_episode(b, test)) {
		m_data->build(test, m_wl);
		index_t sup = m_wl.fsupport(m_params.win);
		double mw = m_wl.msupport();
		double cond = m_params.c == threshold_support ? sup : mw;

		if (cond < m_params.thresh) {
			delete test;
			return;
		}

		indexvector m;
		graph *cand = m_data->closure(test, m, m_wl);

		episode * ep = new episode(test, cand, m, sup, mw, cond, m_episodecnt++);
		add_episode(b, ep);
		m_closed.add(ep);
		if (m_params.ar) m_ruleminer.add(ep);
		future_candidates(b, ep);
	}
	else {
		delete test;
	}
}




void
episode_collection::expand_episode(base *b, base::episode_tree *tr)
{

	episode *ep = tr->value();
	graph *cl = ep->m_closure;
	graph *g = ep->m_graph;


	indexvector & m = ep->m_nodemap;

	const graph::edge *last = g->last_essential();
	if (last == 0) return;



	sigul cur = (sigul)(*last);
	uint32_t clindex = cur.from;
	graph::edge *out = TAILQ_FIRST(&cl->get_node(m[clindex]).out.edges);
	graph::edge *in = TAILQ_FIRST(&cl->get_node(m[clindex]).in.edges);

	graph::edge *essout = TAILQ_FIRST(&g->get_node(cur.from).out.edges);
	graph::edge *essin = TAILQ_FIRST(&g->get_node(cur.to).in.edges);

	std::list<sigul> cands;
	seq_tree::find_super(tr, cur, cands);


	for (std::list<sigul>::iterator it = cands.begin(); it != cands.end(); ++it) {
		sigul s = *it;

		if (s.from != clindex) {
			clindex = s.from;
			out = TAILQ_FIRST(&cl->get_node(m[clindex]).out.edges);
			in = TAILQ_FIRST(&cl->get_node(m[clindex]).in.edges);
		}

		while (out && out->out.target < m[s.to])
			out = TAILQ_NEXT(out, out.entries);
		if (out && out->out.target == m[s.to])
			continue;

		while (in && in->in.target < m[s.to])
			in = TAILQ_NEXT(in, in.entries);
		if (in && in->in.target == m[s.to])
			continue;

		if (s.from == cur.to) {
			while (essout && essout->out.target < s.to)
				essout = TAILQ_NEXT(essout, out.entries);
			if (!essout || essout->out.target != s.to)
				continue;
		}

		if (s.to == cur.from) {
			while (essin && essin->in.target < s.from)
				essin = TAILQ_NEXT(essin, in.entries);
			if (!essin || essin->in.target != s.from)
				continue;
		}

		tryout_episode(b, g, s.from, s.to);
	}

	graph::edge *e;

	TAILQ_FOREACH(e, &g->get_node(cur.from).out.essentials, out.essential) {
		if (e->out.target == cur.to) continue;
		if (cl->adjacent(m[e->out.target], m[cur.to]) || cl->adjacent(m[cur.to], m[e->out.target]))
			continue;
		if (g->potential(e->out.target, cur.to))
			tryout_episode(b, g, e->out.target, cur.to);
	}

	TAILQ_FOREACH(e, &g->get_node(cur.to).in.essentials, in.essential) {
		if (e->in.target == cur.from) continue;
		if (cl->adjacent(m[e->in.target], m[cur.from]) || cl->adjacent(m[cur.from], m[e->in.target]))
			continue;
		if (g->potential(cur.from, e->in.target))
			tryout_episode(b, g, cur.from, e->in.target);
	}
}


void
episode_collection::init_episode(base *b)
{
	if (b->m_flat == 0) return; // No parallel episode => don't generate new episodes.

	graph *g = b->m_flat->m_graph;
	graph *cl = b->m_flat->m_closure;
	indexvector & m = b->m_flat->m_nodemap;


	if (g->nodecnt() == 1)
		return;
	

	for (uint32_t from = 0; from < g->nodecnt(); from++) {
		if (0 < from && g->get_label(from) == g->get_label(from - 1))
			continue;

		graph::edge *out = TAILQ_FIRST(&cl->get_node(m[from]).out.edges);
		graph::edge *in = TAILQ_FIRST(&cl->get_node(m[from]).in.edges);


		for (uint32_t to = 0; to < g->nodecnt(); to++) {
			if (from == to) continue;
			if (to < g->nodecnt() - 1 && g->get_label(to) == g->get_label(to + 1))
				continue;

			while (out && out->out.target < m[to])
				out = TAILQ_NEXT(out, out.entries);

			if (out && out->out.target == m[to])
				continue;

			while (in && in->in.target < m[to])
				in = TAILQ_NEXT(in, in.entries);

			if (in && in->in.target == m[to])
				continue;
			
			tryout_episode(b, g, from, to);
		}
	}
}


void
episode_collection::compute_episode(base *b)
{
	uint32_t n = b->m_symbols.size();
	if (n < 2) return;
	std::vector<episodelist> & cur = b->m_cands;

	init_episode(b);

	for (uint32_t i = 1; i < cur.size(); i++) {
		episodelist next;
		for (episodelist::iterator it = cur[i].begin(); it != cur[i].end(); ++it) 
			expand_episode(b, *it);
		cur[i].clear();
	}
}




episode_collection::base_tree *
episode_collection::add_base(const symbolvector & sym)
{
	base *bas = new base(sym);
	episode_collection::base_tree *b =  seq_tree::add(&m_bases, label_iterator(sym, 0), label_iterator(sym, sym.size()) , bas);

	basemap::iterator it = m_cands.find(sym.size());

	if (it == m_cands.end()) {
		baselist *bl = new baselist;
		bl->push_back(b);
		m_cands[sym.size()] = bl;
	}
	else
		it->second->push_back(b);

	return b;
}

bool
episode_collection::test_base(const symbolvector & sym)
{
	baselist subs;
	label_iterator end(sym, sym.size());

	if (!seq_tree::test(&m_bases, label_iterator(sym, 0), end, subs))
		return false;

	baselist::iterator sit = subs.begin();

	for (label_iterator it = label_iterator(sym, 0); it != end; ++it) {
		if (it.test()) {
			episode *ep = (*sit)->value()->m_flat;
			if (ep == 0) return false;
			if (it.solitary() && ep->m_closure->label_exists(*it))
				return false;
			++sit;
		}
	}

	return true;
}


void
episode_collection::tryout_base(const symbolvector & syms)
{
	if (syms.size() == 1 || test_base(syms)) {
		graph *test = graph::create_empty_graph(syms); 
		m_data->build(test, m_wl);
		index_t sup = m_wl.fsupport(m_params.win);
		double mw = m_wl.msupport();
		double cond = m_params.c == threshold_support ? sup : mw;

		if (cond < m_params.thresh) {
			delete test;
			return;
		}
		
		indexvector m;
		graph *cand = m_data->closure(test, m, m_wl);
		episode *ep = new episode(test, cand, m, sup, mw, cond, m_episodecnt++);
		base_tree *b = add_episode(syms, ep);
		m_closed.add(ep);
		if (m_params.ar) m_ruleminer.add(ep);
		future_candidates(b->value(), ep);
	}
}

void
episode_collection::expand_base(base_tree *bt)
{
	base *b = bt->value();
	assert(b);
	if (b->m_flat == 0) return;

	symbolvector syms(b->m_symbols.size() + 1);
	symbol_t s = b->m_symbols.back();
	s.occur++;


	std::copy(b->m_symbols.begin(), b->m_symbols.end(), syms.begin());

	if (!m_params.unique) {
		syms.back() = s;
		tryout_base(syms);
	}


	base_tree::childmap::iterator it = bt->location();
	++it;
	for (; it != bt->parent()->children().end(); ++it) {
		if (it->second->value() == 0 || it->second->value()->m_flat == 0) continue;
		s = it->second->value()->m_symbols.back();
		syms.back() = s;
		tryout_base(syms);
	}
}

void
episode_collection::init_base()
{
	symbolvector v(1);
	v[0].occur = 0;

	for (v[0].label = 0; v[0].label < m_data->symbolcnt(); v[0].label++) {
		tryout_base(v);

	}
}




void
episode_collection::compute_base()
{
	printf("Initiating the base\n");
	init_base();

	uint32_t level = 2;

	while (!m_cands.empty()) {
		baselist *cur = m_cands.begin()->second;

		for (baselist::iterator it = cur->begin(); it != cur->end(); ++it)
			compute_episode((*it)->value());

		printf("Building episodes with %d nodes (%d candidates)\n", level++, (uint32_t)cur->size());

		if (level - 1 <=  m_params.maxnodes) {
			for (baselist::iterator it = cur->begin(); it != cur->end(); ++it)
				expand_base(*it);
		}

		delete cur;
		m_cands.erase(m_cands.begin());
	}
}



void
episode_collection::print(FILE *f, const labelvector & labels)
{
	m_closed.print(f, labels, m_params.fcl, false);
}

void
episode_collection::print_sql(FILE *f, const labelvector & labels)
{

	fprintf(f, "BEGIN TRANSACTION;\n");
	fprintf(f, "DROP TABLE IF EXISTS EPISODES;\n");
	fprintf(f, "DROP TABLE IF EXISTS NODES;\n");
	fprintf(f, "DROP TABLE IF EXISTS EDGES;\n");
	fprintf(f, "DROP TABLE IF EXISTS SUPPORT;\n");
	fprintf(f, "DROP TABLE IF EXISTS LABELS;\n");
	fprintf(f, "DROP INDEX IF EXISTS NODES_INDEX;\n");
	fprintf(f, "DROP INDEX IF EXISTS EDGES_INDEX;\n");

	fprintf(f, "CREATE TABLE EPISODES ( EPISODEID INTEGER PRIMARY KEY, NODECNT INTEGER, EDGECNT INTEGER, TYPE TEXT);\n");
	fprintf(f, "CREATE TABLE NODES ( EPISODEID INTEGER, NODEID INTEGER, LABEL INTEGER);\n");
	fprintf(f, "CREATE TABLE EDGES ( EPISODEID INTEGER, SOURCE INTEGER, TARGET INTEGER, TYPE TEXT);\n");
	fprintf(f, "CREATE TABLE SUPPORT ( EPISODEID INTEGER PRIMARY KEY, FIXEDSUPPORT INTEGER, MINSUPPORT INTEGER, CLOSED INTEGER, ICLOSED INTEGER);\n");
	fprintf(f, "CREATE TABLE LABELS ( ID INTEGER PRIMARY KEY, NAME TEXT);\n");

	for (uint32_t i = 0; i < labels.size(); i++)
		fprintf(f, "INSERT INTO LABELS (ID, NAME) VALUES (%d, '%s');\n", i, labels[i].c_str());

	m_closed.print(f, labels, m_params.fcl, true);
	fprintf(f, "CREATE INDEX NODES_INDEX ON NODES (EPISODEID);\n");
	fprintf(f, "CREATE INDEX EDGES_INDEX ON EDGES (EPISODEID);\n");
	fprintf(f, "COMMIT;\n");
}
