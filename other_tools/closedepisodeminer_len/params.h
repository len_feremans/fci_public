#ifndef PARAMS_H
#define PARAMS_H

#include <limits>
#include <stdint.h>

#include "defines.h"

enum threshold_condition {threshold_support, threshold_minwin, threshold_weighted};



struct params {
	params() :
		win(0), thresh(1), maxnodes(std::numeric_limits<uint32_t>::max()),
		fcl(false), sparse(false), unique(false),
		nclosure(true), eclosure(true), sql(false), ar(false), ar_thresh(1),
		c(threshold_support) {};

	index_t win;
	double thresh;
	uint32_t maxnodes;

	bool fcl;
	bool sparse;
	bool unique;
	bool nclosure;
	bool eclosure;
	bool sql;

	bool ar;
	double ar_thresh;

	threshold_condition c;
};

#endif
