#ifndef LATTICE_H
#define LATTICE_H

#include "defines.h"
#include <list>

template<class V>
class lattice
{
	public:

		struct node;

		typedef std::list<node *> nodelist;
		
		struct node {
			node(const V & v) : value(v), seed(0) {}
			V value;
			uint32_t seed;
			uint32_t count;
			nodelist in, out;
		};

		node *
		add(const V & v)
		{
			node *n =  new node(v);
			m_nodes.push_back(n);
			return n;
		};

		uint32_t cnt() const {return m_nodes.size();}


		void
		build()
		{
			for (typename nodelist::iterator it = m_nodes.begin(); it != m_nodes.end(); ++it) {
				node *n = *it;
				//printf("build:\n");
				//n->value->episodes.front()->closure()->print(stdout);
				for (typename nodelist::iterator j = n->in.begin(); j != n->in.end(); ++j) {
					node *m = *j;
					m->out.push_back(n);
					//m->value->episodes.front()->closure()->print(stdout);
				}
			}
		}

		nodelist & nodes() {return m_nodes;}


	protected:
		nodelist m_nodes;
};


template<class L, class comp>
void
lattice_bind(typename L::node *from, typename L::node *to, comp cmp = comp())
{
	if (from == to) return;

	for (typename L::nodelist::iterator it = to->in.begin(), itnext; it != to->in.end(); it = itnext) {
		itnext = it; ++itnext;
		typename L::node *c = *it;
		if (cmp(from->value, c->value))
			return;
		if (cmp(c->value, from->value))
			to->in.erase(it);
	}
	to->in.push_back(from);
}

template<class L, class comp>
void
lattice_init(typename L::node *n, typename L::node *base, comp & cmp)
{
	n->count = 0;
	for (typename L::nodelist::iterator j = n->in.begin(); j != n->in.end(); ++j) {
		typename L::node *m = *j;
		if (cmp(base->value, m->value))
			n->count++;
	}
}

template<class L, class comp>
void
lattice_accept(typename L::node *n, typename L::node *base, typename L::nodelist & out, uint32_t seed, comp cmp = comp())
{
	for (typename L::nodelist::iterator it = n->out.begin(); it != n->out.end(); ++it) {
		typename L::node *m = *it;
		//m->value->episodes.front()->closure()->print(stdout);
		if (seed != m->seed) {
			lattice_init<L, comp>(m, base, cmp);
			m->seed = seed;
		}
		assert(m->count > 0);

		m->count--;

		if (m->count == 0)
			out.push_back(m);
	}
}



#endif
