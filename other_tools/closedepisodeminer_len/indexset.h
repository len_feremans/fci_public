#ifndef INDEXSET_H
#define INDEXSET_H

#include <list>
#include <vector>

namespace indexset {

template <class K, class T> class set {
	public:

		struct node {
			uint32_t id;
			uint32_t keycnt;
			T value;
			
		};

	node * add(const T & v) {node n = {m_nodes.size(), 0, v}; m_nodes.push_back(n); return &m_nodes.back();}

	void bind(const K & k, node * n) {m_keys[k].push_back(n); n->keycnt++;}

	typedef std::vector<node *> nodevector;

	void
	init(const K & k, nodevector & r, uint32_t c = 0)
	{
		typename keymap::iterator it = m_keys.find(k);
		if (it == m_keys.end()) {
			r.resize(0);
			return;
		}

		nodeplist & p = it->second;

		r.resize(p.size());

		uint32_t i = 0;
		for (typename nodeplist::iterator it = p.begin(); it != p.end(); ++it)
			if (c == 0 || (*it)->keycnt == c)
				r[i++] = *it;
		r.resize(i);
	}

	void
	init(nodevector & r, uint32_t c = 0)
	{
		r.resize(m_nodes.size());

		uint32_t i = 0;
		for (typename nodelist::iterator it = m_nodes.begin(); it != m_nodes.end(); ++it)
			if (c == 0 || (*it).keycnt == c)
				r[i++] = &(*it);
		r.resize(i);
	}

	void
	sieve(const K & k, nodevector & r)
	{
		typename keymap::iterator it = m_keys.find(k);
		if (it == m_keys.end()) {
			r.resize(0);
			return;
		}

		nodeplist & p = it->second;
		typename nodeplist::iterator pit = p.begin();
		uint32_t i = 0;
		uint32_t j = 0;

		while (j < r.size() && pit != p.end()) {
			node *n = *pit;
			if (n->id == r[j]->id) {
				if (i != j) r[i] = r[j];
				j++;
				i++;
				++pit;
			}
			else if (n->id < r[j]->id)
				++pit;
			else
				j++;
		}
		r.resize(i);
	}

	uint32_t
	size(const K & k)
	{
		typename keymap::iterator it = m_keys.find(k);
		if (it == m_keys.end())
			return 0;
		return it->second.size();
	}

	typedef std::list<node> nodelist;

	nodelist & nodes() {return m_nodes;}

	protected:
		typedef std::list<node *> nodeplist;

		typedef std::map<K, nodeplist> keymap;

		nodelist m_nodes;
		keymap m_keys;
};


template <class S, class iterator, class V>
typename S::node *
add(S & s, iterator begin, iterator end, V & v)
{
	typename S::node *n = s.add(v);

	for (; begin != end; ++begin)
		s.bind(*begin, n);

	return n;
}

template <class S, class iterator>
void
query(S & s, iterator begin, iterator end, typename S::nodevector & r, uint32_t c = 0)
{
	if (begin == end) {
		s.init(r, c);
		return;
	}

	iterator small = begin;
	uint32_t size = std::numeric_limits<uint32_t>::max();


	for (iterator it = begin; it != end; ++it) {
		uint32_t cand = s.size(*it);
		if (cand < size) {
			small = it;
			size = cand;
		}
	}

	s.init(*small, r, c);
	if (r.size() == 0) return;

	for (iterator it = begin; it != end; ++it) {
		if (it != small)
			s.sieve(*it, r);
		if (r.size() == 0) return;
	}
}

template <class S, class iterator, class V>
typename S::node *
safeadd(S & s, iterator begin, iterator end, V & v, uint32_t c)
{
	typename S::nodevector r;
	query(s, begin, end, r, c);

	if (r.size() > 0)
		return r[0];

	typename S::node *n = s.add(v);

	for (; begin != end; ++begin)
		s.bind(*begin, n);

	return n;
}

};


#endif
