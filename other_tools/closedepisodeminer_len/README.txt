Closepi source code was provided by Tatti. 
Original paper: Tatti, Nikolaj, and Boris Cule. "Mining closed strict episodes." Data Mining and Knowledge Discovery(2012).


Changes closepi (closedepisodeminer_len):

Output of closed rules was changed, in assoc.c 
from:
void rule_collection::print(FILE *f, const labelvector & labels)
{
	for (rulelist::iterator it = m_rules.begin(); it != m_rules.end(); ++it)
		//Len change:
		if((*it)->closed)
			print(*it, f, labels);
}

to:
void rule_collection::print(FILE *f, const labelvector & labels)
{
	for (rulelist::iterator it = m_rules.begin(); it != m_rules.end(); ++it)
		//Len change:
		//if((*it)->closed)
		print(*it, f, labels);
}

//in addition added contraint for rules to limit it
//1) to parallel episodes 
//2) upto maxlength 5 (hard coded)
//3) + debug info for profiling
//see assoc.cpp:
for (entrylist::iterator it = m_entries.begin(); it != m_entries.end(); ++it) {
		//len: added status reporting
		if(i++ % 1000 == 0){
			printf("%d/%d\n", i++, l);
		}
		entry *from = *it;
		//len added
		if(from->g->last_essential() != 0 || from->g->nodecnt() > maxlength){
			continue;
		}
