#include "graph.h"

void
normalize(symbolvector & s)
{
	s[0].occur = 0;
	for (uint32_t k = 1; k < s.size(); k++)
		s[k].occur = (s[k].label == s[k - 1].label ? s[k - 1].occur + 1 : 0);
}


graph *
graph::create_full_graph(const graph *g)
{
	uint32_t cnt = g->nodecnt();
	graph *ret = new graph(cnt, cnt*(cnt - 1));

	for (uint32_t i = 0; i < cnt; i++)
		ret->m_nodes[i].symbol = g->m_nodes[i].symbol;

	uint32_t k = 0;
	for (uint32_t i = 0 ; i < cnt; i++) {
		for (uint32_t j = 0 ; j < i; j++)
			ret->bind_edge(i, j, k++);

		for (uint32_t j = i + 1 ; j < cnt; j++)
			ret->bind_edge(i, j, k++);
	}

	return ret;
}


graph *
graph::create_empty_graph(const symbolvector & syms)
{
	uint32_t edgecnt = 0;
	uint32_t nodecnt = syms.size();
	for (uint32_t i = 0; i < nodecnt; i++)
		edgecnt += syms[i].occur;

	graph *ret = new graph(nodecnt, edgecnt);
	for (uint32_t i = 0; i < nodecnt; i++) {
		ret->m_nodes[i].symbol = syms[i];
	}

	uint32_t k = 0;
	for (uint32_t i = 0; i < nodecnt; i++)
		for (uint32_t j = 0; j < syms[i].occur; j++)
			ret->bind_edge(i - j - 1, i, k++);

	ret->build_essentials();

	return ret;
}


graph *
graph::create_expanded_graph(const graph *g, uint32_t from, uint32_t to)
{
	graph *ret = new graph(*g, 1);
	ret->bind_edge(from, to, ret->m_edgecnt++);
	ret->build_essentials();
	return ret;
}

graph *
graph::create_expanded_graph(const graph *g, uint32_t extraedges)
{
	graph *ret = new graph(*g, extraedges);
	return ret;
}


graph *
graph::create_expanded_graph(const graph *g, const symbolvector & s, const indexvector & m)
{
	uint32_t edgecnt = g->edgecnt();
	uint32_t nodecnt = s.size();

	for (uint32_t i = 0; i < nodecnt; i++)
		edgecnt += s[i].occur;

	for (uint32_t i = 0; i < g->nodecnt(); i++)
		edgecnt -= g->get_symbol(i).occur;

	graph *ret = new graph(nodecnt, edgecnt);
	for (uint32_t i = 0; i < nodecnt; i++) {
		ret->m_nodes[i].symbol = s[i];
	}

	uint32_t k = 0;
	for (uint32_t i = 0; i < nodecnt; i++)
		for (uint32_t j = 0; j < s[i].occur; j++)
			ret->bind_edge(i - j - 1, i, k++);

	for (uint32_t i = 0; i < g->nodecnt(); i++) {
		edge *e;
		uint32_t l = g->get_label(i);
		TAILQ_FOREACH(e, &g->m_nodes[i].out.edges, out.entries) {
			if (l != g->get_label(e->out.target))
				ret->bind_edge(m[i], m[e->out.target], k++);
		}
	}

	ret->build_essentials();

	return ret;
}



graph *
graph::create_copy_graph(const graph *g)
{
	graph *ret = new graph(*g, 0);
	ret->build_essentials();
	return ret;
}

graph *
graph::create_potential_graph(const graph *g, const graph *cl, uint32_t from, uint32_t to)
{
	graph *ret = new graph(*g, cl->edgecnt() - g->edgecnt());

	ret->bind_closure(from, to, g, false);

	edge *e;
	edge *f = TAILQ_FIRST(&g->m_nodes[to].in.edges);

	TAILQ_FOREACH(e, &g->m_nodes[from].in.edges, in.entries) {
		while (f && f->in.target < e->in.target)
			f = TAILQ_NEXT(f, in.entries);
		if (f && f->in.target == e->in.target)
			continue;
		ret->bind_closure(e->in.target, to, g, true);
	}


	ret->build_essentials();
	return ret;
}


void
graph::check() const
{
	for (uint32_t i = 0; i < m_nodes.size(); i++) {
		int32_t j = -1;
		edge *e;
		TAILQ_FOREACH(e, &m_nodes[i].out.edges, out.entries) {
			if (j >= (int32_t)e->out.target) {
				print(stdout);
				assert(false);
			}
			j = e->out.target;
		}
	}

	for (uint32_t i = 0; i < m_nodes.size(); i++) {
		int32_t j = -1;
		edge *e;
		TAILQ_FOREACH(e, &m_nodes[i].in.edges, in.entries) {
			if (j >= (int32_t)e->in.target) {
				print(stdout);
				assert(false);
			}
			j = e->in.target;
		}
	}


}

void
graph::bind_closure(uint32_t from, uint32_t to, const graph *ref, bool addself)
{
	if (addself) {
		bind_edge(from, to, m_edgecnt++);
	}

	edge *e;
	edge *f = TAILQ_FIRST(&ref->m_nodes[from].out.edges);

	TAILQ_FOREACH(e, &ref->m_nodes[to].out.edges, out.entries) {
		while (f && f->out.target < e->out.target)
			f = TAILQ_NEXT(f, out.entries);
		if (f && f->out.target == e->out.target)
			continue;
		bind_edge(from, e->out.target, m_edgecnt++);
	}
	
}



graph::graph(const graph & g, uint32_t extraedges) :
	m_nodes(g.nodecnt()),
	m_edges(g.edgecnt() + extraedges),
	m_edgeend(this, g.nodecnt(), NULL),
	m_edgecnt(g.edgecnt()),
	m_sourcecnt(0)
{
	TAILQ_INIT(&m_sources);
	for (uint32_t i = 0 ; i < m_nodes.size(); i++) {
		m_nodes[i].index = i;
		m_nodes[i].symbol = g.m_nodes[i].symbol;
		m_nodes[i].out.degree = g.m_nodes[i].out.degree;
		TAILQ_INIT(&m_nodes[i].out.edges);
		TAILQ_INIT(&m_nodes[i].out.essentials);
		m_nodes[i].in.degree = g.m_nodes[i].in.degree;
		TAILQ_INIT(&m_nodes[i].in.edges);
		TAILQ_INIT(&m_nodes[i].in.essentials);
	}

	uint32_t j = 0;
	for (uint32_t i = 0; i < nodecnt(); i++) {
		edge *e;
		TAILQ_FOREACH(e, &g.m_nodes[i].out.edges, out.entries) {
			bind_edge(i, e->out.target, j++);
		}
	}

}



graph::graph(uint32_t nodecnt, uint32_t edgecnt) :
	m_nodes(nodecnt),
	m_edges(edgecnt),
	m_edgeend(this, nodecnt, NULL),
	m_edgecnt(edgecnt),
	m_sourcecnt(0)
{
	TAILQ_INIT(&m_sources);
	for (uint32_t i = 0 ; i < nodecnt; i++) {
		m_nodes[i].index = i;
		m_nodes[i].in.degree = 0;
		TAILQ_INIT(&m_nodes[i].in.edges);
		TAILQ_INIT(&m_nodes[i].in.essentials);
		m_nodes[i].out.degree = 0;
		TAILQ_INIT(&m_nodes[i].out.edges);
		TAILQ_INIT(&m_nodes[i].out.essentials);
	}

}

void
graph::bind_edge(uint32_t from, uint32_t to, uint32_t ind)
{
	assert(ind < m_edges.size());

	m_edges[ind].out.target = to;
	m_edges[ind].in.target = from;

	edge *e = TAILQ_LAST(&m_nodes[from].out.edges, edgelist);
	while (e && e->out.target > to)
		e = TAILQ_PREV(e, edgelist, out.entries);

	if (e)
		TAILQ_INSERT_AFTER(&m_nodes[from].out.edges, e, &m_edges[ind], out.entries);
	else
		TAILQ_INSERT_HEAD(&m_nodes[from].out.edges, &m_edges[ind], out.entries);

	e = TAILQ_LAST(&m_nodes[to].in.edges, edgelist);
	while (e && e->in.target > from)
		e = TAILQ_PREV(e, edgelist, in.entries);

	if (e)
		TAILQ_INSERT_AFTER(&m_nodes[to].in.edges, e, &m_edges[ind], in.entries);
	else
		TAILQ_INSERT_HEAD(&m_nodes[to].in.edges, &m_edges[ind], in.entries);
}



void
graph::sieve(const uintvector & order)
{
	for (uint32_t i = 0 ; i < m_nodes.size(); i++) {
		edge *e, *enext;
		for (e = TAILQ_FIRST(&m_nodes[i].out.edges); e != NULL; e = enext) {
			enext = TAILQ_NEXT(e, out.entries);
			if (order[e->out.target] < order[i]) {
				TAILQ_REMOVE(&m_nodes[i].out.edges, e, out.entries);
				TAILQ_REMOVE(&m_nodes[e->out.target].in.edges, e, in.entries);
				m_edgecnt--;
			}
		}
	}
}


void
graph::build_essentials()
{
	for (uint32_t i = 0 ; i < m_nodes.size(); i++) {
		m_nodes[i].in.degree = 0;
		m_nodes[i].out.degree = 0;
	}

	for (uint32_t i = 0 ; i < m_nodes.size(); i++) {
		node * n = &m_nodes[i];
		TAILQ_INIT(&n->out.essentials);

		edge *e;

		TAILQ_FOREACH(e, &n->out.edges, out.entries) {
			TAILQ_INSERT_TAIL(&n->out.essentials, e, out.essential);
			e->isess = true;
		}

		TAILQ_FOREACH(e, &n->out.edges, out.entries) {
			node *m = &m_nodes[e->out.target];

			edge *c = TAILQ_FIRST(&m->out.edges);
			edge *ess = TAILQ_FIRST(&n->out.essentials);

			while (ess && c) {
				if (ess->out.target < c->out.target)
					ess = TAILQ_NEXT(ess, out.essential);
				else if (ess->out.target > c->out.target)
					c = TAILQ_NEXT(c, out.entries);
				else {
					edge *t = ess;
					ess = TAILQ_NEXT(ess, out.essential);
					c = TAILQ_NEXT(c, out.entries);

					t->isess = false;
					TAILQ_REMOVE(&n->out.essentials, t, out.essential);
				}
			}
		}
	}

	for (uint32_t i = 0 ; i < m_nodes.size(); i++) {
		node * n = &m_nodes[i];
		edge *e;
		TAILQ_FOREACH(e, &n->out.essentials, out.essential) {
			node *m = &m_nodes[e->out.target];

			n->out.degree++;
			m->in.degree++;
			TAILQ_INSERT_TAIL(&m->in.essentials, e, in.essential);
		}
	}


	TAILQ_INIT(&m_sources);
	m_sourcecnt = 0;

	for (uint32_t i = 0 ; i < m_nodes.size(); i++) {
		if (m_nodes[i].in.degree == 0) {
			TAILQ_INSERT_TAIL(&m_sources, &m_nodes[i], sources);
			m_sourcecnt++;
		}
	}
}


bool
graph::adjacent(uint32_t from, uint32_t to) const
{
	edge *f;
	TAILQ_FOREACH(f, &m_nodes[from].out.edges, out.entries) {
		if (f->out.target == to)
			return true;
		if (f->out.target > to)
			return false;
	}
	return false;
}

bool
graph::potential(graph *cl, const indexvector & m, uint32_t from, uint32_t to) const
{
	edge *f;

	edge *e = TAILQ_FIRST(&cl->m_nodes[m[from]].out.edges);

	TAILQ_FOREACH(f, &m_nodes[to].out.essentials, out.essential) {
		while (e && e->out.target < m[f->out.target])
			e = TAILQ_NEXT(e, out.entries);
		if (!e || e->out.target != m[f->out.target])
			return false;
	}

	e = TAILQ_FIRST(&cl->m_nodes[m[to]].in.edges);

	TAILQ_FOREACH(f, &m_nodes[from].in.essentials, in.essential) {
		while (e && e->in.target < m[f->in.target])
			e = TAILQ_NEXT(e, in.entries);
		if (!e || e->in.target != m[f->in.target])
			return false;
	}

	return true;
}

bool
graph::potential(uint32_t from, uint32_t to) const
{
	edge *f;

	edge *e = TAILQ_FIRST(&m_nodes[from].out.edges);

	TAILQ_FOREACH(f, &m_nodes[to].out.essentials, out.essential) {
		while (e && e->out.target < f->out.target)
			e = TAILQ_NEXT(e, out.entries);
		if (!e || e->out.target != f->out.target)
			return false;
	}

	e = TAILQ_FIRST(&m_nodes[to].in.edges);

	TAILQ_FOREACH(f, &m_nodes[from].in.essentials, in.essential) {
		while (e && e->in.target < f->in.target)
			e = TAILQ_NEXT(e, in.entries);
		if (!e || e->in.target != f->in.target)
			return false;
	}

	return true;
}

bool
graph::isloner(uint32_t ind) const
{
	const node & n = m_nodes[ind];

	bool indeg = n.in.degree == 0 || (n.in.degree == 1 && get_label(TAILQ_FIRST(&n.in.essentials)->in.target) == n.symbol.label);
	bool outdeg = n.out.degree == 0 || (n.out.degree == 1 && get_label(TAILQ_FIRST(&n.out.essentials)->out.target) == n.symbol.label);

	return indeg && outdeg;
}

bool
graph::label_exists(uint32_t l) const
{
	for (uint32_t i = 0; i < nodecnt(); i++) {
		if (get_label(i) == l)
			return true;
		else if (get_label(i) > l)
			return false;
	}
	return false;
}

const graph::edge *
graph::last_essential() const
{
	for (int32_t i = nodecnt() - 1; i >= 0; i--) {
		const node *n = &m_nodes[i];
		edge *e;
		TAILQ_FOREACH_REVERSE(e, &n->out.essentials, edgelist, out.essential) {
			if (get_label(e->out.target) != n->symbol.label)
				return e;
		}
	}
	return NULL;
}


bool
graph::subset(const graph *g, const indexvector &  m) const
{
	for (uint32_t i = 0; i < nodecnt(); i++) {
		const node *n1 = &m_nodes[i];
		const node *n2 = &g->m_nodes[m[i]];
		const edge *e1;
		const edge *e2 = TAILQ_FIRST(&n2->out.edges);
		TAILQ_FOREACH(e1, &n1->out.edges, out.entries) {
			while (e2 && e2->out.target < m[e1->out.target])
				e2 = TAILQ_NEXT(e2, out.entries);
			if (!e2 || e2->out.target > m[e1->out.target])
				return false;
		}
	}
	return true;
}

bool
graph::subset(const graph *g, indexvector & m, uint32_t a, uint32_t b) const
{
	if (b == nodecnt())
		return subset(g, m);

	while (a < g->nodecnt() && g->get_label(a) < get_label(b))
		a++;

	while (a < g->nodecnt() && g->get_label(a) == get_label(b)) {
		m[b] = a;
		a++;
		if (subset(g, m, a, b + 1)) return true;
	}

	return false;
}

bool
graph::subset(const graph *g) const
{
	indexvector m(nodecnt());
	return subset(g, m, 0, 0);
}






void
graph::print(FILE *f) const
{
	uint32_t ecnt = 0;
	for (uint32_t i = 0; i < nodecnt(); i++) 
		ecnt += m_nodes[i].in.degree;

	fprintf(f, "size: %d %d\n", nodecnt(), ecnt);
	fprintf(f, "nodes:");
	for (uint32_t i = 0; i < nodecnt(); i++)
		fprintf(f, " %d", get_label(i));

	fprintf(f, "\n");

	fprintf(f, "edges:");

	for (uint32_t i = 0; i < nodecnt(); i++) {
		edge *e;
		TAILQ_FOREACH(e, &m_nodes[i].out.essentials, out.essential)
			fprintf(f, " %d -> %d", i, e->out.target);
	}
	fprintf(f, "\n");

	if (last_essential() == NULL)
		fprintf(f, "type: parallel\n");
	else if (edgecnt() == nodecnt() * (nodecnt() - 1) / 2)
		fprintf(f, "type: serial\n");
	else
		fprintf(f, "type: general\n");
}



void
graph::print_sql(FILE *f, uint32_t id) const
{
	uint32_t ecnt = 0;
	for (uint32_t i = 0; i < nodecnt(); i++) 
		ecnt += m_nodes[i].in.degree;

	const char *type = 0;

	if (last_essential() == NULL)
		type = "parallel";
	else if (edgecnt() == nodecnt() * (nodecnt() - 1) / 2)
		type = "serial";
	else
		type = "general";

	fprintf(f, "INSERT INTO EPISODES (EPISODEID, NODECNT, EDGECNT, TYPE) VALUES (%d, %d, %d, \"%s\");\n", id, nodecnt(), ecnt, type);

	for (uint32_t i = 0; i < nodecnt(); i++) {
		fprintf(f, "INSERT INTO NODES (EPISODEID, NODEID, LABEL) VALUES (%d, %d, %d);\n", id, i, get_label(i));
	}


	for (uint32_t i = 0; i < nodecnt(); i++) {
		edge *e;
		TAILQ_FOREACH(e, &m_nodes[i].out.essentials, out.essential)
			fprintf(f, "INSERT INTO EDGES (EPISODEID, SOURCE, TARGET) VALUES (%d, %d, %d);\n", id, i, e->out.target);
	}
}


