#ifndef SEQ_TREE
#define SEQ_TREE

#include <map>
#include <memory>
#include <list>

namespace seq_tree {


template <class K, class T> class tree {
	public:
		typedef std::map<K, tree *> childmap;
		typedef tree<K, T> _type;

		tree() : m_parent(), m_value() {}
		virtual ~tree();
		tree(const T & value, _type *par) : m_parent(par), m_value(value) {}

		_type * child(const K & key);
		_type * parent() {return m_parent;}
		childmap & children() {return m_children;}
		const typename childmap::iterator & location() {return m_loc;}
		_type * add(const K & key, const T & value = T());

		void remove();

		T & value() {return m_value;}
		void set(const T & v) {m_value = v;}

	protected:
		_type *m_parent;
		typename childmap::iterator m_loc;

		childmap m_children;

		T m_value;
};


template <class T, typename F>
void
traverse(T * t, F & f)
{
	f(t);
	for (typename T::childmap::iterator it = t->children().begin(); it != t->children().end(); ++it)
		traverse(it->second, f);
}

template <class T>
void
clean(T * t)
{
	delete t->value();
	for (typename T::childmap::iterator it = t->children().begin(); it != t->children().end(); ++it)
		clean(it->second);
}



template <class K, class T>
void
tree<K, T>::remove()
{
	m_parent->m_children.erase(m_loc);
}

template <class K, class T>
tree<K, T>::~tree()
{
	for (typename childmap::iterator it = m_children.begin(); it != m_children.end(); ++it)
		delete it->second;
}

template <class K, class T>
tree<K, T> *
tree<K, T>::child(const K & key)
{
	typename childmap::iterator it = m_children.find(key);
	if (it != m_children.end())
		return it->second;
	else
		return 0;
}

template <class K, class T>
tree<K, T> *
tree<K, T>::add(const K & key, const T & value)
{
	_type *child = new _type(value, this);
	child->m_loc = m_children.insert(std::make_pair(key, child)).first;
	return child;
}



template <class T, class iterator>
T *
find(T * tree, iterator begin, const iterator & end)
{
	while (begin != end) {
		tree = tree->child(*begin);
		if (!tree) return 0;
		++begin;
	}
	return tree;
}

template <class T, class iterator>
T *
safefind(T * tree, iterator begin, const iterator & end)
{
	T *par = tree;
	while (begin != end) {
		tree = tree->child(*begin);
		if (!tree) break;

		++begin;
		par = tree;
	}

	while (begin != end) {
		tree = par->add(*begin);
		++begin;
		par = tree;
	}

	return tree;
}


template <class T, class iterator, class V>
T *
add(T * tree, iterator begin, const iterator & end, const V & value)
{
	T *par = tree;
	while (begin != end) {
		tree = tree->child(*begin);
		if (!tree) tree = par->add(*begin, V());
		++begin;
		par = tree;
	}
	tree->set(value);
	return tree;
}

template <class T, class iterator, class V>
T *
safeadd(T * tree, iterator begin, const iterator & end, const V & value)
{
	T *par = tree;
	while (begin != end) {
		tree = tree->child(*begin);
		if (!tree) {
			while (begin != end) {
				tree = par->add(*begin, V());
				++begin;
				par = tree;
			}
			tree->set(value);
			return tree;
		}
		++begin;
		par = tree;
	}
	return tree;
}


template <class T, class iterator, class treelist>
bool
test(T *tree, iterator begin, const iterator & end, treelist & out)
{
	while (begin != end) {
		if (begin.test()) {
			iterator it = begin;
			T * r = find(tree, ++it, end);
			if (r == 0 || !r->value())
				return false;
			else
				out.push_back(r);
		}

		tree = tree->child(*begin);
		++begin;
		if (!tree) {
			while (begin != end) {
				if (begin.test()) {
					return false;
				}
				++begin;
			}
			return true;
		}
	}
	return true;
}

template <class T, class key, class keylist>
void
find_super(T *tree, const key & k, keylist & out)
{
	std::list<key> tail;
	while (tree->location()->first != k) {
		tail.push_front(tree->location()->first);
		tree = tree->parent();
	}

	typename T::childmap::iterator it = tree->location();
	typename T::childmap::iterator end = tree->parent()->children().end();

	while (it != end) {
		if (tail.empty() || it->first < tail.front()) {
			T *c = find(it->second, tail.begin(), tail.end());
			if (c && c->value())
				out.push_back(it->first);
			++it;
		}
		else if (it->first == tail.front()) {
			tree = it->second;
			it = tree->children().begin();
			end = tree->children().end();
			tail.pop_front();
		}
		else
			return;
	}
}



};

#endif
