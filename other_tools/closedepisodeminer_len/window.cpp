#include "window.h"
#include <assert.h>


void
windowlist::add(event *start, event *end, double w)
{
	assert(m_cnt == 0 || *m_windows[m_cnt - 1].start <= *start);

	if (m_cnt > 0 && m_windows[m_cnt - 1].start->index == start->index) {
		assert(*m_windows[m_cnt - 1].end <= *end);
		return;
	}

	if (m_cnt > 0 && m_windows[m_cnt - 1].end->index == end->index && *m_windows[m_cnt - 1].start < *start)
		m_cnt--;
	m_windows[m_cnt].start = start;
	m_windows[m_cnt].end = end;
	m_windows[m_cnt].w = w;
	m_cnt++;
}


void
windowlist::compute_disjoint()
{
	for (uint32_t i = 0, j = 0; i < m_cnt; i++) {
		while (j < m_cnt && *m_windows[j].start <= *m_windows[i].end)
			j++;
		m_windows[i].disjoint = j;
	}
}

index_t
windowlist::fsupport(index_t winsize) const
{
	index_t sup = 0;
	index_t last = std::numeric_limits<index_t>::min();

	for (uint32_t i = 0; i < m_cnt; i++) {
		event *e = m_windows[i].start;
		event *f = m_windows[i].end;

		index_t d = winsize - (f->index - e->index);
		index_t start = f->index - winsize + 1;

		if (start <= last)
			d -= 1 + last - start;

		sup += d;
		last = e->index;
	}

	return sup;
}

double
windowlist::msupport() const
{
	if (m_cnt == 0) return 0;

	return m_windows[0].total;
}

double
windowlist::mconfidence(windowlist & wl) const
{
	double up = 0;
	uint32_t best = 0;

	for (uint32_t i = 0; i < m_cnt; i++) {
		const window & w = m_windows[i];
		double bw = 0;

		while (best < wl.size() && *wl[best].end < *w.end) best++;

		for (uint32_t j = best; j < wl.size() && *wl[j].start <= *w.start; j++) {
			if (wl[j].w >= bw) {
				bw = wl[j].w;
				best = j;
			}
		}


		up += bw / w.w;
	}

	return up / m_cnt;
}


void
windowlist::compute_total()
{
	for (int32_t i = m_cnt - 1; i >= 0; i--) {
		window & w = m_windows[i];
		if (w.disjoint == m_cnt)
			w.total = w.w;
		else 
			w.total = std::max(w.w + m_windows[w.disjoint].total, m_windows[i + 1].total);
	}
}
