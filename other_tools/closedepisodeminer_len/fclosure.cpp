#include "episode.h"

void
base::fclosure(episode *ep)
{
	m_magic++;
	episode::childlist stack;

	episode::childlist::iterator itnext;
	for (episode::childlist::iterator it = m_maxepisodes.begin(); it != m_maxepisodes.end(); it = itnext) {
		itnext = it; ++itnext;
		episode *t = *it;

		if (t->m_closure->subset(ep->m_closure)) {
			if (t->condition() == ep->condition())
				t->m_fclosed = ep->m_id;
			t->m_parentcnt++;
			ep->m_subs.push_back(t);
			m_maxepisodes.erase(it);
		}
		else if (t->condition() <= ep->condition()) {
			for (episode::childlist::iterator it = t->m_subs.begin(); it != t->m_subs.end(); ++it) {
				episode *c = *it;
				c->m_visited = c->m_magic == m_magic ? c->m_visited + 1 : 1;
				c->m_magic = m_magic;
				if (c->m_visited == c->m_parentcnt)
					stack.push_back(c);
			}
		}
	}

	while (!stack.empty()) {
		episode *t = stack.front();
		stack.pop_front();
		
		if (t->m_closure->subset(ep->m_closure)) {
			if (t->condition() == ep->condition() && t->m_fclosed == 0)
				t->m_fclosed = ep->m_id;
			t->m_parentcnt++;
			ep->m_subs.push_back(t);
		}
		else if (t->condition() <= ep->condition()) {
			for (episode::childlist::iterator it = t->m_subs.begin(); it != t->m_subs.end(); ++it) {
				episode *c = *it;
				c->m_visited = c->m_magic == m_magic ? c->m_visited + 1 : 1;
				c->m_magic = m_magic;
				if (c->m_visited == c->m_parentcnt)
					stack.push_back(c);
			}
		}
	}

	stack.clear();
	m_magic++;

	for (episode::childlist::iterator it = m_maxepisodes.begin(); it != m_maxepisodes.end(); ++it) {
		episode *t = *it;

		if (ep->m_closure->subset(t->m_closure)) {

			if (t->condition() == ep->condition() && ep->m_fclosed == 0)
				ep->m_fclosed = t->m_id;

			for (episode::childlist::iterator it = t->m_subs.begin(); it != t->m_subs.end(); ++it) {
				episode *c = *it;
				c->m_visited = c->m_magic == m_magic ? c->m_visited + 1 : 1;
				c->m_magic = m_magic;
				if (c->m_visited == c->m_parentcnt)
					stack.push_back(c);
			}
			t->m_subs.push_back(ep);
			ep->m_parentcnt++;
		}
	}

	while (!stack.empty()) {
		episode *t = stack.front();
		stack.pop_front();
		if (ep->m_closure->subset(t->m_closure)) {

			if (t->condition() == ep->condition() && ep->m_fclosed == 0)
				ep->m_fclosed = t->m_id;

			for (episode::childlist::iterator it = t->m_subs.begin(); it != t->m_subs.end(); ++it) {
				episode *c = *it;
				c->m_visited = c->m_magic == m_magic ? c->m_visited + 1 : 1;
				c->m_magic = m_magic;
				if (c->m_visited == c->m_parentcnt)
					stack.push_back(c);
			}
			t->m_subs.push_back(ep);
			ep->m_parentcnt++;
		}
	}


	if (ep->m_parentcnt == 0)
		m_maxepisodes.push_back(ep);	
}

void
base::external_fclosure(episode *ep, const std::vector<uint32_t> & m)
{
	m_magic++;
	episode::childlist stack;

	episode::childlist::iterator itnext;
	for (episode::childlist::iterator it = m_maxepisodes.begin(); it != m_maxepisodes.end(); it = itnext) {
		itnext = it; ++itnext;
		episode *t = *it;

		if (t->m_closure->subset(ep->m_closure, m)) {
			if (t->condition() == ep->condition())
				t->m_fclosed = ep->m_id;
		}
		else if (t->condition() <= ep->condition()) {
			for (episode::childlist::iterator it = t->m_subs.begin(); it != t->m_subs.end(); ++it) {
				episode *c = *it;
				c->m_visited = c->m_magic == m_magic ? c->m_visited + 1 : 1;
				c->m_magic = m_magic;
				if (c->m_visited == c->m_parentcnt)
					stack.push_back(c);
			}
		}
	}

	while (!stack.empty()) {
		episode *t = stack.front();
		stack.pop_front();
		
		if (t->m_closure->subset(ep->m_closure, m)) {
			if (t->condition() == ep->condition() && t->m_fclosed == 0)
				t->m_fclosed = ep->m_id;
		}
		else if (t->condition() <= ep->condition()) {
			for (episode::childlist::iterator it = t->m_subs.begin(); it != t->m_subs.end(); ++it) {
				episode *c = *it;
				c->m_visited = c->m_magic == m_magic ? c->m_visited + 1 : 1;
				c->m_magic = m_magic;
				if (c->m_visited == c->m_parentcnt)
					stack.push_back(c);
			}
		}
	}
}
