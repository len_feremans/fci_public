#ifndef ASSOC_H
#define ASSOC_H

#include "episode.h"
#include "params.h"
#include "graphindex.h"


class rule_collection {
	public:
		rule_collection(const params & par) : m_params(par), m_rulecnt(0) {}

		void add(episode *from, episode *to, double cond);

		uint32_t purge();

		uint32_t cnt() const {return m_rules.size();}
		uint32_t frequent() const {return m_rulecnt;}
	
		struct rule {
			episode *from, *to;
			bool closed;
			double condition;

			bool operator <= (const rule & b) const {return to->closure()->subset(b.to->closure()) && b.from->generator()->subset(from->generator());}

			std::list<rule *> super;
		};


		void print(FILE *f, const labelvector & labels);

	protected:
		typedef std::list<rule *> rulelist;

		typedef graphindex<rule *> tailindex;

		struct tail {
			graph *key;
			tailindex rules;
		};
		typedef graphindex<tail> ruleindex;

		void bind(rule *a, rule *b);

		void print(rule *r, FILE *f, const labelvector & labels);

		rulelist m_rules;
		ruleindex m_index;
		const params & m_params;
		uint32_t m_rulecnt;
};


class assoc {
	public:
		assoc(sequence *s, const params & par) : m_data(s), m_params(par), m_rules(par), m_episodecnt(0) {}

		void add(episode *ep);

		uint32_t cnt() const {return m_episodecnt;}

		void mine();

		rule_collection & rules() {return m_rules;}

	protected:
		typedef std::list<episode *> episodelist;
		
		struct entry {
			graph *g;
			episodelist episodes;
			entry *visited;
		};

		typedef graphindex<entry *> entryindex;
		entryindex m_index;

		typedef std::list<entry *> entrylist;


		void test(entry *from, entry *to, windowlist & wl);

		double condition(entry *n, entry *m, windowlist & wl);

		entrylist m_entries;

		sequence * m_data;
		const params & m_params;


		rule_collection m_rules;
		uint32_t m_episodecnt;
};


#endif
