#ifndef CLOSURE_H
#define CLOSURE_H

#include <stdint.h>
#include <stdio.h>
#include "seq_tree.h"
#include "graphset.h"

class closure {
	public:
		void add(episode *ep) {m_episodes.add(ep->closure(), ep);}

		void mark() {episode_traverser bm(this); traverse(m_episodes, bm);}

		void print(FILE *f, const labelvector & labels, bool fcl, bool sql) {episode_printer bp(f, labels, fcl, sql); traverse(m_episodes, bp);}


	protected:
		typedef graphset<episode *> episodeset;

		struct episode_traverser {
			episode_traverser(closure *cl) : m_cl(cl) {}
			void operator()  (episodeset::graph_tree *et) {if (et->value()) {m_cl->mark(et->value());}}
			closure *m_cl;
		};
		
		struct episode_marker {
			episode_marker(closure *cl, episode *ep) : m_episode(ep), m_cl(cl) {}
			bool operator()  (episodeset::graph_tree *et) {return m_cl->mark(m_episode, et);}

			episode *m_episode;
			closure *m_cl;
		};


		struct episode_printer {
			episode_printer(FILE *o, const labelvector & l, bool f, bool s) : out(o), labels(l), fcl(f), sql(s) {}
			void operator()  (episodeset::graph_tree *et) {if (et->value() && (!fcl || et->value()->fclosed())) if (sql) et->value()->print_sql(out); else  et->value()->print(out, labels);}
			FILE * out;
			const labelvector & labels;
			bool fcl;
			bool sql;
		};


		void mark(episode *ep);
		bool mark(episode *ep, episodeset::graph_tree *et);


		graphset<episode *> m_episodes;	
};

#endif
