#include "event.h"

event *
event::find(uint32_t sym)
{
	if (nbhd.empty())
		return NULL;

	int32_t s = 0;
	int32_t e = nbhd.size() - 1;

	while (s <= e) {
		int32_t m = (s + e) / 2;
		uint32_t c = nbhd[m]->symbol;

		if (c == sym)
			return nbhd[m];
		else if (c < sym)
			s = m + 1;
		else
			e = m - 1;
	}

	return NULL;
}

event *
event::find_inside(uint32_t sym, index_t last) {
	event *i = find(sym);
	event *c = this;

	while (i == NULL) {
		c = TAILQ_NEXT(c, similar);
		if (c == NULL || c->index > last)
			return NULL;
		i = c->find(sym);
	}

	if (i->index > last)
		return NULL;
	return i;
}
