#ifndef GRAPHSET_H
#define GRAPHSET_H

#include "defines.h"
#include "seq_tree.h"
#include "graph.h"

template <class V>
class graphset {
	public:
		typedef seq_tree::tree<sigul, V> graph_tree;
		typedef seq_tree::tree<uint32_t, graph_tree *> base_tree;

		graph_tree *
		add(graph *g, const V & v)
		{
			graph_tree *c = safefind(g);
			if (!c->value()) c->set(v); 
			return c;
		}

		graph_tree *
		safefind(graph *g)
		{
			base_tree *bt = seq_tree::safefind(&m_graphs, g->label_begin(), g->label_end());
			if (!bt->value()) bt->set(new graph_tree);

			graph_tree *c = seq_tree::safefind(bt->value(), g->edge_begin(), g->edge_end());
			return c;
		}

		base_tree & root() {return m_graphs;}

	protected:
		base_tree m_graphs;
};

template <class F, class T>
struct graphset_traverser {

	graphset_traverser(F & fun) : f(fun) {}

	void operator()  (T *bt) {if (bt->value()) seq_tree::traverse(bt->value(), f);}
	F f;
};

template<class S, class F>
void
traverse(S & s, F & f)
{
	graphset_traverser<F, typename S::base_tree> gf(f);
	seq_tree::traverse(&s.root(), gf);
}


template<class S, class F>
void
traverse_subset(typename S::graph_tree *et, graph::edge_iterator cur, const graph::edge_iterator & end, F & f, indexvector & m)
{
	if (et->value())
		if (!f(et)) return;

	for (typename S::graph_tree::childmap::iterator it = et->children().begin(); it != et->children().end(); ++it) {
		sigul s = it->first;
		// Remap the sigul into closure nodes.
		s.from = m[s.from];
		s.to = m[s.to];
		while (cur != end  && *cur < s) 
			++cur;
		if (cur == end) return;
		if (*cur == s)
			traverse_subset<S, F>(it->second, cur, end, f, m);
	}
}


template<class S, class F>
void
traverse_subset(typename S::base_tree *bt, graph::label_iterator cur, const graph::label_iterator & end, graph *g, F & f, indexvector & c)
{
	if (bt->value())
		traverse_subset<S, F>(bt->value(), g->edge_begin(), g->edge_end(), f, c);

	while (cur != end) {
		typename S::base_tree *ct = bt->child(*cur);
		if (ct) {
			c.push_back(cur.index());
			traverse_subset<S, F>(ct, ++cur, end, g, f, c);
			c.pop_back();
		}
		else
			++cur;
	}
}

template<class S, class F>
void
traverse_subset(typename S::base_tree *bt, graph *g, F & f)
{
	indexvector m;
	m.reserve(g->nodecnt());

	traverse_subset<S, F>(bt, g->label_begin(), g->label_end(), g, f, m);
}

template<class S, class F>
bool
traverse_maxsubset(typename S::graph_tree *et, graph::edge_iterator cur, const graph::edge_iterator & end, F & f, indexvector & m, boolvector & used, bool & found)
{
	bool full = true;


	for (typename S::graph_tree::childmap::iterator it = et->children().begin(); it != et->children().end(); ++it) {
		sigul s = it->first;
		// Remap the sigul into closure nodes.
		s.from = m[s.from];
		s.to = m[s.to];

		while (cur != end  && *cur < s) {
			if (used[cur->from] && used[cur->to]) full = false;
			++cur;
		}

		if (cur == end) break;

		if (*cur == s) {
			bool d = false;
			if (!traverse_maxsubset<S, F>(it->second, ++cur, end, f, m, used, d))
				full = false;
			if (d) found = true;
			if (full) break;
			full = false;
		}

	}

	if (!found && et->value()) {
		found = f(et);
		//printf("%d\n", full);
		while (cur != end && full) {
			if (used[cur->from] && used[cur->to]) full = false;
			++cur;
		}
	}

	//printf("d %d\n", full && found);

	return full && found;
}


template<class S, class F>
bool
traverse_maxsubset(typename S::base_tree *bt, graph::label_iterator cur, const graph::label_iterator & end, graph *g, F & f, indexvector & c, boolvector & used)
{
	bool full = true;
	while (cur != end) {
		typename S::base_tree *ct = bt->child(*cur);
		if (ct) {
			uint32_t ind = cur.index();
			c.push_back(ind);
			used[ind] = true;
			if (!traverse_maxsubset<S, F>(ct, ++cur, end, g, f, c, used))
				full = false;
			//printf("a %d\n", full);
			c.pop_back();
			used[ind] = false;
			if (full)
				return true;
		}
		else
			++cur;
		full = false;
	}

	bool found = false;
	if (bt->value()) {
		if (!traverse_maxsubset<S, F>(bt->value(), g->edge_begin(), g->edge_end(), f, c, used, found))
			full = false;
	}
	//printf("r %d\n", full);
	return full;
}

template<class S, class F>
bool
traverse_maxsubset(typename S::base_tree *bt, graph *g, F & f)
{
	indexvector m;
	m.reserve(g->nodecnt());
	boolvector used(g->nodecnt());

	return traverse_maxsubset<S, F>(bt, g->label_begin(), g->label_end(), g, f, m, used);
}

#endif
