#include <algorithm>
#include "sequence.h"

#include <limits>
#include <algorithm>
#include <map>


sequence::sequence(const params & par) : m_params(par)
{
	TAILQ_INIT(&m_sequence);
}

bool
event_comp(event *a, event *b)
{
	return a->symbol < b->symbol;
}

void
sequence::read_full(FILE *f)
{
	uint32_t cnt = 0;
	uint32_t a;
	uint32_t m = 0;
	while (fscanf(f, "%u", &a) == 1) {
		cnt++;
		if (a > m) m = a;
	}

	m_data.resize(cnt);
	m_events.resize(m + 1);

	for (uint32_t i = 0; i < m_events.size(); i++)
		TAILQ_INIT(&m_events[i]);

	rewind(f);

	uint32_t i = 0;
	while (fscanf(f, "%u", &a) == 1) {
		event *e = &m_data[i];
		e->symbol = a;
		e->index = i;
		e->id = i;
		TAILQ_INSERT_TAIL(&m_sequence, e, entries);
		TAILQ_INSERT_TAIL(&(m_events[a]), e, similar);
		i++;
	}
}


void
sequence::read_sparse(FILE *f)
{
	uint32_t cnt = 0;
	uint32_t a;
	int32_t ind;
	uint32_t m = 0;
	while (fscanf(f, "%d%u", &ind, &a) == 2) {
		cnt++;
		if (a > m) m = a;
	}

	m_data.resize(cnt);
	m_events.resize(m + 1);

	for (uint32_t i = 0; i < m_events.size(); i++)
		TAILQ_INIT(&m_events[i]);

	rewind(f);

	uint32_t i = 0;
	while (fscanf(f, "%d%u", &ind, &a) == 2) {
		if (i > 0 && ind < m_data[i - 1].index)  {
			fprintf(stderr, "Input file doesn't have monotonic indices: %u (line %u) vs. %u (line %u)\n", ind, i, m_data[i - 1].index, i - 1);
			exit(1);
		}
			
		event *e = &m_data[i];
		e->symbol = a;
		e->index = ind;
		e->id = i;
		TAILQ_INSERT_TAIL(&m_sequence, e, entries);
		TAILQ_INSERT_TAIL(&(m_events[a]), e, similar);
		i++;
	}
}

void
sequence::build_parallel(const graph *g, windowlist & wl)
{
	wl.reset();

	stackvector s(g->nodecnt());

	graph::node *n;

	event *last = NULL;

	TAILQ_FOREACH(n, g->sources(), sources) {
		s[n->index].ev = TAILQ_FIRST(&m_events[n->symbol.label]);
		if (s[n->index].ev == NULL) return;
		s[n->index].index = n->index;
		if (last == NULL || last->index < s[n->index].ev->index)
			last = s[n->index].ev;
	}

	std::make_heap(s.begin(), s.end(), std::greater<stackentry>());

	while (true) {
		std::pop_heap(s.begin(), s.end(), std::greater<stackentry>());
		stackentry & se = s.back();

		event *next = se.ev;
		while (next != NULL && next->index <= s.front().ev->index) {
			se.ev = next;
			next = TAILQ_NEXT(next, similar);
		}

		if ((next == NULL || next->index > last->index) && last->index - se.ev->index <= m_params.win - 1) {
			add(wl, se.ev, last);
		}

		if (next == NULL) break;
		s.back().ev = next;
		std::push_heap(s.begin(), s.end(), std::greater<stackentry>());

		if (next->index > last->index)
			last = next;
	}

	wl.compute_disjoint();
	wl.compute_total();
}


void
sequence::build(const graph *g, windowlist & wl)
{
	
	if (g->edgecnt() == 0) {
		build_parallel(g, wl);
		return;
	}

	wl.reset();


	stackvector src(g->sourcecnt());
	scanner scan(g->nodecnt(), g->nodecnt() - g->sourcecnt());

	event *last = NULL;



	for (uint32_t i = 0, j = 0, k = 0; i < g->nodecnt(); i++) {
		const graph::node & n = g->get_node(i);
		event *e = TAILQ_FIRST(&m_events[n.symbol.label]);
		if (e == NULL) return;
		if (last == NULL || *last < *e) last = e;

		if (n.in.degree == 0) {
			src[j].ev = e;
			src[j].index = n.index;
			j++;
		}
		else 
			scan.init(k++, &n, e);
	}

	for (uint32_t i = 0; i < src.size(); i++)
		scan.mark(&g->get_node(src[i].index), src[i].ev);

	last = scan.update(last);

	if (last == NULL) return; // Not a single entry found


	std::make_heap(src.begin(), src.end(), std::greater<stackentry>());

	while (true) {
		event *first = src.front().ev;
		//assert(last->index - first->index + 1 >= int(g->nodecnt()));
		std::pop_heap(src.begin(), src.end(), std::greater<stackentry>());
		stackentry & se = src.back();

		if (last->index - se.ev->index <= m_params.win - 1)
			add(wl, first, last);


		event *next = TAILQ_NEXT(se.ev, similar);
		if (next == NULL) break;

		scan.mark(&g->get_node(se.index), next);
		last = scan.update(last);
		if (last == NULL) break;

		src.back().ev = next;
		std::push_heap(src.begin(), src.end(), std::greater<stackentry>());

		if (*last <  *next) last = next;
	}
	wl.compute_disjoint();
	wl.compute_total();
}



void
sequence::sieve_edges(const graph *g, edgelist & edges) const
{
	stackvector src(g->sourcecnt());
	scanner scan(g->nodecnt(), g->nodecnt());

	event *last = NULL;
	for (uint32_t i = 0, j = 0; i < g->nodecnt(); i++) {
		const graph::node & n = g->get_node(i);
		event *e = TAILQ_FIRST(&m_events[n.symbol.label]);

		if (e == NULL) return; // should not happen
		if (last == NULL || *last < *e) last = e;

		scan.init(i, &n, e);

		if (n.in.degree == 0) {
			src[j].ev = e;
			src[j].index = n.index;
			j++;
		}
	}

	for (uint32_t i = 0; i < src.size(); i++)
		scan.mark(&g->get_node(src[i].index), src[i].ev);

	last = scan.update(last);
	if (last == NULL) return; // Not a single entry found

	std::make_heap(src.begin(), src.end(), std::greater<stackentry>());


	while (true) {
		//assert(last->index - src.front().ev->index + 1 >= int(g->nodecnt())); 

		if (last->index - src.front().ev->index <= m_params.win - 1) {
			scanner::statevector states = scan.states();

			for (edgelist::iterator it = edges.begin(), itnext; it != edges.end(); it = itnext) {
				itnext = it; ++itnext;
				edge & e = *it;


				if (states[e.from].ev->index >= states[e.to].ev->index)
					edges.erase(it);
				else if (states[e.from].ev != src.front().ev)  { // Don't try to move the first element, there will be another window where you can do that.
					scan.mark(e.from, states[e.to].ev->index, false);
					event *nl = scan.update(last);
					if (nl != NULL && nl->index - src.front().ev->index <= m_params.win - 1)
						edges.erase(it);

					scan.set(states);
				}
			}

			if (edges.empty()) break;
		}


		std::pop_heap(src.begin(), src.end(), std::greater<stackentry>());
		stackentry & se = src.back();

		event *next = TAILQ_NEXT(se.ev, similar);
		if (next == NULL) break;

		scan.set(se.index, next);
		scan.mark(&g->get_node(se.index), next);
		last = scan.update(last);
		if (last == NULL) break;

		se.ev = next;
		std::push_heap(src.begin(), src.end(), std::greater<stackentry>());

		if (*last < *next) last = next;
	}
}




graph *
sequence::closure(const graph *g, indexvector & m, const windowlist & wl) const
{
	if (m_params.nclosure) {
		graph *g2 = closure_nodes(g, m, wl);
		if (m_params.eclosure) {
			graph *ret = closure_edges(g2);
			delete g2;
			return ret;
		}
		else
			return g2;
	}
	else {
		m.resize(g->nodecnt());
		for (uint32_t i = 0; i < m.size(); i++) m[i] = i;
		return m_params.eclosure ? closure_edges(g) : graph::create_copy_graph(g);
	}
}


graph *
sequence::closure_edges(const graph *g) const
{
	edgelist edges;
	init_edges(g, edges);
	sieve_edges(g, edges);

	graph *ret = graph::create_expanded_graph(g, edges.size());

	for (edgelist::iterator it = edges.begin(); it != edges.end(); ++it) 
		ret->bind(it->from, it->to);

	ret->build_essentials();
	return ret;
}

void
sequence::init_edges(const graph *g, edgelist & edges) const
{
	edges.clear();

	for (uint32_t i = 0; i < g->nodecnt(); i++) {
		const graph::node & n = g->get_node(i);
		graph::edge *e;
		uint32_t ind = 0;
		TAILQ_FOREACH(e, &n.out.edges, out.entries) {
			for (; ind < e->out.target; ind++) {
				if (ind == i) continue;
				edge e = {i, ind};
				edges.push_back(e);
			}
			ind++;
		}
		for (; ind < g->nodecnt(); ind++) {
			if (ind == i) continue;
			edge e = {i, ind};
			edges.push_back(e);
		}
	}
}



bool
sequence::test_node(const event *ev, const windowlist & wl) const
{
	for (uint32_t i = 0; i < wl.size(); i++) {
		event *m = wl[i].start;
		while (*ev < *m) {
			ev = TAILQ_NEXT(ev, similar);
			if (ev == NULL) return false;
		}
		if (*ev > *wl[i].end)
			return false;
	}
	return true;
}





graph *
sequence::closure_nodes(const graph *g, indexvector & m, const windowlist & wl) const
{
	typedef std::map<uint32_t, event *> symmap;
	symmap u;

	event *first = wl[0].start;
	event *last = wl[0].end;

	while (true) {
		event *c = TAILQ_NEXT(last, entries);
		if (c != NULL && c->index == last->index)
			last = c;
		else
			break;
	}

	while (true) {
		event *c = TAILQ_PREV(first, eventlist, entries);
		if (c != NULL && c->index == first->index)
			first = c;
		else
			break;
	}


	while (first != last) {
		u[first->symbol] = first;
		first = first->next();
		assert(first != NULL);
	}


	uintlist symbols;
	uint32_t i = 0;
	for (symmap::iterator it = u.begin(); it != u.end(); ++it) {
		
		while (i < g->nodecnt() && g->get_label(i) < it->first)
			i++;
		if (i < g->nodecnt() && g->get_label(i) == it->first)
			continue;
		if (test_node(it->second, wl))
			symbols.push_back(it->first);
	}



	m.resize(g->nodecnt());
	
	symbolvector s(m.size() + symbols.size());
	
	i = 0;
	uint32_t j = 0;


	while (i <  g->nodecnt() || !symbols.empty()) {
		if (i < g->nodecnt() && (symbols.empty() || symbols.front() > g->get_label(i))) {
			m[i] = j;
			s[j] = g->get_symbol(i);
			i++;
			j++;
		}
		else {
			s[j].occur = 0;
			s[j++].label = symbols.front();
			symbols.erase(symbols.begin());
		}
	}

	return graph::create_expanded_graph(g, s, m);
}
