/*	$Id$	*/
#ifndef SEQUENCE_H
#define SEQUENCE_H

#include <stdio.h>
#include <stdint.h>
#include "event.h"
#include "graph.h"
#include "params.h"

#include <vector>
#include "window.h"
#include "scanner.h"



class sequence {
	public:
		sequence(const params & par);
		void read(FILE *f) {if (m_params.sparse) read_sparse(f); else read_full(f);}

		void build(const graph *g, windowlist & wl);

		graph *closure(const graph *g, indexvector & m, const windowlist & wl) const;

		uint32_t symbolcnt() const {return m_events.size();}
		uint32_t size() const {return m_data.size();}


		struct stackentry {
		    event *ev;
			uint32_t index;
			bool operator < (const stackentry & b) const {return ev->index < b.ev->index || (ev->index == b.ev->index && ev->id < b.ev->id);}
			bool operator > (const stackentry & b) const {return ev->index > b.ev->index || (ev->index == b.ev->index && ev->id > b.ev->id);}
		};

		typedef std::vector<stackentry> stackvector;



	protected:
		void read_full(FILE *f);
		void read_sparse(FILE *f);

		void build_parallel(const graph *g, windowlist & wl);

		void
		add(windowlist & wl, event *start, event *end)
		{
			if (m_params.c == threshold_weighted)
				wl.add(start, end, 1.0 / (end->index - start->index + 1));
			else
				wl.add(start, end, 1);
		}

		graph *closure_edges(const graph *g) const;
		graph *closure_nodes(const graph *g, indexvector & m, const windowlist & wl) const;

		bool test_node(const event *ev, const windowlist & wl) const;

		struct edge {
			uint32_t from, to;
		};

		typedef std::list<edge> edgelist;

		void init_edges(const graph *g, edgelist & edges) const;
		void sieve_edges(const graph *g, edgelist & edges) const;

		const params & m_params;

		eventlist m_sequence;

		std::vector<eventlist> m_events;
		std::vector<event> m_data;
};


#endif
