#include "scanner.h"


void
scanner::init(uint32_t eind, const graph::node *n, event *ev)
{
	m_states[eind].ev = ev;
	m_states[eind].border = index_minimal;
	m_entries[eind].n = n;
	m_entries[eind].index = eind;
	m_entries[eind].marked = false;

	m_nodemap[n->index] = eind;
}

void
scanner::mark(uint32_t i, index_t b, bool proper)
{
	state & s = m_states[i];
	entry & se = m_entries[i];

	bool force = s.border == index_minimal;

	if (s.border < b) {
		s.border = b;
		if ((force || s.ev->index <= s.border) && !se.marked) {
			se.marked = true;
			se.proper = proper;
			TAILQ_INSERT_TAIL(&m_update, &se, update);
		}
	}
}



void
scanner::mark(const graph::node *n, const event *ev)
{
	const graph::edge *e;
	TAILQ_FOREACH(e, &n->out.essentials, out.essential) {
		mark(m_nodemap[e->out.target], ev->index);
	}
}

event *
scanner::update(event *last)
{

	while (!TAILQ_EMPTY(&m_update)) {

		entry *se = TAILQ_FIRST(&m_update);
		state & s = m_states[se->index];

		TAILQ_REMOVE(&m_update, se, update);
		se->marked = false;

		while (s.ev != NULL && !( s.ev->index > s.border || (se->proper == false && s.ev->index >= s.border)))
			s.ev = TAILQ_NEXT(s.ev, similar);

		// Failed so clean up
		if (s.ev == NULL) {
			while (!TAILQ_EMPTY(&m_update)) {
				se = TAILQ_FIRST(&m_update);
				TAILQ_REMOVE(&m_update, se, update);
				se->marked = false;
			}
			return NULL;
		}

		if (*last < *s.ev) last = s.ev;

		mark(se->n, s.ev);
	}


	return last;

}
