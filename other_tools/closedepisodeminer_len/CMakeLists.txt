cmake_minimum_required(VERSION 3.8)
project(untitled3)

set(CMAKE_CXX_STANDARD 11)
set (CMAKE_CXX_FLAGS "-Wno-narrowing")

set(SOURCE_FILES assoc.cpp assoc.h closepi.cpp closure.cpp closure.h defines.h episode.cpp episode.h future.cpp graph.cpp graph.h graphindex.h graphset.h lattice.h library.cpp library.h params.h queue.h scanner.cpp scanner.h seq_tree.h sequence.cpp sequence.h window.cpp window.h)
#event.cpp event.h fclosure.cpp indexset.h
add_executable(closepi ${SOURCE_FILES})