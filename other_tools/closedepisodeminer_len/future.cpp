#include "episode.h"

static void
build_symbols(episode *ep, uint32_t node, uint32_t index, indexvector & m, indexvector & m2, symbolvector & syms)
{
	graph *cl = ep->closure();
	graph *g = ep->generator();

	for (uint32_t i = 0; i < index; i++) {
		m[i] = i;
		m2[i] = ep->nodemap()[i];
		syms[i] = g->get_symbol(i);
	}

	syms[index].label = cl->get_label(node);
	m2[index] = node;

	for (uint32_t i = index; i < g->nodecnt(); i++) {
		m[i] = i + 1;
		m2[i + 1] = ep->nodemap()[i];
		syms[i + 1] = g->get_symbol(i);
	}

	normalize(syms);
}

void
episode_collection::future_node(episode *ep, uint32_t node, uint32_t index)
{
	graph *cl = ep->m_closure;
	graph *g = ep->m_graph;

	indexvector m(g->nodecnt());
	indexvector m2(g->nodecnt() + 1);
	symbolvector syms(g->nodecnt() + 1);


	build_symbols(ep, node, index, m, m2, syms);

	graph *future = graph::create_expanded_graph(g, syms, m);

	if (test(future)) {
		delete future;
		return;
	}

	episode *fep = new episode(future, graph::create_copy_graph(cl), m2,
		ep->frequency(), ep->minwincnt(), ep->condition(), m_episodecnt++, true);

	base_tree *bt = add_episode(syms, fep);

	graph::edge *e;

	uint32_t n = 0;
	TAILQ_FOREACH(e, &cl->get_node(node).out.edges, out.entries) {
		while (n < m2.size() && m2[n] < e->out.target)
			n++;
		if (n == m2.size() || m2[n] > e->out.target)
			continue;
		future_edges(bt->value(), fep, index, n);
	}

	n = 0;
	TAILQ_FOREACH(e, &cl->get_node(node).in.edges, in.entries) {
		while (n < m2.size() && m2[n] < e->in.target)
			n++;
		if (n == m2.size() || m2[n] > e->in.target)
			continue;
		future_edges(bt->value(), fep, n, index);
	}

	n = index;

	for (uint32_t i = node + 1; i < cl->nodecnt(); i++) {
		while (n < m2.size() && m2[n] < i)
			n++;
		if (n < m2.size() && i == m2[n])
			continue;
		if (cl->adjacent(node, i) || cl->adjacent(i, node)) 
			continue;
			
		future_doublenode(fep, i, n);
	}

}



void
episode_collection::future_doublenode(episode *ep, uint32_t node, uint32_t index)
{
	graph *cl = ep->m_closure;
	graph *g = ep->m_graph;

	indexvector m(g->nodecnt());
	indexvector m2(g->nodecnt() + 1);
	symbolvector syms(g->nodecnt() + 1);

	build_symbols(ep, node, index, m, m2, syms);

	graph *future = graph::create_expanded_graph(g, syms, m);

	if (!test(future))  {
		episode *fep = new episode(future, graph::create_copy_graph(cl), m2,
			ep->frequency(), ep->minwincnt(), ep->condition(), m_episodecnt++, true);

		add_episode(syms, fep);
	}
	else
		delete future;
}


void
episode_collection::future_nodes(episode *ep)
{
	graph *cl = ep->m_closure;
	indexvector & m = ep->m_nodemap;

	uint32_t n = 0;

	for (uint32_t i = 0; i < cl->nodecnt(); i++) {
		while (n < m.size() && m[n] < i)
			n++;
		if (n < m.size() && i == m[n])
			continue;
		future_node(ep, i, n);
	}
}



void
episode_collection::future_edges(base *b, episode *ep, uint32_t from, uint32_t to)
{
	graph *cl = ep->m_closure;
	graph *g = ep->m_graph;
	indexvector & m = ep->m_nodemap;

	graph::edge *e;
	TAILQ_FOREACH(e, &g->get_node(from).out.essentials, out.essential) {
		// Consider adding e->out.target -> to
		if (cl->adjacent(m[e->out.target], m[to]) || cl->adjacent(m[to], m[e->out.target]))
			continue;
		if (!g->potential(cl, m, e->out.target, to))
			continue;

		graph *future = graph::create_potential_graph(g, cl, e->out.target, to);


		if (!b->test(future)) {
			episode *fep = new episode(future, graph::create_copy_graph(cl), ep->m_nodemap,
				ep->frequency(), ep->minwincnt(), ep->condition(), m_episodecnt++, true);
			add_episode(b, fep);
		}
		else
			delete future;

	}

	TAILQ_FOREACH(e, &g->get_node(to).in.essentials, in.essential) {
		// Consider adding from -> e->in.target
		if (cl->adjacent(m[from], m[e->in.target]) || cl->adjacent(m[from], m[e->in.target]))
			continue;
		if (!g->potential(cl, m, from, e->in.target))
			continue;

		graph *future = graph::create_potential_graph(g, cl, from, e->in.target);


		if (!b->test(future)) {
			episode *fep = new episode(future, graph::create_copy_graph(cl), ep->m_nodemap,
				ep->frequency(), ep->minwincnt(), ep->condition(), m_episodecnt++, true);
			add_episode(b, fep);
		}
		else
			delete future;
	}
}


void
episode_collection::future_edges(base *b, episode *ep)
{
	graph *cl = ep->m_closure;
	graph *g = ep->m_graph;
	indexvector & m = ep->m_nodemap;


	uint32_t from = 0;
	uint32_t to = 0;

	graph::edge_iterator eit = g->edge_begin();
	for (graph::edge_iterator cleit = cl->edge_begin(); cleit != cl->edge_end(); ++cleit) {

		// Ignore all nodes that are not in g.
		while (from < g->nodecnt() && m[from] < (*cleit).from) {
			from++;
			to = 0;
		}
		if (from == g->nodecnt() || m[from] > (*cleit).from)
			continue;

		while (to < g->nodecnt() && m[to] < (*cleit).to)
			to++;
		if (to == g->nodecnt() || m[to] > (*cleit).to)
			continue;

		// Find an edge that is in closure but not in episode.
		while (eit != g->edge_end() && (*eit).map(m) < *cleit)
			++eit;
		if (eit != g->edge_end() && (*eit).map(m) == *cleit)
			continue;


		future_edges(b, ep, from, to);
	}
}

void
episode_collection::future_candidates(base *b, episode *ep)
{
	future_nodes(ep);
	future_edges(b, ep);
}
