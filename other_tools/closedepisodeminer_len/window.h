#ifndef WINDOW_H
#define WINDOW_H

#include <stdio.h>
#include <stdint.h>
#include "event.h"

#include <vector>


class windowlist {
	public:
		windowlist(uint32_t size) : m_windows(size), m_cnt(0) {}

		void reset() {m_cnt = 0;}
		void add(event *start, event *end, double w);
		void compute_disjoint();
		void compute_total();

		index_t fsupport(index_t winsize) const;
		double msupport() const;

		double mconfidence(windowlist & wl) const;

		
		struct window {
			event *start;
			event *end;
			double w, total;

			uint32_t disjoint;

		};


		uint32_t size() const {return m_cnt;}

		window & operator [] (uint32_t i)  {return m_windows[i];}
		const window & operator [] (uint32_t i)  const {return m_windows[i];}

	protected:

		typedef std::vector<window> windowvector;

		windowvector m_windows;
		uint32_t m_cnt;
};

#endif
