#ifndef GRAPHINDEX_H
#define GRAPHINDEX_H

#include "graph.h"
#include "indexset.h"

template <class V> class graphindex {
	public:
		typedef indexset::set<sigul, V> edgeset;
		typedef indexset::set<symbol_t, std::pair<graph *, edgeset> > graphset;
		typedef typename edgeset::node node;

		
		typename edgeset::node *
		safeadd(graph *g, V & v)
		{
			std::pair<graph *, edgeset> foo;
			foo.first = g;
			typename graphset::node *n= indexset::safeadd(m_entries, g->symbol_begin(), g->symbol_end(), foo, g->nodecnt());
			return indexset::safeadd(n->value.second, g->edge_begin(), g->edge_end(), v, g->edgecnt());
		}

		class iterator {
			public:
				iterator(graphindex *gi) : m_gi(gi)
				{
					m_git = m_gi->m_entries.nodes().begin();
					m_eit = m_git->value.second.nodes().begin();
					incgraph();
				}

				iterator & operator ++ () { ++m_eit; incgraph(); return *this; }

				bool end() const {return m_git == m_gi->m_entries.nodes().end();}

				V & operator *() {return m_eit->value;}
				V * operator -> () {return &(m_eit->value);}

			protected:

				void
				incgraph()
				{
					while (m_eit == m_git->value.second.nodes().end()) {
						++m_git;
						if (m_git == m_gi->m_entries.nodes().end()) return;
						m_eit = m_git->value.second.nodes().begin();
					}
				}



				typename edgeset::nodelist::iterator m_eit;
				typename graphset::nodelist::iterator m_git;

				graphindex *m_gi;
		};

		class qiterator {
			public:
				qiterator(graph *g, graphindex *gi) : m_base(g), m_setind(0), m_map(g->nodecnt()), m_edgeind(0), m_gi(gi)
				{
					indexset::query(m_gi->m_entries, m_base->symbol_begin(), m_base->symbol_end(), m_sets);
					if (m_setind == m_sets.size()) return;
					fillmap(0, 0);
					indexset::query(m_sets[m_setind]->value.second, m_base->edge_begin(&m_map), m_base->edge_end(), m_graphs);
					incgraph();
				}

				qiterator & operator ++ () { m_edgeind++; incgraph(); return *this; }

				bool end() const {return m_setind == m_sets.size();}

				V & operator *() {return m_graphs[m_edgeind]->value;}
				V * operator -> () {return &m_graphs[m_edgeind]->value;}

			protected:

				void
				incgraph()
				{
					while (m_edgeind == m_graphs.size()) {
						if (!incmap()) {
							m_setind++;
							if (m_setind == m_sets.size()) return;
							fillmap(0, 0);
						}
						indexset::query(m_sets[m_setind]->value.second, m_base->edge_begin(&m_map), m_base->edge_end(), m_graphs);
						m_edgeind = 0;
					}
				}


				void
				fillmap(uint32_t start, uint32_t ind)
				{
					graph *g = m_sets[m_setind]->value.first;
					for (uint32_t i = start; i < m_map.size(); i++) {
						while (g->get_label(ind) != m_base->get_label(i)) {
							ind++;
							assert(ind < g->nodecnt());
						}
						m_map[i] = ind++;
					}
				}

				bool
				incmap()
				{
					const graph *g = m_sets[m_setind]->value.first;
					for (int32_t j = m_map.size() - 1; j >= 0; j--) {
						uint32_t i  = m_map[j];
						if (i + 1 < g->nodecnt() && g->get_label(i + 1) == g->get_label(i) && (j + 1 == int32_t(m_map.size()) || m_map[j + 1] != i + 1)) {
							m_map[j]++;
							fillmap(j + 1, m_map[j] + 1);
							return true;
						}
					}
					return false;
				}

				graph *m_base;

				typename graphset::nodevector m_sets;
				uint32_t m_setind;

				uintvector m_map;

				typename edgeset::nodevector m_graphs;
				uint32_t m_edgeind;

				graphindex *m_gi;
		};

		qiterator query(graph *g) {return qiterator(g, this);}
		iterator nodes() {return iterator(this);}

	protected:

		graphset m_entries;
};

#endif
