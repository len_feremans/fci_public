/*	$Id$	*/
#ifndef EVENT_H
#define EVENT_H

#include <vector>
#include <assert.h>
#include <stdint.h>
#include "queue.h"
#include "defines.h"



struct event;
TAILQ_HEAD(eventlist, event);

struct event {
	uint32_t symbol;
	index_t index;
	uint32_t id;

	bool operator < (const event & b) const {return index < b.index;}
	bool operator > (const event & b) const {return index > b.index;}
	bool operator <= (const event & b) const {return index <= b.index;}
	bool operator >= (const event & b) const {return index >= b.index;}

	event * next() {return TAILQ_NEXT(this, entries);}

	TAILQ_ENTRY(event) entries;
	TAILQ_ENTRY(event) similar;
};



#endif
