import os
import os.path
import subprocess
PATH_FCI_SEQ = './target/FCI-seq-1.1.0-jar-with-dependencies.jar'

if os.path.isfile(PATH_FCI_SEQ):
    print("Found dependencies")
else:
    print("ERROR: Please change python_wrapper.py and refer to existing directories. Also run \"mvn clean compile assembly:single -DskipTests=True\" to compile code. PATH_FCI_SEQ=" + PATH_FCI_SEQ)
    
def runFCI_seq(labels_file, sequence_file, min_sup=1, max_size=5, min_coh=0.5, seq=None, epi=None, rule=None, sparse=False, split=None, preprocess=False,outputfile=None):
    command = ["java", "-jar", os.path.abspath(PATH_FCI_SEQ), "-s", str(min_sup), "-m", str(max_size), "-c", str(min_coh), "-i", str(sequence_file), "-v"]
    if labels_file:
         command =  command + ["-l", labels_file]
    if seq:
        command =  command + ["-seq", str(seq)]
    if epi:
        command =  command + ["-epi", str(epi)]
    if rule:
        command =  command + ["-rul", str(rule)]
    if sparse:
        command =  command + ["-S"]
    if split:
        command =  command + ["-split", str(split)]
    if preprocess:
        command =  command + ["-P"]
    if outputfile:
        command = command + ["-o", outputfile]
    print(" ".join(command))
    subprocess.call(command, timeout=1000*60*6) #default: 6 hour time-out (if it works)

def run_examples():
    sequence_file='./data/trump_tweets_2017_text.csv'
    if not(os.path.isfile(PATH_FCI_SEQ)):
        print("Could not find {}".format(os.path.abspath(sequence_file)))
        return
    runFCI_seq(labels_file=None, sequence_file=sequence_file, min_sup=5, max_size=4, min_coh=0.1, 
               seq=0.5, epi=0.5, rule=0.5, preprocess=True)
    runFCI_seq(labels_file=None, sequence_file=sequence_file, min_sup=5, max_size=4, min_coh=0.1, 
               rule=0.5, split=10, preprocess=True)

if __name__ == "__main__":
    run_examples()