# README #

Implementation of _Efficiently Mining Cohesion-based Patterns and Rules in Event Sequences_, 
by _Len Feremans_, data scientist at the University of Antwerp (Belgium) within the Adrem data labs research group.
Paper authors are Boris Cule, Len Feremans, and Bart Goethals - University of Antwerp, Belgium.

Accepted to Data Mining and Knowledge Discovery on 2019-02-11

Abstract
> Discovering patterns in long event sequences is an important data mining task. Traditionally, research focused on frequency-based quality measures that allow algorithms to use the anti-monotonicity property to prune the search space and efficiently discover the most frequent patterns. In this work, we step away from such measures, and evaluate patterns using cohesion --- a measure of how close to each other the items making up the pattern appear in the sequence on average. We tackle the fact that cohesion is not an anti-monotonic measure by developing an upper bound on cohesion in order to prune the search space. By doing so, we are able to efficiently unearth rare, but strongly cohesive, patterns that existing methods often fail to discover. Furthermore, having found the occurrences of cohesive itemsets in the input sequence, we use them to discover the most dominant sequential patterns and partially ordered episodes, without going through the computationally expensive candidate generation procedures typically associated with sequential pattern and episode mining. Experiments show that our method efficiently discovers important patterns that existing state-of-the-art methods fail to discover. 

### Online demo ###

At this time an online version is available at [Online Version](http://houweel.uantwerpen.be:5000/). 
![Screenshot2](https://bytebucket.org/len_feremans/fci_public_demo/raw/006183a3da09fa3b9e70b04bca9f46cae6c5d770/examples.png)
![Screenshot1](https://bytebucket.org/len_feremans/fci_public_demo/raw/006183a3da09fa3b9e70b04bca9f46cae6c5d770/patterns.png)


### What is this repository for? ###
You can use _FCI-seq_to mine _cohesive_ itemsets, and post-processing to report _dominant sequential patterns_, _dominant episodes_ and _association rules_ in timestamped or continuous long event sequence of events,
or multiple short sequences after pre-processing.  Itemsets are ranked using _cohesion_, where cohesion is defined as the average, for each item occurrence _t_ in the sequence, of _minimal window_ lengths, where the minimal window 
is the minimal subsequence at _t_ that contains all items of the itemset.

### Quick start ###
The implementation is written in Java and uses [Maven](https://maven.apache.org/) for compilation. 
To compile the project use:
```
mvn clean compile assembly:single -DskipTests=True
```

To run the algorithm _command-line_:
```
java -jar FCI-seq-1.1.0.jar -s <min sup> -m <max size> -c <min cohesion> -i <input file sequence> -l <label file> -o <pattern output file>
    [-SPI] [-seq <occurence ratio sequences>] [-epi <occurence ratio episodes>] [-rul confidence threshold]
    -h    print this help
    -v    verbose mode
 Cohesive itemset miner:
    -s     * <min sup> minimimal absolute support for each item 
    -m     * <max size> maximum length of pattern
    -c     * <min cohesion> minimum threshold on cohesion in [0,1], e.g. if 0.5 then on average at most as many gaps as there are items in pattern
    -i     * <input file> single line sequence file of format <item1> <item2> ...., where each <item> is an integer x represented by a label on line x
    -l     <label file> multi line sequence of format <label1>\n<label2>\n..., where each <label> on line x (zero-based) is a string representing item x
    -o     <pattern output file> filename of pattern output
 Dominant sequential pattern/episode miner:
    -seq   <minimal occurrence ratio> for sequential pattern mining in [0,1]
    -epi   <minimal occurrence ratio> for episode mining in [0,1]
 Association rules miner:
    -rul   <confidence threshold> in [0,1]
 Input file options r:
    -S     signals that input file sequence is in sparse format, that is like <timepoint1> <item1>\n<timepoint2> <item2>...where <timepoint> is a long
    -P     signals that input file is a raw text file, and that it first pre-processsed using stemming/stop-word removal, and then convert to sequence/label format
    -split  <number of splits> splits sequences in to x pieces, and runs FCI on each, then joins resulting patterns. Usefull for find local patterns in long sequences.
```

Input file formats is either _text_, _dat-lab_ format or _sparse_ format.

For dat-lab format, the ".lab" is  a file with all item labels, seperate by a newline. The ".dat" file is a space separated file, with all tokens in stream, and each item with integer code X corresponds with a label on line X. For example:
_mylabels.lab_:
```
this
are
the 
labels
```
_mysequence.dat_: 
``` 
0 1 2 3 4
``` 
For sparse format the input consist of <timepoint1> <label1> on each line. For example:
_mysequence_parse.dat_.:
``` 
1001 0
1003 1
1010 2
1020 3
``` 

### Examples use ###
Pre-processing text inputfile (stemming, stopwords), and mine cohesive itemsets with min sup=5, max size=5 and min cohesion=0.1:
```
JAR=FCI-seq-1.1.0.jar
DATA=./data/trump_tweets_2017_text.csv
java -jar $JAR -s 5 -m 5 -c 0.1 -i $DATA -P 
```
Mine _cohesive itemsets_, without pre-processing, and data in _dat-lab_ format:
```
java -jar $JAR -s 5 -m 5 -c 0.1 -i "${DATA}_processed.dat" -l "${DATA}_processed.lab"
```
Mine _cohesive itemsets_, in sparse format:
```
java -jar $JAR -s 5 -m 5 -c 0.1 -i "${DATA}_sparse_processed.dat" -l "${DATA}_processed.lab" -S
```
Mine _sequential patterns_, that occur in at least 50% of occurrences of each cohesive itemset 
```
java -jar $JAR -s 5 -m 5 -c 0.1 -i $DATA -P -seq 0.5
```
Mine _episodes_, that occur in at least 50% of ocurrences of each cohesive itemset
```
java -jar $JAR -s 5 -m 5 -c 0.1 -i $DATA -P -epi 0.5
```
Mine _association rules_ with confidence of 90%
```
java -jar $JAR -s 5 -m 5 -c 0.1 -i $DATA -P -rul 0.9
```
Mine in _mixed_ mode, where we report different pattern types:
```
java -jar $JAR -s 5 -m 5 -c 0.1 -i $DATA -P -seq 0.7 -epi 0.7 -rul 0.9
```

Somethimes the sequences is very long, and the average cohesion becomes larger due to _outliers_ in ocurrences for itemsets.
Therefore we can _segment_ or _split_ the sequence in X parts,  mine cohesive patterns in each segment and join the resulting _local_ patterns:
```
java -jar $JAR -s 5 -m 5 -c 0.1 -i $DATA -P -split 5
```

To run from _Java_:
```java
import be.uantwerpen.pattern_mining.MainDFSAlgorithm
import be.uantwerpen.pattern_mining.MainCommandLine
import be.uantwerpen.util.Utils
double mincoh = 0.02;
int maxlen = 5;
int minsupp = 5;
MainDFSAlgorithm service = new MainDFSAlgorithm("mylabels.lab","mysequence.dat", minsupp);
File output = service.minePatterns(maxlen, mincoh);
File output_sorted = Utils.sortCSVFile(output, MainCommandLine.SortItemsets, true);
Utils.printHead(output_sorted);
```

To run from _python_:
```python
import python_wrapper

python_wrapper.runFCI_seq(labels_file, sequence_file, min_sup=5, max_size=5, min_coh=0.1)    
```

### More information for researchers and contributors ###
The current version is 1.1.0, created on march 2019.

Dependencies: 

 * _Java_ 8, Junit 4 for testing and maven3 for compiling
 * Snowball-stemmer, and weka, for pre-processing text in experiments (as configured in maven pom.xml)
 * Google guave and apache commons libraries
 * We used _python_ for plotting, that is python 3.4 with pandas (0.18), numpy (1.11) and matplotlib (1.1.1).
 * The repository for the web-based demo interface is <https://bitbucket.org/len_feremans/fci_public_demo>
 
To reproduce the experiments:

 * For comparision with WINEPI, LAXMAN and MARBLES: closedepisode miner is downloaded from <http://users.ics.aalto.fi/ntatti/software/closedepisodeminer.zip>. 
 * The C++ code for closed episode miner from Tatti was adapted, and is included in ./other\_tools/closedepisodeminer\_len
 * The C++ code for compact minimal windows from Tatti was adapted, and is included in ./other\_tools/tatti\_episodes\_len
 * The synthetic dataset benchmark used a generator from Zimmerman, downloaded from <https://zimmermanna.users.greyc.fr/software.html>
 
 
Experimental scripts are coded as java unit tests in the package _be.uantwerpen.pattern\_mining.experiments_.
Datasets are provided in _/data/_. _Origin of Species_, and _David Copperfield_ (in several languages) where 
downloaded from www.gutenberg.org. _Trump tweets_ are downloaded from http://www.trumptwitterarchive.com/.

To re-run experiments: 
```
./run_experiments_quality.sh 
./run_experiments_performance.sh
./run_experiments_synth.sh
```

To contribute to this project, note that the design of the code is clean and you could extend this codebase for research. 

### Who do I talk to? ###
E-mail Len Feremans (gmail) for more information.

### Licence ###
Copyright (c) [2019] [Universiteit Antwerpen - Len Feremans]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.