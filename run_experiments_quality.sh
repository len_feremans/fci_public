mvn clean compile assembly:single -DskipTests=True
(cd other_tools/closedepisodeminer_len/; make)
mvn test -Dtest=be.uantwerpen.pattern_mining.experiments.quality.Test_Quality_Itemsets_Trump
mvn test -Dtest=be.uantwerpen.pattern_mining.experiments.quality.Test_Quality_Itemsets_Species
mvn test -Dtest=be.uantwerpen.pattern_mining.experiments.quality.Test_Quality_Sequential_Patterns_Trump
mvn test -Dtest=be.uantwerpen.pattern_mining.experiments.quality.Test_Quality_Sequential_Patterns_Species
mvn test -Dtest=be.uantwerpen.pattern_mining.experiments.quality.Test_Quality_Episodes_Trump
mvn test -Dtest=be.uantwerpen.pattern_mining.experiments.quality.Test_Quality_Episodes_Species
mvn test -Dtest=be.uantwerpen.pattern_mining.experiments.quality.Test_Quality_Rules_Trump
mvn test -Dtest=be.uantwerpen.pattern_mining.experiments.quality.Test_Quality_Rules_Species
mvn test -Dtest=be.uantwerpen.pattern_mining.experiments.quality.Test_Quality_Rules_DavidCopperfield
